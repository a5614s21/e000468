﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Service;

namespace Web.Controllers
{
    public class BaseController : Controller
    {
        protected string Owasp = ConfigurationManager.ConnectionStrings["Owasp"].ConnectionString;

        //網站入侵檢測功能
        protected string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;

        //預設語系
        protected NameValueCollection langData = new NameValueCollection();

        protected System.Web.Caching.Cache cacheContainer = HttpRuntime.Cache;

        //更改較少資訊Cache使用
        protected int CacheSaveHour = int.Parse(ConfigurationManager.ConnectionStrings["CacheSaveHour"].ConnectionString);

        //Cache 儲存(小時)
        protected string webURL = "";

        protected Model DB = new Model();
        protected web_data webData = new web_data();
        protected string ID = "";
        protected string ApiPath = ConfigurationManager.ConnectionStrings["ApiPath"].ConnectionString;

        //會員API path
        protected ServiceModels.Info memberInfo = new ServiceModels.Info();

        protected ServiceModels.Login logins = new ServiceModels.Login();
        private static MemoryCache _cache = MemoryCache.Default;

        /// <summary>
        /// 紀錄取得的資料庫資料
        /// </summary>
        protected dynamic Data
        {
            set
            {
                ViewBag.Data = value;
            }
        }

        protected dynamic Categorys
        {
            set
            {
                ViewBag.Categorys = value;
            }
        }

        /// <summary>
        /// Banner
        /// </summary>
        protected string Banner
        {
            set
            {
                ViewBag.Banner = value;
            }
        }

        /// <summary>
        /// 各頁顯示標題用
        /// </summary>
        protected string PathTitle
        {
            set
            {
                ViewBag.PathTitle = value;
            }
        }

        /// <summary>
        /// SEO 標題
        /// </summary>
        protected string Title
        {
            set
            {
                ViewBag.Title = value + webData.title;
            }
        }

        /// <summary>
        /// SEO Description
        /// </summary>
        protected string Description
        {
            set
            {
                if (value == null || value == "")
                {
                    value = webData.seo_description;
                }
                ViewBag.Description = value;
            }
        }

        /// <summary>
        /// SEO Keywords
        /// </summary>
        protected string Keywords
        {
            set
            {
                if (value == null || value == "")
                {
                    value = webData.seo_keywords;
                }
                ViewBag.Keywords = value;
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ViewBag.Owasp = Owasp;
            webURL = FunctionService.getWebUrl();
            ViewBag.webURL = webURL;
            ViewBag.ID = "";
            ViewBag.ApiPath = ApiPath;

            if (RouteData.Values["lang"] != null)
            {
                defLang = RouteData.Values["lang"].ToString();
            }

            if (RouteData.Values["id"] != null && RouteData.Values["id"].ToString() != "")
            {
                ID = RouteData.Values["id"].ToString();
                ViewBag.ID = RouteData.Values["id"].ToString();
            }

            webData = DB.web_data.Where(model => model.lang == defLang).FirstOrDefault();//取得網站基本資訊
            if (webData != null)
            {
                ViewBag.webData = webData;
                ViewBag.SEO = FunctionService.SEO(webData, "", "", "");
                ViewBag.FileRoot = Url.Content("~/Content/");
            }

            ViewBag.Lang = defLang;
            langData = FunctionService.getLangData(defLang);//取得語系檔案
            ViewBag.ResLang = langData;//取得語系檔案

            //熱門建案
            ViewBag.Cases = null;

            try
            {
                List<cases> Cases = DB.cases.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.verify == "Y").OrderBy(m => m.sortIndex).ToList();
                ViewBag.Cases = Cases;
            }
            catch { }

            //熱門建案地區
            ViewBag.CasesCategory = null;
            try
            {
                List<cases_category> CasesCategory = DB.cases_category.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
                ViewBag.CasesCategory = CasesCategory;
            }
            catch { }

            //取得會員資料
            ViewBag.LoginMember = null;

            HttpCookie cookie_mid = Request.Cookies["mid"];
            if (cookie_mid != null)
            {
                var decryptUserID = FunctionService.aesDecryptBase64(cookie_mid.Value.ToString(), "minmax");
                if (!string.IsNullOrEmpty(decryptUserID) && _cache["member" + decryptUserID] != null)
                {
                    string apiUrl = ApiPath + "/api/CreMember/Infos";
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    logins = (ServiceModels.Login)_cache["member" + decryptUserID];
                    data.Add("jwtToken", logins.result.jwtToken.ToString());
                    string response = FunctionService.CallApi(apiUrl, "Info", data);

                    if (response.ToString() != "")
                    {
                        //測試資料
                        //string test = "{ \"isSuccess\": true, \"state\": \"100\", \"message\": \"取得會員房屋資料成功\", \"result\": { \"preSale\": [ { \"hmmcu\": \"1B12566\",\"mcdc\": \"國泰禾\",\"hmbuno\": \"A6\",\"hmhsn\": \"05\",\"hmhnpl\": \"\",\"hI75T8DTH\": null,\"cP75T8DTCP\": null,\"cmdtcca\": null,\"abalph\": \"Jeff\",\"abtax\": \"J222802867\",\"pC59CARDN\": \"CA01222031\",\"pceftb\": \"2017/06/22\",\"pcefte\": \"2023/06/22\",\"pceftB_Datetime\": \"2017-06-22T00:00:00\",\"pceftE_Datetime\": \"2023-06-22T00:00:00\",\"hmhsta\":\"900\",\"hmbufl\": \"2F\" },{ \"hmmcu\": \"1B12566\",\"mcdc\": \"國泰禾\",\"hmbuno\": \"A6\",\"hmhsn\": \"05\",\"hmhnpl\": \"\",\"hI75T8DTH\": null,\"cP75T8DTCP\": null,\"cmdtcca\": null,\"abalph\": \"Jeff\",\"abtax\": \"J222802867\",\"pC59CARDN\": \"CA01222031\",\"pceftb\": \"2017/06/22\",\"pcefte\": \"2023/06/22\",\"pceftB_Datetime\": \"2017-06-22T00:00:00\",\"pceftE_Datetime\": \"2023-06-22T00:00:00\",\"hmhsta\":\"900\",\"hmbufl\": \"2F\" } ],\"sale\": [] }}";
                        //memberInfo = JsonConvert.DeserializeObject<ServiceModels.Info>(test);
                        memberInfo = JsonConvert.DeserializeObject<ServiceModels.Info>(response);
                        ViewBag.LoginMember = memberInfo;
                        ViewBag.LoginMemberPreSale = memberInfo.result.preSale;
                        ViewBag.PaymentStatus = memberInfo.result.preSale.Where(x => x.hmhsta == "999").Any();

                        TempData["PaymentStatus"] = memberInfo.result.preSale.Where(x => x.hmhsta == "999").Any();
                    }
                    else
                    {
                        Response.Redirect(Url.Content("~/" + defLang + "/Member/Login"));
                    }
                }
            }

            //測試帳號 更新請註解掉
            //else
            //{
            //    //string r = "{ \"isSuccess\": true, \"state\": \"100\", \"message\": \"取得會員房屋資料成功\", \"result\": { \"preSale\": [ { \"hmmcu\": \"1B12566\", \"mcdc\": \"國泰禾\", \"hmbuno\": \"A6\", \"hmhsn\": \"05\", \"hmhnpl\": \"\", \"hI75T8DTH\": null, \"cP75T8DTCP\": null, \"cmdtcca\": null, \"abalph\": \"林琬婷\", \"abtax\": \"J222802867\", \"pC59CARDN\": \"CA01222031\", \"pceftb\": \"2017/06/22\", \"pcefte\": \"2023/06/22\", \"pceftB_Datetime\": \"2017-06-22T00:00:00\", \"pceftE_Datetime\": \"2023-06-22T00:00:00\" } ], \"sale\": [] }}";

            //    memberInfo = JsonConvert.DeserializeObject<ServiceModels.Info>(r);
            //    ViewBag.LoginMember = memberInfo;
            //}
        }
    }
}