﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Service;
using PagedList;

namespace Web.Controllers
{
    public class CasesController : BaseController
    {
        /// <summary>
        /// 熱銷建案
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult Index(int? page)
        {
            Title = ViewBag.ResLang["熱銷建案"] + "|";

            ViewBag.pageView = "";
            ViewBag.SearchCases = null;
            ViewBag.SearchCasesCount = 0;

            try
            {
                List<cases_category> Category = ViewBag.CasesCategory;
                //單一區域
                if (!string.IsNullOrEmpty(ID))
                {
                    //區域資訊
                    cases_category data = new cases_category();
                    data = Category.Where(m => m.guid == (string)ViewBag.ID).FirstOrDefault();
                    ViewBag.Title = data.title + "|" + ViewBag.Title;
                    ViewBag.Keywords = data.seo_keywords;
                    ViewBag.Description = data.seo_description;
                    Data = data;

                    int pageWidth = 10;//預設每頁長度
                    var pageNumeber = page ?? 1;
                    List<cases> temp = ViewBag.Cases;
                    ViewBag.SearchCasesCount = temp.Where(m => m.category == ID).Count();//取總筆數
                    var Cases = temp.Where(m => m.category == ID);

                    Cases = Cases.ToPagedList(pageNumeber, pageWidth);
                    ViewBag.SearchCases = Cases;
                    int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)ViewBag.SearchCasesCount / pageWidth));//總頁數
                    ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/Cases/Index/" + ID, Allpage, pageNumeber, ViewBag.SearchCasesCount, "");//頁碼
                }
            }
            catch
            {
            }

            return View();
        }

        /// <summary>
        /// 熱銷建案(內頁)
        /// </summary>
        /// <returns></returns>
        public ActionResult Detail()
        {
            ViewBag.CategoryData = null;
            ViewBag.Form = null;
            if (!string.IsNullOrEmpty(ID))
            {
                List<cases_category> Category = ViewBag.CasesCategory;

                List<cases> temp = ViewBag.Cases;
                cases item = temp.Where(m => m.guid == ID).FirstOrDefault();
                if (item != null)
                {
                    Data = item;
                    Title = item.title + "|" + ViewBag.ResLang["熱銷建案"] + "|";

                    ViewBag.CategoryData = Category.Where(m => m.guid == item.category).FirstOrDefault();
                }

                if (Session["CasesContact"] != null)
                {
                    ViewBag.Form = Session["CasesContact"];
                }

                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 年度總覽
        /// </summary>
        /// <returns></returns>
        public ActionResult Annual(string year = "")
        {
            Title = ViewBag.ResLang["年度總覽"] + "|";
            List<string> Year = new List<string>();

            List<annuals> annuals = DB.annuals.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
            foreach (annuals item in annuals.OrderByDescending(m => m.year).ToList())
            {
                if (Year.IndexOf(item.year) == -1)
                {
                    Year.Add(item.year.ToString());
                }
            }

            if (year != "all")
            {
                if (year != "")
                {
                    annuals = annuals.Where(m => m.year == year).ToList();
                }
                else
                {
                    year = Year.FirstOrDefault().ToString();
                    annuals = annuals.Where(x => x.year == year).ToList();
                }
            }

            ViewBag.Year = Year;
            ViewBag.getYear = year;
            Data = annuals;

            return View();
        }

        /// <summary>
        /// 購屋試算
        /// </summary>
        /// <returns></returns>
        public ActionResult Calculation()
        {
            Title = ViewBag.ResLang["購屋試算"] + "|";
            Data = Repository.Notes.NotesData.Single(defLang, "4");
            return View();
        }

        /// <summary>
        /// 送出預約賞屋
        /// </summary>
        /// <param name="form"></param>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void ContactSend(FormCollection form)
        {
            try
            {
                //註冊資料暫存
                Session.Remove("CasesContact");
                Session.Add("CasesContact", form);
                string keep = form["keep"];

                if (form["verification"].ToString() != Session["_ValCheckCode"].ToString())
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                    alertData.Add("text", "");
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Cases/Detail/" + form["category"].ToString() + "?log=open"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
                else if (form["keep"] == null)
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["請勾選我已閱讀並同意個人資料保護條款"].ToString());
                    alertData.Add("text", "");
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Cases/Detail/" + form["category"].ToString() + "?log=open"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
                else
                {
                    cases_reservation cases_reservation = new cases_reservation();
                    cases_reservation.guid = FunctionService.getGuid();
                    cases_reservation.name = form["name"].ToString();
                    cases_reservation.tel = form["tel"].ToString();
                    cases_reservation.email = form["email"].ToString();
                    cases_reservation.create_date = DateTime.Now;
                    cases_reservation.cases = form["cases"].ToString();
                    cases_reservation.status = "N";
                    cases_reservation.need = form["need"].ToString();
                    cases_reservation.re_mail = "N";
                    try
                    {
                        DB.cases_reservation.Add(cases_reservation);
                        DB.SaveChanges();

                        Session.Remove("CasesContact");

                        Dictionary<String, Object> alertData = new Dictionary<string, object>();
                        alertData.Add("title", langData["送出完成"].ToString() + "!");
                        alertData.Add("text", langData["我們會盡快回覆您謝謝您"].ToString());
                        alertData.Add("type", "success");
                        alertData.Add("url", Url.Content("~/" + defLang + "/Cases/Detail/" + form["category"].ToString()));

                        Session.Remove("alertData");

                        cases cases = Repository.Cases.CasesData.Single(defLang, form["category"].ToString());

                        if (!string.IsNullOrEmpty(cases.get_mail))
                        {
                            List<string> mailList = cases.get_mail.Split(',').ToList();
                            FunctionService.sendMail(mailList, "6", defLang, webURL.ToString(), form);
                        }

                        Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Cases/ContactSubmit/" + form["category"].ToString() + "'") + ";</script>");
                    }
                    catch
                    {
                        Dictionary<String, Object> alertData = new Dictionary<string, object>();
                        alertData.Add("title", langData["送出失敗"].ToString() + "!");
                        alertData.Add("text", langData["請檢查檢查相關資訊或洽官網管理員"].ToString());
                        alertData.Add("type", "error");
                        alertData.Add("url", Url.Content("~/" + defLang + "/Cases/Detail/" + form["category"].ToString()));

                        Session.Remove("alertData");
                        Session.Add("alertData", alertData);

                        Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                    }
                }
            }
            catch (Exception ex)
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["送出失敗"].ToString() + "!");
                alertData.Add("text", "系統錯誤，請洽網站管理員!");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang + "/Cases/Detail/" + form["category"].ToString()));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
            }
        }

        /// <summary>
        /// 完成表單
        /// </summary>
        /// <returns></returns>
        public ActionResult ContactSubmit()
        {
            Title = ViewBag.ResLang["預約賞屋"] + "|";

            Data = Repository.Cases.CasesData.Single(defLang, ID);

            return View();
        }
    }
}