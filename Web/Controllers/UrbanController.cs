﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Service;
using PagedList;

namespace Web.Controllers
{
    public class UrbanController : BaseController
    {
        public System.DateTime todayFirst = System.DateTime.Parse(System.DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00");
        public System.DateTime todayLast = System.DateTime.Parse(System.DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59");

        /// <summary>
        /// 都市更新消息
        /// </summary>
        /// <returns></returns>
        public ActionResult News()
        {
            Title = ViewBag.ResLang["都市更新消息"] + "|";
            Data = Repository.Urban.UrbanNews.All(defLang);

            return View();
        }

        /// <summary>
        /// 都市更新Q&A
        /// </summary>
        /// <returns></returns>
        public ActionResult Faq(int? page)
        {
            Title = ViewBag.ResLang["都市更新Q&A"] + "|";
            Data = Repository.Urban.UrbanFaq.All(defLang);
            int pageWidth = 10;//預設每頁長度
            var pageNumeber = page ?? 1;

            var faq = Repository.Urban.UrbanFaq.All(defLang).Where(m => m.title != "");

            Data = faq.ToPagedList(pageNumeber, pageWidth);
            string PagePath = Url.Content("~/") + defLang + "/Urban/Faq";
            List<urban_faq> newsTotal = new List<urban_faq>();

            newsTotal = faq.ToList();
            Data = faq.ToPagedList(pageNumeber, pageWidth);

            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)newsTotal.Count / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, newsTotal.Count, "");//頁碼

            return View();
        }

        /// <summary>
        /// 都市更新專區
        /// </summary>
        /// <returns></returns>
        public ActionResult Column(int? page)
        {
            Title = ViewBag.ResLang["都市更新專區"] + "|";

            ViewBag.CategoryData = null;
            int pageWidth = 8;//預設每頁長度
            var pageNumeber = page ?? 1;

            List<urban_column_category> Category = Repository.Urban.UrbanColumnCategory.All(defLang);
            ViewBag.Category = Category.OrderBy(x=> x.sortIndex).ToList();
            var column = Repository.Urban.UrbanColumn.All(defLang).Where(m => m.title != "").OrderBy(x => x.sortIndex).ThenByDescending(x => x.create_date).AsEnumerable();

            List<urban_column> newsTotal = new List<urban_column>();
            if (string.IsNullOrEmpty(ID))
            {
                urban_column_category temp = Category.OrderBy(m => m.sortIndex).FirstOrDefault();
                ID = temp.guid;
            }

            ViewBag.CategoryData = Category.Where(m => m.guid == ID).FirstOrDefault();
            Title = ViewBag.CategoryData.title + "|" + ViewBag.ResLang["都市更新專區"] + " |";
            column = column.Where(m => m.category == ID);
            Data = column.Where(m => m.category == ID).ToPagedList(pageNumeber, pageWidth);
            string PagePath = Url.Content("~/") + defLang + "/Urban/Column/" + ID;

            newsTotal = column.ToList();
            Data = column.ToPagedList(pageNumeber, pageWidth);

            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)newsTotal.Count / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, newsTotal.Count, "");//頁碼
            return View();
        }

        /// <summary>
        /// 都市更新專區(內頁)
        /// </summary>
        /// <returns></returns>
        public ActionResult ColumnDetail()
        {
            Title = ViewBag.ResLang["都市更新專區"] + "|";

            if (!string.IsNullOrEmpty(ID))
            {
                //社區關懷分類

                urban_column item = Repository.Urban.UrbanColumn.Single(defLang, ID);
                ViewBag.Files = null;
                if (item != null)
                {
                    Data = item;
                    Title = item.title + "|" + ViewBag.ResLang["都市更新專區"] + "|";

                    ViewBag.Files = Repository.Urban.UrbanColumnFiles.All(defLang, ID);
                }

                ViewBag.urban_column_category = Repository.Urban.UrbanColumnCategory.Single(defLang, item.category);

                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 都市更新知識庫
        /// </summary>
        /// <returns></returns>
        public ActionResult Knowledge()
        {
            Title = ViewBag.ResLang["都市更新知識庫"] + "|";
            Data = Repository.Urban.UrbanKnowledge.All(defLang);
            return View();
        }

        /// <summary>
        /// 與我們聯絡
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            Title = ViewBag.ResLang["與我們聯絡"] + "|";

            ViewBag.residential = Repository.Forms.FormSubject.All(defLang, "2");//住宅區
            ViewBag.business = Repository.Forms.FormSubject.All(defLang, "3");//商業區
            ViewBag.subject = Repository.Forms.FormSubject.All(defLang, "4");//整合情況說明
            ViewBag.house_status = Repository.Forms.FormSubject.All(defLang, "5");//房屋(土地)狀況

            Data = null;

            if (Session["UrbanContact"] != null)
            {
                Data = Session["UrbanContact"];
            }

            return View();
        }

        /// <summary>
        /// 送出表單
        /// </summary>
        /// <param name="form"></param>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void ContactSend(FormCollection form)
        {
            try
            {
                //註冊資料暫存
                Session.Remove("UrbanContact");
                Session.Add("UrbanContact", form);

                if (form["verification"].ToString() != Session["_ValCheckCode"].ToString())
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                    alertData.Add("text", "");
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Urban/Contact"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
                else
                {
                    urban_contacts urban_contacts = new urban_contacts();
                    urban_contacts.guid = FunctionService.getGuid();
                    urban_contacts.name = form["name"].ToString();
                    urban_contacts.tel = form["tel"].ToString();
                    urban_contacts.email = form["email"].ToString();
                    urban_contacts.create_date = DateTime.Now;
                    urban_contacts.content = form["content"].ToString();
                    urban_contacts.fax = form["fax"].ToString();
                    urban_contacts.house_status = form["house_status"].ToString();
                    urban_contacts.address = form["address"].ToString();
                    urban_contacts.residential = form["residential"].ToString();
                    urban_contacts.business = form["business"].ToString();
                    urban_contacts.land_number = form["land_number"].ToString();
                    urban_contacts.area = form["area"].ToString();
                    urban_contacts.subject = form["subject"].ToString();
                    urban_contacts.status = "N";
                    urban_contacts.re_mail = "N";
                    try
                    {
                        DB.urban_contacts.Add(urban_contacts);
                        DB.SaveChanges();

                        Session.Remove("UrbanContact");

                        Session.Remove("alertData");

                        List<string> mailList = webData.UrbanContactEmail.Split(',').ToList();
                        FunctionService.sendMail(mailList, "3", defLang, webURL.ToString(), form);

                        Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Urban/ContactSubmit'") + ";</script>");
                    }
                    catch (Exception ex)
                    {
                        Dictionary<String, Object> alertData = new Dictionary<string, object>();
                        alertData.Add("title", langData["送出失敗"].ToString() + "!");
                        alertData.Add("text", langData["請檢查檢查相關資訊或洽官網管理員"].ToString());
                        alertData.Add("type", "error");
                        alertData.Add("url", Url.Content("~/" + defLang + "/Urban/ContactForm"));

                        Session.Remove("alertData");
                        Session.Add("alertData", alertData);

                        Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                    }
                }
            }
            catch (Exception ex)
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["送出失敗"].ToString() + "!");
                alertData.Add("text", "系統錯誤，請洽網站管理員!");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang + "/Home/Contact"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
            }
        }

        /// <summary>
        /// 完成表單
        /// </summary>
        /// <returns></returns>
        public ActionResult ContactSubmit()
        {
            Title = ViewBag.ResLang["聯絡詢問表單"] + "|";

            return View();
        }
    }
}