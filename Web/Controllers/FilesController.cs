﻿using System.Web.Mvc;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Web.Models;




namespace ElFinder.Sample.Controllers
{
    public class FilesController : Controller
    {
        private Connector _connector;
    

        public Connector Connector
        {
      

            get
            {
                if (_connector == null)
                {
                

                    FileSystemDriver driver = new FileSystemDriver();
                    DirectoryInfo thumbsStorage = new DirectoryInfo(Server.MapPath("~/Content/Upload/"));
                 
                  

                    driver.AddRoot(new Root(new DirectoryInfo(Server.MapPath("~/Content/Upload")), Url.Content("~/Content/Upload/"))                  
                    {
                   
                        Alias = "Documents",
              
                        //StartPath = new DirectoryInfo(Server.MapPath("~/Content/Upload/tmb")),
                        ThumbnailsStorage = thumbsStorage,
                        MaxUploadSizeInMb = 1000,
                        ThumbnailsUrl = Url.Content("~/Thumbnails/"),
                       
                    });


                   
                    _connector = new Connector(driver);


                }
                return _connector;
            }
        }

    


       public ActionResult Index()
        {
            return Connector.Process(this.HttpContext.Request);
        }

          public ActionResult SelectFile(string target)
        {
            return Json(Connector.GetFileByHash(target).FullName);
        }

        public ActionResult Thumbs(string tmb)
        {
            return Connector.GetThumbnail(Request, Response, tmb);
        }


     
    }
}


