﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Service;
using PagedList;

namespace Web.Controllers
{
    public class InvestorController : BaseController
    {
        public System.DateTime todayFirst = System.DateTime.Parse(System.DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00");
        public System.DateTime todayLast = System.DateTime.Parse(System.DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59");


        /// <summary>
        /// 公司基本資料
        /// </summary>
        /// <returns></returns>
        public ActionResult Info()
        {
            Title = ViewBag.ResLang["公司基本資料"] + "|";
            Data = Repository.Investor.InvestorInfo.Single(defLang, "1");

            return View();
        }

        /// <summary>
        /// 每月財務報表
        /// </summary>
        /// <returns></returns>
        public ActionResult FinanceStatement(int? page)
        {
            Title = ViewBag.ResLang["月營收報告"] + "|";

            int pageWidth = 24;//預設每頁長度
            var pageNumeber = page ?? 1;

            List<finance_report> Finance_report = Repository.Investor.FinanceReport.All(defLang, "1");
            ViewBag.DataUsePage = Finance_report.ToPagedList(pageNumeber, pageWidth);
            string PagePath = Url.Content("~/") + defLang + "/Investor/FinanceStatement";
            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Finance_report.Count / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, Finance_report.Count, "");//頁碼  

            return View();
        }
        /// <summary>
        /// 每月營運報告
        /// </summary>
        /// <returns></returns>
        public ActionResult FinanceOperation(int? page)
        {
            Title = ViewBag.ResLang["財務報表"] + "|";

            int pageWidth = 24;//預設每頁長度
            var pageNumeber = page ?? 1;

            List<finance_report> Finance_report = Repository.Investor.FinanceReport.All(defLang, "2");
            ViewBag.DataUsePage = Finance_report.ToPagedList(pageNumeber, pageWidth);
            string PagePath = Url.Content("~/") + defLang + "/Investor/FinanceOperation";
            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Finance_report.Count / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, Finance_report.Count, "");//頁碼  

            return View();
        }
        /// <summary>
        /// 公司年報
        /// </summary>
        /// <returns></returns>
        public ActionResult FinanceAnnual(int? page)
        {
            Title = ViewBag.ResLang["公司年報"] + "|";

            int pageWidth = 12;//預設每頁長度
            var pageNumeber = page ?? 1;

            List<finance_report> Finance_report = Repository.Investor.FinanceReport.All(defLang, "3");
            ViewBag.DataUsePage = Finance_report.ToPagedList(pageNumeber, pageWidth);
            string PagePath = Url.Content("~/") + defLang + "/Investor/FinanceAnnual";
            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Finance_report.Count / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, Finance_report.Count, "");//頁碼  

            return View();
        }
        /// <summary>
        /// 重要公告
        /// </summary>
        /// <returns></returns>
        public ActionResult Announcement()
        {
            Title = ViewBag.ResLang["重要公告"] + "|";
            Data = Repository.Investor.InvestorInfo.Single(defLang, "2");
            return View();
        }
        /// <summary>
        /// 股權結構&股價資訊
        /// </summary>
        /// <returns></returns>
        public ActionResult Shareholder()
        {
            Title = ViewBag.ResLang["股權結構&股價資訊"] + "|";
            Data = Repository.Investor.Shareholders.Single(defLang, "1");         
            Dictionary<string, List<string>> subData = FunctionService.InfinityJsonFormat(ViewBag.Data.content);
            List<string> color = new List<string>();
            List<string> title = new List<string>();
            List<string> per = new List<string>();

            ViewData["JsColor"] = "";
            ViewData["JsTitle"] = "";
            ViewData["JsPer"] = "";

            if (subData.Count > 0)
            {
                foreach (KeyValuePair<string, List<string>> item in subData)
                {
                    if (item.Key == "color")
                    {
                        foreach (string tmp in item.Value)
                        {
                            color.Add(tmp);                      
                            ViewData["JsColor"] +=  "'" + tmp + "',";
                        }
                    }
                    if (item.Key == "title")
                    {
                        foreach (string tmp in item.Value)
                        {
                            title.Add(tmp);
                            string setTitle = tmp;
                            if (tmp.Length > 5)
                            {
                                setTitle = tmp.Insert(5, @"\n");
                            }
                            ViewData["JsTitle"] += "'" + setTitle + "',";
                        }
                    }
                    if (item.Key == "per")
                    {
                        foreach (string tmp in item.Value)
                        {
                            per.Add(tmp);
                            ViewData["JsPer"] += tmp.Replace("%","") + "," ;
                        }
                    }
                }
            }


            Dictionary<string, List<string>> ListData = new Dictionary<string, List<string>>();
            if(color.Count > 0)
            {
                ListData.Add("color", color);
                ListData.Add("title", title);
                ListData.Add("per", per);
            }

            ViewBag.ListData = ListData;



                return View();
        }
        /// <summary>
        /// 股東服務
        /// </summary>
        /// <returns></returns>
        public ActionResult ShareholderMeeting()
        {
            Title = ViewBag.ResLang["股東常會"] + "|";
            ViewBag.UrlPath = "ShareholderMeeting";
            Data = Repository.Investor.ShareDownload.All(defLang, "1");


            return View();
        }

        /// <summary>
        /// 股利資訊
        /// </summary>
        /// <returns></returns>
        public ActionResult ShareholderDividend()
        {
            Title = ViewBag.ResLang["股利資訊"] + "|";
            Data = Repository.Investor.InvestorInfo.Single(defLang, "3");
            ViewBag.UrlPath = "ShareholderDividend";

            return View();
        }

        /// <summary>
        /// 投資人問答集
        /// </summary>
        /// <returns></returns>
        public ActionResult ShareholderFaq(int? page)
        {
            Title = ViewBag.ResLang["投資人問答集"] + "|";
            ViewBag.UrlPath = "ShareholderFaq";

            int pageWidth = 10;//預設每頁長度
            var pageNumeber = page ?? 1;

            List<share_faq> Share_faq = Repository.Investor.ShaerFaq.All(defLang);
            ViewBag.DataUsePage = Share_faq.ToPagedList(pageNumeber, pageWidth);
            string PagePath = Url.Content("~/") + defLang + "/Investor/ShareholderFaq";
            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Share_faq.Count / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, Share_faq.Count, "");//頁碼  

            return View();
        }

        /// <summary>
        /// 股東辦理事項表單下載
        /// </summary>
        /// <returns></returns>
        public ActionResult ShareholderDownload()
        {
            Title = ViewBag.ResLang["股東辦理事項表單下載"] + "|";
            ViewBag.UrlPath = "ShareholderDownload";
            Data = Repository.Investor.ShareDownload.All(defLang, "2");

            return View();
        }

        /// <summary>
        /// 其他
        /// </summary>
        /// <returns></returns>
        public ActionResult Others()
        {
            Title = ViewBag.ResLang["其他"] + "|";
            Data = Repository.Notes.NotesData.Single(defLang, "6");
            return View();
        }

    }
}