﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Service;
using PagedList;
using System.Collections;
using Newtonsoft.Json;
using Web.ServiceModels;
using Web.Repository.System;
using System.Runtime.Caching;

namespace Web.Controllers
{
    public class LoginController : BaseController
    {
        private static MemoryCache _cache = MemoryCache.Default;

        /// <summary>
        /// 送出登入
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void LoginAction(FormCollection form)
        {
            if (form["verification"].ToString() != Session["_ValCheckCode"].ToString())
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang + "/Member/Login"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");

                //新增登入紀錄
                MemberLogService ml = new MemberLogService();
                var ip = FunctionService.GetIP();
                ml.InsertLog(form["UserId"].ToString(), ip, "N", "ver");
            }
            else
            {
                HttpCookie c = new HttpCookie("cres_token");
                c.Values.Remove("jwtToken");
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);

                _cache.Remove("member");
                _cache.Remove("UserID");

                string apiUrl = ApiPath + "/api/CreMember/login";
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("UserId", form["UserId"].ToString());
                data.Add("Password", form["Password"].ToString());

                string response = FunctionService.CallApi(apiUrl, "Login", data);

                if (response.IndexOf("Error") != -1)
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", "系統連線錯誤");
                    alertData.Add("text", response);
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Member/Login"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
                else
                {
                    if (response.ToString() != "")
                    {
                        ServiceModels.Login logins = JsonConvert.DeserializeObject<ServiceModels.Login>(response);

                        //登入失敗
                        if (logins.isSuccess == "false")
                        {
                            //新增登入紀錄
                            MemberLogService ml = new MemberLogService();
                            var ip = FunctionService.GetIP();
                            ml.InsertLog(form["UserId"].ToString(), ip, "N", "api");

                            Dictionary<String, Object> alertData = new Dictionary<string, object>();
                            alertData.Add("title", logins.message.ToString());
                            alertData.Add("text", "");
                            alertData.Add("type", "error");
                            alertData.Add("url", Url.Content("~/" + defLang + "/Member/Login"));

                            Session.Remove("alertData");
                            Session.Add("alertData", alertData);

                            Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                        }
                        //登入成功
                        else
                        {
                            if (logins.state == "100" || logins.state == "101")
                            {
                                //Session["member"] = logins;
                                //Session["UserID"] = form["UserId"].ToString();

                                var encryptUserID = FunctionService.aesEncryptBase64(form["UserId"].ToString(), "minmax");
                                HttpCookie cookie_mid = new HttpCookie("mid");
                                cookie_mid.Value = encryptUserID;
                                cookie_mid.Expires = DateTime.Now.AddDays(1);
                                Response.Cookies.Add(cookie_mid);

                                CacheItemPolicy cacheItemPolicy = new CacheItemPolicy() { AbsoluteExpiration = DateTime.Now.AddDays(1) };
                                _cache.Add("member" + form["UserId"].ToString(), logins, cacheItemPolicy);
                                _cache.Add("UserID" + form["UserId"].ToString(), form["UserId"].ToString(), cacheItemPolicy);

                                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                                alertData.Add("title", langData["登入成功"].ToString());
                                alertData.Add("text", "");
                                alertData.Add("type", "success");
                                //alertData.Add("url", Url.Content("~/" + defLang + "/Member/Index"));
                                alertData.Add("url", Url.Content("~/tw" + "/Member/Index"));

                                //新增TOKEN
                                //LoginTokenService LoginToken = new LoginTokenService();
                                //TokenManager _tokenManager = new TokenManager();
                                //var user_id = form["UserId"].ToString();
                                //var token = _tokenManager.Create(user_id);
                                //LoginToken.InsertToken(token);
                                //Session["LoginToken"] = token.refresh_token;

                                //產生一個Cookie
                                WebDataService webData = new WebDataService();
                                var wdata = webData.GetData();

                                if (wdata.token_timeout != null && wdata.token_timeout > 1)
                                {
                                    HttpCookie cookie = new HttpCookie("cres_token");
                                    cookie.Domain = ".cathay-red.com.tw";
                                    cookie.Value = logins.result.jwtToken;
                                    double tokentimeout = (double)wdata.token_timeout;
                                    cookie.Expires = DateTime.Now.AddMinutes(tokentimeout);
                                    Response.Cookies.Add(cookie);
                                }
                                else
                                {
                                    HttpCookie cookie = new HttpCookie("cres_token");
                                    cookie.Domain = ".cathay-red.com.tw";
                                    cookie.Value = logins.result.jwtToken;
                                    cookie.Expires = DateTime.Now.AddHours(2);
                                    Response.Cookies.Add(cookie);
                                }

                                Session.Remove("alertData");
                                Session.Add("alertData", alertData);

                                //新增登入紀錄
                                MemberLogService ml = new MemberLogService();
                                var ip = FunctionService.GetIP();
                                ml.InsertLog(form["UserId"].ToString(), ip, "Y", logins.state);

                                Response.Redirect(Url.Content("~/" + defLang + "/Home/Alert"));
                            }
                            else
                            {
                                //新增登入紀錄
                                MemberLogService ml = new MemberLogService();
                                var ip = FunctionService.GetIP();
                                ml.InsertLog(form["UserId"].ToString(), ip, "N", logins.state);

                                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                                alertData.Add("title", logins.message.ToString());
                                alertData.Add("text", "");
                                alertData.Add("type", "error");
                                alertData.Add("url", Url.Content("~/" + defLang + "/Member/Login"));

                                Session.Remove("alertData");
                                Session.Add("alertData", alertData);

                                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        public void Logout()
        {
            HttpCookie c = new HttpCookie("cres_token");
            c.Values.Remove("jwtToken");
            c.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(c);

            HttpCookie c_mid = Request.Cookies["mid"];

            if(c_mid != null)
            {

                var decryptUserID = FunctionService.aesDecryptBase64(c_mid.Value.ToString(), "minmax");
                if (!string.IsNullOrEmpty(decryptUserID))
                {
                    _cache.Remove("member" + decryptUserID);
                    _cache.Remove("UserID" + decryptUserID);
                    //_cache.Remove("GetClassPayList" + decryptUserID);


                    

                    HttpRuntime.Cache.Remove("GetClassPayList_Title" + decryptUserID);

                    
                    var AllKey = GetAllKey();
                    var DelKeyList = AllKey.Where(x => x.Contains("GetClassPayList_Data" + decryptUserID)).ToList();
                    foreach(var key in DelKeyList)
                    {
                        HttpRuntime.Cache.Remove(key);
                    }

                    c_mid.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(c_mid);
                }

                //刪除TOKEN
                //LoginTokenService LoginToken = new LoginTokenService();
                //var token = Session["LoginToken"].ToString();
                //LoginToken.DeleteData(token);

                Session.Abandon();

                //Session.Remove("member");
                Response.Redirect(Url.Content("~/" + defLang + "/Member/Login"));
            }
            else
            {
                Session.Abandon();

                Response.Redirect(Url.Content("~/" + defLang + "/Member/Login"));
            }
        }

        public IEnumerable<string> GetAllKey()
        {
            IDictionaryEnumerator CacheEnum = HttpRuntime.Cache.GetEnumerator();
            while (CacheEnum.MoveNext())
            {
                yield return CacheEnum.Key.ToString();
            }
        }
    }
}