﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Service;
using PagedList;
using System.Web.UI;
using Newtonsoft.Json;
using System.Collections;
using Web.ServiceModels;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity;

namespace Web.Controllers
{
    public class HomeController : BaseController
    {
        public System.DateTime todayFirst = System.DateTime.Parse(System.DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00");
        public System.DateTime todayLast = System.DateTime.Parse(System.DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59");

        public ActionResult Index()
        {

            Title = "";

            List<news> news = DB.news.OrderByDescending(m => m.startdate)
                              .Where(m => m.lang == defLang).Where(m => m.tops == "Y").Where(m => m.status == "Y").Where(m => m.verify == "Y").Where(m => m.title != "")
                              .Where(m => m.startdate <= todayFirst)
                              .Where(m => m.enddate >= todayLast || m.enddate.ToString() == null || m.enddate.ToString() == "").ToList();

            ViewBag.News = news;

            var Repositorys = new Repository.EFUnitOfWork(DB);

            //Banner
            ViewBag.Banners = Repository.Home.Banners.All(defLang);

            //首頁其他資訊
            ViewBag.IndexInfo = Repository.Home.IndexInfo.Single(defLang, "1");

            return View();
        }

        /// <summary>
        /// 人才招募
        /// </summary>
        /// <returns></returns>
        public ActionResult Career()
        {
            Title = ViewBag.ResLang["人才招募"] + "|";
            Data = Repository.Notes.NotesData.Single(defLang, "1");

            return View();
        }

        /// <summary>
        /// 隱私權政策
        /// </summary>
        /// <returns></returns>
        public ActionResult Privacy()
        {
            Title = ViewBag.ResLang["隱私權政策"] + "|";
            Data = Repository.Notes.NotesData.Single(defLang, "2");
            return View();
        }

        /// <summary>
        /// 隱私權政策
        /// </summary>
        /// <returns></returns>
        public ActionResult Terms()
        {
            Title = ViewBag.ResLang["使用者條款"] + "|";
            Data = Repository.Notes.NotesData.Single(defLang, "3");
            return View();
        }

        public ActionResult Alert()
        {
            if (Session["alertData"] != null)
            {
                ViewBag.alertData = Session["alertData"];
                Session.Remove("alertData");

                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public string Ajax(FormCollection form)
        {
            Dictionary<String, Object> dic = new Dictionary<string, object>();

            NameValueCollection getData = FunctionService.reSubmitFormDataJson(HttpUtility.UrlDecode(form["Val"].ToString()));//回傳JSON資料
            dic.Add("Func", form["Func"].ToString());

            switch (form["Func"].ToString())
            {
                //購物試算
                case "calculation":
                    if (getData["verification"].ToString() != Session["_ValCheckCode"].ToString())
                    {
                        dic.Add("re", "Error");
                        dic.Add("msg", langData["驗證碼比對錯誤"].ToString());
                    }
                    else
                    {
                        dic.Add("re", "OK");
                        dic.Add("type", getData["type"].ToString());
                        Dictionary<int, object> YearPrice = new Dictionary<int, object>();
                        //總支出-本金
                        double principal_all = 0;
                        double interest_all = 0;
                        double month_price_all = 0;
                        double tired_all = 0;
                        double every_month = 0;

                        try
                        {
                            //購屋金額
                            float price = float.Parse(getData["price"].ToString());
                            //貸款成數
                            float percentage = float.Parse(getData["percentage"].ToString()) / 100;
                            //貸款金額
                            float loan_amount = price * percentage;
                            //自備款
                            float SelfSupplied = price - loan_amount;
                            //貸款年限
                            float year = float.Parse(getData["year"].ToString());
                            //單一利率
                            float interest = float.Parse(getData["interest"].ToString());
                            interest = Convert.ToSingle(interest * 0.01);
                            double totalMonth = year * 12;

                            switch (getData["type"].ToString())
                            {
                                //本金攤還
                                case "1":

                                    for (int y = 1; y <= year; y++)
                                    {
                                        List<CalculationModel> CalculationList = new List<CalculationModel>();

                                        for (int m = 1; m <= 12; m++)
                                        {
                                            CalculationModel calculation = new CalculationModel();

                                            double principal = Math.Round((loan_amount / 20) / 12, 0, MidpointRounding.AwayFromZero);//本金攤還

                                            double monthNo = ((y - 1) * 12) + m;//月份代碼

                                            if (y == 1 && m == 1)
                                            {
                                                calculation.principal = principal;//本金攤還
                                            }
                                            else
                                            {
                                                if (totalMonth >= monthNo)
                                                {
                                                    calculation.principal = principal;//本金攤還
                                                }
                                                else
                                                {
                                                    calculation.principal = 0;//本金攤還
                                                }
                                            }

                                            calculation.interest = Math.Round(((loan_amount - calculation.principal * (monthNo - 1)) * interest) / 12, 0, MidpointRounding.AwayFromZero);//利息攤還
                                            calculation.month = monthNo;
                                            calculation.month_price = calculation.principal + calculation.interest; // 每月還款

                                            //總計
                                            principal_all = principal_all + calculation.principal;
                                            interest_all = interest_all + calculation.interest;
                                            month_price_all = month_price_all + calculation.month_price;

                                            CalculationList.Add(calculation);
                                        }

                                        YearPrice.Add(y, CalculationList);
                                    }

                                    if (month_price_all > 0)
                                    {
                                        every_month = Math.Round(month_price_all / (year * 12), 0, MidpointRounding.AwayFromZero);//均月支出
                                    }

                                    break;
                                //本息攤還
                                case "2":

                                    for (int y = 1; y <= year; y++)
                                    {
                                        List<CalculationModel> CalculationList = new List<CalculationModel>();

                                        for (int m = 1; m <= 12; m++)
                                        {
                                            CalculationModel calculation = new CalculationModel();
                                            double monthNo = ((y - 1) * 12) + m;//月份代碼
                                            float interest_month = interest / 12; // 月利率

                                            interest_month = float.Parse(Math.Round(Convert.ToDecimal(interest_month), 8, MidpointRounding.AwayFromZero).ToString());

                                            double month_price = Math.Round(loan_amount * (
                                                   Math.Pow((1 + interest_month), (totalMonth)) * interest_month
                                                   ) / (
                                                   Math.Pow((1 + interest_month), (totalMonth)) - 1
                                                  ), MidpointRounding.AwayFromZero);//每月還款

                                            calculation.month_price = month_price;
                                            double tired = 0;//本金累還
                                            if (y == 1 && m == 1)
                                            {
                                                calculation.interest = Math.Round((loan_amount - 0) * interest_month, 0, MidpointRounding.AwayFromZero);//利息攤還
                                                tired = 0;//本金累還
                                            }
                                            else
                                            {
                                                CalculationModel prevData = new CalculationModel();

                                                if (y > 1 && (m - 1 == 0) && CalculationList.Count == 0)
                                                {
                                                    List<CalculationModel> tmp = (List<CalculationModel>)YearPrice[y - 1];
                                                    prevData = tmp[11];
                                                }
                                                else
                                                {
                                                    prevData = CalculationList[m - 2];
                                                }

                                                tired = prevData.tired;
                                                calculation.interest = Math.Round((loan_amount - tired) * interest_month, 0, MidpointRounding.AwayFromZero);//利息攤還
                                            }

                                            calculation.principal = month_price - calculation.interest;//本金攤還
                                            calculation.tired = calculation.principal + tired;//本金累還
                                            calculation.month = monthNo;//月碼

                                            //總計
                                            principal_all = principal_all + calculation.principal;
                                            interest_all = interest_all + calculation.interest;
                                            month_price_all = month_price_all + calculation.month_price;
                                            tired_all = tired_all + calculation.tired;

                                            CalculationList.Add(calculation);
                                        }

                                        YearPrice.Add(y, CalculationList);
                                    }

                                    if (month_price_all > 0)
                                    {
                                        every_month = Math.Round(month_price_all / (year * 12), 0, MidpointRounding.AwayFromZero);//均月支出
                                    }

                                    break;

                                //到期還本
                                case "3":
                                    for (int y = 1; y <= year; y++)
                                    {
                                        List<CalculationModel> CalculationList = new List<CalculationModel>();
                                        for (int m = 1; m <= 12; m++)
                                        {
                                            CalculationModel calculation = new CalculationModel();
                                            double monthNo = ((y - 1) * 12) + m;//月份代碼
                                            calculation.interest = Math.Round(((loan_amount - calculation.principal * (monthNo - 1)) * interest) / 12, 0, MidpointRounding.AwayFromZero);//利息攤還
                                            calculation.month = monthNo;

                                            if (y == year && m == 12)
                                            {
                                                calculation.principal = loan_amount;
                                                calculation.month_price = loan_amount + calculation.interest;
                                            }
                                            else
                                            {
                                                calculation.month_price = calculation.principal + calculation.interest;
                                            }

                                            //總計
                                            principal_all = principal_all + calculation.principal;
                                            interest_all = interest_all + calculation.interest;
                                            month_price_all = month_price_all + calculation.month_price;

                                            CalculationList.Add(calculation);
                                        }
                                        YearPrice.Add(y, CalculationList);
                                    }

                                    //if(month_price_all > 0)
                                    //{
                                    //    every_month = Math.Round(month_price_all / (year * 12), 0, MidpointRounding.AwayFromZero);//均月支出
                                    //}
                                    break;
                            }
                        }
                        catch
                        {
                            dic.Add("re", "ErrorVal");
                            dic.Add("msg", langData["您有數值輸入錯誤"].ToString());
                        }

                        dic.Add("total", langData["總支出"].ToString());
                        dic.Add("every_month", every_month);
                        dic.Add("data", YearPrice);
                        dic.Add("principal_all", principal_all);
                        dic.Add("interest_all", interest_all);
                        dic.Add("month_price_all", month_price_all);
                        dic.Add("tired_all", tired_all);
                    }

                    break;
            }

            string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
            //輸出json格式
            return json;
        }

        /// <summary>
        /// 清除暫存
        /// </summary>
        public void ClearCache()
        {
            List<string> cacheKeys = new List<string>();
            IDictionaryEnumerator cacheEnum = HttpRuntime.Cache.GetEnumerator();
            while (cacheEnum.MoveNext())
            {
                cacheKeys.Add(cacheEnum.Key.ToString());
            }
            foreach (string cacheKey in cacheKeys)
            {
                Response.Write(cacheKey.ToString() + "：Clear!<br>");

                HttpRuntime.Cache.Remove(cacheKey);
            }

            Response.Write("All Cache is Clear!<br>");
        }

        public partial class CalculationModel
        {
            public double principal { get; set; }//本金攤還
            public double interest { get; set; }//利息攤還
            public double month_price { get; set; }//每月還款

            public double month { get; set; }//月分

            public double tired { get; set; }//本金累還
        }
    }
}