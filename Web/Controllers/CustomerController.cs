﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Service;
using PagedList;
using System.Collections;

namespace Web.Controllers
{
    public class CustomerController : BaseController
    {
        public System.DateTime todayFirst = System.DateTime.Parse(System.DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00");
        public System.DateTime todayLast = System.DateTime.Parse(System.DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59");
        // GET: Customer

        /// <summary>
        /// 建築工法
        /// </summary>
        /// <returns></returns>
        public ActionResult Construction()
        {
            Title = ViewBag.ResLang["建築工法"] + "|";

            List<constructions> Constructions = Repository.Customer.Constructions.All(defLang);
            ViewBag.Category = Constructions;
            Data = Constructions.FirstOrDefault();
            if(!string.IsNullOrEmpty(ID))
            {
                Data = Constructions.Where(m=>m.guid == ID).FirstOrDefault();
            }

            return View();
        }


        /// <summary>
        /// 社區關懷
        /// </summary>
        /// <returns></returns>
        public ActionResult Community(int? page)
        {
            Title = ViewBag.ResLang["社區關懷"] + "|";

            ViewBag.CategoryData = null;
            int pageWidth = 8;//預設每頁長度
            var pageNumeber = page ?? 1;



            List<community_category> Category = Repository.Customer.CommunityCategory.All(defLang);
            ViewBag.Category = Category;
           var communitys = Repository.Customer.CommunityDetail.All(defLang).OrderByDescending(m => m.startdate)
                            .Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "")
                            .Where(m => m.startdate <= todayFirst)
                            .Where(m => m.enddate >= todayLast || m.enddate.ToString() == null || m.enddate.ToString() == "");

            List<communitys> newsTotal = new List<communitys>();
            if (string.IsNullOrEmpty(ID))
            {
                //community_category temp = Category.OrderBy(m => m.sortIndex).FirstOrDefault();
                //ID = temp.guid;
                Title = ViewBag.ResLang["社區關懷"] + " |";
                Data = communitys.ToPagedList(pageNumeber, pageWidth);
            }
            else
            {
                ViewBag.CategoryData = Category.Where(m => m.guid == ID).FirstOrDefault();
                communitys = communitys.Where(m => m.category == ID);
                Title = ViewBag.CategoryData.title + "|" + ViewBag.ResLang["社區關懷"] + " |";
                Data = communitys.Where(m => m.category == ID).ToPagedList(pageNumeber, pageWidth);
            }
              

       
            string PagePath = Url.Content("~/") + defLang + "/Customer/Community/" + ID;

            newsTotal = communitys.ToList();
            Data = communitys.ToPagedList(pageNumeber, pageWidth);

            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)newsTotal.Count / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, newsTotal.Count, "");//頁碼
            return View();

        }
        /// <summary>
        /// 社區關懷(內容)
        /// </summary>
        /// <returns></returns>
        public ActionResult CommunityDetail()
        {
            Title = ViewBag.ResLang["社區關懷"] + "|";

            ViewBag.Category = null;
            ViewBag.CategoryData = null;
            if (!string.IsNullOrEmpty(ID))
            {
                //社區關懷分類
                List<community_category> Category = Repository.Customer.CommunityCategory.All(defLang);
                ViewBag.Category = Category;
                communitys item = Repository.Customer.CommunityDetail.Single(defLang , ID);
                if (item != null)
                {
                    Data = item;
                    Title = item.title + "|" + ViewBag.ResLang["公益活動"] + "|";

                    ViewBag.CategoryData = Category.Where(m => m.guid == item.category).FirstOrDefault();

                    #region 上下筆
                    var communitysList = DB.communitys.OrderByDescending(m => m.startdate)
                               .Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "")
                               .Where(m => m.category == item.category)
                               .Where(m => m.startdate <= todayFirst)
                               .Where(m => m.enddate >= todayLast || m.enddate.ToString() == null || m.enddate.ToString() == "")
                               .Select(a => new { guid = a.guid }).ToList();

                    ArrayList guidList = new ArrayList();
                    foreach (var val in communitysList)
                    {
                        guidList.Add(val.guid);
                    }

                    int nowIndex = guidList.IndexOf(ID);

                    ViewData["prev"] = null;
                    ViewData["next"] = null;

                    if ((nowIndex - 1) >= 0)
                    {
                        string tempGuid = guidList[(nowIndex - 1)].ToString();
                        ViewData["prev"] = tempGuid;
                    }
                    if ((nowIndex + 1) < guidList.Count)
                    {
                        string tempGuid = guidList[(nowIndex + 1)].ToString();
                        ViewData["next"] = tempGuid;
                    }
                    #endregion
                }

                return View();

            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }

        }

        /// <summary>
        /// 居家常識
        /// </summary>
        /// <returns></returns>
        public ActionResult Knowledge()
        {
            Title = ViewBag.ResLang["居家常識"] + "|";
            Data = Repository.Customer.Knowledges.All(defLang);
            return View();
        }

        /// <summary>
        /// 聯絡我們
        /// </summary>
        /// <returns></returns>
        public ActionResult ContactInfo()
        {
            Title = ViewBag.ResLang["聯絡我們"] + "|";
            Data = Repository.Customer.ContactInfo.All(defLang);

            return View();
        }
        /// <summary>
        /// 聯絡詢問表單
        /// </summary>
        /// <returns></returns>
        public ActionResult ContactForm()
        {
            Title = ViewBag.ResLang["聯絡詢問表單"] + "|";

            ViewBag.formSubject = Repository.Forms.FormSubject.All(defLang, "1");//主題

            Data = null;

            if (Session["Contact"] != null)
            {
                Data = Session["Contact"];
            }


            return View();
        }
        /// <summary>
        /// 客戶會員專區
        /// </summary>
        /// <returns></returns>
        public ActionResult MemberLogin()
        {
            Title = ViewBag.ResLang["客戶會員專區"] + "|";

            return View();
        }


        /// <summary>
        /// App下載
        /// </summary>
        /// <returns></returns>
        public ActionResult AppDownload()
        {
            Title = ViewBag.ResLang["App下載"] + "|";
            return View();
        }

        /// <summary>
        /// 尊榮優惠
        /// </summary>
        /// <returns></returns>
        public ActionResult Vip()
        {
            Title = ViewBag.ResLang["尊榮優惠"] + "|";
            Data = Repository.Notes.NotesData.Single(defLang, "5");
            return View();
        }

        /// <summary>
        /// 送出意見信箱
        /// </summary>
        /// <param name="form"></param>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void ContactSend(FormCollection form)
        {


            try
            {       
                //註冊資料暫存
                Session.Remove("Contact");
                Session.Add("Contact", form);

                if (form["verification"].ToString() != Session["_ValCheckCode"].ToString())
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                    alertData.Add("text", "");
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Customer/ContactForm"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");

                }
                else
                {
                    contacts contacts = new contacts();
                    contacts.guid = FunctionService.getGuid();
                    contacts.name = form["name"].ToString();
                    contacts.tel = form["tel"].ToString();
                    contacts.email = form["email"].ToString();
                    contacts.create_date = DateTime.Now;
                    contacts.content = form["content"].ToString();
                    contacts.company = form["company"].ToString();
                    contacts.status = "N";
                    contacts.subject = form["subject"].ToString();
                    contacts.re_mail = "N";
                    try
                    {
                        DB.contacts.Add(contacts);
                        DB.SaveChanges();

                        Session.Remove("Contact");
                     
                        Dictionary<String, Object> alertData = new Dictionary<string, object>();
                        alertData.Add("title", langData["送出完成"].ToString() + "!");
                        alertData.Add("text", langData["我們會盡快回覆您謝謝您"].ToString());
                        alertData.Add("type", "success");
                        alertData.Add("url", Url.Content("~/" + defLang + "/Customer/ContactFormSubmit"));
                        
                        Session.Remove("alertData");


                        List<string> mailList = webData.ContactEmail.Split(',').ToList();
                        FunctionService.sendMail(mailList , "2" , defLang , webURL.ToString() , form);

                        Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Customer/ContactFormSubmit'") + ";</script>");
                    
                  }
                  catch(Exception ex)
                  {
                      Dictionary<String, Object> alertData = new Dictionary<string, object>();
                      alertData.Add("title", langData["送出失敗"].ToString() + "!");
                      alertData.Add("text", langData["請檢查檢查相關資訊或洽官網管理員"].ToString());
                      alertData.Add("type", "error");
                      alertData.Add("url", Url.Content("~/" + defLang + "/Urban/ContactForm"));

                      Session.Remove("alertData");
                      Session.Add("alertData", alertData);

                      Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                  }

            }

            }
            catch (Exception ex)
            {

                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["送出失敗"].ToString() + "!");
                alertData.Add("text", "系統錯誤，請洽網站管理員!");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang + "/Home/Contact"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
            }

        }


        /// <summary>
        /// 完成表單
        /// </summary>
        /// <returns></returns>
        public ActionResult ContactFormSubmit()
        {
            Title = ViewBag.ResLang["聯絡詢問表單"] + "|";

            return View();
        }

    }
}