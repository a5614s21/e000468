﻿using Newtonsoft.Json;
using PagedList;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Service;
using Web.ServiceModels;

namespace Web.Controllers
{
    [SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]
    public class MemberController : BaseController
    {
        public System.DateTime todayFirst = System.DateTime.Parse(System.DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00");
        public System.DateTime todayLast = System.DateTime.Parse(System.DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59");
        private static MemoryCache _cache = MemoryCache.Default;
        // GET: Customer

        /// <summary>
        /// 會員登入
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            HttpCookie cookie_mid = Request.Cookies["mid"];
            if (cookie_mid != null)
            {
                var decryptUserID = FunctionService.aesDecryptBase64(cookie_mid.Value.ToString(), "minmax");

                if (!string.IsNullOrEmpty(decryptUserID) && _cache["member" + decryptUserID] != null)
                {
                    return RedirectToAction("Index");
                }
            }

            Session.Abandon();
            Title = ViewBag.ResLang["會員登入"] + "|";

            return View();
        }

        /// <summary>
        /// 客戶會員專區
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            Title = ViewBag.ResLang["客戶會員專區"] + "|";
            ViewBag.SubActive = "Index";

            HttpCookie cookie_mid = Request.Cookies["mid"];
            if(cookie_mid == null)
            {
                return RedirectToAction("Logout", "Login");
            }

            var decryptUserID = FunctionService.aesDecryptBase64(cookie_mid.Value.ToString(), "minmax");

            if (!string.IsNullOrEmpty(decryptUserID) && _cache["member" + decryptUserID] == null)
            {
                return RedirectPermanent(Url.Content("~/" + defLang + "/Member/Login"));
            }

            var PaymentStatus = TempData["PaymentStatus"];
            if ((Boolean)PaymentStatus == true)
            {
                return RedirectToAction("Payment");
            }
            else
            {
                return RedirectToAction("Progress");
            }
        }

        /// <summary>
        /// 工程進度
        /// </summary>
        /// <returns></returns>
        public ActionResult Progress(int? page)
        {
            Title = ViewBag.ResLang["工程進度"] + "|";
            ViewBag.SubActive = "Progress";

            HttpCookie cookie_mid = Request.Cookies["mid"];
            if (cookie_mid == null)
            {
                return RedirectToAction("Logout", "Login");
            }
            var decryptUserID = FunctionService.aesDecryptBase64(cookie_mid.Value.ToString(), "minmax");

            if (!string.IsNullOrEmpty(decryptUserID) && _cache["member" + decryptUserID] == null)
            {
                return RedirectPermanent(Url.Content("~/" + defLang + "/Member/Login"));
            }

            //會員資料
            List<InfoData> MemberInfoData = ViewBag.LoginMemberPreSale;

            int pageWidth = 12;//預設每頁長度
            var pageNumeber = page ?? 1;

            ViewBag.Cases = null;

            //取得建案資料
            List<cases> cases = Repository.Cases.CasesData.AllHaveProgressByInfoData(defLang, MemberInfoData);

            //篩選有工程進度的資料
            if (string.IsNullOrEmpty(ID) && cases != null && cases.Count != 0)
            {
                cases temp = cases.FirstOrDefault();
                ID = temp.guid;
            }

            int casecount = 0;
            if (cases != null)
            {
                foreach (var all in cases)
                {
                    var isPermissionAll = (from a in DB.project_progress
                                           where a.title.ToUpper() == "ALL" && a.lang == "tw" && a.status == "Y"
                                           select a).FirstOrDefault();
                    if (isPermissionAll == null)
                    {
                        var idstring = all.hmmcu.ToString().Trim();
                        var isPermission = (from a in DB.project_progress
                                            where a.title == idstring && a.lang == "tw" && a.status == "Y"
                                            select a).FirstOrDefault();
                        if (isPermission == null)
                        {
                            all.seo_description = "0";
                        }
                        else
                        {
                            var permissionoption = isPermission.typeoption.Split(',');
                            int ischecck = 0;
                            foreach (var item in permissionoption)
                            {
                                if (item == "工程進度")
                                {
                                    ischecck = 1;
                                }
                            }

                            if (ischecck == 0)
                            {
                                all.seo_description = "0";
                            }
                            else
                            {
                                all.seo_description = "1";
                                casecount = 1;
                            }
                        }
                    }
                    else
                    {
                        var permissionalloption = isPermissionAll.typeoption.Split(',');
                        int ischeckall = 0;
                        foreach (var itemall in permissionalloption)
                        {
                            if (itemall == "工程進度")
                            {
                                ischeckall = 1;
                            }
                        }
                        if (ischeckall == 0)
                        {
                            all.seo_description = "0";
                        }
                        else
                        {
                            var idstring = all.hmmcu.ToString().Trim();
                            var isPermission = (from a in DB.project_progress
                                                where a.title == idstring && a.lang == "tw" && a.status == "Y"
                                                select a).FirstOrDefault();
                            if (isPermission == null)
                            {
                                all.seo_description = "0";
                            }
                            else
                            {
                                var permissionoption = isPermission.typeoption.Split(',');
                                int ischecck = 0;
                                foreach (var item in permissionoption)
                                {
                                    if (item == "工程進度")
                                    {
                                        ischecck = 1;
                                    }
                                }

                                if (ischecck == 0)
                                {
                                    all.seo_description = "0";
                                }
                                else
                                {
                                    all.seo_description = "1";
                                    casecount = 1;
                                }
                            }
                        }

                    }
                }
            }

            ViewBag.casecount = casecount;
            ViewBag.Cases = cases;
            if (cases != null)
            {
                ViewBag.CasesData = cases.Where(m => m.guid == ID).FirstOrDefault();
            }

            var Progress = Repository.Cases.CasesProgress.All(defLang, ID).Where(m => m.title != "");

            Data = Progress.ToPagedList(pageNumeber, pageWidth);
            string PagePath = Url.Content("~/") + defLang + "/Member/Progress/" + ID;
            List<cases_progress> newsTotal = new List<cases_progress>();

            newsTotal = Progress.ToList();
            Data = Progress.ToPagedList(pageNumeber, pageWidth);

            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)newsTotal.Count / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, newsTotal.Count, "");//頁碼

            return View();
        }

        /// <summary>
        /// 工程進度(內頁)
        /// </summary>
        /// <returns></returns>
        public ActionResult ProgressDetail()
        {
            Title = ViewBag.ResLang["工程進度"] + "|";
            ViewBag.SubActive = "Progress";

            HttpCookie cookie_mid = Request.Cookies["mid"];
            if (cookie_mid == null)
            {
                return RedirectToAction("Logout", "Login");
            }
            var decryptUserID = FunctionService.aesDecryptBase64(cookie_mid.Value.ToString(), "minmax");

            if (!string.IsNullOrEmpty(decryptUserID) && _cache["member" + decryptUserID] == null)
            {
                return RedirectPermanent(Url.Content("~/" + defLang + "/Member/Login"));
            }

            ViewBag.Cases = null;

            //會員資料
            List<InfoData> MemberInfoData = ViewBag.LoginMemberPreSale;

            //取得建案資料
            List<cases> cases = Repository.Cases.CasesData.AllHaveProgressByInfoData(defLang, MemberInfoData);

            ViewBag.Cases = cases;
            cases_progress data = Repository.Cases.CasesProgress.Single(defLang, ID);
            ViewBag.CasesData = cases.Where(m => m.guid == data.category).FirstOrDefault();

            #region 上下筆

            var newsList = Repository.Cases.CasesProgress.All(defLang, data.category).Where(m => m.title != "").Select(a => new { guid = a.guid }).ToList();

            ArrayList guidList = new ArrayList();
            foreach (var val in newsList)
            {
                guidList.Add(val.guid);
            }

            int nowIndex = guidList.IndexOf(ID);

            ViewData["prev"] = null;
            ViewData["next"] = null;

            if ((nowIndex - 1) >= 0)
            {
                string tempGuid = guidList[(nowIndex - 1)].ToString();
                ViewData["prev"] = tempGuid;
            }
            if ((nowIndex + 1) < guidList.Count)
            {
                string tempGuid = guidList[(nowIndex + 1)].ToString();
                ViewData["next"] = tempGuid;
            }

            #endregion

            Data = data;

            return View();
        }

        /// <summary>
        /// 繳款紀錄
        /// </summary>
        /// <returns></returns>
        public ActionResult Payment()
        {
            Title = ViewBag.ResLang["繳款紀錄"] + "|";
            ViewBag.SubActive = "Payment";

            HttpCookie cookie_mid = Request.Cookies["mid"];
            if (cookie_mid == null)
            {
                return RedirectToAction("Logout", "Login");
            }
            var decryptUserID = FunctionService.aesDecryptBase64(cookie_mid.Value.ToString(), "minmax");

            var PaymentStatus = TempData["PaymentStatus"];
            if (!string.IsNullOrEmpty(decryptUserID) && (_cache["member" + decryptUserID] == null || (Boolean)PaymentStatus == false))
            {
                return RedirectPermanent(Url.Content("~/" + defLang + "/Member/Login"));
            }

            //取得繳費紀錄CallWebService
            ViewBag.Payments = null;
            ViewBag.PaymentsFirst = null;

            var ApiData_Title = FunctionService.CallWebService_TitleList(_cache["UserID" + decryptUserID].ToString());
            if (ApiData_Title != "Error")
            {
                List<PaymentListModel> title = JsonConvert.DeserializeObject<List<PaymentListModel>>(ApiData_Title);
                title = title.OrderBy(x => x.WFDL01).ThenBy(x => x.WFDL010).ToList();
                ViewBag.PaymentTitle = title;

                int paymentcount = 0;
                if (title != null)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(ID))
                        {
                            var data = ID.Split('§');
                            var data_id = data[0].ToString();
                            var data_name = data[1].ToString();

                            ViewBag.PaymentTitleList = title;
                            var title_data = title.Where(x => x.WFMCU.Contains(data_id) && x.WFDL010.Contains(data_name)).FirstOrDefault();
                            var ApiData = FunctionService.CallWebService_DataList(title_data.WFMCU, title_data.WFDL01, title_data.WFDL010, _cache["UserID" + decryptUserID].ToString());
                            List<PaymentDataModel> Data = JsonConvert.DeserializeObject<List<PaymentDataModel>>(ApiData);

                            foreach (var all in title)
                            {
                                var isPermissionAll = (from a in DB.project_progress
                                                       where a.title.ToUpper() == "ALL" && a.lang == "tw" && a.status == "Y"
                                                       select a).FirstOrDefault();
                                if (isPermissionAll == null)
                                {
                                    var isPermission = (from a in DB.project_progress
                                                        where a.title == all.WFMCU.Trim() && a.lang == "tw" && a.status == "Y"
                                                        select a).FirstOrDefault();
                                    if (isPermission == null)
                                    {
                                        all.checkshow = 0;
                                        title_data.checkshow = 0;
                                    }
                                    else
                                    {
                                        var permissionoption = isPermission.typeoption.Split(',');
                                        int ischecck = 0;
                                        foreach (var item in permissionoption)
                                        {
                                            if (item == "繳款紀錄")
                                            {
                                                ischecck = 1;
                                            }
                                        }

                                        if (ischecck == 0)
                                        {
                                            all.checkshow = 0;
                                            title_data.checkshow = 0;
                                        }
                                        else
                                        {
                                            all.checkshow = 1;
                                            title_data.checkshow = 1;
                                            paymentcount = 1;
                                        }
                                    }
                                }
                                else
                                {
                                    var permissionalloption = isPermissionAll.typeoption.Split(',');
                                    int ischeckall = 0;
                                    foreach (var itemall in permissionalloption)
                                    {
                                        if (itemall == "繳款紀錄")
                                        {
                                            ischeckall = 1;
                                        }
                                    }
                                    if (ischeckall == 0)
                                    {
                                        all.checkshow = 0;
                                        title_data.checkshow = 0;
                                    }
                                    else
                                    {
                                        var isPermission = (from a in DB.project_progress
                                                            where a.title == all.WFMCU.Trim() && a.lang == "tw" && a.status == "Y"
                                                            select a).FirstOrDefault();
                                        if (isPermission == null)
                                        {
                                            all.checkshow = 0;
                                            title_data.checkshow = 0;
                                        }
                                        else
                                        {
                                            var permissionoption = isPermission.typeoption.Split(',');
                                            int ischecck = 0;
                                            foreach (var item in permissionoption)
                                            {
                                                if (item == "繳款紀錄")
                                                {
                                                    ischecck = 1;
                                                }
                                            }

                                            if (ischecck == 0)
                                            {
                                                all.checkshow = 0;
                                                title_data.checkshow = 0;
                                            }
                                            else
                                            {
                                                all.checkshow = 1;
                                                title_data.checkshow = 1;
                                                paymentcount = 1;
                                            }
                                        }
                                    }

                                }
                            }

                            ViewBag.PaymentTitle = title_data;
                            ViewBag.PaymentData = Data;

                        }
                        else
                        {
                            ViewBag.PaymentTitleList = title;
                            var ApiData = FunctionService.CallWebService_DataList(title[0].WFMCU, title[0].WFDL01, title[0].WFDL010, _cache["UserID" + decryptUserID].ToString());
                            List<PaymentDataModel> Data = JsonConvert.DeserializeObject<List<PaymentDataModel>>(ApiData);

                            foreach (var all in title)
                            {
                                var isPermissionAll = (from a in DB.project_progress
                                                       where a.title.ToUpper() == "ALL" && a.lang == "tw" && a.status == "Y"
                                                       select a).FirstOrDefault();
                                if (isPermissionAll == null)
                                {
                                    var isPermission = (from a in DB.project_progress
                                                        where a.title == all.WFMCU.Trim() && a.lang == "tw" && a.status == "Y"
                                                        select a).FirstOrDefault();
                                    if (isPermission == null)
                                    {
                                        all.checkshow = 0;
                                        title[0].checkshow = 0;
                                    }
                                    else
                                    {
                                        var permissionoption = isPermission.typeoption.Split(',');
                                        int ischecck = 0;
                                        foreach (var item in permissionoption)
                                        {
                                            if (item == "繳款紀錄")
                                            {
                                                ischecck = 1;
                                            }
                                        }

                                        if (ischecck == 0)
                                        {
                                            all.checkshow = 0;
                                            title[0].checkshow = 0;
                                        }
                                        else
                                        {
                                            all.checkshow = 1;
                                            title[0].checkshow = 1;
                                            paymentcount = 1;
                                        }
                                    }
                                }
                                else
                                {
                                    var permissionalloption = isPermissionAll.typeoption.Split(',');
                                    int ischeckall = 0;
                                    foreach (var itemall in permissionalloption)
                                    {
                                        if (itemall == "繳款紀錄")
                                        {
                                            ischeckall = 1;
                                        }
                                    }
                                    if (ischeckall == 0)
                                    {
                                        all.checkshow = 0;
                                        title[0].checkshow = 0;
                                    }
                                    else
                                    {
                                        var isPermission = (from a in DB.project_progress
                                                            where a.title == all.WFMCU.Trim() && a.lang == "tw" && a.status == "Y"
                                                            select a).FirstOrDefault();
                                        if (isPermission == null)
                                        {
                                            all.checkshow = 0;
                                            title[0].checkshow = 0;
                                        }
                                        else
                                        {
                                            var permissionoption = isPermission.typeoption.Split(',');
                                            int ischecck = 0;
                                            foreach (var item in permissionoption)
                                            {
                                                if (item == "繳款紀錄")
                                                {
                                                    ischecck = 1;
                                                }
                                            }

                                            if (ischecck == 0)
                                            {
                                                all.checkshow = 0;
                                                title[0].checkshow = 0;
                                            }
                                            else
                                            {
                                                all.checkshow = 1;
                                                title[0].checkshow = 1;
                                                paymentcount = 1;
                                            }
                                        }
                                    }

                                }
                              
                            }

                            ViewBag.PaymentTitle = title[0];
                            ViewBag.PaymentData = Data;

                            ViewBag.paymentcount = paymentcount;
                        }
                    }
                    catch (Exception ex)
                    {
                        return RedirectToAction("Progress");
                    }
                }
                return View();
            }
            else
            {
                return RedirectToAction("Progress");
            }
        }

        /// <summary>
        /// 常見問題
        /// </summary>
        /// <returns></returns>
        public ActionResult Faq(int? page)
        {
            Title = ViewBag.ResLang["常見問題"] + "|";
            ViewBag.SubActive = "Faq";

            HttpCookie cookie_mid = Request.Cookies["mid"];
            if (cookie_mid == null)
            {
                return RedirectToAction("Logout", "Login");
            }
            var decryptUserID = FunctionService.aesDecryptBase64(cookie_mid.Value.ToString(), "minmax");

            if (!string.IsNullOrEmpty(decryptUserID) && _cache["member" + decryptUserID] == null)
            {
                return RedirectPermanent(Url.Content("~/" + defLang + "/Member/Login"));
            }

            Data = Repository.Member.MemberFaq.All(defLang);
            int pageWidth = 10;//預設每頁長度
            var pageNumeber = page ?? 1;

            var faq = Repository.Member.MemberFaq.All(defLang).Where(m => m.title != "");

            Data = faq.ToPagedList(pageNumeber, pageWidth);
            string PagePath = Url.Content("~/") + defLang + "/Member/Faq";
            List<member_faq> newsTotal = new List<member_faq>();

            newsTotal = faq.ToList();
            Data = faq.ToPagedList(pageNumeber, pageWidth);

            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)newsTotal.Count / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, newsTotal.Count, "");//頁碼

            return View();
        }

        /// <summary>
        /// 修改密碼
        /// </summary>
        /// <returns></returns>
        public ActionResult Password()
        {
            Title = ViewBag.ResLang["修改密碼"] + "|";
            ViewBag.SubActive = "Password";

            HttpCookie cookie_mid = Request.Cookies["mid"];
            if (cookie_mid == null)
            {
                return RedirectToAction("Logout", "Login");
            }
            var decryptUserID = FunctionService.aesDecryptBase64(cookie_mid.Value.ToString(), "minmax");

            if (!string.IsNullOrEmpty(decryptUserID) && _cache["member" + decryptUserID] == null)
            {
                return RedirectPermanent(Url.Content("~/" + defLang + "/Member/Login"));
            }

            return View();
        }

        /// <summary>
        /// 修改密碼
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void ChangePassword(FormCollection form)
        {
            string apiUrl = ApiPath + "/api/CreMember/Password";
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("OldPassword", form["OldPassword"].ToString());
            data.Add("NewPassword", form["NewPassword"].ToString());
            data.Add("jwtToken", logins.result.jwtToken);

            string response = FunctionService.CallApi(apiUrl, "Password", data);

            ServiceModels.Password passwords = JsonConvert.DeserializeObject<ServiceModels.Password>(response);

            Dictionary<String, Object> alertData = new Dictionary<string, object>();
            alertData.Add("title", passwords.message.ToString());
            alertData.Add("text", "");

            if (passwords.isSuccess == "false")
            {
                alertData.Add("type", "error");
            }
            else
            {
                alertData.Add("type", "success");
            }
            alertData.Add("type", "error");

            alertData.Add("url", Url.Content("~/" + defLang + "/Member/Password"));
            Session.Remove("alertData");
            Session.Add("alertData", alertData);
            Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
        }

        /// <summary>
        /// 修改密碼(結束)
        /// </summary>
        /// <returns></returns>
        public ActionResult PasswordSubmit()
        {
            Title = ViewBag.ResLang["修改密碼"] + "|";
            ViewBag.SubActive = "Password";

            HttpCookie cookie_mid = Request.Cookies["mid"];
            if (cookie_mid == null)
            {
                return RedirectToAction("Logout", "Login");
            }
            var decryptUserID = FunctionService.aesDecryptBase64(cookie_mid.Value.ToString(), "minmax");

            if (!string.IsNullOrEmpty(decryptUserID) && _cache["member" + decryptUserID] == null)
            {
                return RedirectPermanent(Url.Content("~/" + defLang + "/Member/Login"));
            }

            return View();
        }

        /// <summary>
        /// 忘記密碼
        /// </summary>
        /// <returns></returns>
        public ActionResult Forget()
        {
            Title = ViewBag.ResLang["忘記密碼"] + "|";
            return View();
        }

        /// <summary>
        /// 送出忘記密碼
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void ForgetAction(FormCollection form)
        {
            string apiUrl = ApiPath + "/api/CreMember/NewPassword";
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("UserId", form["UserId"].ToString());
            string response = FunctionService.CallApi(apiUrl, "NewPassword", data);

            ServiceModels.Password passwords = JsonConvert.DeserializeObject<ServiceModels.Password>(response);

            if (passwords.isSuccess == "false")
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", passwords.message.ToString());
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang + "/Member/Forget"));
                Session.Remove("alertData");
                Session.Add("alertData", alertData);
                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
            }
            else
            {
                Response.Redirect(Url.Content("~/" + defLang + "/Member/ForgetSubmit"));
            }
        }

        /// <summary>
        /// 忘記密碼(結束)
        /// </summary>
        /// <returns></returns>
        public ActionResult ForgetSubmit()
        {
            Title = ViewBag.ResLang["忘記密碼"] + "|";
            return View();
        }

        //[HttpPost]
        //public string IsAuthenticated(string token)
        //{
        //    var result = "";
        //    //if (Session["member"] == null)
        //    //{
        //    //    UserData u = new UserData()
        //    //    {
        //    //        user_id = "",
        //    //        message = "官網已登出",
        //    //        auth = false
        //    //    };
        //    //    result = JsonConvert.SerializeObject(u);
        //    //}
        //    //else
        //    //{
        //    //    TokenManager _AuthManger = new TokenManager();
        //    //    var user = _AuthManger.GetUserIfLogin(token);
        //    //    result = JsonConvert.SerializeObject(user);
        //    //}

        //    TokenManager _AuthManger = new TokenManager();
        //    var user = _AuthManger.GetUserIfLogin(token);
        //    result = JsonConvert.SerializeObject(user);

        //    return result;
        //}

        //[HttpPost]
        //public string Refresh(string token)
        //{
        //    if (!string.IsNullOrEmpty(token))
        //    {
        //        TokenManager _AuthManger = new TokenManager();
        //        var result = _AuthManger.Refresh(token);
        //        if (result == null)
        //        {
        //            return "Refresh_Error";
        //        }
        //        else
        //        {
        //            return result.refresh_token;
        //        }
        //    }

        //    return "Token_Error";
        //}
    }
}