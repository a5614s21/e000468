﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Service;
using PagedList;
using System.Collections;

namespace Web.Controllers
{
    public class AboutController : BaseController
    {
        public System.DateTime todayFirst = System.DateTime.Parse(System.DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00");
        public System.DateTime todayLast = System.DateTime.Parse(System.DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59");

        /// <summary>
        /// 最新消息
        /// </summary>
        /// <returns></returns>
        public ActionResult News(int? page)
        {
            Title = ViewBag.ResLang["最新消息"] + "|";

            ViewBag.CategoryData = null;
            int pageWidth = 8;//預設每頁長度
            var pageNumeber = page ?? 1;
            string PagePath = Url.Content("~/") + defLang + "/About/News";

            //最新消息分類
            List<news_category> Category = DB.news_category.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
            ViewBag.Category = Category;

            var CategoryGuid = Category.Select(x => x.guid).ToList();
            var news = DB.news.OrderByDescending(m => m.startdate).Where(m => m.verify == "Y")
                                .Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "")
                                .Where(m => m.startdate <= todayFirst)
                                .Where(m => m.enddate >= todayLast || m.enddate.ToString() == null || m.enddate.ToString() == "")
                                .Where(m => CategoryGuid.Contains(m.category));

            List<news> newsTotal = new List<news>();
            if (!string.IsNullOrEmpty(ID))
            {
                ViewBag.CategoryData = Category.Where(m => m.guid == ID).FirstOrDefault();
                Title = ViewBag.CategoryData.title + "|" + ViewBag.ResLang["最新消息"] + "|";
                news = news.Where(m => m.category == ID);
                Data = news.Where(m => m.category == ID).ToPagedList(pageNumeber, pageWidth);
                PagePath = Url.Content("~/") + defLang + "/About/News/" + ID;
            }

            newsTotal = news.ToList();
            Data = news.ToPagedList(pageNumeber, pageWidth);

            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)newsTotal.Count / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, newsTotal.Count, "");//頁碼

            return View();
        }

        /// <summary>
        /// 最新消息內頁
        /// </summary>
        /// <returns></returns>
        public ActionResult NewsDetail()
        {
            ViewBag.Category = null;
            ViewBag.CategoryData = null;
            if (!string.IsNullOrEmpty(ID))
            {
                //最新消息分類
                List<news_category> Category = DB.news_category.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
                ViewBag.Category = Category;

                news item = DB.news.Where(m => m.guid == ID).Where(m => m.lang == defLang).FirstOrDefault();
                if (item != null)
                {
                    Data = item;
                    Title = item.title + "|" + ViewBag.ResLang["最新消息"] + "|";

                    ViewBag.CategoryData = Category.Where(m => m.guid == item.category).FirstOrDefault();

                    #region 上下筆

                    var newsList = DB.news.OrderByDescending(m => m.startdate)
                               .Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "")
                               .Where(m => m.category == item.category)
                               .Where(m => m.startdate <= todayFirst)
                               .Where(m => m.enddate >= todayLast || m.enddate.ToString() == null || m.enddate.ToString() == "")
                               .Select(a => new { guid = a.guid }).ToList();

                    ArrayList guidList = new ArrayList();
                    foreach (var val in newsList)
                    {
                        guidList.Add(val.guid);
                    }

                    int nowIndex = guidList.IndexOf(ID);

                    ViewData["prev"] = null;
                    ViewData["next"] = null;

                    if ((nowIndex - 1) >= 0)
                    {
                        string tempGuid = guidList[(nowIndex - 1)].ToString();
                        ViewData["prev"] = tempGuid;
                    }
                    if ((nowIndex + 1) < guidList.Count)
                    {
                        string tempGuid = guidList[(nowIndex + 1)].ToString();
                        ViewData["next"] = tempGuid;
                    }

                    #endregion
                }

                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 公司簡介
        /// </summary>
        /// <returns></returns>
        public ActionResult Info()
        {
            Title = ViewBag.ResLang["公司簡介"] + "|";

            ViewBag.CasesList = null;
            ViewBag.Classic = null;
            ViewBag.CasesCategory = null;
            try
            {
                List<cases> Cases = ViewBag.Cases;

                //經典建案
                //Cases = Cases.Where(m => m.classic != "0").OrderBy(x=> x.sortIndex).ToList();
                //ViewBag.CasesList = Cases;
                //List<cases_classic> cases_classic = DB.cases_classic.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
                //ViewBag.Classic = cases_classic;

                //熱銷建案
                Cases = Cases.Where(m => m.category != "0").OrderBy(x => x.sortIndex).ToList();
                ViewBag.CasesList = Cases;
                List<cases_category> cases_category = DB.cases_category.Where(x => x.lang == defLang && x.status == "Y").OrderBy(x => x.sortIndex).ToList();
                ViewBag.CasesCategory = cases_category;

                Data = DB.abouts.Where(m => m.lang == defLang).Where(m => m.guid == "1").FirstOrDefault();

                return View();
            }
            catch
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 公司組織
        /// </summary>
        /// <returns></returns>
        public ActionResult Organization()
        {
            Title = ViewBag.ResLang["公司組織"] + "|";

            Data = DB.abouts.Where(m => m.lang == defLang).Where(m => m.guid == "2").FirstOrDefault();

            return View();
        }

        /// <summary>
        /// 經典建案
        /// </summary>
        /// <returns></returns>
        public ActionResult Cases(int? page)
        {
            Title = ViewBag.ResLang["經典建案"] + "|";
            ViewBag.Classic = null;

            try
            {
                //取的分類
                List<cases_classic> cases_classic = DB.cases_classic.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
                ViewBag.Classic = cases_classic;
                cases_classic cases_classic_data = new cases_classic();
                List<cases> Cases = ViewBag.Cases;
                //預設鍵值
                if (string.IsNullOrEmpty(ID))
                {
                    // cases_classic_data = cases_classic.FirstOrDefault();
                    // ID = cases_classic_data.guid;
                    Cases = Cases.Where(x => x.classic != "0").OrderBy(x => x.sortIndex).ToList();
                }
                //傳入鍵值
                else
                {
                    cases_classic_data = cases_classic.Where(m => m.guid == ID).FirstOrDefault();
                    Cases = Cases.Where(m => m.classic == cases_classic_data.guid).OrderBy(x => x.sortIndex).ToList();
                }
                Data = cases_classic_data;

                Title = ViewBag.ResLang["經典建案"] + "|";

                if (!string.IsNullOrEmpty(cases_classic_data.title))
                {
                    Title = cases_classic_data.title + "|" + ViewBag.ResLang["經典建案"] + "|";
                }

                int pageWidth = 10;//預設每頁長度
                var pageNumeber = page ?? 1;

                ViewBag.CasesUsePage = Cases.ToPagedList(pageNumeber, pageWidth);

                string PagePath = Url.Content("~/") + defLang + "/About/Cases/" + cases_classic_data.guid;
                int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Cases.Count / pageWidth));//總頁數
                ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, Cases.Count, "");//頁碼

                return View();
            }
            catch
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 熱銷建案(內頁)
        /// </summary>
        /// <returns></returns>
        public ActionResult CasesDetail()
        {
            ViewBag.CategoryData = null;
            ViewBag.Form = null;
            ViewBag.casesClassic = null;
            if (!string.IsNullOrEmpty(ID))
            {
                List<cases_classic> cases_classic = DB.cases_classic.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
                ViewBag.casesClassic = cases_classic;
                List<cases> temp = ViewBag.Cases;
                cases item = temp.Where(m => m.guid == ID).FirstOrDefault();
                if (item != null)
                {
                    Data = item;
                    Title = item.title + "|" + ViewBag.ResLang["經典建案"] + " |";

                    ViewBag.CategoryData = cases_classic.Where(m => m.guid == item.classic).FirstOrDefault();
                }

                if (Session["CasesContact"] != null)
                {
                    ViewBag.Form = Session["CasesContact"];
                }

                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 經營理念
        /// </summary>
        /// <returns></returns>
        public ActionResult Philosophy()
        {
            Title = ViewBag.ResLang["經營理念"] + "|";

            Data = DB.abouts.Where(m => m.lang == defLang).Where(m => m.guid == "3").FirstOrDefault();

            return View();
        }

        /// <summary>
        /// 營運概況
        /// </summary>
        /// <returns></returns>
        public ActionResult Operation()
        {
            Title = ViewBag.ResLang["營運概況"] + "|";

            ViewBag.CasesList = null;
            ViewBag.Classic = null;
            ViewBag.CasesCategory = null;
            try
            {
                List<cases> Cases = ViewBag.Cases;

                //經典建案
                //Cases = Cases.Where(m => m.classic != "0").OrderBy(x => x.sortIndex).ToList();
                //ViewBag.CasesList = Cases;
                //List<cases_classic> cases_classic = DB.cases_classic.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
                //ViewBag.Classic = cases_classic;

                //熱銷建案
                Cases = Cases.Where(m => m.category != "0").OrderBy(x => x.sortIndex).ToList();
                ViewBag.CasesList = Cases;
                List<cases_category> cases_category = DB.cases_category.Where(x => x.lang == defLang && x.status == "Y").OrderBy(x => x.sortIndex).ToList();
                ViewBag.CasesCategory = cases_category;

                Data = DB.abouts.Where(m => m.lang == defLang).Where(m => m.guid == "4").FirstOrDefault();

                return View();
            }
            catch
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 投資事業
        /// </summary>
        /// <returns></returns>
        public ActionResult Investment()
        {
            List<about_investment> investments = Repository.About.Investment.All(defLang);
            ViewBag.investments = investments;
            about_investment about_investment = new about_investment();

            if (!string.IsNullOrEmpty(ID))
            {
                about_investment = investments.Where(m => m.guid == ID).FirstOrDefault();
            }
            else
            {
                about_investment = investments.FirstOrDefault();
            }

            Data = about_investment;
            Title = about_investment.title + "|" + ViewBag.ResLang["投資事業"] + "|";
            Description = about_investment.seo_description;
            Keywords = about_investment.seo_keywords;

            return View();
        }

        /// <summary>
        /// 得獎紀事
        /// </summary>
        /// <returns></returns>
        public ActionResult Awards()
        {
            Title = ViewBag.ResLang["得獎紀事"] + "|";
            List<awards> awards = DB.awards.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderByDescending(m => m.year).ThenBy(m => m.sortIndex).ToList();

            List<string> yearGroup = new List<string>();
            foreach (awards item in awards)
            {
                if (yearGroup.IndexOf(item.year) == -1)
                {
                    yearGroup.Add(item.year);
                }
            }
            ViewBag.yearGroup = yearGroup;
            Data = awards;

            return View();
        }

        /// <summary>
        /// 房地產指數
        /// </summary>
        /// <returns></returns>
        public ActionResult House(int? page)
        {
            Title = ViewBag.ResLang["房地產指數"] + "|";

            int pageWidth = 16;//預設每頁長度
            var pageNumeber = page ?? 1;

            Session["ishouse"] = "ishouse";
            List<about_houses> item = DB.about_houses.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.verify == "Y").OrderByDescending(m => m.title).ToList();
            //List<about_houses> item = DB.about_houses.Where(m => m.lang == "tw").Where(m => m.status == "Y").Where(m => m.verify == "Y").OrderByDescending(m => m.title).ToList();
            ViewBag.AboutHouses = item.ToPagedList(pageNumeber, pageWidth);
            string PagePath = Url.Content("~/") + defLang + "/About/House";
            //string PagePath = Url.Content("~/") + "tw/About/House";
            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)item.Count / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, item.Count, "");//頁碼
            ViewBag.Abouts = DB.abouts.Where(x => x.lang == defLang && x.status == "Y" && x.guid == "7").FirstOrDefault();
            //ViewBag.Abouts = DB.abouts.Where(x => x.lang == "tw" && x.status == "Y" && x.guid == "7").FirstOrDefault();
            ViewBag.ThreeData = item.Take(3).ToList();

            return View();
        }

        /// <summary>
        /// 公司治理 - 董事會
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageDirectors()
        {
            Title = ViewBag.ResLang["董事會介紹"] + "|" + ViewBag.ResLang["公司治理"] + "|";
            ViewBag.SubActive = "ManageDirectors";
            Data = DB.about_manages.Where(m => m.lang == defLang).Where(m => m.guid == "1").FirstOrDefault();

            return View();
        }

        /// <summary>
        /// 資訊安全
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageSafety()
        {
            Title = ViewBag.ResLang["資訊安全管理"] + "|" + ViewBag.ResLang["公司治理"] + "|";
            ViewBag.SubActive = "ManageSafety";
            Data = DB.manage_safety.Where(m => m.lang == defLang).Where(m => m.guid == "1").FirstOrDefault();

            return View();
        }

        /// <summary>
        /// 功能性委員會決議事項
        /// </summary>
        /// <returns></returns>
        public ActionResult AuditCommittee(int? page)
        {
            Title = ViewBag.ResLang["功能性委員會決議事項"] + "|" + ViewBag.ResLang["公司治理"] + "|";
            ViewBag.SubActive = "AuditCommittee";

            int pageWidth = 10;//預設每頁長度
            var pageNumeber = page ?? 1;

            List<auditcommittee> AuditCommittee = DB.auditcommittee.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
            ViewBag.AuditCommitteeUsePage = AuditCommittee.ToPagedList(pageNumeber, pageWidth);
            string PagePath = Url.Content("~/") + defLang + "/About/AuditCommittee";
            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)AuditCommittee.Count / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, AuditCommittee.Count, "");//頁碼

            return View();
        }

        /// <summary>
        /// 薪酬委員會
        /// </summary>
        /// <returns></returns>
        public ActionResult Remuneration(int? page)
        {
            Title = ViewBag.ResLang["功能性委員會決議事項"] + "|" + ViewBag.ResLang["公司治理"] + "|";
            ViewBag.SubActive = "Remuneration";

            int pageWidth = 10;//預設每頁長度
            var pageNumeber = page ?? 1;

            List<remuneration> Remuneration = DB.remuneration.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
            ViewBag.RemunerationUsePage = Remuneration.ToPagedList(pageNumeber, pageWidth);
            string PagePath = Url.Content("~/") + defLang + "/About/Remuneration";
            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Remuneration.Count / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, Remuneration.Count, "");//頁碼

            return View();
        }

        /// <summary>
        /// 公司治理 - 重要公司內規
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageRegulation()
        {
            Title = ViewBag.ResLang["重要公司內規"] + "|" + ViewBag.ResLang["公司治理"] + "|";
            ViewBag.SubActive = "ManageRegulation";

            Data = DB.regulations.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();

            return View();
        }

        /// <summary>
        /// 公司治理 - 獨立董事選任資訊
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageElect()
        {
            Title = ViewBag.ResLang["獨立董事選任資訊"] + "|" + ViewBag.ResLang["公司治理"] + "|";
            ViewBag.SubActive = "ManageElect";

            return View();
        }

        /// <summary>
        /// 公司治理 - 董事會決議事項
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageResolution(int? page)
        {
            Title = ViewBag.ResLang["董事會決議事項"] + "|" + ViewBag.ResLang["公司治理"] + "|";
            ViewBag.SubActive = "ManageResolution";

            int pageWidth = 10;//預設每頁長度
            var pageNumeber = page ?? 1;

            List<resolutions> Resolutions = DB.resolutions.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
            ViewBag.ResolutionsUsePage = Resolutions.ToPagedList(pageNumeber, pageWidth);
            string PagePath = Url.Content("~/") + defLang + "/About/ManageResolution";
            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Resolutions.Count / pageWidth));//總頁數
            ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, Resolutions.Count, "");//頁碼

            return View();
        }

        /// <summary>
        /// 公司治理 - 委員會
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageCommittee()
        {
            Title = ViewBag.ResLang["委員會"] + "|" + ViewBag.ResLang["公司治理"] + "|";
            ViewBag.SubActive = "ManageCommittee";

            Data = DB.about_manages.Where(m => m.lang == defLang).Where(m => m.guid == "2").FirstOrDefault();

            return View();
        }

        /// <summary>
        /// 公司治理 - 獨立董事與內部稽核主管及會計師之溝通情形
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageCommunication()
        {
            Title = ViewBag.ResLang["獨立董事與內部稽核主管及會計師之溝通情形"] + "|" + ViewBag.ResLang["公司治理"] + "|";
            ViewBag.SubActive = "ManageCommunication";
            Data = DB.about_manages.Where(m => m.lang == defLang).Where(m => m.guid == "3").FirstOrDefault();
            return View();
        }

        /// <summary>
        /// 其他資訊
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageOthers()
        {
            Title = ViewBag.ResLang["其他資訊"] + "|" + ViewBag.ResLang["公司治理"] + "|";
            ViewBag.SubActive = "ManageOthers";

            Data = DB.about_manages.Where(m => m.lang == defLang).Where(m => m.guid == "4").FirstOrDefault();

            return View();
        }

        /// <summary>
        /// 內部稽核
        /// </summary>
        /// <returns></returns>
        public ActionResult Audit()
        {
            Title = ViewBag.ResLang["內部稽核"] + "|";
            Data = DB.abouts.Where(m => m.lang == defLang).Where(m => m.guid == "5").FirstOrDefault();

            return View();
        }

        /// <summary>
        /// 公益活動
        /// </summary>
        /// <returns></returns>
        //public ActionResult Charity(int? page)
        //{
        //    Title = ViewBag.ResLang["公益活動"] + "|";

        //    ViewBag.CategoryData = null;
        //    int pageWidth = 8;//預設每頁長度
        //    var pageNumeber = page ?? 1;

        //    //公益活動分類
        //    List<charity_category> Category = DB.charity_category.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
        //    ViewBag.Category = Category;

        //    var charitys = DB.charitys.OrderByDescending(m => m.startdate)
        //                        .Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "")
        //                        .Where(m => m.startdate <= todayFirst)
        //                        .Where(m => m.enddate >= todayLast || m.enddate.ToString() == null || m.enddate.ToString() == "");

        //    List<charitys> newsTotal = new List<charitys>();
        //    if (string.IsNullOrEmpty(ID))
        //    {
        //        charity_category temp = Category.OrderBy(m => m.sortIndex).FirstOrDefault();
        //        ID = temp.guid;
        //    }

        //    ViewBag.CategoryData = Category.Where(m => m.guid == ID).FirstOrDefault();
        //    Title = ViewBag.CategoryData.title + "|" + ViewBag.ResLang["公益活動"] + "|";
        //    charitys = charitys.Where(m => m.category == ID);
        //    Data = charitys.Where(m => m.category == ID).ToPagedList(pageNumeber, pageWidth);
        //    string PagePath = Url.Content("~/") + defLang + "/About/Charity/" + ID;

        //    newsTotal = charitys.ToList();
        //    Data = charitys.ToPagedList(pageNumeber, pageWidth);

        //    int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)newsTotal.Count / pageWidth));//總頁數
        //    ViewBag.pageView = FunctionService.getPageNum(PagePath, Allpage, pageNumeber, newsTotal.Count, "");//頁碼
        //    return View();
        //}

        /// <summary>
        /// 公益活動內容
        /// </summary>
        /// <returns></returns>
        public ActionResult CharityDetail()
        {
            ViewBag.Category = null;
            ViewBag.CategoryData = null;
            if (!string.IsNullOrEmpty(ID))
            {
                //公益活動分類
                List<charity_category> Category = DB.charity_category.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
                ViewBag.Category = Category;

                charitys item = DB.charitys.Where(m => m.guid == ID).Where(m => m.lang == defLang).FirstOrDefault();
                if (item != null)
                {
                    Data = item;
                    Title = item.title + "|" + ViewBag.ResLang["公益活動"] + "|";

                    ViewBag.CategoryData = Category.Where(m => m.guid == item.category).FirstOrDefault();

                    #region 上下筆

                    var charitysList = DB.charitys.OrderByDescending(m => m.startdate)
                               .Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "")
                               .Where(m => m.category == item.category)
                               .Where(m => m.startdate <= todayFirst)
                               .Where(m => m.enddate >= todayLast || m.enddate.ToString() == null || m.enddate.ToString() == "")
                               .Select(a => new { guid = a.guid }).ToList();

                    ArrayList guidList = new ArrayList();
                    foreach (var val in charitysList)
                    {
                        guidList.Add(val.guid);
                    }

                    int nowIndex = guidList.IndexOf(ID);

                    ViewData["prev"] = null;
                    ViewData["next"] = null;

                    if ((nowIndex - 1) >= 0)
                    {
                        string tempGuid = guidList[(nowIndex - 1)].ToString();
                        ViewData["prev"] = tempGuid;
                    }
                    if ((nowIndex + 1) < guidList.Count)
                    {
                        string tempGuid = guidList[(nowIndex + 1)].ToString();
                        ViewData["next"] = tempGuid;
                    }

                    #endregion
                }

                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 得獎紀事
        /// </summary>
        /// <returns></returns>
        public ActionResult Linyuan()
        {
            Title = ViewBag.ResLang["霖園集團"] + "|";
            Data = DB.abouts.Where(m => m.lang == defLang).Where(m => m.guid == "6").FirstOrDefault();

            return View();
        }
    }
}