﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Repository.Investor
{
    public class FinanceReport : Controller
    {
        // GET: message

        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom(dynamic types)
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '說明','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("year", "[{'subject': '年度','type': 'year','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'urban_column_category'}]");

            if (types.GetType().ToString() == "System.String" && types == "1")
            {
                main.Add("month", "[{'subject': '月份','type': 'month','default': '','class': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            }
            if (types.GetType().ToString() == "System.String" && types == "2")
            {
                main.Add("month", "[{'subject': '季別','type': 'season','default': '','class': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            }

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            media.Add("files", "[{'subject': '相關檔案','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\"></small>','filetype': 'application/pdf','multiple': 'N','useLang':'Y'}]");

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'Y'}]");
            other.Add("sortIndex", "[{'subject': '標題','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");
            other.Add("verify", "[{'subject': '審核狀態','type': 'verify','defaultVal': 'E','classVal': 'col-lg-10','required': '','notes': '','data':'尚未審核/通過/退件','Val':'E/Y/N','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("info", "內容摘要");
            re.Add("year", "年度");
            re.Add("month", "季別");
            //re.Add("create_date", "發佈日期");
            //re.Add("sortIndex", "排序");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "year");
            re.Add("orderByType", "desc");

            return re;
        }

        /// <summary>
        /// 全部
        /// </summary>
        /// <param name="Lang"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public static List<finance_report> All(string Lang, string types)
        {
            Model DB = new Model();
            return DB.finance_report.Where(m => m.status == "Y").Where(m => m.lang == Lang).Where(m => m.types == types).Where(m => m.verify == "Y").OrderByDescending(m => m.year).ThenBy(m => m.month).ToList();
        }

        /// <summary>
        /// 單筆
        /// </summary>
        /// <param name="Lang"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static finance_report Single(string Lang, string guid)
        {
            Model DB = new Model();
            finance_report data = DB.finance_report.Where(m => m.status == "Y").Where(m => m.lang == Lang).Where(m => m.guid == guid).FirstOrDefault();
            return data;
        }

        public static Dictionary<string, string> ChangeTitle(string id)
        {
            Dictionary<string, string> re = new Dictionary<string, string>();
            re.Add("id", id);
            switch (id)
            {
                case "1":
                    re.Add("title", "每月財務報表");
                    break;

                case "2":
                    re.Add("title", "每月營運報告");
                    break;

                case "3":
                    re.Add("title", "公司年報 ");
                    break;
            }
            return re;
        }
    }
}