﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Repository.Customer
{
    public class Contact : Controller
    {
        // GET: message

        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "N";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定
            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("name", "[{'subject': '連絡人','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            main.Add("tel", "[{'subject': '連絡電話','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            main.Add("email", "[{'subject': 'E-Mail','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            main.Add("company", "[{'subject': '公司名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
         
            main.Add("subject", "[{'subject': '主題','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");

            main.Add("content", "[{'subject': '內容','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'N'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();


            #endregion

            #region 進階
            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("status", "[{'subject': '處理狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'已處理/未處理','Val':'Y/N','useLang':'N'}]");

            other.Add("re_content", "[{'subject': '處理敘述','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'N'}]");
            
            other.Add("re_mail", "[{'subject': '是否回信','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">是否將處理敘述回信給詢問者，若詢問者無輸入Email則不會寄發</small>','data':'是/否','Val':'Y/N','useLang':'N'}]");


            #endregion

            #region 隱藏欄位
            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("def_re_mail", "[{'subject': '是否回信','type': 'hidden','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\"></small>','data':'是/否','Val':'Y/N','useLang':'N'}]");



            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;

        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("name", "姓名");
            re.Add("tel", "連絡電話");  
            re.Add("subject", "主題");
            re.Add("create_date", "諮詢日期");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }



        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "create_date");
            re.Add("orderByType", "desc");

            return re;
        }

        /// <summary>
        /// 全部
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        public static List<contacts> All(string Lang)
        {
            Model DB = new Model();
            return DB.contacts.OrderByDescending(m=>m.create_date).ToList();
        }

        /// <summary>
        /// 單筆
        /// </summary>
        /// <param name="Lang"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static contacts Single(string Lang, string guid)
        {
            Model DB = new Model();
            contacts data = DB.contacts.Where(m => m.status == "Y").Where(m => m.guid == guid).FirstOrDefault();
            return data;
        }

    }
}