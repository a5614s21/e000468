﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using Web.Models;
using Web.Service;

namespace Web.Repository.System
{
    public class MemberLog
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "N";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            /* main.Add("title", "[{'subject': '名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
             main.Add("ip", "[{'subject': 'IP位置','type': 'text','default': '','class': 'col-lg-10','required': 'required','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">EX：xxx.xxx.xxx.xxx </small>',}]");
             main.Add("notes", "[{'subject': '其他敘述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '',}]");
             */

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            //other.Add("forum_status", "[{'subject': '問題回覆檢視','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '','data':'全部顯示/僅分派之回覆','Val':'Y/N'}]");
            //other.Add("get_ask_mail", "[{'subject': '接收提問通知信件','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '','data':'接收/不接收','Val':'Y/N'}]");
            // other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/封鎖','Val':'Y/N'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;
        }

        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("title", "標題");
            re.Add("username", "登入帳號");
            re.Add("ip", "IP位置");
            re.Add("create_date", "登入日期");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "create_date");
            re.Add("orderByType", "desc");

            return re;
        }
    }

    public class MemberLogService
    {
        private Model _context = new Model();

        public void InsertLog(string username, string ip, string status, string note)
        {
            var title = "";
            switch (note)
            {
                //登入成功
                case "100":
                case "101":
                    title = "登入成功";
                    break;
                //登入失敗(預售帳號表，密碼錯誤)
                case "001":
                    title = "登入失敗，預售帳號表，密碼錯誤";
                    break;
                //登入失敗(ERP 預設密碼輸入錯誤)
                case "002":
                    title = "登入失敗，ERP預設密碼輸入錯誤";
                    break;
                //登入失敗(無預設屋會員資料)
                case "003":
                    title = "登入失敗，無預設屋會員資料";
                    break;
                //登入失敗(驗證碼錯誤)
                case "ver":
                    title = "登入失敗，驗證碼錯誤";
                    break;
                //登入失敗(API回傳驗證失敗)
                case "api":
                    title = "登入失敗，API回傳驗證失敗";
                    break;
            }

            member_log ml = new member_log();
            ml.guid = Guid.NewGuid().ToString();
            ml.title = title;
            ml.ip = ip;
            ml.status = status;
            ml.modifydate = DateTime.Now;
            ml.create_date = DateTime.Now;
            ml.username = FunctionService.aesEncryptBase64(username, "minmax");

            _context.Entry(ml).State = EntityState.Added;
            _context.SaveChanges();
        }
    }
}