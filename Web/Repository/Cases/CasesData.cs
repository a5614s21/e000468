﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.ServiceModels;

namespace Web.Repository.Cases
{
    public class CasesData : Controller
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '建案名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("hmmcu", "[{'subject': '建案(工地)代號','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">對應API取得資訊用，請正確輸入!</small>','useLang':'Y'}]");

            main.Add("sub_title", "[{'subject': '建案標語','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("year", "[{'subject': '西元年','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            main.Add("floor", "[{'subject': '樓層','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("address", "[{'subject': '建案地址','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("map_link", "[{'subject': 'Google Map','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            main.Add("pings", "[{'subject': '規劃坪數','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("phone", "[{'subject': '建案電話','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");

            main.Add("url", "[{'subject': '建案連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'N'}]");

            main.Add("category", "[{'subject': '熱銷建案分類','type': 'select2','default': '','class': 'col-lg-10','required': 'required','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果不需要在熱銷建案顯示該筆建案，請選擇\"無\"</small>','inherit':'cases_category','useLang':'N'}]");

            main.Add("classic", "[{'subject': '經典建案分類','type': 'select2','default': '','class': 'col-lg-10','required': 'required','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果不需要在經典建案顯示該筆建案，請選擇\"無\"</small>','inherit':'cases_classic','useLang':'N'}]");

            main.Add("content", "[{'subject': '內容介紹','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            media.Add("index_pic", "[{'subject': '首頁圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">640 x 640 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            media.Add("list_pic", "[{'subject': '列表圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">330 x 440 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            media.Add("header_pic", "[{'subject': '選單用圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">240 x 160 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");

            media.Add("pic", "[{'subject': '內頁圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">970 x 645 (px)，可複選</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'Y'}]");

            media.Add("files", "[{'subject': '型錄檔案','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\"></small>','filetype': 'application/pdf','multiple': 'N','useLang':'Y'}]");

            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("get_mail", "[{'subject': '預約賞屋收件信箱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\"></small>','useLang':'N'}]");

            other.Add("start_date", "[{'subject': '實際開工日期','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\"></small>','useLang':'N'}]");
            other.Add("end_date", "[{'subject': '預計完工日期','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\"></small>','useLang':'N'}]");
            // other.Add("sticky", "[{'subject': '置頂顯示','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">啟用此選項文章將被優先排序到最前面</small>','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("seo_description", "[{'subject': 'SEO敘述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            other.Add("seo_keywords", "[{'subject': 'SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            other.Add("tops", "[{'subject': '首頁展示','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("header", "[{'subject': '選單展示','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");

            other.Add("reservation", "[{'subject': '預約鑑賞','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");

            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'Y'}]");

            //  other.Add("verify", "[{'subject': '審核狀態','type': 'verify','defaultVal': 'E','classVal': 'col-lg-10','required': '','notes': '','data':'尚未審核/通過/退件','Val':'E/Y/N','useLang':'N'}]");
            other.Add("sortIndex", "[{'subject': '標題','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");
            other.Add("verify", "[{'subject': '審核狀態','type': 'verify','defaultVal': 'E','classVal': 'col-lg-10','required': '','notes': '','data':'尚未審核/通過/退件','Val':'E/Y/N','useLang':'N'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;
        }

        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("list_pic", "圖片");
            re.Add("info", "建案摘要");
            re.Add("category", "熱銷建案所屬地區");
            re.Add("classic", "經典建案所屬地區");
            re.Add("button", "工程進度");
            re.Add("CasesReservation", "預約賞屋");
            re.Add("create_date", "發佈日期");
            re.Add("sortIndex", "排序");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }

        /// <summary>
        /// 全部
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        public static List<cases> All(string Lang)
        {
            Model DB = new Model();
            return DB.cases.Where(m => m.status == "Y").Where(m => m.lang == Lang).ToList();
        }

        /// <summary>
        /// 單筆
        /// </summary>
        /// <param name="Lang"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static cases Single(string Lang, string guid)
        {
            Model DB = new Model();
            cases data = DB.cases.Where(m => m.status == "Y").Where(m => m.lang == Lang).Where(m => m.guid == guid).FirstOrDefault();
            return data;
        }

        /// <summary>
        /// 取得有工程進度的建案
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        //public static List<cases> AllHaveProgress(string Lang, string hmmcu = "")
        //{
        //    Model DB = new Model();
        //    List<cases> cases = DB.cases.Where(m => m.status == "Y").Where(m => m.lang == Lang).ToList();

        //    if (!string.IsNullOrEmpty(hmmcu))
        //    {
        //        cases = cases.Where(m => m.hmmcu == hmmcu).ToList();
        //    }

        //    List<cases> newCase = new List<cases>();

        //    foreach (cases item in cases)
        //    {
        //        if (DB.cases_progress.Where(m => m.status == "Y").Where(m => m.lang == Lang).Where(m => m.category == item.guid).Count() > 0)
        //        {
        //            newCase.Add(item);
        //        }
        //    }
        //    return newCase;
        //}

        public static List<cases> AllHaveProgressByInfoData(string Lang, List<InfoData> InfoData)
        {
            try
            {
                Model DB = new Model();
                List<cases> result = new List<cases>();
                var cases = DB.cases.Where(x => x.lang == Lang && x.status == "Y").AsQueryable();

                foreach (var item in InfoData)
                {
                    var c = cases.Where(x => x.hmmcu == item.hmmcu).SingleOrDefault();
                    if (DB.cases_progress.Where(x => x.status == "Y" && x.lang == Lang && x.category == c.guid).Count() > 0)
                    {
                        result.Add(c);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}