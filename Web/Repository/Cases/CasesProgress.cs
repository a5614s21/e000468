﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Repository.Cases
{
    public class CasesProgress : Controller
    {
        // GET: message

        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
                  

            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定
            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '工程名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("notes", "[{'subject': '進度描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("post_date", "[{'subject': '發布日期','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\"></small>','useLang':'N'}]");


            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            media.Add("pic", "[{'subject': '內頁圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">1120 x 747 (px)，可複選</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'Y'}]");


            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階
            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'Y'}]");
            other.Add("sortIndex", "[{'subject': '標題','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            #region 隱藏欄位
            Dictionary<String, Object> hidden = new Dictionary<string, object>();  


            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;

        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");          
            re.Add("title", "工程名稱");  
            re.Add("post_date", "發布日期");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }



        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns> 
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "post_date");
            re.Add("orderByType", "desc");

            return re;
        }

        /// <summary>
        /// 全部
        /// </summary>
        /// <param name="Lang"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public static List<cases_progress> All(string Lang , string category)
        {
            Model DB = new Model();
            return DB.cases_progress.Where(m => m.status == "Y").Where(m => m.lang == Lang).Where(m => m.category == category).OrderByDescending(m=>m.post_date).ToList();
        }

        /// <summary>
        /// 單筆
        /// </summary>
        /// <param name="Lang"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static cases_progress Single(string Lang, string guid)
        {
            Model DB = new Model();
            cases_progress data = DB.cases_progress.Where(m => m.status == "Y").Where(m => m.lang == Lang).Where(m => m.guid == guid).FirstOrDefault();
            return data;
        }

     

    }
}