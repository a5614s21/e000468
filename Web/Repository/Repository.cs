﻿using System;
using System.Collections;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Web.Repository
{


    /// <summary>
    /// 實作Unit Of Work的interface。
    /// </summary>
    public interface IUnitOfWork : IDisposable
        {
            /// <summary>
            /// 儲存所有異動。
            /// </summary>
            void Save();

            /// <summary>
            /// 取得某一個Entity的Repository。
            /// 如果沒有取過，會initialise一個
            /// 如果有就取得之前initialise的那個。
            /// </summary>
            /// <typeparam name="T">此Context裡面的Entity Type</typeparam>
            /// <returns>Entity的Repository</returns>
            IRepository<T> Repository<T>() where T : class;
        }


        /// <summary>
        /// 實作Entity Framework Unit Of Work的class
        /// </summary>
        public class EFUnitOfWork : IUnitOfWork
        {
            private readonly DbContext _context;

            private bool _disposed;
            private Hashtable _repositories;

            /// <summary>
            /// 設定此Unit of work(UOF)的Context。
            /// </summary>
            /// <param name="context">設定UOF的context</param>
            public EFUnitOfWork(DbContext context)
            {
                _context = context;
            }

            /// <summary>
            /// 儲存所有異動。
            /// </summary>
            public void Save()
            {
                _context.SaveChanges();
            }

            /// <summary>
            /// 清除此Class的資源。
            /// </summary>
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// <summary>
            /// 清除此Class的資源。
            /// </summary>
            /// <param name="disposing">是否在清理中？</param>
            protected virtual void Dispose(bool disposing)
            {
                if (!_disposed)
                {
                    if (disposing)
                    {
                        _context.Dispose();
                    }
                }

                _disposed = true;
            }

            /// <summary>
            /// 取得某一個Entity的Repository。
            /// 如果沒有取過，會initialise一個
            /// 如果有就取得之前initialise的那個。
            /// </summary>
            /// <typeparam name="T">此Context裡面的Entity Type</typeparam>
            /// <returns>Entity的Repository</returns>
            public IRepository<T> Repository<T>() where T : class
            {
                if (_repositories == null)
                {
                    _repositories = new Hashtable();
                }

                var type = typeof(T).Name;

                if (!_repositories.ContainsKey(type))
                {
                    var repositoryType = typeof(EFGenericRepository<>);

                    var repositoryInstance =
                        Activator.CreateInstance(repositoryType
                                .MakeGenericType(typeof(T)), _context);

                    _repositories.Add(type, repositoryInstance);
                }

                return (IRepository<T>)_repositories[type];
            }

        
        }

        /// <summary>
        /// 代表一個Repository的interface。
        /// </summary>
        /// <typeparam name="T">任意model的class</typeparam>
        public interface IRepository<T>
        {
            /// <summary>
            /// 新增一筆資料。
            /// </summary>
            /// <param name="entity">要新增到的Entity</param>
            void Create(T entity);

            /// <summary>
            /// 取得第一筆符合條件的內容。如果符合條件有多筆，也只取得第一筆。
            /// </summary>
            /// <param name="predicate">要取得的Where條件。</param>
            /// <returns>取得第一筆符合條件的內容。</returns>
            T Read(Expression<Func<T, bool>> predicate);

            /// <summary>
            /// 取得Entity全部筆數的IQueryable。
            /// </summary>
            /// <returns>Entity全部筆數的IQueryable。</returns>
            IQueryable<T> Reads();


           /// <summary>
            /// 取得Entity全部筆數的IQueryable。(含where)
            /// </summary>
            /// <returns>Entity全部筆數的IQueryable。</returns>
            IQueryable<T> ReadsWhere(Expression<Func<T, bool>> predicate);


            /// <summary>
            /// 更新一筆資料的內容。
            /// </summary>
            /// <param name="entity">要更新的內容</param>
            void Update(T entity);

            /// <summary>
            /// 刪除一筆資料內容。
            /// </summary>
            /// <param name="entity">要被刪除的Entity。</param>
            void Delete(T entity);

            /// <summary>
            /// 儲存異動。
            /// </summary>
            void SaveChanges();
        }

        /// <summary>
        /// 實作Entity Framework Generic Repository 的 Class。
        /// </summary>
        /// <typeparam name="TEntity">EF Model 裡面的Type</typeparam>
        public class EFGenericRepository<TEntity> : IRepository<TEntity>
            where TEntity : class
        {
            private DbContext Context { get; set; }

            /// <summary>
            /// 建構EF一個Entity的Repository，需傳入此Entity的Context。
            /// </summary>
            /// <param name="inContext">Entity所在的Context</param>
            public EFGenericRepository(DbContext inContext)
            {
                Context = inContext;
            }


      

        /// <summary>
        /// 新增一筆資料到資料庫。
        /// </summary>
        /// <param name="entity">要新增到資料的庫的Entity</param>
        public void Create(TEntity entity)
            {
                   
                        Context.Set<TEntity>().Add(entity);
                  
             
            }

            /// <summary>
            /// 取得第一筆符合條件的內容。如果符合條件有多筆，也只取得第一筆。
            /// </summary>
            /// <param name="predicate">要取得的Where條件。</param>
            /// <returns>取得第一筆符合條件的內容。</returns>
            public TEntity Read(Expression<Func<TEntity, bool>> predicate)
            {
                return Context.Set<TEntity>().Where(predicate).FirstOrDefault();
            }

            /// <summary>
            /// 取得Entity全部筆數的IQueryable。
            /// </summary>
            /// <returns>Entity全部筆數的IQueryable。</returns>
            public IQueryable<TEntity> Reads()
            {
                return Context.Set<TEntity>().AsQueryable();
            }

        /// <summary>
        /// 取得Entity全部筆數的IQueryable。(含where)
        /// </summary>
        /// <returns>Entity全部筆數的IQueryable。</returns>
        public IQueryable<TEntity> ReadsWhere(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Where(predicate).AsQueryable();
        }

        /// <summary>
        /// 更新一筆Entity內容。
        /// </summary>
        /// <param name="entity">要更新的內容</param>
        public void Update(TEntity entity)
            {
                Context.Entry<TEntity>(entity).State = EntityState.Modified;
            }

            /// <summary>
            /// 更新一筆Entity的內容。只更新有指定的Property。
            /// </summary>
            /// <param name="entity">要更新的內容。</param>
            /// <param name="updateProperties">需要更新的欄位。</param>
            public void Update(TEntity entity, Expression<Func<TEntity, object>>[] updateProperties)
            {
                Context.Configuration.ValidateOnSaveEnabled = false;

                Context.Entry<TEntity>(entity).State = EntityState.Unchanged;

                if (updateProperties != null)
                {
                    foreach (var property in updateProperties)
                    {
                        Context.Entry<TEntity>(entity).Property(property).IsModified = true;
                    }
                }
            }

            /// <summary>
            /// 刪除一筆資料內容。
            /// </summary>
            /// <param name="entity">要被刪除的Entity。</param>
            public void Delete(TEntity entity)
            {
                Context.Entry<TEntity>(entity).State = EntityState.Deleted;
            }

            /// <summary>
            /// 儲存異動。
            /// </summary>
            public void SaveChanges()
            {
                Context.SaveChanges();

                // 因為Update 單一model需要先關掉validation，因此重新打開
                if (Context.Configuration.ValidateOnSaveEnabled == false)
                {
                    Context.Configuration.ValidateOnSaveEnabled = true;
                }
            }
        }




    
}