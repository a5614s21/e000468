/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    // config.uiColor = '#AADC6E';

   /* config.toolbar_Full = [['Source', '-', 'Save', 'NewPage', 'Preview', '-', 'Templates'],
                          ['Undo', 'Redo', '-', 'SelectAll', 'RemoveFormat'],
                          ['Styles', 'Format', 'Font', 'FontSize'],
                          ['TextColor', 'BGColor'],
                          ['Maximize', 'ShowBlocks', '-', 'About'], '/',
                          ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
                          ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'],
                          ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                          ['Link', 'Unlink', 'Anchor'],
                          ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
                          ['Code']];*/
    config.toolbar = [['Source'],
                      ['Undo', 'Redo', '-', 'SelectAll', 'RemoveFormat'],
                      ['Format', 'Font', 'FontSize'],
                      ['TextColor', 'BGColor'],
                      ['Maximize', 'ShowBlocks', '-', 'About'], '/',
                      ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
                      ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'],
                      ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                      ['Link', 'Unlink', 'Anchor'],
                      ['Newplugin', 'Image', 'Table', 'HorizontalRule', 'SpecialChar'],
                      ['Code']];
    config.extraPlugins = 'newplugin';

    config.removeButtons = 'Anchor';
    config.allowedContent = true;
    config.disallowedContent = 'img{width,height};img[width,height]';
    //config.skin = 'moono';
  //  config.contentsCss = ['../../../../Styles/style.css'];
    config.baseFloatZIndex = 10000000;
    //config.enterMode = CKEDITOR.ENTER_BR;
    //config.shiftEnterMode = CKEDITOR.ENTER_P;
    config.htmlEncodeOutput = false;


    //config.filebrowserImageBrowseUrl = '/siteadmin/Files/Mag';
    config.font_names = 'Open Sans;Roboto Slab;Rokkitt;Arial;Arial Black;Comic Sans MS;Courier New;Tahoma;Times New Roman;Verdana';
  //  config.filebrowserBrowseUrl = 'path/to/editor/file', // eg. 'includes/elFinder/elfinder-cke.html'

};

CKEDITOR.on('instanceReady', function (ev) {

    // Ends self closing tags the HTML4 way, like <br>.
    ev.editor.dataProcessor.htmlFilter.addRules(
		{
		    elements:
			{
			    $: function (element) {
			        // Output dimensions of images as width and height
			        if (element.name == 'img') {
			            var style = element.attributes.style;

			            if (style) {
			                // Get the width from the style.
			                var match = /(?:^|\s)width\s*:\s*(\d+)px/i.exec(style),
								width = match && match[1];

			                // Get the height from the style.
			                match = /(?:^|\s)height\s*:\s*(\d+)px/i.exec(style);
			                var height = match && match[1];

			                // Get the float from the style.
			                match = /(?:^|\s)float\s*:\s*(\w+)/i.exec(style);
			                var float = match && match[1];

			                if (width) {
			                    //element.attributes.style = element.attributes.style.replace(/(?:^|\s)width\s*:\s*(\d+)px;?/i, 'width:100%');
			                    //element.attributes.width = width;
			                }

			                if (height) {
			                    element.attributes.style = element.attributes.style.replace(/(?:^|\s)height\s*:\s*(\d+)px;?/i, '');
			                    // element.attributes.height = height;
			                }
			                if (float) {
			                    element.attributes.style = element.attributes.style.replace(/(?:^|\s)float\s*:\s*(\w+)/i, '');
			                    //element.attributes.align = float;
			                }

			            }
			        }



			        if (!element.attributes.style)
			            delete element.attributes.style;

			        return element;
			    }
			}
		});
});
