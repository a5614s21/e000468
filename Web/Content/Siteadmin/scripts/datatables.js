$(document).ready(function () {
    /*--------------------------------------------
          		datatables  預設
    ---------------------------------------------*/
    $.extend( $.fn.dataTable.defaults, {
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRowImmediate,
                type: ''
            }
        },
    } );
/*--------------------------------------------
          		datatables  表格
    ---------------------------------------------*/
    var table = $(".datatables");
    table.each(function () {
        var that = $(this);
        that.dataTable({
            "sAjaxSource": $('#dataTableRoot').val(),
            sPaginationType: "full_numbers",
            checkAllId: "checkAll",
            language: {
                aria: {
                    sortAscending: ": 生序",
                    sortDescending: ": 降序"
                },

                emptyTable: "無資料",
                info: "<span class='text-muted'> ，在 _START_ 到 _END_ 筆，全部 _TOTAL_ 筆</span>",
                infoEmpty: "無資料",
                infoFiltered: "(篩選1 from _MAX_ total 筆)",
                lengthMenu: "<span class='mr-1 text-muted'>每頁</span>  _MENU_",
                search: "<i class='icon-search'></i> 搜尋：",
                zeroRecords: "無資料",
                paginate: {
                    previous: "<i class='icon-keyboard_arrow_left'></i>",
                    next: "<i class='icon-keyboard_arrow_right'></i>",
                    last: "<i class='icon-last_page'></i>",
                    first: "<i class='icon-first_page'></i>"
                }
            },
            buttons: [{
                extend: "print",
                className: "btn dark btn-outline"
            }, {
                extend: "copy",
                className: "btn red btn-outline"
            }, {
                extend: "pdf",
                className: "btn green btn-outline"
            }, {
                extend: "excel",
                className: "btn yellow btn-outline "
            }, {
                extend: "csv",
                className: "btn purple btn-outline "
            }, {
                extend: "colvis",
                className: "btn dark btn-outline",
                text: "欄位"
            }],
            order: [
                [0, "asc"]
            ],
            lengthMenu: [
                [10, 20, 50, 100, 150, -1],
                [10, 20, 50, 100, 150, "All"]
            ],
            pageLength: 20,
            dom: 't<"row mt-1 align-items-center"<"col-md-6 text-center text-md-left"li><"col-md-6 text-center text-md-right"p>>',
            order: [
                [1, "asc"]
            ],
            columnDefs: [{
                "targets": "nosort",
                "orderable": false
            }],
        });
        /* datatables -輸出按鈕*/
        $("#datatableTools a.tool-action").on("click", function () {
            var e = $(this).attr("data-action");
            that.DataTable().button(e).trigger();
        });

    });

    /* datatables - 全選
    table.find(".group-checkable").change(function () {
        var e = jQuery(this).attr("data-set"),
            t = jQuery(this).is(":checked");
        jQuery(e).each(function () {
            t ? ($(this).prop("checked", !0),
                $(this).parents("tr").addClass("active")) : ($(this).prop("checked", !1),
                $(this).parents("tr").removeClass("active"))
        })
    });
    table.on("change", "tbody tr .checkboxes", function () {
        $(this).parents("tr").toggleClass("active")
    });
    */
    /*--------------------------------------------
          		datatables  無分頁
    ---------------------------------------------*/
    var table = $(".datatables_nop");
    table.each(function () {
        var that = $(this);
        that.dataTable({
         
            sPaginationType: "full_numbers",
            checkAllId: "checkAll",
            language: {
                aria: {
                    sortAscending: ": 生序",
                    sortDescending: ": 降序"
                },

                emptyTable: "無資料",
                info: "<span class='text-muted'> ，在 _START_ 到 _END_ 筆，全部 _TOTAL_ 筆</span>",
                infoEmpty: "無資料",
                infoFiltered: "(篩選1 from _MAX_ total 筆)",
                lengthMenu: "<span class='mr-1 text-muted'>每頁</span>  _MENU_",
                search: "<i class='icon-search'></i> 搜尋：",
                zeroRecords: "無資料",
                paginate: {
                    previous: "<i class='icon-keyboard_arrow_left'></i>",
                    next: "<i class='icon-keyboard_arrow_right'></i>",
                    last: "<i class='icon-last_page'></i>",
                    first: "<i class='icon-first_page'></i>"
                }
            },
            buttons: [{
                extend: "print",
                className: "btn dark btn-outline"
            }, {
                extend: "copy",
                className: "btn red btn-outline"
            }, {
                extend: "pdf",
                className: "btn green btn-outline"
            }, {
                extend: "excel",
                className: "btn yellow btn-outline "
            }, {
                extend: "csv",
                className: "btn purple btn-outline "
            }, {
                extend: "colvis",
                className: "btn dark btn-outline",
                text: "欄位"
            }],
            order: [
                [0, "asc"]
            ],
            lengthMenu: [
                [10, 20, 50, 100, 150, -1],
                [10, 20, 50, 100, 150, "All"]
            ],
            pageLength: 20,
            dom: 't<"row mt-3"<"col-md-6 text-center text-md-left"><"col-md-6 text-center text-md-right">>',
            order: [
                [1, "asc"]
            ],
            columnDefs: [{
                "targets": "nosort",
                "orderable": false
            }],
        });
        /* datatables -輸出按鈕*/
        $("#datatableTools a.tool-action").on("click", function () {
            var e = $(this).attr("data-action");
            that.DataTable().button(e).trigger();
        });

    });
});