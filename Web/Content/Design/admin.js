﻿var fileUpload = Array();
fileUpload["field"] = "";
fileUpload["types"] = "";
fileUpload["lay"] = "";
fileUpload["multiple"] = "";

$(document).ready(function () {
    $("body").delegate(".infinityDel", "click", function () {
        $(this).parent('li').remove();

        /*  var delQty = parseInt($(this).attr('data-delQty'));

          for (var i = 0; i < delQty; i++) {
              $(this).prev('div').remove();
          }

          $(this).remove();*/
    });

    $('.infinity').each(function () {
        $("#sortable").sortable();
        $("#sortable").disableSelection();
    });

    //無限欄位新增
    $("body").delegate(".infinity", "click", function () {
        var RowData = $(this).attr('data-row').split('/');
        var RowVal = $(this).attr('data-val').split('/');
        var required = $(this).attr('data-required');
        var delQty = $(this).attr('data-delQty');

        $('.changInputLang').each(function () {
            var addText = '<li class="form-row col-sm-10"> <span class="ui-icon ui-icon-arrowthick-2-n-s" style="margin-top:7px;"></span>';
            for (var i = 0; i < RowData.length; i++) {
                addText += '<div class="col-sm-auto valList">';
                addText += '<div class="form-row">';
                addText += '<div class="col-auto form-text">' + RowData[i] + '</div>';
                addText += '<div class="col mb-2">';
                addText += ' <input class="form-control ' + RowVal[i] + '_' + $(this).attr('data-lang') + '" type="text" ' + required + ' value=""  />';
                addText += '</div>';
                addText += '</div>';
                addText += '</div>';
            }
            addText += '<a class="btn btn-outline btn-sm infinityDel" title="刪除" data-delQty="' + delQty + '" href="#1" role="button"><i class="icon-trash"></i><span class="text-hide">刪除</span></a>';
            addText += '</li>';
            $('.infinityLayOut_' + $(this).attr('data-lang')).append(addText);
        });

        $("#sortable").sortable();
        $("#sortable").disableSelection();
    });

    $("body").delegate(".changeCity", "change", function () {
        var valsTemp = new Array();

        var arr = new Object();
        arr.name = "city";
        arr.value = $(this).val();
        valsTemp.push(arr);

        var vals = JSON.stringify(valsTemp);
        useAjax('changeCity', vals);
        return false;
    });

    //切換語系
    $('.changInputLang').click(function () {
        $('.changInputLang').removeClass('selected');
        $(this).addClass('selected');
        $('.nowLangTitle').html($(this).html());
        $('.vLay').hide();

        $('.v_' + $(this).attr('data-lang')).show();
    });

    //DataTable關鍵字----------------------------------------------------------------------

    $("body").delegate(".keywordSearch", "click", function () {
        dataTableSearch();
        return false;
    });
    $("body").delegate(".statusSearch,.categorySearch,.returnStatusSearch", "change", function () {
        dataTableSearch();
        return false;
    });

    function dataTableSearch() {
        var sSearch = "[{'keyword': '" + $('.keywordsInput').val() + "','status': '" + $('.statusSearch :selected').val() + "','category': '" + $('.categorySearch :selected').val() + "','re_status': '" + $('.returnStatusSearch :selected').val() + "'}]";
        $(".datatables").DataTable().search(sSearch, true, false).draw();
    }

    //--------------------------------------------------------------------------------

    //全選動作
    $("body").delegate(".changeAll", "click", function () {
        var guid = "";
        var title = "";

        var status = $(this).attr('data-status');

        $('.selectAll').each(function () {
            if ($(this).prop('checked')) {
                guid = guid + $(this).val() + ',';
                title = title + $(this).attr('data-title') + '|';
            }
        });

        if (guid == "") {
            swal({
                title: "系統訊息",
                text: "請至少選擇一項須執行動作之項目!",
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#e7505a",
                confirmButtonText: "確定!",
                closeOnConfirm: false,
                cancelButtonText: "取消",
            });
            return false;
        }
        else {
            guid = guid.substring(0, guid.length - 1);
            title = title.substring(0, title.length - 1);

            if ($('#tables').val() != 'ashcan') {
                var msg = "您將刪除選取內容，一但刪除將會放置垃圾桶，若需還原請至垃圾桶區還原，若於垃圾桶刪除之項目將無法還原‧";

                if (status != "D") {
                    msg = "您確定要執行批次啟用/停用動作?"
                }

                swal({
                    title: "系統訊息",
                    text: msg,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#e7505a",
                    confirmButtonText: "確定!",
                    closeOnConfirm: false,
                    cancelButtonText: "取消",
                },
                    function () {
                        var valsTemp = new Array();

                        var arr = new Object();
                        arr.name = "guid";
                        arr.value = guid;
                        valsTemp.push(arr);

                        var arr = new Object();
                        arr.name = "tables";
                        arr.value = $('#tables').val();
                        valsTemp.push(arr);

                        var arr = new Object();
                        arr.name = "title";
                        arr.value = title;
                        valsTemp.push(arr);

                        var arr = new Object();
                        arr.name = "status";
                        arr.value = status;
                        valsTemp.push(arr);

                        var vals = JSON.stringify(valsTemp);
                        useAjax('changeStatus', vals);

                        // console.log(vals);
                    });
            }
            else {
                if (status == "D") {
                    swal({
                        title: "是否確認刪除",
                        text: "確定永遠刪除該項目？",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#e7505a",
                        confirmButtonText: "確定!",
                        closeOnConfirm: false,
                        cancelButtonText: "取消",
                    },
                        function () {
                            var valsTemp = new Array();

                            var arr = new Object();
                            arr.name = "guid";
                            arr.value = guid;
                            valsTemp.push(arr);

                            var arr = new Object();
                            arr.name = "tables";
                            arr.value = $('#tables').val();
                            valsTemp.push(arr);

                            var arr = new Object();
                            arr.name = "title";
                            arr.value = title;
                            valsTemp.push(arr);

                            var arr = new Object();
                            arr.name = "type";
                            arr.value = "del";
                            valsTemp.push(arr);

                            var vals = JSON.stringify(valsTemp);
                            useAjax('removeData', vals);
                        });
                }
                else {
                    swal({
                        title: "系統訊息",
                        text: '確定批次還原所選項目?',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#e7505a",
                        confirmButtonText: "確定!",
                        closeOnConfirm: false,
                        cancelButtonText: "取消",
                    },
                        function () {
                            var valsTemp = new Array();

                            var arr = new Object();
                            arr.name = "guid";
                            arr.value = guid;
                            valsTemp.push(arr);

                            var arr = new Object();
                            arr.name = "tables";
                            arr.value = $('#tables').val();
                            valsTemp.push(arr);

                            var arr = new Object();
                            arr.name = "title";
                            arr.value = title;
                            valsTemp.push(arr);

                            var arr = new Object();
                            arr.name = "type";
                            arr.value = "reduction";
                            valsTemp.push(arr);

                            var vals = JSON.stringify(valsTemp);
                            useAjax('removeData', vals);
                        });
                }
            }
        }
    });
    //全選
    $("body").delegate("#selectall", "click", function () {
        var checked = $(this).prop('checked');

        $('.selectAll').each(function () {
            $(this).prop('checked', checked);
        });
    });

    //單選
    $("body").delegate(".selectAll", "click", function () {
        var checked = $(this).prop('checked');
        $(this).prop('checked', checked);
    });

    //切換主題
    $("body").delegate("#forum_categoryguid", "change", function () {
        var valsTemp = new Array();

        var arr = new Object();
        arr.name = "guid";
        arr.value = $(this).val();
        valsTemp.push(arr);

        var vals = JSON.stringify(valsTemp);
        useAjax('changeSubCategory', vals);
    });

    //切換主辦人
    $("body").delegate("#forum_subcategoryguid", "change", function () {
        var valsTemp = new Array();

        var arr = new Object();
        arr.name = "guid";
        arr.value = $(this).val();
        valsTemp.push(arr);

        var vals = JSON.stringify(valsTemp);
        useAjax('changeMasterUser', vals);
    });

    //權限選擇
    $("body").delegate(".topPermissions", "click", function () {
        var topGuid = $(this).attr('data-topguid');
        var chked = $(this).prop('checked');

        $('.subPermissions').each(function () {
            if (topGuid == $(this).attr('data-topguid')) {
                $(this).prop('checked', chked);
            }
        });

        $('.permissions').each(function () {
            if (topGuid == $(this).attr('data-topguid') && $(this).val() == "F") {
                $(this).prop('checked', chked);
            }
        });
    });

    //重製驗證碼

    $('#captcha').click(function () {
        var src = $('#captcha').attr('src').split('?');

        $('#captcha').attr('src', src[0] + "?d=" + Date.now());
    });

    //移除檔案
    $("body").delegate(".delFiles", "click", function () {
        $('#' + $(this).attr('data-id')).remove();
    });

    //異動排序---------------------------------------------------------------
    $("body").delegate(".cahngeSortIndex", "click", function () {
        cahngeSortIndex($(this).attr('data-guid'), $(this).attr('data-type'));
    });

    $("body").delegate(".cahngeSortIndexInput", "keyup", function () {
        cahngeSortIndex($(this).attr('data-guid'), "input");
    });
    function cahngeSortIndex(guid, type) {
        var sortIndex = $('#' + "sortIndex_" + guid).val();

        if (type == "up") {
            if (parseInt(sortIndex) > 1) {
                sortIndex = parseInt(sortIndex) - 1;
            }
        }

        if (type == "down") {
            sortIndex = parseInt(sortIndex) + 1;
        }

        $('#' + "sortIndex_" + guid).val(sortIndex);

        var valsTemp = new Array();

        var arr = new Object();
        arr.name = "guid";
        arr.value = guid;
        valsTemp.push(arr);

        var arr = new Object();
        arr.name = "tables";
        arr.value = $('#tables').val();
        valsTemp.push(arr);

        var arr = new Object();
        arr.name = "sortIndex";
        arr.value = sortIndex;
        valsTemp.push(arr);

        var vals = JSON.stringify(valsTemp);
        useAjax('cahngeSortIndex', vals);
    }

    //---------------------------------------------------------------------

    //異動狀態

    $("body").delegate(".changeStatus", "click", function () {
        var guid = $(this).attr('data-guid');
        var title = $(this).attr('data-title');

        if ($(this).attr('data-type') == 'del') {
            if ($('#tables').val() != 'ashcan') {
                swal({
                    title: "是否確認刪除",
                    text: "您將刪除選取內容，一但刪除將會放置垃圾桶，若需還原請至垃圾桶區還原，若於垃圾桶刪除之項目將無法還原‧",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#e7505a",
                    confirmButtonText: "確定!",
                    closeOnConfirm: false,
                    cancelButtonText: "取消",
                },
                    function () {
                        var valsTemp = new Array();

                        var arr = new Object();
                        arr.name = "guid";
                        arr.value = guid;
                        valsTemp.push(arr);

                        var arr = new Object();
                        arr.name = "tables";
                        arr.value = $('#tables').val();
                        valsTemp.push(arr);

                        var arr = new Object();
                        arr.name = "title";
                        arr.value = title;
                        valsTemp.push(arr);

                        var arr = new Object();
                        arr.name = "status";
                        arr.value = "D";
                        valsTemp.push(arr);

                        var vals = JSON.stringify(valsTemp);

                        useAjax('changeStatus', vals);
                    });
            }
            else {
                swal({
                    title: "是否確認刪除",
                    text: "確定永遠刪除該項目？",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#e7505a",
                    confirmButtonText: "確定!",
                    closeOnConfirm: false,
                    cancelButtonText: "取消",
                },
                    function () {
                        var valsTemp = new Array();

                        var arr = new Object();
                        arr.name = "guid";
                        arr.value = guid;
                        valsTemp.push(arr);

                        var arr = new Object();
                        arr.name = "tables";
                        arr.value = $('#tables').val();
                        valsTemp.push(arr);

                        var arr = new Object();
                        arr.name = "title";
                        arr.value = title;
                        valsTemp.push(arr);

                        var arr = new Object();
                        arr.name = "type";
                        arr.value = "del";
                        valsTemp.push(arr);

                        var vals = JSON.stringify(valsTemp);
                        useAjax('removeData', vals);
                    });
            }
        }
        if ($(this).attr('data-type') == 'change') {
            var valsTemp = new Array();

            var arr = new Object();
            arr.name = "guid";
            arr.value = guid;
            valsTemp.push(arr);

            var arr = new Object();
            arr.name = "tables";
            arr.value = $('#tables').val();
            valsTemp.push(arr);

            var status = 'Y';

            if ($(this).attr('data-status') == 'Y') {
                status = 'N';
                $(this).removeClass("badge-danger");
                $(this).addClass("badge-secondary");
                $(this).text("停用");
            }
            else {
                status = 'Y';
                $(this).removeClass("badge-secondary");
                $(this).addClass("badge-danger");
                $(this).text("啟用");
            }

            $(this).attr('data-status', status);

            var arr = new Object();
            arr.name = "status";
            arr.value = status;
            valsTemp.push(arr);

            var arr = new Object();
            arr.name = "title";
            arr.value = title;
            valsTemp.push(arr);

            var vals = JSON.stringify(valsTemp);
            useAjax('changeStatus', vals);
        }
    });

    /*取得圖片*/
    $("body").delegate(".getImages", "click", function () {
        fileUpload["field"] = $(this).attr('data-field');
        fileUpload["types"] = $(this).attr('data-type');
        fileUpload["lay"] = $(this).attr('data-lay');
        fileUpload["multiple"] = $(this).attr('data-multiple');

        //檔案管理器

        elFinder.prototype.i18.zh_TW.messages['cmdimportBut'] = '嵌入';
        elFinder.prototype._options.commands.push('importBut');
        //
        elFinder.prototype.commands.importBut = function () {
            this.exec = function (hashes) {
                //do whatever

                var file = this.files(hashes);

                //圖檔直接加入
                if (file[0].mime != 'directory') {
                    // console.log(fileUpload["field"]);

                    for (var p = 0; p < file.length; p++) {
                        if (file[p].mime != 'directory') {
                            var hash = decodeURI(file[p].hash);
                            var fm = this.fm;
                            var url = fm.url(hash);

                            if (fileUpload["lay"] == 'area') {
                                var addImg = "";
                                var i = 1;
                                setUploadFile(fileUpload["types"], fileUpload["lay"], url, fileUpload["field"], hash, fileUpload["multiple"]);
                            }
                            else {
                                if ($('#siteadminArea').val().indexOf($('#projeftNo').val()) != -1) {
                                    //url = '../../' + url.replace($('#webPath').val(), "") + '"';
                                    url = '../../../' + url.replace($('#webPath').val(), "") + '"';
                                }

                                addImg = '<img src="' + url + '"  >';
                                var oEditor = CKEDITOR.instances[fileUpload["field"]];
                                var html = addImg;

                                var newElement = CKEDITOR.dom.element.createFromHtml(html, oEditor.document);
                                oEditor.insertElement(newElement);
                            }
                        }
                        else {
                            this.exec('quicklook');//開啟資料夾
                        }
                    }
                }
                else {
                    this.exec('quicklook');//開啟資料夾
                }
            }
            this.getstate = function () {
                //return 0 to enable, -1 to disable icon access
                return 0;
            }
        };

        var myCommands = elFinder.prototype._options.commands;
        var disabled = ['extract', 'archive', 'help', 'select'];
        $.each(disabled, function (i, cmd) {
            (idx = $.inArray(cmd, myCommands)) !== -1 && myCommands.splice(idx, 1);
        });
        var selectedFile = null;
        var options = {
            url: $('#fileMegRoot').val(),
            commands: myCommands,
            lang: 'zh_TW',
            // onlyMimes: ['application/pdf'],
            locale: 'utf-8',
            uiOptions: {
                toolbar: [
                    ['back', 'forward'],
                    ['reload'],
                    ['home', 'up'],
                    ['mkdir', 'mkfile', 'upload'],
                    ['open', 'download'],
                    ['info'],
                    ['quicklook'],
                    ['copy', 'cut', 'paste'],
                    ['rm'],
                    ['duplicate', 'rename', 'edit', 'resize'],
                    ['view', 'sort', 'importBut']
                ]
            },
            contextmenu: {
                // current directory file menu
                files: [
                    'open', 'quicklook', 'sharefolder', '|', 'download', '|', 'copy', 'cut', 'paste', 'rm', '|', 'rename', '|', 'importBut', '|', 'info'
                ]
            },
            reloadClearHistory: true,
            rememberLastDir: false,

            resizable: false,
            // height: '500px',
            handlers: {
                select: function (event, elfinderInstance) {
                    if (event.data.selected.length == 1) {
                        var item = $('#' + event.data.selected[0]);
                        if (!item.hasClass('directory')) {
                            selectedFile = event.data.selected[0];
                            $('#elfinder-selectFile').show();
                            return;
                        }
                    }
                    $('#elfinder-selectFile').hide();
                    selectedFile = null;
                },

                //上傳檔案監聽
                upload: function (event, elfinderInstance) {
                    if (event.data.removed != null) {
                        console.log('add');

                        var arr = Array();

                        for (var i = 0; i < event.data.added.length; i++) {
                            var $this = event.data.added[i];

                            var encPath = $this.hash.substr($this.hash.indexOf('_') + 1);

                            var tests = decode64(encPath).split('\\');

                            var url = '';

                            for (var s = 1; s < (tests.length - 1); s++) {
                                url = url + "/" + tests[s];
                            }

                            url = url + '/' + $this.name
                            arr[i] = url;
                        }

                        if (arr.length > 0) {
                            var vals = JSON.stringify(arr);
                            useFTP("Upload", vals);
                        }
                    }

                    return false;
                },
                //移除時
                rm: function (event, elfinderInstance) {
                },

                //判斷點選兩下時
                dblclick: function (event, elfinderInstance) {
                    event.preventDefault();
                    elfinderInstance.exec('importBut');
                    return false;
                },
            }
        };
        $('#elfinder').elfinder(options).elfinder('instance');

        /* $('#elfinder-selectFile').click(function () {
             if (selectedFile != null)
                 $.post('files/selectFile', { target: selectedFile }, function (response) {
                     alert(response);
                 });
         });*/
    });

    /*儲存並發送*/

    $('.saveAndSend').click(function () {
        $('#saveAndSend').val("Y");
        $("#saveForm").submit();
    });

    /**
 * 後台表單驗證
 */
    $("#saveForm").each(function () {
        $("#saveForm").validate({
            submitHandler: function (form) {
                //圖片或檔案
                $('.getImages').each(function () {
                    var field = $(this).attr('data-field');

                    var files = "";

                    $('.' + field + "_fileArea").find(".fileData").each(function () {
                        var tempFile = $(this).attr('data-file');

                        var fileSplitData = tempFile.split(',');

                        if (fileSplitData.length > 1) {
                            tempFile = tempFile.substr(1, tempFile.length);
                        }
                        //files += tempFile.replace('projects/public/e000468/test', '').replace('test/', '').replace('/Styles/', 'Styles/') + ",";
                        files += tempFile.replace('projects/public/e000468/test', '').replace('test/', '') + ",";
                    });

                    var file_alt = "";
                    if ($('#' + field + '_alt').val() != null) {
                        $('.' + field + "_alt").each(function () {
                            file_alt += $(this).val() + "§";
                        });
                    }

                    if (files != "") {
                        files = files.substring(0, files.length - 1);
                    }
                    if (file_alt != "") {
                        file_alt = file_alt.substring(0, file_alt.length - 1);
                    }

                    $('#' + field).val(files);
                    if ($('#' + field + '_alt').val() != null) {
                        $('#' + field + '_alt').val(file_alt);
                    }
                });

                //非上傳用圖片、檔案、連結
                if ($('#tables').val() == 'forum') {
                    var img = '';
                    $('.noUploadImg').each(function () {
                        img += $(this).attr('data-value') + ",";
                    });
                    if (img != '') {
                        img = img.substring(0, img.length - 1);
                        $('#img').val(img);
                    }

                    var file = '';
                    $('.noUploadFile').each(function () {
                        file += $(this).attr('data-value') + ",";
                    });
                    if (file != '') {
                        file = file.substring(0, file.length - 1);
                        $('#file').val(file);
                    }

                    var url = '';
                    $('.noUploadUrl').each(function () {
                        url += $(this).attr('data-value') + ",";
                    });
                    if (url != '') {
                        url = url.substring(0, url.length - 1);
                        $('#url').val(file);
                    }
                }

                //checkBox==========================================================

                $('.checkBoxField').each(function () {
                    if ($(this).prop("checked") == true) {
                        if ($('#' + $(this).attr('data-key')).val() == null || $('#' + $(this).attr('data-key')).val() == "") {
                            $('#' + $(this).attr('data-key')).val($(this).val());
                        }
                        else {
                            $('#' + $(this).attr('data-key')).val($('#' + $(this).attr('data-key')).val() + "," + $(this).val());
                        }
                    }
                });

                //=================================================================

                //無限欄位 ----------------------------------------------
                $('.infinity').each(function () {
                    var Field = $(this).attr('data-field');
                    var RowVal = $(this).attr('data-val').split('/');

                    // var allData = new Array();

                    $('.changInputLang').each(function () {
                        var lang = $(this).attr('data-lang');
                        var subData = new Array();

                        for (var i = 0; i < RowVal.length; i++) {
                            var arr = new Object();
                            arr.key = RowVal[i];
                            var val = new Array();
                            var s = 0;
                            $('.' + RowVal[i] + '_' + lang).each(function () {
                                val[s] = $(this).val();
                                s++;
                            });
                            arr.val = val;

                            subData.push(arr);
                        }

                        var jsonText = JSON.stringify(subData);
                        $('#' + Field + '_' + lang).val(jsonText);
                    });
                });

                //-------------------------------------------------------
                //有限欄位 ----------------------------------------------
                $('.fixedConcent').each(function () {
                    var Field = $(this).attr('data-field');
                    var Val = $(this).val();
                    var Lang = $(this).attr('data-lang');
                    // var allData = new Array();

                    $('.changInputLang').each(function () {
                        if (Lang == $(this).attr('data-lang')) {
                            $('#' + Field).val($('#' + Field).val() + Val + ',');
                        }
                    });
                });

                //-------------------------------------------------------
                //權限
                var permissionsTemp = new Array();
                $('.subPermissions').each(function () {
                    if ($(this).prop("checked") == true) {
                        var arr = new Object();
                        arr.name = $(this).attr('data-subguid');
                        arr.value = $('input[name=' + $(this).attr('data-subguid') + ']:checked').val();
                        permissionsTemp.push(arr);
                    }
                });

                //console.log(valsTemp);

                if (permissionsTemp.length > 0) {
                    var permissions = JSON.stringify(permissionsTemp);
                    $('#permissions').val(permissions);
                }

                swal({
                    title: "系統資訊",
                    text: "確定新增或異動資料?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#e7505a",
                    confirmButtonText: "確定!",
                    closeOnConfirm: false,
                    cancelButtonText: "取消",
                },
                    function () {
                        $('.submitBut').attr("disabled", true);

                        //修改使用者時若沒輸入密碼則帶入原本密碼
                        /*  if ($('#guid').val() != "" && $('#password').val() == '') {
                              $('#password').val($('#defPassword').val());
                          }*/

                        form.submit();
                        return false;
                    });
            },
            errorPlacement: function (error, element) {
                element.attr('style', 'border:#FF0000 1px solid;');

                element.next("._formErrorMsg").html('<div style="color: #FF0000; padding-bottom: 10px; padding-left: 10px;">' + error.text() + '</div>');
            },
            success: function (error) {
                var findID = $('#' + error[0].id.replace('-error', ''));

                //console.log(error[0].id);

                $(findID).attr('style', '');
                findID.next(".formNotice").html('');
                return false;
            }
        });
    });

    $('#username').each(function () {
        if ($('#guid').val() == "") {
            $('#username').rules('add', {
                required: true,
                remote: $('#siteadminArea').val() + '/ckAccount/' + $(this).attr('data-tables') + '@' + $(this).attr('data-category')
            });
            /* $('#password').rules('add', {
                 required: true,
                 minlength: 6,
                 maxlength: 14,
             });
             $('#password_chk').rules('add', {
                 required: true,
                 minlength: 6,
                 maxlength: 14,
                 equalTo: "#password",
             });*/
        }

        $('#password').keyup(function () {
            if ($(this).val() != '') {
                $('#password').rules('add', {
                    required: true,
                    minlength: 6,
                    maxlength: 14,
                });
                $('#password_chk').rules('add', {
                    required: true,
                    minlength: 6,
                    maxlength: 14,
                    equalTo: "#password",
                });
            }
            else {
                $('#password').rules('remove');
                $('#password_chk').rules('remove');
                $('#password').attr('style', '');
                $('#password_chk').attr('style', '');
                $('#password').next('.formNotice').html('');
                $('#password_chk').next('.formNotice').html('');
            }
        });
    });

	/**
	 * 登入驗證
	 */
    $("#loginForm").each(function () {
        $("#loginForm").validate({
            submitHandler: function (form) {
                $('.submitBut').attr("disabled", true);

                var valsTemp = $("#loginForm").serializeArray();

                var vals = JSON.stringify(valsTemp);

                useAjax('sysLogin', vals);

                return false;
            },
            errorPlacement: function (error, element) {
                element.attr('style', 'border:#FF0000 1px solid;');

                element.next("._formErrorMsg").html('<div style="color: #FF0000; padding-bottom: 10px; padding-left: 10px;">' + error.text() + '</div>');
            },
            success: function (error) {
                var findID = $('#' + error[0].id.replace('-error', ''));

                //console.log(error[0].id);

                $(findID).attr('style', '');
                findID.next(".formNotice").html('');
                return false;
            }
        });
    });

    //直接上傳
    $(".fileUpload").click(function () {
        var multiple = $(this).attr('data-multiple');

        //console.log(multiple);

        $(this).change(function () {
            var thisField = $(this).attr('data-value');

            setLoadPlayer('');

            //創建FormData對象
            var data = new FormData();

            //為FormData物件添加資料

            var fileField = '#inputfile_'.thisField;

            var needType = Array();

            var thisTypes = $(this).attr('accept');

            if (thisTypes.indexOf("image") != -1) {
                needType = Array("gif", "png", "jpg");
            }
            if (thisTypes.indexOf("video") != -1) {
                needType = Array("mp4");
            }
            if (thisTypes.indexOf("pdf") != -1) {
                needType = Array("pdf");
            }
            var multiple = $(this).attr('data-multiple');

            var addQty = 0;

            $.each($(this)[0].files, function (i, file) {
                var extIndex = file.name.lastIndexOf('.');
                var ext = file.name.substr(extIndex + 1, file.name.length);  //副檔名
                ext = ext.toLowerCase();

                if (needType.indexOf(ext) != -1) {
                    data.append('tables', $('#tables').val());
                    data.append('upload_file' + i, file);
                    data.append('lang', '');
                    data.append('types', thisTypes);
                    data.append('Field', thisField);
                    addQty++;
                }
            });

            //  $(".loading_pic").show();	//顯示載入圖片

            if (addQty > 0) {
                //發送資料
                $.ajax({
                    url: $('#siteadminArea').val() + '/fileUpload',
                    type: 'POST',
                    data: data,
                    cache: false,
                    contentType: false,		//不可缺參數
                    processData: false,		//不可缺參數
                    success: function (data) {
                        console.log(data);

                        if (data.Message.indexOf("sizeError") == -1) {
                            var fileField = thisField;
                            var dataArr = data.Message.split('§');
                            //data = $(data).html();

                            var addImg = "";
                            for (var p = 0; p < dataArr.length; p++) {
                                if (dataArr[p] != "Error" && dataArr[p] != "") {
                                    //var addImg = "";

                                    var hash = "tt" + guid();
                                    var url = dataArr[p];
                                    setUploadFile(thisTypes, "area", url, thisField, hash, multiple);

                                    setLoadPlayer('none');
                                }
                                else {
                                    // alert("格式錯誤!");
                                    return false;
                                }
                            }

                            $("body").delegate(".movePic", "click", function () {
                                var picDataArr = $(this).attr('data-id').split('.');

                                var li = $('#' + picDataArr[0]);
                                var act = $(this).attr('data-act');

                                if (act == "left") { li.prev().before(li); }
                                else { li.next().after(li); }

                                return false;
                            });
                        }
                        else {
                            swal({
                                title: "系統資訊",
                                text: "圖片寬高不符!",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonText: "確定",
                                closeOnConfirm: false
                            });
                        }
                    },
                    error: function () {
                        alert('上傳錯誤');
                        $(".loading_pic").hide();	//載入失敗移除載入圖片
                    }
                });
            }

            $(this).val('');
        });
    });

    $("body").delegate(".movePic", "click", function () {
        var picDataArr = $(this).attr('data-id').split('.');

        var li = $('#' + picDataArr[0]);
        var act = $(this).attr('data-act');

        if (act == "left") { li.prev().before(li); }
        else { li.next().after(li); }

        return false;
    });
});

/**
 * AJAX動作
 * @param ACT
 * @param needVal
 */
function useAjax(ACT, needVal) {
    $.ajax({
        type: 'POST',
        url: $('#ajaxRoot').val(),
        data: {
            Func: ACT,
            Val: encodeURI(needVal)
        },
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (json) {
            //setLoadPlayer("none");

            switch (json.Func) {
                case "changeCity":

                    var district = $.parseJSON(json.district);
                    var add = "";
                    for (var i = 0; i < district.length; i++) {
                        var selected = "";
                        if (i == 0) {
                            selected = " selected";
                        }
                        add += '<option value="' + district[i].district + '" ' + selected + '>' + district[i].district + '</option>';
                    }
                    $('#district').empty();
                    $('#district').html(add);
                    $('#district').selectpicker('refresh');
                    return false;

                    break;

                case "changeMasterUser":

                    $('#master').val(json.re);

                    break;

                case "changeSubCategory":

                    $('#forum_subcategoryguid').html(json.select);
                    $("#forum_subcategoryguid").val('default');
                    $("#forum_subcategoryguid").selectpicker("refresh");

                    break;

                case "sysLogin":

                    if (json.re != 'OK') {
                        var MsgText = '';
                        if (json.re == 'codeError') {
                            MsgText = '登入失敗!';                        
                        }
                        if (json.re == 'accountError') {
                            MsgText = '登入失敗!';
                        }
                        if (json.re == 'usernameError') {
                            MsgText = '登入失敗!';
                        }
                        if (json.re == 'passwordError') {
                            MsgText = '登入失敗!';
                        }
                        if (json.re == 'statusError') {
                            MsgText = '登入失敗!';
                        }

                        swal({
                            title: "系統資訊",
                            text: MsgText,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonText: "確定",
                            closeOnConfirm: false
                        });

                        $('#captcha').click();
                        $('#verification').val('');

                        $('.submitBut').attr("disabled", false);
                        return false;
                    }
                    else {
                        window.open(json.url, '_self')
                    }

                    break;

                case "changeStatus":

                    var msg = "執行完成!";

                    if (json.status == "D") {
                        msg = "該資料已移往資源回收桶!";
                    }

                    swal({
                        title: "系統資訊",
                        text: msg,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonText: "確定",
                        closeOnConfirm: false
                    });

                    var page_info = $(".datatables").DataTable().page.info();
                    $(".datatables").DataTable().page(page_info.page).draw(false);

                    break;

                case "removeData":

                    var msg = "該資料已永久刪除!";

                    if (json.types == "chkAll") {
                        msg = "批次動作已執行!";
                    }

                    swal({
                        title: "系統資訊",
                        text: msg,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonText: "確定",
                        closeOnConfirm: false
                    });

                    var page_info = $(".datatables").DataTable().page.info();
                    $(".datatables").DataTable().page(page_info.page).draw(false);

                    break;
            }
        },
        complete: function () { //生成分頁條
        },
        error: function () {
            //alert("讀取錯誤!");
        }
    });
}

/**
 * 建置上傳圖片或檔案
 */
function setUploadFile(types, lay, url, field, hash, multiple) {
    if (types.indexOf("image") != -1) {
        if (lay == "area") {
            addImg = "";
            addImg += '<div class="card" id="' + field + '_img' + hash + '" style="float:left;">';
            addImg += ' <input class="form-control form-control-sm mb-1 ' + field + '_alt" type="text" value="" placeholder="圖片敘述"  />';
            addImg += '<a class="thumb" href="' + url + '" data-fancybox=""><span class="imgFill imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;' + url + '&quot;); background-size: cover; background-position: center center; background-repeat: no-repeat;"><img src="' + url + '" class="imgData fileData"  data-file="' + url + '" style="display: none;"></span></a>';
            addImg += '<div class="btn-group btn-group-sm justify-content-center">';
            addImg += '<button class="btn btn-outline-default movePic" type="button" title="排列往前" data-act="left" data-id="' + field + '_img' + hash + '"><i class="icon-chevron-thin-left"></i></button>';
            // addImg += '<button class="btn btn-outline-default open_modal_picname" type="button" title="設定" data-target="#modal_picname" data-toggle="modal" data-access="alt_' + field + '_' + field + '_img' + hash + '" data-filename="' + url + '"><i class="icon-wrench"></i></button>';
            addImg += '<button class="btn btn-outline-default delFiles" type="button" data-guid="" data-id="' + field + '_img' + hash + '" data-field=\"' + field + '\" data-filename="' + url + '"><i class="icon-trash2"></i></button>';
            addImg += '<button class="btn btn-outline-default movePic" type="button" title="排列往後" data-act="right" data-id="' + field + '_img' + hash + '"><i class="icon-chevron-thin-right"></i></button>';
            addImg += '</div>';
            addImg += '<input type="hidden" name="alt_' + field + '_' + field + '_img' + hash + '" id="alt_' + field + '_' + field + '_img' + hash + '" class="form-control altData" placeholder="描述(alt)" >';
            addImg += '</div>';

            if (multiple == "Y") {
                $('#' + field + "_img").append(addImg);
            }
            else {
                $('#' + field + "_img").html(addImg);
            }
        }
        else {
            //  console.log(url.replace($('#webPath').val(), ""));

            addImg = '<img src="../../../' + url.replace($('#webPath').val(), "") + '"  >';
            var oEditor = CKEDITOR.instances[field];
            var html = addImg;

            var newElement = CKEDITOR.dom.element.createFromHtml(html, oEditor.document);
            oEditor.insertElement(newElement);
        }
    }
    //檔案
    else {
        var temp = url.split('/');
        var fileExName = temp[temp.length - 1];
        var id = guid();
        $('#' + field + '_img').html('<div class="fileData" data-file="' + url + '" id="file_' + id + '"><a href="' + url + '" target="_blank">' + decodeURI(fileExName) + '</a> <button type="button" class="delFiles" data-field="' + field + '" data-id="file_' + id + '"><i class="icon-trash"></i></button></div>');
    }
}

function setLoadPlayer(view) {
    if (view == 'none') {
        $.unblockUI();
    }
    else {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
    }
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

var keyStr = "ABCDEFGHIJKLMNOP" +
    "QRSTUVWXYZabcdef" +
    "ghijklmnopqrstuv" +
    "wxyz0123456789+/" +
    "=";