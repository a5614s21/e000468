namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_tops_to_cases_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.cases", "tops", c => c.String(maxLength: 1));
            Sql("execute sp_addextendedproperty 'MS_Description', N'�����خ�' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'tops'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.cases", "tops");
        }
    }
}
