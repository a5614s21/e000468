namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__WebData : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "facebook_url", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "facebook_url");
        }
    }
}
