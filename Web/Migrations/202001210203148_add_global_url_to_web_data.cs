namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_global_url_to_web_data : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "global_url", c => c.String());
            AddColumn("dbo.web_data", "mail_app_url", c => c.String());

            Sql("execute sp_addextendedproperty 'MS_Description', N'地產集團員工網' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'global_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'集團電子郵件平台' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'mail_app_url'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "mail_app_url");
            DropColumn("dbo.web_data", "global_url");
        }
    }
}
