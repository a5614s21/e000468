namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_constructions_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.constructions",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        notes = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        sortIndex = c.Int(),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'工法名稱' ,'SCHEMA', N'dbo','TABLE', N'constructions', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'備註' ,'SCHEMA', N'dbo','TABLE', N'constructions', 'COLUMN', N'notes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'詳細內容' ,'SCHEMA', N'dbo','TABLE', N'constructions', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'constructions', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'constructions', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'constructions', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'constructions', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'constructions', 'COLUMN', N'lang'");

        }
        
        public override void Down()
        {
            DropTable("dbo.constructions");
        }
    }
}
