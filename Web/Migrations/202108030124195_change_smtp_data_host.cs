namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_smtp_data_host : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.smtp_data", "host", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.smtp_data", "host", c => c.String(maxLength: 20));
        }
    }
}
