namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_cases_progress_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.cases_progress",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        category = c.String(),
                        post_date = c.DateTime(),
                        notes = c.String(),
                        content = c.String(),
                        files = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'工程名稱' ,'SCHEMA', N'dbo','TABLE', N'cases_progress', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'進度描述' ,'SCHEMA', N'dbo','TABLE', N'cases_progress', 'COLUMN', N'notes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'發布日期' ,'SCHEMA', N'dbo','TABLE', N'cases_progress', 'COLUMN', N'post_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'所屬建案' ,'SCHEMA', N'dbo','TABLE', N'cases_progress', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'cases_progress', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'工程照片' ,'SCHEMA', N'dbo','TABLE', N'cases_progress', 'COLUMN', N'pic'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'cases_progress', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'cases_progress', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'cases_progress', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'cases_progress', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'cases_progress', 'COLUMN', N'lang'");


            AddColumn("dbo.cases", "start_date", c => c.DateTime());
            AddColumn("dbo.cases", "end_date", c => c.DateTime());
            AddColumn("dbo.cases", "related", c => c.String());

            Sql("execute sp_addextendedproperty 'MS_Description', N'實際開工日期' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'start_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'預計完工日期' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'end_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'預留關聯' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'related'");

        }
        
        public override void Down()
        {
            DropColumn("dbo.cases", "related");
            DropColumn("dbo.cases", "end_date");
            DropColumn("dbo.cases", "start_date");
            DropTable("dbo.cases_progress");
        }
    }
}
