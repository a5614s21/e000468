namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_verify_to_news : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news", "verify", c => c.String(maxLength: 30));
            AddColumn("dbo.news", "verify_info", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'�f�֪��A' ,'SCHEMA', N'dbo','TABLE', N'news', 'COLUMN', N'verify'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'�f�ֻ�������' ,'SCHEMA', N'dbo','TABLE', N'news', 'COLUMN', N'verify_info'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.news", "verify_info");
            DropColumn("dbo.news", "verify");
        }
    }
}
