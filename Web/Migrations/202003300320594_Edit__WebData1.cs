namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__WebData1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "token_seconds", c => c.Int());
            AddColumn("dbo.web_data", "token_url", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "token_url");
            DropColumn("dbo.web_data", "token_seconds");
        }
    }
}
