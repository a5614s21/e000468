namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_index_info_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.index_info",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        sub_title = c.String(),
                        year = c.String(),
                        household = c.String(),
                        area = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'Banner標語' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'Banner副標語' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'sub_title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'年' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'year'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'累計戶數' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'household'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建設總面積' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'area'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'四大保證資訊' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'lang'");

        }
        
        public override void Down()
        {
            DropTable("dbo.index_info");
        }
    }
}
