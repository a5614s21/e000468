namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_re_to_contacts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.contacts", "re_date", c => c.DateTime());
            AddColumn("dbo.contacts", "re_content", c => c.String(storeType: "ntext"));
            AddColumn("dbo.contacts", "re_mail", c => c.String(maxLength: 1));
            AddColumn("dbo.urban_contacts", "re_date", c => c.DateTime());
            AddColumn("dbo.urban_contacts", "re_content", c => c.String(storeType: "ntext"));
            AddColumn("dbo.urban_contacts", "re_mail", c => c.String(maxLength: 1));

            Sql("execute sp_addextendedproperty 'MS_Description', N'回覆日期' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N're_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'處理敘述' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N're_content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'是否回信' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N're_mail'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'回覆日期' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N're_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'處理敘述' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N're_content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'是否回信' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N're_mail'");


        }

        public override void Down()
        {
            DropColumn("dbo.urban_contacts", "re_mail");
            DropColumn("dbo.urban_contacts", "re_content");
            DropColumn("dbo.urban_contacts", "re_date");
            DropColumn("dbo.contacts", "re_mail");
            DropColumn("dbo.contacts", "re_content");
            DropColumn("dbo.contacts", "re_date");
        }
    }
}
