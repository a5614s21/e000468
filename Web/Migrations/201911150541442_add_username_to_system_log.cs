namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_username_to_system_log : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.system_log", "username", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'�ϥΪ�' ,'SCHEMA', N'dbo','TABLE', N'system_log', 'COLUMN', N'username'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.system_log", "username");
        }
    }
}
