namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_about_investment_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.about_investment",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'事業名稱' ,'SCHEMA', N'dbo','TABLE', N'about_investment', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'詳細說明' ,'SCHEMA', N'dbo','TABLE', N'about_investment', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'about_investment', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'about_investment', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'about_investment', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'about_investment', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'SEO關鍵字' ,'SCHEMA', N'dbo','TABLE', N'about_investment', 'COLUMN', N'seo_keywords'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'SEO敘述' ,'SCHEMA', N'dbo','TABLE', N'about_investment', 'COLUMN', N'seo_description'");

        }
        
        public override void Down()
        {
            DropTable("dbo.about_investment");
        }
    }
}
