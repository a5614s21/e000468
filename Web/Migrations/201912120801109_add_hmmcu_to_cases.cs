namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_hmmcu_to_cases : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.cases", "hmmcu", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'建案(工地)代號' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'hmmcu'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.cases", "hmmcu");
        }
    }
}
