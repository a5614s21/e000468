namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_header_to_cases : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.cases", "header", c => c.String(maxLength: 1));
            AddColumn("dbo.cases", "header_pic", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'選單顯示' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'header'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'選單圖片' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'header_pic'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.cases", "header_pic");
            DropColumn("dbo.cases", "header");
        }
    }
}
