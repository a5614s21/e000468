namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__WebData2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "token_timeout", c => c.Int());
            DropColumn("dbo.web_data", "token_seconds");
        }
        
        public override void Down()
        {
            AddColumn("dbo.web_data", "token_seconds", c => c.Int());
            DropColumn("dbo.web_data", "token_timeout");
        }
    }
}
