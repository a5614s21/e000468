﻿namespace Web.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Web;
    using System.Web.Hosting;
    using Web.Models;
    using Web.Migrations.Seed;

    internal sealed class Configuration : DbMigrationsConfiguration<Web.Models.Model>
    {

        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            //AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Web.Models.Model context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.


            //系統基本資訊(選單權限等)
            SystemSeeder systemSeeder = new SystemSeeder();
            systemSeeder.run();

            //信件樣板
            MailContentsSeeder mailContentsSeeder = new MailContentsSeeder();
            mailContentsSeeder.run();

            //關於我們
            AboutsSeeder aboutsSeeder = new AboutsSeeder();
            aboutsSeeder.run();

            //最新消息 + 分類
            NewsSeeder newsSeeder = new NewsSeeder();
            newsSeeder.run();


            //熱銷建案(包含年度總攬)
            CasesSeeder casesSeeder = new CasesSeeder();
            casesSeeder.run();                      


            //公益活動
            CharitysSeeder charitysSeeder = new CharitysSeeder();
            charitysSeeder.run();            

            //得獎記事
            AwardsSeeder awardsSeeder = new AwardsSeeder();
            awardsSeeder.run();

            //客戶專區 
            CustomerSeeder communitysSeeder = new CustomerSeeder();
            communitysSeeder.run();

            //都市更新
            UrbanSeeder urbanSeeder = new UrbanSeeder();
            urbanSeeder.run();

            //投資人專區
            InvestorSeeder investorSeeder = new InvestorSeeder();
            investorSeeder.run();

            //其他資訊頁面
            NotesSeeder notesSeeder = new NotesSeeder();
            notesSeeder.run();

            //會員專區
            MemberSeeder memberSeeder = new MemberSeeder();
            memberSeeder.run();

            //表單項目
            FormSubjectSeeder formSubjectSeeder = new FormSubjectSeeder();
            formSubjectSeeder.run();

            HomeSeeder homeSeeder = new HomeSeeder();
            homeSeeder.run();

            //台灣縣市
            TaiwanSeeder taiwanSeeder = new TaiwanSeeder();
            taiwanSeeder.run();

        }
        

    }
}
