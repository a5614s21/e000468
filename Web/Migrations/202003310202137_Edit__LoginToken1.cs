namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__LoginToken1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.logintokens", "timeout_date", c => c.DateTime());
            DropColumn("dbo.logintokens", "createdate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.logintokens", "createdate", c => c.DateTime());
            DropColumn("dbo.logintokens", "timeout_date");
        }
    }
}
