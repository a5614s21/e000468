namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_shareholders_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.shareholders",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        link = c.String(),
                        content = c.String(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'shareholders', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'證交所連結' ,'SCHEMA', N'dbo','TABLE', N'shareholders', 'COLUMN', N'link'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'股權結構' ,'SCHEMA', N'dbo','TABLE', N'shareholders', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'shareholders', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'shareholders', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'shareholders', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'shareholders', 'COLUMN', N'lang'");

        }
        
        public override void Down()
        {
            DropTable("dbo.shareholders");
        }
    }
}
