namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_finance_report_momth_to_int : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.finance_report", "month", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.finance_report", "month", c => c.String());
        }
    }
}
