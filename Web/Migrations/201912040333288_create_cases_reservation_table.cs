namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_cases_reservation_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.cases_reservation",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        name = c.String(),
                        category = c.String(),
                        cases = c.String(),
                        tel = c.String(),
                        email = c.String(),
                        need = c.String(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        re_date = c.DateTime(),
                        re_content = c.String(storeType: "ntext"),
                        re_mail = c.String(maxLength: 1),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'姓名' ,'SCHEMA', N'dbo','TABLE', N'cases_reservation', 'COLUMN', N'name'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'聯絡電話' ,'SCHEMA', N'dbo','TABLE', N'cases_reservation', 'COLUMN', N'tel'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'Email' ,'SCHEMA', N'dbo','TABLE', N'cases_reservation', 'COLUMN', N'email'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'預約需求' ,'SCHEMA', N'dbo','TABLE', N'cases_reservation', 'COLUMN', N'need'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'預約建案' ,'SCHEMA', N'dbo','TABLE', N'cases_reservation', 'COLUMN', N'cases'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建案ID' ,'SCHEMA', N'dbo','TABLE', N'cases_reservation', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'cases_reservation', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'回覆日期' ,'SCHEMA', N'dbo','TABLE', N'cases_reservation', 'COLUMN', N're_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'處理敘述' ,'SCHEMA', N'dbo','TABLE', N'cases_reservation', 'COLUMN', N're_content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'是否回信' ,'SCHEMA', N'dbo','TABLE', N'cases_reservation', 'COLUMN', N're_mail'");

        }
        
        public override void Down()
        {
            DropTable("dbo.cases_reservation");
        }
    }
}
