namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_verify_to_any_tables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.about_houses", "verify", c => c.String(maxLength: 30));
            AddColumn("dbo.about_houses", "verify_info", c => c.String());
            AddColumn("dbo.cases", "verify", c => c.String(maxLength: 30));
            AddColumn("dbo.cases", "verify_info", c => c.String());
            AddColumn("dbo.finance_report", "verify", c => c.String(maxLength: 30));
            AddColumn("dbo.finance_report", "verify_info", c => c.String());
            AddColumn("dbo.urban_column", "verify", c => c.String(maxLength: 30));
            AddColumn("dbo.urban_column", "verify_info", c => c.String());

            Sql("execute sp_addextendedproperty 'MS_Description', N'�f�֪��A' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'verify'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'�f�ֻ�������' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'verify_info'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'�f�֪��A' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'verify'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'�f�ֻ�������' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'verify_info'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'�f�֪��A' ,'SCHEMA', N'dbo','TABLE', N'finance_report', 'COLUMN', N'verify'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'�f�ֻ�������' ,'SCHEMA', N'dbo','TABLE', N'finance_report', 'COLUMN', N'verify_info'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'�f�֪��A' ,'SCHEMA', N'dbo','TABLE', N'urban_column', 'COLUMN', N'verify'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'�f�ֻ�������' ,'SCHEMA', N'dbo','TABLE', N'urban_column', 'COLUMN', N'verify_info'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.urban_column", "verify_info");
            DropColumn("dbo.urban_column", "verify");
            DropColumn("dbo.finance_report", "verify_info");
            DropColumn("dbo.finance_report", "verify");
            DropColumn("dbo.cases", "verify_info");
            DropColumn("dbo.cases", "verify");
            DropColumn("dbo.about_houses", "verify_info");
            DropColumn("dbo.about_houses", "verify");
        }
    }
}
