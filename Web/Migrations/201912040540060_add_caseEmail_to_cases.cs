namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_caseEmail_to_cases : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "CaseEmail", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'預約賞屋收件信箱' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'CaseEmail'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "CaseEmail");
        }
    }
}
