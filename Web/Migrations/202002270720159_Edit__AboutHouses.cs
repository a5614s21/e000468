namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__AboutHouses : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.about_houses", "pic", c => c.String());
            AddColumn("dbo.about_houses", "pic_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.about_houses", "pic_alt");
            DropColumn("dbo.about_houses", "pic");
        }
    }
}
