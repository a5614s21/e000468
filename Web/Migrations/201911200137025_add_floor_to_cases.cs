namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_floor_to_cases : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.cases", "floor", c => c.String());
            AddColumn("dbo.cases", "pings", c => c.String());
            AddColumn("dbo.cases", "index_pic", c => c.String());
            AddColumn("dbo.cases", "index_pic_alt", c => c.String());

            Sql("execute sp_addextendedproperty 'MS_Description', N'樓層' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'floor'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'規劃坪數' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'pings'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'首頁用展示圖' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'index_pic'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.cases", "index_pic_alt");
            DropColumn("dbo.cases", "index_pic");
            DropColumn("dbo.cases", "pings");
            DropColumn("dbo.cases", "floor");
        }
    }
}
