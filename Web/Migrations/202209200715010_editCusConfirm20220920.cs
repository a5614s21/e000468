namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editCusConfirm20220920 : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.cus_problem");
            DropTable("dbo.online_building");
            DropTable("dbo.pay_record");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.pay_record",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        typeoption = c.String(),
                        takenote = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.online_building",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        typeoption = c.String(),
                        takenote = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.cus_problem",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        typeoption = c.String(),
                        takenote = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
    }
}
