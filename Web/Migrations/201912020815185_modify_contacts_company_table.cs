namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modify_contacts_company_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.contacts", "company", c => c.String());
            DropColumn("dbo.contacts", "compony");
        }
        
        public override void Down()
        {
            AddColumn("dbo.contacts", "compony", c => c.String());
            DropColumn("dbo.contacts", "company");
        }
    }
}
