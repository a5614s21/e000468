namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__Payment_ApiLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.payment_apilog",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        username = c.String(),
                        ip = c.String(),
                        status = c.String(),
                        create_date = c.DateTime(),
                        data = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.payment_apilog");
        }
    }
}
