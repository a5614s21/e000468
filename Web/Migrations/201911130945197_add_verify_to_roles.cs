namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_verify_to_roles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.roles", "verify", c => c.String(maxLength: 1));
            Sql("execute sp_addextendedproperty 'MS_Description', N'�f�֪��A' ,'SCHEMA', N'dbo','TABLE', N'roles', 'COLUMN', N'verify'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.roles", "verify");
        }
    }
}
