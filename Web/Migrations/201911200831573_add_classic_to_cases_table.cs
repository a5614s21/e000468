namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_classic_to_cases_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.cases", "classic", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'經典建案分類' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'classic'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.cases", "classic");
        }
    }
}
