namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_notes_to_news_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news", "notes", c => c.String());
            AddColumn("dbo.news", "tops", c => c.String(maxLength: 1));

            Sql("execute sp_addextendedproperty 'MS_Description', N'�C��²�z' ,'SCHEMA', N'dbo','TABLE', N'news', 'COLUMN', N'notes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'�����T��' ,'SCHEMA', N'dbo','TABLE', N'news', 'COLUMN', N'tops'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.news", "tops");
            DropColumn("dbo.news", "notes");
        }
    }
}
