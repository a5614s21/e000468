namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Delete__LoginToken : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.logintokens");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.logintokens",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        access_token = c.String(),
                        refresh_token = c.String(),
                        timeout_date = c.DateTime(),
                    })
                .PrimaryKey(t => t.id);
            
        }
    }
}
