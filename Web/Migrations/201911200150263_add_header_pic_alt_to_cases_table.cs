namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_header_pic_alt_to_cases_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.cases", "header_pic_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.cases", "header_pic_alt");
        }
    }
}
