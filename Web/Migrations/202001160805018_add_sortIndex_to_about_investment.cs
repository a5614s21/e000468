namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_sortIndex_to_about_investment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.about_investment", "sortIndex", c => c.Int());
            Sql("execute sp_addextendedproperty 'MS_Description', N'�Ƨ�' ,'SCHEMA', N'dbo','TABLE', N'about_investment', 'COLUMN', N'sortIndex'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.about_investment", "sortIndex");
        }
    }
}
