namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_about_manages_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.about_manages",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        notes = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'about_manages', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'備註' ,'SCHEMA', N'dbo','TABLE', N'about_manages', 'COLUMN', N'notes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'about_manages', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'about_manages', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'圖片' ,'SCHEMA', N'dbo','TABLE', N'about_manages', 'COLUMN', N'pic'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'about_manages', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'about_manages', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'about_manages', 'COLUMN', N'lang'");


            CreateTable(
                "dbo.regulations",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        files = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'項目' ,'SCHEMA', N'dbo','TABLE', N'regulations', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'PDF檔案' ,'SCHEMA', N'dbo','TABLE', N'regulations', 'COLUMN', N'files'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'regulations', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'regulations', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'regulations', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'regulations', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'regulations', 'COLUMN', N'lang'");


            CreateTable(
                "dbo.resolutions",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        post_date = c.DateTime(),
                        files = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'會議名稱' ,'SCHEMA', N'dbo','TABLE', N'resolutions', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'檔案日期' ,'SCHEMA', N'dbo','TABLE', N'resolutions', 'COLUMN', N'post_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'PDF檔案' ,'SCHEMA', N'dbo','TABLE', N'resolutions', 'COLUMN', N'files'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'resolutions', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'resolutions', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'resolutions', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'resolutions', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'resolutions', 'COLUMN', N'lang'");

        }
        
        public override void Down()
        {
            DropTable("dbo.resolutions");
            DropTable("dbo.regulations");
            DropTable("dbo.about_manages");
        }
    }
}
