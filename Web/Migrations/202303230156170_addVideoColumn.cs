namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addVideoColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.index_info", "video", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.index_info", "video");
        }
    }
}
