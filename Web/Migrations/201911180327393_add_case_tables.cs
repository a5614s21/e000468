namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_case_tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.cases",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        sub_title = c.String(),
                        year = c.String(),
                        category = c.String(maxLength: 64),
                        notes = c.String(),
                        content = c.String(),
                        phone = c.String(),
                        address = c.String(),
                        url = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        list_pic = c.String(),
                        list_pic_alt = c.String(),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'建案名稱' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建案標語' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'sub_title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'西元年' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'year'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建案區域' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'備註' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'notes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'電話' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'phone'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'位置' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'address'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建案連結' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'列表圖' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'list_pic'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'圖片' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'pic'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'lang'");

            CreateTable(
                "dbo.cases_category",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'分類名稱' ,'SCHEMA', N'dbo','TABLE', N'cases_category', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'cases_category', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'cases_category', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'cases_category', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'圖片' ,'SCHEMA', N'dbo','TABLE', N'cases_category', 'COLUMN', N'pic'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'cases_category', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'cases_category', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'cases_category', 'COLUMN', N'lang'");


        }

        public override void Down()
        {
            DropTable("dbo.cases_category");
            DropTable("dbo.cases");
        }
    }
}
