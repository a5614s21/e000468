namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__LoginToken : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.logintokens",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        token = c.String(),
                        user_id = c.String(),
                        createdate = c.DateTime(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.logintokens");
        }
    }
}
