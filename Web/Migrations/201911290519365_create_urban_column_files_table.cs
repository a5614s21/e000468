namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_urban_column_files_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.urban_column_files",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        category = c.String(),
                        files = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'urban_column_files', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'檔案' ,'SCHEMA', N'dbo','TABLE', N'urban_column_files', 'COLUMN', N'files'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'所屬都更' ,'SCHEMA', N'dbo','TABLE', N'urban_column_files', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'urban_column_files', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'urban_column_files', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'urban_column_files', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'urban_column_files', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'urban_column_files', 'COLUMN', N'lang'");
        }
        
        public override void Down()
        {
            DropTable("dbo.urban_column_files");
        }
    }
}
