namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_urban_contacts_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.urban_contacts",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        name = c.String(),
                        email = c.String(),
                        tel = c.String(),
                        fax = c.String(),
                        house_status = c.String(),
                        address = c.String(),
                        residential = c.String(),
                        business = c.String(),
                        land_number = c.String(),
                        area = c.String(),
                        subject = c.String(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        content = c.String(storeType: "ntext"),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'姓名' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N'name'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'聯絡電話' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N'tel'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'Email' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N'email'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'傳真電話' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N'fax'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'房屋(土地)狀況' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N'house_status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'地址' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N'address'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'住宅區' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N'residential'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'商業區' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N'business'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'土地地段地號' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N'land_number'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'基地面積' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N'area'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'其他' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'urban_contacts', 'COLUMN', N'status'");

        }
        
        public override void Down()
        {
            DropTable("dbo.urban_contacts");
        }
    }
}
