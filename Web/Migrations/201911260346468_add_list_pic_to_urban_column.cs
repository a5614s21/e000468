namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_list_pic_to_urban_column : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.urban_column", "list_pic", c => c.String());
            AddColumn("dbo.urban_column", "list_pic_alt", c => c.String());

            Sql("execute sp_addextendedproperty 'MS_Description', N'�C����' ,'SCHEMA', N'dbo','TABLE', N'urban_column', 'COLUMN', N'list_pic'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.urban_column", "list_pic_alt");
            DropColumn("dbo.urban_column", "list_pic");
        }
    }
}
