namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_files_to_case_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.cases", "files", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'����' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'files'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.cases", "files");
        }
    }
}
