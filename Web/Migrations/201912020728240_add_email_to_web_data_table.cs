namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_email_to_web_data_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "ContactEmail", c => c.String());
            AddColumn("dbo.web_data", "UrbanContactEmail", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'客戶專區收件信箱' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'ContactEmail'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'都更收件信箱' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'UrbanContactEmail'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "UrbanContactEmail");
            DropColumn("dbo.web_data", "ContactEmail");
        }
    }
}
