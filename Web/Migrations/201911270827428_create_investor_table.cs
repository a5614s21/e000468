namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_investor_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.finance_report",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        year = c.String(),
                        month = c.String(),
                        files = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        types = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'finance_report', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'西元年' ,'SCHEMA', N'dbo','TABLE', N'finance_report', 'COLUMN', N'year'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'月份' ,'SCHEMA', N'dbo','TABLE', N'finance_report', 'COLUMN', N'month'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'PDF檔案' ,'SCHEMA', N'dbo','TABLE', N'finance_report', 'COLUMN', N'files'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'finance_report', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'finance_report', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'finance_report', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'finance_report', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'finance_report', 'COLUMN', N'lang'");

            CreateTable(
                "dbo.investor_info",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        notes = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'investor_info', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'investor_info', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'investor_info', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'圖片' ,'SCHEMA', N'dbo','TABLE', N'investor_info', 'COLUMN', N'pic'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'investor_info', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'investor_info', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'investor_info', 'COLUMN', N'lang'");
        }

    
        
        public override void Down()
        {
            DropTable("dbo.investor_info");
            DropTable("dbo.finance_report");
        }
    }
}
