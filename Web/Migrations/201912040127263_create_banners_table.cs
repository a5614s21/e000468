namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_banners_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.banners",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        link = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'banners', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'圖片' ,'SCHEMA', N'dbo','TABLE', N'banners', 'COLUMN', N'pic'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'連結' ,'SCHEMA', N'dbo','TABLE', N'banners', 'COLUMN', N'link'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'banners', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'banners', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'banners', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'banners', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'banners', 'COLUMN', N'lang'");
        }
        
        public override void Down()
        {
            DropTable("dbo.banners");
        }
    }
}
