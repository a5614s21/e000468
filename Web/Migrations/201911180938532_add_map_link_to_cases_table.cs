namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_map_link_to_cases_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.cases", "map_link", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'�a�ϳs��' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'map_link'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.cases", "map_link");
        }
    }
}
