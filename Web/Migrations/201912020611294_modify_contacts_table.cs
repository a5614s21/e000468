namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modify_contacts_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.contacts", "name", c => c.String());
            AddColumn("dbo.contacts", "compony", c => c.String());
            AddColumn("dbo.contacts", "tel", c => c.String());
            AddColumn("dbo.contacts", "email", c => c.String());
            AddColumn("dbo.contacts", "subject", c => c.String());
            DropColumn("dbo.contacts", "title");
            DropColumn("dbo.contacts", "sortIndex");
            DropColumn("dbo.contacts", "en_title");

            Sql("execute sp_addextendedproperty 'MS_Description', N'姓名' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'name'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'公司名稱' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'compony'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'聯絡電話' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'tel'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'Email' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'email'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'主題' ,'SCHEMA', N'dbo','TABLE', N'contacts', 'COLUMN', N'subject'");
        }
        
        public override void Down()
        {
            AddColumn("dbo.contacts", "en_title", c => c.String(maxLength: 255));
            AddColumn("dbo.contacts", "sortIndex", c => c.Int());
            AddColumn("dbo.contacts", "title", c => c.String());
            DropColumn("dbo.contacts", "subject");
            DropColumn("dbo.contacts", "email");
            DropColumn("dbo.contacts", "tel");
            DropColumn("dbo.contacts", "compony");
            DropColumn("dbo.contacts", "name");
        }
    }
}
