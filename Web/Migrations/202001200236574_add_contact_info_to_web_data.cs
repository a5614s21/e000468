namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_contact_info_to_web_data : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "contact_info", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'�}�o���߸�T' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'contact_info'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "contact_info");
        }
    }
}
