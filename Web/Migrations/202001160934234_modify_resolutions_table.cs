namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modify_resolutions_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.resolutions", "content", c => c.String());
            DropColumn("dbo.resolutions", "post_date");
            DropColumn("dbo.resolutions", "files");

            Sql("execute sp_addextendedproperty 'MS_Description', N'���e' ,'SCHEMA', N'dbo','TABLE', N'resolutions', 'COLUMN', N'content'");
        }
        
        public override void Down()
        {
            AddColumn("dbo.resolutions", "files", c => c.String());
            AddColumn("dbo.resolutions", "post_date", c => c.DateTime());
            DropColumn("dbo.resolutions", "content");
        }
    }
}
