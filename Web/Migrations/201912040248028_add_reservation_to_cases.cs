namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_reservation_to_cases : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.cases", "reservation", c => c.String(maxLength: 1));
            Sql("execute sp_addextendedproperty 'MS_Description', N'�w��Ų��' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'reservation'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.cases", "reservation");
        }
    }
}
