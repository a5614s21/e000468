namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_get_mail_to_case_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.cases", "get_mail", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'預約賞屋收件信箱' ,'SCHEMA', N'dbo','TABLE', N'cases', 'COLUMN', N'get_mail'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.cases", "get_mail");
        }
    }
}
