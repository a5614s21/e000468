namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_about_houses_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.about_houses",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        year = c.String(),
                        news_file1 = c.String(),
                        news_file2 = c.String(),
                        news_file3 = c.String(),
                        news_file4 = c.String(),
                        season_file1 = c.String(),
                        season_file2 = c.String(),
                        season_file3 = c.String(),
                        season_file4 = c.String(),
                        files = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'年度' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'title'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'新聞稿Q1' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'news_file1'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'新聞稿Q2' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'news_file2'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'新聞稿Q3' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'news_file3'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'新聞稿Q4' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'news_file4'");


            Sql("execute sp_addextendedproperty 'MS_Description', N'季報Q1' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'season_file1'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'季報Q2' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'season_file2'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'季報Q3' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'season_file3'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'季報Q4' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'season_file4'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'市場回顧(PDF)' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'files'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'lang'");
        }
        
        public override void Down()
        {
            DropTable("dbo.about_houses");
        }
    }
}
