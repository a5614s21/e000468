namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__LoginToken : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.logintokens", "access_token", c => c.String());
            AddColumn("dbo.logintokens", "refresh_token", c => c.String());
            DropColumn("dbo.logintokens", "token");
            DropColumn("dbo.logintokens", "user_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.logintokens", "user_id", c => c.String());
            AddColumn("dbo.logintokens", "token", c => c.String());
            DropColumn("dbo.logintokens", "refresh_token");
            DropColumn("dbo.logintokens", "access_token");
        }
    }
}
