namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_detect_file_to_about_house : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.about_houses", "detect_file1", c => c.String());
            AddColumn("dbo.about_houses", "detect_file2", c => c.String());
            AddColumn("dbo.about_houses", "detect_file3", c => c.String());
            AddColumn("dbo.about_houses", "detect_file4", c => c.String());

            Sql("execute sp_addextendedproperty 'MS_Description', N'�˴�Q1' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'detect_file1'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'�˴�Q2' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'detect_file2'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'�˴�Q3' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'detect_file3'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'�˴�Q4' ,'SCHEMA', N'dbo','TABLE', N'about_houses', 'COLUMN', N'detect_file4'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.about_houses", "detect_file4");
            DropColumn("dbo.about_houses", "detect_file3");
            DropColumn("dbo.about_houses", "detect_file2");
            DropColumn("dbo.about_houses", "detect_file1");
        }
    }
}
