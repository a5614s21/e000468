﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.Migrations;
using Newtonsoft.Json;

namespace Web.Migrations.Seed
{
    public class MemberSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {           
         
            faq();
        }
    
        public void faq()
        {
            //最新消息
            var member_faq = new List<member_faq>
            {
                 new member_faq {
                    guid = "1",
                    sortIndex = 1,
                    title = "都市更新是什麼？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",                  
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                  new member_faq {
                    guid = "1",
                    sortIndex = 1,
                    title = "都市更新是什麼？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new member_faq {
                    guid = "2",
                    sortIndex = 2,
                    title = "辦都市更新的好處有哪些？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                    new member_faq {
                    guid = "2",
                    sortIndex = 2,
                    title = "辦都市更新的好處有哪些？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new member_faq {
                    guid = "3",
                    sortIndex = 3,
                    title = "都市更新計畫和都市更新事業計畫有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                   new member_faq {
                    guid = "3",
                    sortIndex = 3,
                    title = "都市更新計畫和都市更新事業計畫有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new member_faq {
                    guid = "4",
                    sortIndex = 4,
                    title = "權利變換和一般合建有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                   new member_faq {
                    guid = "4",
                    sortIndex = 4,
                    title = "權利變換和一般合建有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },


                   new member_faq {
                    guid = "5",
                    sortIndex = 5,
                    title = "都市更新是什麼？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                  new member_faq {
                    guid = "6",
                    sortIndex = 6,
                    title = "都市更新是什麼？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new member_faq {
                    guid = "7",
                    sortIndex = 7,
                    title = "辦都市更新的好處有哪些？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                    new member_faq {
                    guid = "8",
                    sortIndex = 8,
                    title = "辦都市更新的好處有哪些？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new member_faq {
                    guid = "9",
                    sortIndex = 9,
                    title = "都市更新計畫和都市更新事業計畫有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                   new member_faq {
                    guid = "10",
                    sortIndex = 10,
                    title = "都市更新計畫和都市更新事業計畫有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new member_faq {
                    guid = "11",
                    sortIndex = 11,
                    title = "權利變換和一般合建有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                   new member_faq {
                    guid = "12",
                    sortIndex =12,
                    title = "權利變換和一般合建有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },


            };
            if (context.member_faq.ToList().Count == 0)
            {
                member_faq.ForEach(s => context.member_faq.Add(s));
                context.SaveChanges();
            }
        }


        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}