﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.Migrations;

namespace Web.Migrations.Seed
{
    public class SystemSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();
        public string sysadminGroupGuid = Guid.NewGuid().ToString();
        public string adminGroupGuid = Guid.NewGuid().ToString();

        public string m1 = Guid.NewGuid().ToString();
        public string m2 = Guid.NewGuid().ToString();
        public string m3 = Guid.NewGuid().ToString();
        public string m4 = Guid.NewGuid().ToString();
        public string m5 = Guid.NewGuid().ToString();
        public string m6 = Guid.NewGuid().ToString();
        public string m7 = Guid.NewGuid().ToString();
        public string m8 = Guid.NewGuid().ToString();
        public string m9 = Guid.NewGuid().ToString();
        public string m10 = Guid.NewGuid().ToString();
        public string m11 = Guid.NewGuid().ToString();
        public string m12 = Guid.NewGuid().ToString();
        public string m13 = Guid.NewGuid().ToString();

        public void run()
        {
            users();
            roles_data();
            system_menu();
            role_per();
            langs();
            smtp();
            web_data();
            system_data();
        }


        public void users()
        {
          

            var Users = new List<user>
             {
                 new user { guid = "001",   username = "sysadmin", password = "HpUf8bETpVZWULhCfdbVIg==", rolename = "" , role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",
                            name = "系統管理員" , email = "design7@e-creative.tw", phone = "", mobile="", address="", note="",status="Y",
                            logindate = DateTime.Now , modifydate = DateTime.Now ,create_date = DateTime.Now  },

                 new user { guid = "002",   username = "admin", password = "4fPH8SWr/V1MMXXg3X8p9A==", rolename = "" , role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",
                            name = "系統管理員" , email = "design7@e-creative.tw", phone = "", mobile="", address="", note="",status="Y",
                            logindate = DateTime.Now , modifydate = DateTime.Now ,create_date = DateTime.Now  }

             };
            if (context.user.ToList().Count == 0)
            {
                Users.ForEach(s => context.user.Add(s));
                context.SaveChanges();
            }
        }

        public void roles_data()
        {
            //群組
            var Roles = new List<roles>
            {
                new roles { guid = sysadminGroupGuid,   title = "總管理", site="", rolenote = "",status="Y", modifydate = DateTime.Now ,create_date = DateTime.Now ,verify = "Y" },
                new roles { guid = adminGroupGuid,   title = "查核人員", site="", rolenote = "",status="Y", modifydate = DateTime.Now ,create_date = DateTime.Now ,verify = "N" },

            };
            if (context.roles.ToList().Count == 0)
            {
                Roles.ForEach(s => context.roles.Add(s));
                context.SaveChanges();
            }
        }

        public void system_menu()
        {
            //選單
            var System_menu = new List<system_menu>
            {
                new system_menu { guid = m1, title = "內容管理", category = "0",tables = "",sortindex=1,status="Y",act_path="",icon="icon-file-text",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "" },
                new system_menu { guid = m2, title = "熱銷建案", category = "0",tables = "",sortindex=2,status="Y",act_path="",icon="icon-home",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "" },
                new system_menu { guid = m3, title = "客戶專區", category = "0",tables = "",sortindex=3,status="Y",act_path="",icon="icon-users",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "" },
                new system_menu { guid = m4, title = "都市更新", category = "0",tables = "",sortindex=4,status="Y",act_path="",icon="icon-handshake-o",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "" },
                new system_menu { guid = m5, title = "企業社會責任", category = "0",tables = "",sortindex=5,status="Y",act_path="",icon="icon-book ",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "" },
                new system_menu { guid = m6, title = "投資人專區", category = "0",tables = "",sortindex=6,status="Y",act_path="",icon="icon-sphere",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "" },
                new system_menu { guid = m7, title = "資訊頁面", category = "0",tables = "",sortindex=7,status="Y",act_path="",icon="icon-man",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "" },
              
                new system_menu { guid = m9, title = "首頁管理", category = "0",tables = "",sortindex=8,status="Y",act_path="",icon="icon-map",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "" },
                new system_menu { guid = m10, title = "控制台", category = "0",tables = "",sortindex=1,status="Y",act_path="",icon="icon-cog",area="system",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "" },
                new system_menu { guid = m11, title = "帳戶資訊", category = "0",tables = "",sortindex=2,status="Y",act_path="",icon="icon-person_pin",area="system",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                new system_menu { guid = m12, title = "系統整合", category = "0",tables = "",sortindex=3,status="Y",act_path="",icon="icon-handshake-o",area="system",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                new system_menu { guid = m13, title = "模組管理", category = "0",tables = "",sortindex=99,status="Y",act_path="",icon="icon-filter",area="admin",category_table="",index_view_url="", can_add = "Y" ,can_edit = "N",can_del="Y" ,prev_table = "" },



                new system_menu { guid = Guid.NewGuid().ToString(), title = "關於國建", category = m1,tables = "abouts",sortindex=1,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N"  ,prev_table = ""},

                new system_menu { guid = Guid.NewGuid().ToString(), title = "最新消息分類", category = m1,tables = "news_category",sortindex=10,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                new system_menu { guid = Guid.NewGuid().ToString(), title = "最新消息", category = m1,tables = "news",sortindex=11,status="Y",act_path="list",icon="",area="",category_table="news_category",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},

             

                new system_menu { guid = Guid.NewGuid().ToString(), title = "公司治理靜態資訊", category = m1,tables = "about_manages",sortindex=70,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N"  ,prev_table = ""},
                new system_menu { guid = Guid.NewGuid().ToString(), title = "重要公司內規", category = m1,tables = "regulations",sortindex=71,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                new system_menu { guid = Guid.NewGuid().ToString(), title = "董事會決議事項", category = m1,tables = "resolutions",sortindex=72,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "投資事業", category = m1,tables = "about_investment",sortindex=77,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "得獎記事", category = m1,tables = "awards",sortindex=78,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "房地產指數", category = m1,tables = "about_houses",sortindex=79,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},

              

                new system_menu { guid = Guid.NewGuid().ToString(), title = "公益活動分類", category = m1,tables = "charity_category",sortindex=90,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                new system_menu { guid = Guid.NewGuid().ToString(), title = "公益活動", category = m1,tables = "charitys",sortindex=91,status="Y",act_path="list",icon="",area="",category_table="charity_category",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},


                //建案
                new system_menu { guid = Guid.NewGuid().ToString(), title = "建案地區", category = m2,tables = "cases_category",sortindex=1,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                new system_menu { guid = Guid.NewGuid().ToString(), title = "經典建案分類", category = m2,tables = "cases_classic",sortindex=2,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                new system_menu { guid = Guid.NewGuid().ToString(), title = "熱銷建案", category = m2,tables = "cases",sortindex=3,status="Y",act_path="list",icon="",area="",category_table="cases_category",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                new system_menu { guid = Guid.NewGuid().ToString(), title = "年度總覽", category = m2,tables = "annuals",sortindex=4,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                new system_menu { guid = Guid.NewGuid().ToString(), title = "購屋試算說明", category = m2,tables = "notes_data",sortindex=5,status="Y",act_path="edit/4",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N"  ,prev_table = ""},

                new system_menu { guid = Guid.NewGuid().ToString(), title = "工程進度", category = m2,tables = "cases_progress",sortindex=98,status="N",act_path="list",icon="",area="",category_table="cases",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                new system_menu { guid = Guid.NewGuid().ToString(), title = "預約賞屋", category = m2,tables = "cases_reservation",sortindex=99,status="N",act_path="list",icon="",area="",category_table="cases",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},



                //客戶專區
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "建築工法", category = m3,tables = "constructions",sortindex=1,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},

                new system_menu { guid = Guid.NewGuid().ToString(), title = "社區關懷分類", category = m3,tables = "community_category",sortindex=10,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                new system_menu { guid = Guid.NewGuid().ToString(), title = "社區關懷", category = m3,tables = "communitys",sortindex=11,status="Y",act_path="list",icon="",area="",category_table="community_category",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},

                new system_menu { guid = Guid.NewGuid().ToString(), title = "居家常識", category = m3,tables = "knowledges",sortindex=12,status="Y",act_path="list",icon="",area="",category_table="community_category",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                new system_menu { guid = Guid.NewGuid().ToString(), title = "聯絡資訊", category = m3,tables = "contact_info",sortindex=20,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},

                new system_menu { guid = Guid.NewGuid().ToString(), title = "會員專區常見問題", category = m3,tables = "member_faq",sortindex=21,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
               
                new system_menu { guid = Guid.NewGuid().ToString(), title = "線上諮詢", category = m3,tables = "contacts",sortindex=22,status="Y",act_path="list/1",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},

                new system_menu { guid = Guid.NewGuid().ToString(), title = "聯絡表單主題", category = m3,tables = "form_subject",sortindex=23,status="Y",act_path="list/1",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},

                new system_menu { guid = Guid.NewGuid().ToString(), title = "尊榮優惠", category = m3,tables = "notes_data",sortindex=5,status="Y",act_path="edit/5",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N"  ,prev_table = ""},
 



                 //都市更新
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "都市更新消息", category = m4,tables = "urban_news",sortindex=10,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "都市更新Q & A", category = m4,tables = "urban_faq",sortindex=11,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "都市更新專區分類", category = m4,tables = "urban_column_category",sortindex=12,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "都市更新專區", category = m4,tables = "urban_column",sortindex=13,status="Y",act_path="list",icon="",area="",category_table="urban_column_category",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "都更法令知識庫", category = m4,tables = "urban_news",sortindex=14,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
              
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "都更相關檔案", category = m4,tables = "urban_column_files",sortindex=13,status="N",act_path="list",icon="",area="",category_table="urban_column",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},

                   new system_menu { guid = Guid.NewGuid().ToString(), title = "都更線上諮詢", category = m4,tables = "urban_contacts",sortindex=20,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},


                 new system_menu { guid = Guid.NewGuid().ToString(), title = "表單-房屋(土地)狀況", category = m4,tables = "form_subject",sortindex=21,status="Y",act_path="list/5",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "表單-住宅區", category = m4,tables = "form_subject",sortindex=22,status="Y",act_path="list/2",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "表單-商業區", category = m4,tables = "form_subject",sortindex=23,status="Y",act_path="list/3",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "表單-整合情況", category = m4,tables = "form_subject",sortindex=24,status="Y",act_path="list/4",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                

                 //投資人專區
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "靜態內容", category = m6,tables = "investor_info",sortindex=1,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "月營收報告", category = m6,tables = "finance_report",sortindex=10,status="Y",act_path="list/1",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "財務報表", category = m6,tables = "finance_report",sortindex=11,status="Y",act_path="list/2",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "公司年報", category = m6,tables = "finance_report",sortindex=12,status="Y",act_path="list/3",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "股權結構", category = m6,tables = "shareholders",sortindex=13,status="Y",act_path="edit/1",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "股東常會", category = m6,tables = "share_download",sortindex=14,status="Y",act_path="list/1",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "股東辦理事項表單", category = m6,tables = "share_download",sortindex=15,status="Y",act_path="list/2",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "投資人問答集", category = m6,tables = "share_faq",sortindex=16,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "其他", category = m6,tables = "notes_data",sortindex=20,status="Y",act_path="edit/6",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N"  ,prev_table = ""},
 

                 //資訊頁面
                new system_menu { guid = Guid.NewGuid().ToString(), title = "人才招募", category = m7,tables = "notes_data",sortindex=1,status="Y",act_path="edit/1",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N"  ,prev_table = ""},
                new system_menu { guid = Guid.NewGuid().ToString(), title = "隱私權政策", category = m7,tables = "notes_data",sortindex=2,status="Y",act_path="edit/2",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N"  ,prev_table = ""},
                new system_menu { guid = Guid.NewGuid().ToString(), title = "使用者條款", category = m7,tables = "notes_data",sortindex=3,status="Y",act_path="edit/3",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N"  ,prev_table = ""},

                //首頁管理
                new system_menu { guid = Guid.NewGuid().ToString(), title = "首頁Banner", category = m9,tables = "banners",sortindex=1,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},

                new system_menu { guid = Guid.NewGuid().ToString(), title = "首頁相關資訊", category = m9,tables = "index_info",sortindex=2,status="Y",act_path="edit/1",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N"  ,prev_table = ""},


                //系統
                new system_menu { guid =  Guid.NewGuid().ToString(), title = "帳戶管理", category = m11,tables = "user",sortindex=1,status="Y",act_path="list",icon="",area="",category_table="roles",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "" },
                new system_menu { guid =  Guid.NewGuid().ToString(), title = "群組管理", category = m11,tables = "roles",sortindex=2,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},

                new system_menu { guid =  Guid.NewGuid().ToString(), title = "網站基本資訊", category = m10,tables = "web_data",sortindex=1,status="Y",act_path="edit/1",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                new system_menu { guid =  Guid.NewGuid().ToString(), title = "SMTP資料管理", category = m10,tables = "smtp_data",sortindex=2,status="Y",act_path="edit/b9732bfe-238d-4e4e-9114-8e6d30c34022",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "" },

                new system_menu { guid = Guid.NewGuid().ToString(), title = "資源回收桶", category = m10,tables = "ashcan",sortindex=90,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "N",can_del="Y"  ,prev_table = ""},

                new system_menu { guid = Guid.NewGuid().ToString(), title = "防火牆設定", category = m10,tables = "firewalls",sortindex=91,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y"  ,prev_table = ""},
                new system_menu { guid = Guid.NewGuid().ToString(), title = "系統日誌", category = m10,tables = "system_log",sortindex=92,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "N",can_del="Y" ,prev_table = "" },

                new system_menu { guid = Guid.NewGuid().ToString(), title = "系統參數", category = m13,tables = "system_data",sortindex=1,status="Y",act_path="edit/4795dabf-18de-490e-9bb2-d57b4d99c127",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "" },

            };
            if (context.system_menu.ToList().Count == 0)
            {

                System_menu.ForEach(s => context.system_menu.Add(s));
                context.SaveChanges();
            }

        }

        public void role_per()
        {
            //群組權限

            List<system_menu> system_menu_use = context.system_menu.Where(m => m.category != "0").ToList();
            List<role_permissions> Role_permissions = new List<role_permissions>();
            foreach (system_menu item in system_menu_use)
            {
                role_permissions data = new role_permissions { role_guid = sysadminGroupGuid, permissions_guid = item.guid, permissions_status = "F", guid = Guid.NewGuid() };
                Role_permissions.Add(data);
            }
            if (context.role_permissions.ToList().Count == 0)
            {
                Role_permissions.ForEach(s => context.role_permissions.Add(s));
                context.SaveChanges();
            }

        }


        public void langs()
        {
            //語系
            var Language = new List<language>
            {
                new language { guid = "001",   title = "繁體中文", codes = "tw",status="Y", date = DateTime.Now , lang = "tw" },
                new language { guid = "002",   title = "English", codes = "en",status="Y", date = DateTime.Now , lang = "en" },
            };
            if (context.language.ToList().Count == 0)
            {
                Language.ForEach(s => context.language.Add(s));
                context.SaveChanges();
            }

        }

        public void smtp()
        {
            var Smtp_data = new List<smtp_data>
            {
                new smtp_data { guid = "b9732bfe-238d-4e4e-9114-8e6d30c34022",   host = "smtp.gmail.com", port = "587",smtp_auth="Y" , username="test@e-creative.tw" , password = "24252151", from_email="test@e-creative.tw", modifydate = DateTime.Now , create_date =  DateTime.Now },
                new smtp_data { guid = "test",   host = "smtp.mailtrap.io", port = "2525",smtp_auth="N" , username="790e5c703fce03" , password = "c6f8afa64e18c8", from_email="test@e-creative.tw", modifydate = DateTime.Now , create_date =  DateTime.Now },
            };
            if (context.smtp_data.ToList().Count == 0)
            {
                Smtp_data.ForEach(s => context.smtp_data.Add(s));
                context.SaveChanges();
            }

        }

        public void web_data()
        {
            //網站基本資料
            var Web_data = new List<web_data>
            {
                new web_data { guid = "1",   title = "國泰建設", url = "https://ecreative.tw", phone="04-24350749" , fax="02-23778399" , ContactEmail="design7@e-creative.tw", UrbanContactEmail="design7@e-creative.tw" , CaseEmail="design7@e-creative.tw" , servicemail = "service@youweb.tw", ext_num="9" , address="台北市敦化南路二段218號2樓", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" , contact_info=MapPath("/Content/Templates/WebData/ContactInfo.html"), global_url = "/", mail_app_url = "/" },
                new web_data { guid = "1",   title = "國泰建設", url = "https://ecreative.tw", phone="886-2-24350749" , fax="886-2-23778399", ContactEmail="design7@e-creative.tw", UrbanContactEmail="design7@e-creative.tw", CaseEmail="design7@e-creative.tw" , servicemail = "service@youweb.tw", ext_num="9" , address="台北市敦化南路二段218號2樓", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" , contact_info=MapPath("/Content/Templates/WebData/ContactInfo.html"), global_url = "/", mail_app_url = "/" },


            };
            if (context.web_data.ToList().Count == 0)
            {
                Web_data.ForEach(s => context.web_data.Add(s));
                context.SaveChanges();
            }

        }

        public void system_data()
        {

            var System_data = new List<system_data>
            {
                new system_data { guid = Guid.Parse("4795dabf-18de-490e-9bb2-d57b4d99c127"),   title = "國泰建設 管理系統", login_title = "國泰建設 管理登入",logo = "Styles/images/static/header-logo-b.png" , logo_alt="國泰建設" , design_by = "Minmax", modifydate = DateTime.Now , create_date =  DateTime.Now , background="/Content/siteadminStyles/images/demo/example/01.jpg" , background_alt="" },

            };
            if (context.system_data.ToList().Count == 0)
            {
                System_data.ForEach(s => context.system_data.Add(s));
                context.SaveChanges();
            }

        }


        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}