﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.Migrations;

namespace Web.Migrations.Seed
{
    public class NewsSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
            category();
            data();
        }


        public void category()
        {
            //最新消息分類
            var news_category = new List<news_category>
            {
                 new news_category {
                    guid = "1",
                    title = "熱銷建案",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 1,
                    seo_description = "熱銷建案",
                    seo_keywords = "熱銷建案",
                    pic = "Styles/images/static/about/hero.jpg",
                  },
                 new news_category {
                    guid = "1",
                    title = "熱銷建案",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 1,
                    seo_description = "熱銷建案",
                    seo_keywords = "熱銷建案",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                 new news_category {
                    guid = "2",
                    title = "國泰房地產指數",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 2,
                    seo_description = "國泰房地產指數",
                    seo_keywords = "國泰房地產指數",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                 new news_category {
                    guid = "2",
                    title = "國泰房地產指數",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 2,
                    seo_description = "國泰房地產指數",
                    seo_keywords = "國泰房地產指數",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                  new news_category {
                    guid = "3",
                    title = "徵才",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 3,
                    seo_description = "徵才",
                    seo_keywords = "徵才",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                    new news_category {
                    guid = "3",
                    title = "徵才",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 3,
                    seo_description = "徵才",
                    seo_keywords = "徵才",
                     pic = "Styles/images/static/about/hero.jpg",
                  },

                  new news_category {
                    guid = "4",
                    title = "活動預告",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 5,
                    seo_description = "活動預告",
                    seo_keywords = "活動預告",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                  new news_category {
                    guid = "4",
                    title = "活動預告",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 5,
                    seo_description = "活動預告",
                    seo_keywords = "活動預告",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                  new news_category {
                    guid = "5",
                    title = "得獎資訊",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 6,
                    seo_description = "得獎資訊",
                    seo_keywords = "得獎資訊",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                  new news_category {
                    guid = "5",
                    title = "得獎資訊",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 6,
                    seo_description = "得獎資訊",
                    seo_keywords = "得獎資訊",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                  new news_category {
                    guid = "6",
                    title = "其他消息",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 7,
                    seo_description = "其他消息",
                    seo_keywords = "其他消息",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                  new news_category {
                    guid = "6",
                    title = "其他消息",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 7,
                    seo_description = "其他消息",
                    seo_keywords = "其他消息",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                   new news_category {
                    guid = "7",
                    title = "CSR",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 8,
                    seo_description = "CSR",
                    seo_keywords = "CSR",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                   new news_category {
                    guid = "7",
                    title = "CSR",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 8,
                    seo_description = "CSR",
                    seo_keywords = "CSR",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                   new news_category {
                    guid = "8",
                    title = "電子報",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 9,
                    seo_description = "電子報",
                    seo_keywords = "電子報",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                   new news_category {
                    guid = "8",
                    title = "電子報",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 9,
                    seo_description = "電子報",
                    seo_keywords = "電子報",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
            };
            if (context.news_category.ToList().Count == 0)
            {
                news_category.ForEach(s => context.news_category.Add(s));
                context.SaveChanges();
            }
        }

        public void data()
        {
            //最新消息
            var news = new List<news>
            {
                 new news {
                    guid = "1",
                    title = "台南「文海硯」社區健康講座",
                    category = "7",
                    notes = "本次 CSR 社區關懷結合國泰健康管理於台南「文海硯」社區舉辦健康講座。",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "台南「文海硯」社區健康講座",
                    seo_keywords = "台南「文海硯」社區健康講座",
                    startdate = DateTime.Parse("2019-08-28 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list01.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = "1",
                    title = "台南「文海硯」社區健康講座",
                    category = "7",
                    notes = "本次 CSR 社區關懷結合國泰健康管理於台南「文海硯」社區舉辦健康講座。",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "台南「文海硯」社區健康講座",
                    seo_keywords = "台南「文海硯」社區健康講座",
                    startdate = DateTime.Parse("2019-08-28 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list01.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = "2",
                    title = "台南「裕農花園新城」社區防災講座",
                    category = "4",
                    notes = "企業社區關懷 108.8.28 來到古都台南舉辦防災講座，本次活動於裕農路上的霖園...",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "台南「裕農花園新城」社區健康講座",
                    seo_keywords = "台南「裕農花園新城」社區健康講座",
                    startdate = DateTime.Parse("2019-08-28 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list02.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = "2",
                    title = "台南「裕農花園新城」社區防災講座",
                    category = "4",
                    notes = "企業社區關懷 108.8.28 來到古都台南舉辦防災講座，本次活動於裕農路上的霖園...",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "台南「裕農花園新城」社區健康講座",
                    seo_keywords = "台南「裕農花園新城」社區健康講座",
                    startdate = DateTime.Parse("2019-08-28 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list02.jpg",
                    tops = "Y",
                  },

                  new news {
                    guid = "3",
                    title = "「板橋第一新城」社區防災講座",
                    category = "6",
                    notes = "企業社區關懷活動 108.8.06 結合國泰文教基金會於霖園圖書館-板橋館舉辦防災推...",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "「板橋第一新城」社區防災講座",
                    seo_keywords = "「板橋第一新城」社區防災講座",
                    startdate = DateTime.Parse("2019-08-06 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list03.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = "3",
                    title = "「板橋第一新城」社區防災講座",
                    category = "6",
                    notes = "企業社區關懷活動 108.8.06 結合國泰文教基金會於霖園圖書館-板橋館舉辦防災推...",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "「板橋第一新城」社區防災講座",
                    seo_keywords = "「板橋第一新城」社區防災講座",
                    startdate = DateTime.Parse("2019-08-06 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list03.jpg",
                    tops = "Y",
                  },
                  new news {
                    guid = "4",
                    title = "三重「中正家園」社區防災講座",
                    category = "6",
                    notes = "企業社區關懷活動 108.8.01 來到新北市三重區「中正家園」舉辦社區防災推廣教...",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "三重「中正家園」社區防災講座",
                    seo_keywords = "三重「中正家園」社區防災講座",
                    startdate = DateTime.Parse("2019-08-01 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list04.jpg",
                    tops = "Y",
                  },
                   new news {
                    guid = "4",
                    title = "三重「中正家園」社區防災講座",
                    category = "6",
                    notes = "企業社區關懷活動 108.8.01 來到新北市三重區「中正家園」舉辦社區防災推廣教...",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "三重「中正家園」社區防災講座",
                    seo_keywords = "三重「中正家園」社區防災講座",
                    startdate = DateTime.Parse("2019-08-01 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list04.jpg",
                    tops = "Y",
                  },
                   new news {
                    guid = "5",
                    title = "國泰台北「名人廣場大樓」社區健康講座",
                    category = "7",
                    notes = "企業社區關懷活動 108.7.26 來到北市信義區永吉路的「名人廣場大樓」舉辦社區...",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "國泰台北「名人廣場大樓」社區健康講座",
                    seo_keywords = "國泰台北「名人廣場大樓」社區健康講座",
                    startdate = DateTime.Parse("2019-07-26 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list05.jpg",
                    tops = "Y",
                  },
                   new news {
                    guid = "5",
                    title = "國泰台北「名人廣場大樓」社區健康講座",
                    category = "7",
                    notes = "企業社區關懷活動 108.7.26 來到北市信義區永吉路的「名人廣場大樓」舉辦社區...",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "國泰台北「名人廣場大樓」社區健康講座",
                    seo_keywords = "國泰台北「名人廣場大樓」社區健康講座",
                    startdate = DateTime.Parse("2019-07-26 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list05.jpg",
                    tops = "Y",
                  },

                    new news {
                    guid = "6",
                    title = "國泰房地產指數季報 2019年第2季",
                    category = "2",
                    notes = "國泰房地產指數季報 2019年第2季",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "國泰房地產指數季報 2019年第2季",
                    seo_keywords = "國泰房地產指數季報 2019年第2季",
                    startdate = DateTime.Parse("2019-07-15 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list06.jpg",
                    tops = "N",
                  },
                     new news {
                    guid = "6",
                    title = "國泰房地產指數季報 2019年第2季",
                    category = "2",
                    notes = "國泰房地產指數季報 2019年第2季",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "國泰房地產指數季報 2019年第2季",
                    seo_keywords = "國泰房地產指數季報 2019年第2季",
                    startdate = DateTime.Parse("2019-07-15 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list06.jpg",
                    tops = "N",
                  },
                    new news {
                    guid = "7",
                    title = "台南「文海硯」社區健康講座",
                    category = "7",
                    notes = "本次 CSR 社區關懷結合國泰健康管理於台南「文海硯」社區舉辦健康講座。",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "台南「文海硯」社區健康講座",
                    seo_keywords = "台南「文海硯」社區健康講座",
                    startdate = DateTime.Parse("2019-07-01 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list01.jpg",
                    tops = "N",
                  },
                  new news {
                    guid = "7",
                    title = "台南「文海硯」社區健康講座",
                    category = "7",
                    notes = "本次 CSR 社區關懷結合國泰健康管理於台南「文海硯」社區舉辦健康講座。",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "台南「文海硯」社區健康講座",
                    seo_keywords = "台南「文海硯」社區健康講座",
                    startdate = DateTime.Parse("2019-07-01 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list01.jpg",
                    tops = "N",
                  },
                  new news {
                    guid = "8",
                    title = "台南「裕農花園新城」社區防災講座",
                    category = "4",
                    notes = "企業社區關懷 108.8.28 來到古都台南舉辦防災講座，本次活動於裕農路上的霖園...",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "台南「裕農花園新城」社區健康講座",
                    seo_keywords = "台南「裕農花園新城」社區健康講座",
                    startdate = DateTime.Parse("2019-06-18 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list07.jpg",
                    tops = "N",
                  },
                  new news {
                    guid = "8",
                    title = "台南「裕農花園新城」社區防災講座",
                    category = "4",
                    notes = "企業社區關懷 108.8.28 來到古都台南舉辦防災講座，本次活動於裕農路上的霖園...",
                    content =  MapPath("/Content/Templates/News/NewsDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "台南「裕農花園新城」社區健康講座",
                    seo_keywords = "台南「裕農花園新城」社區健康講座",
                    startdate = DateTime.Parse("2019-06-18 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list07.jpg",
                    tops = "N",
                  },


            };
            if (context.news.ToList().Count == 0)
            {
                news.ForEach(s => context.news.Add(s));
                context.SaveChanges();
            }
        }

        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}