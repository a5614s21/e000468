﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.Migrations;
using Newtonsoft.Json;

namespace Web.Migrations.Seed
{
    public class UrbanSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {           
            news();
            knowledges();
            faq();
            columnCategory();
            column();
            column_file();
        }




        public void news()
        {
            //最新消息
            var urban_news = new List<urban_news>
            {
                 new urban_news {
                    guid = "1",
                    sortIndex = 1,
                    title = "中央都市更新訊息",                  
                    link = "http://glrs.moi.gov.tw/index.aspx",
                    pic = "Styles/images/static/urban/urban-news/news01.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",                
                  
                 
                  },
                 new urban_news {
                    guid = "1",
                    sortIndex = 1,
                    title = "中央都市更新訊息",
                    link = "http://glrs.moi.gov.tw/index.aspx",
                    pic = "Styles/images/static/urban/urban-news/news01.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                 new urban_news {
                    guid = "2",
                    sortIndex = 2,
                    title = "臺北市都市更新訊息",
                    link = "https://uro.gov.taipei/Default.aspx",
                    pic = "Styles/images/static/urban/urban-news/news02.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                 new urban_news {
                    guid = "2",
                    sortIndex = 2,
                    title = "臺北市都市更新訊息",
                    link = "https://uro.gov.taipei/Default.aspx",
                    pic = "Styles/images/static/urban/urban-news/news02.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                 new urban_news {
                    guid = "3",
                    sortIndex = 3,
                    title = "新北市都市更新訊息",
                    link = "https://www.uro.ntpc.gov.tw/news/?parent_id=10003&type_id=10035",
                    pic = "Styles/images/static/urban/urban-news/news03.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                 new urban_news {
                    guid = "3",
                    sortIndex = 3,
                    title = "新北市都市更新訊息",
                    link = "https://www.uro.ntpc.gov.tw/news/?parent_id=10003&type_id=10035",
                    pic = "Styles/images/static/urban/urban-news/news03.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },

            };
            if (context.urban_news.ToList().Count == 0)
            {
                urban_news.ForEach(s => context.urban_news.Add(s));
                context.SaveChanges();
            }
        }

        public void knowledges()
        {
            //最新消息
            var urban_knowledge = new List<urban_knowledge>
            {
                 new urban_knowledge {
                    guid = "1",
                    sortIndex = 1,
                    title = "營建署 都更法令查詢",
                    link = "https://www.cpami.gov.tw/index.php?option=com_rgsys&view=rgsys&Itemid=201",
                    pic = "Styles/images/static/urban/urban-knowledge/news01.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",

                  },

                 new urban_knowledge {
                    guid = "1",
                    sortIndex = 1,
                    title = "營建署 都更法令查詢",
                    link = "https://www.cpami.gov.tw/index.php?option=com_rgsys&view=rgsys&Itemid=201",
                    pic = "Styles/images/static/urban/urban-news/news02.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",

                  },

                 new urban_knowledge {
                    guid = "2",
                    sortIndex = 2,
                    title = "臺北市都市更新相關法規",
                    link = "https://uro.gov.taipei/News.aspx?n=F511910B5A36351D&sms=234E13F5FE7D88EA",
                    pic = "Styles/images/static/urban/urban-news/news02.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",

                  },
                 new urban_knowledge {
                    guid = "2",
                    sortIndex = 2,
                    title = "臺北市都市更新相關法規",
                    link = "https://uro.gov.taipei/News.aspx?n=F511910B5A36351D&sms=234E13F5FE7D88EA",
                    pic = "Styles/images/static/urban/urban-news/news02.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",

                  },
                 new urban_knowledge {
                    guid = "3",
                    sortIndex = 3,
                    title = "臺北市都市更新處",
                    link = "https://uro.gov.taipei/Default.aspx",
                    pic = "Styles/images/static/urban/urban-news/news02.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",

                  },
                 new urban_knowledge {
                    guid = "3",
                    sortIndex = 3,
                    title = "臺北市都市更新處",
                    link = "https://uro.gov.taipei/Default.aspx",
                    pic = "Styles/images/static/urban/urban-news/news02.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",

                  },
                 new urban_knowledge {
                    guid = "4",
                    sortIndex = 4,
                    title = "新北市都市更新訊息",
                    link = "https://uro.gov.taipei/Default.aspx",
                    pic = "Styles/images/static/urban/urban-news/news03.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",

                  },
                 new urban_knowledge {
                    guid = "4",
                    sortIndex = 4,
                    title = "新北市都市更新訊息",
                    link = "https://uro.gov.taipei/Default.aspx",
                    pic = "Styles/images/static/urban/urban-news/news03.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",

                  },

            };
            if (context.urban_knowledge.ToList().Count == 0)
            {
                urban_knowledge.ForEach(s => context.urban_knowledge.Add(s));
                context.SaveChanges();
            }
        }

        public void faq()
        {
            //最新消息
            var urban_faq = new List<urban_faq>
            {
                 new urban_faq {
                    guid = "1",
                    sortIndex = 1,
                    title = "都市更新是什麼？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",                  
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                  new urban_faq {
                    guid = "1",
                    sortIndex = 1,
                    title = "都市更新是什麼？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new urban_faq {
                    guid = "2",
                    sortIndex = 2,
                    title = "辦都市更新的好處有哪些？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                    new urban_faq {
                    guid = "2",
                    sortIndex = 2,
                    title = "辦都市更新的好處有哪些？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new urban_faq {
                    guid = "3",
                    sortIndex = 3,
                    title = "都市更新計畫和都市更新事業計畫有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                   new urban_faq {
                    guid = "3",
                    sortIndex = 3,
                    title = "都市更新計畫和都市更新事業計畫有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new urban_faq {
                    guid = "4",
                    sortIndex = 4,
                    title = "權利變換和一般合建有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                   new urban_faq {
                    guid = "4",
                    sortIndex = 4,
                    title = "權利變換和一般合建有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },


                   new urban_faq {
                    guid = "5",
                    sortIndex = 5,
                    title = "都市更新是什麼？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                  new urban_faq {
                    guid = "6",
                    sortIndex = 6,
                    title = "都市更新是什麼？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new urban_faq {
                    guid = "7",
                    sortIndex = 7,
                    title = "辦都市更新的好處有哪些？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                    new urban_faq {
                    guid = "8",
                    sortIndex = 8,
                    title = "辦都市更新的好處有哪些？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new urban_faq {
                    guid = "9",
                    sortIndex = 9,
                    title = "都市更新計畫和都市更新事業計畫有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                   new urban_faq {
                    guid = "10",
                    sortIndex = 10,
                    title = "都市更新計畫和都市更新事業計畫有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new urban_faq {
                    guid = "11",
                    sortIndex = 11,
                    title = "權利變換和一般合建有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                   new urban_faq {
                    guid = "12",
                    sortIndex =12,
                    title = "權利變換和一般合建有何不同？",
                    notes = "針對本市窳陋、老舊、衰敗的地區,透過政府與私人團體的努力,擬定綜合性再發展計畫,並經由專家學者審議過程,辦理拆除重建、整建維護,以達增進都市機能、 改善居住環境、提升房地產價值的目的。 因此,都市更新不是狹義的房屋改建而已,而是透過公私合作、專家審議、民眾參 與等方式,在兼顧社會、經濟、環境等層面下,加速都市再發展。",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },


            };
            if (context.urban_faq.ToList().Count == 0)
            {
                urban_faq.ForEach(s => context.urban_faq.Add(s));
                context.SaveChanges();
            }
        }

        public void columnCategory()
        {
            //分類
            var urban_column_category = new List<urban_column_category>
            {
                 new urban_column_category {
                    guid = "1",
                    title = "規劃中案件",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 1,
                    seo_description = "規劃中案件",
                    seo_keywords = "規劃中案件",
                  },
                 new urban_column_category {
                    guid = "1",
                    title = "規劃中案件",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 1,
                    seo_description = "規劃中案件",
                    seo_keywords = "規劃中案件",
                  },
                 new urban_column_category {
                    guid = "2",
                    title = "已完成案件",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 2,
                    seo_description = "已完成案件",
                    seo_keywords = "已完成案件",
                  },
                 new urban_column_category {
                    guid = "2",
                    title = "已完成案件",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 2,
                    seo_description = "已完成案件",
                    seo_keywords = "已完成案件",
                  },
                  new urban_column_category {
                    guid = "3",
                    title = "執行中案件",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 3,
                    seo_description = "執行中案件",
                    seo_keywords = "執行中案件",
                  },
                    new urban_column_category {
                    guid = "3",
                    title = "執行中案件",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 3,
                    seo_description = "執行中案件",
                    seo_keywords = "執行中案件",
                  },

                
            };
            if (context.urban_column_category.ToList().Count == 0)
            {
                urban_column_category.ForEach(s => context.urban_column_category.Add(s));
                context.SaveChanges();
            }
        }

        public void column()
        {

          /*  List<Object> dic = new List<object>();
            Dictionary<string, Object> subDic = new Dictionary<string, object>();
            subDic.Add("title", "大事紀");
            subDic.Add("files", "Content/Upload/files/test.pdf");
            dic.Add(subDic);
            subDic.Clear();
            subDic.Add("title", "事業計畫內容");
            subDic.Add("files", "Content/Upload/files/test.pdf");
            dic.Add(subDic);
            subDic.Clear();
            subDic.Add("title", "相關會議記錄");
            subDic.Add("files", "Content/Upload/files/test.pdf");
            dic.Add(subDic);*/


            //內容
            var urban_column = new List<urban_column>
            {
                 new urban_column {
                    guid = "1",
                    title = "霖園大廈",
                    sortIndex = 1,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list01.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",                    
                    seo_description = "霖園大廈",
                    seo_keywords = "霖園大廈",
                  },
                new urban_column {
                    guid = "1",
                    title = "霖園大廈",
                    sortIndex = 1,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list01.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "霖園大廈",
                    seo_keywords = "霖園大廈",
                  },
                new urban_column {
                    guid = "2",
                    title = "民生建國",
                    sortIndex = 2,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list02.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "民生建國",
                    seo_keywords = "民生建國",
                  },

                new urban_column {
                    guid = "2",
                    title = "民生建國",
                    sortIndex = 2,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list02.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "民生建國",
                    seo_keywords = "民生建國",
                  },
                new urban_column {
                    guid = "3",
                    title = "國泰plus",
                    sortIndex = 3,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list03.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "國泰plus",
                    seo_keywords = "國泰plus",
                  },
                new urban_column {
                    guid = "3",
                    title = "國泰plus",
                    sortIndex = 3,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list03.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "國泰plus",
                    seo_keywords = "國泰plus",
                  },
                new urban_column {
                    guid = "4",
                    title = "中山區榮星六小段",
                    sortIndex = 4,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list04.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "中山區榮星六小段",
                    seo_keywords = "中山區榮星六小段",
                  },
                new urban_column {
                    guid = "4",
                    title = "中山區榮星六小段",
                    sortIndex = 4,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list04.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "中山區榮星六小段",
                    seo_keywords = "中山區榮星六小段",
                  },
                new urban_column {
                    guid = "5",
                    title = "中山區榮星六小段",
                    sortIndex = 5,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list05.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "中山區榮星六小段",
                    seo_keywords = "中山區榮星六小段",
                  },
                new urban_column {
                    guid = "5",
                    title = "中山區榮星六小段",
                    sortIndex = 5,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list05.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "中山區榮星六小段",
                    seo_keywords = "中山區榮星六小段",
                  },
                new urban_column {
                    guid = "6",
                    title = "南港三小段(A區)",
                    sortIndex = 6,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list06.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "南港三小段(A區)",
                    seo_keywords = "南港三小段(A區)",
                  },
                new urban_column {
                    guid = "6",
                    title = "中山區榮星六小段",
                    sortIndex = 6,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list06.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "中山區榮星六小段",
                    seo_keywords = "中山區榮星六小段",
                  },
                new urban_column {
                    guid = "7",
                    title = "南港三小段(B區)",
                    sortIndex = 7,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list07.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "南港三小段(B區)",
                    seo_keywords = "南港三小段(B區)",
                  },
                new urban_column {
                    guid = "7",
                    title = "南港三小段(B區)",
                    sortIndex = 7,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list07.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "南港三小段(B區)",
                    seo_keywords = "南港三小段(B區)",
                  },


                new urban_column {
                    guid = "8",
                    title = "木柵公寓",
                    sortIndex = 8,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list08.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "木柵公寓",
                    seo_keywords = "木柵公寓",
                  },
                new urban_column {
                    guid = "8",
                    title = "木柵公寓",
                    sortIndex = 8,
                    category = "1",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list08.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "木柵公寓",
                    seo_keywords = "木柵公寓",
                  },

                  new urban_column {
                    guid = "9",
                    title = "南港三小段(B區)",
                    sortIndex = 9,
                    category = "2",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list07.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "南港三小段(B區)",
                    seo_keywords = "南港三小段(B區)",
                  },
                new urban_column {
                    guid = "9",
                    title = "南港三小段(B區)",
                    sortIndex = 9,
                    category = "2",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list07.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "南港三小段(B區)",
                    seo_keywords = "南港三小段(B區)",
                  },


                new urban_column {
                    guid = "10",
                    title = "木柵公寓",
                    sortIndex = 10,
                    category = "2",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list08.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "木柵公寓",
                    seo_keywords = "木柵公寓",
                  },
                new urban_column {
                    guid = "10",
                    title = "木柵公寓",
                    sortIndex = 10,
                    category = "2",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list08.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "木柵公寓",
                    seo_keywords = "木柵公寓",
                  },
                  new urban_column {
                    guid = "11",
                    title = "木柵公寓",
                    sortIndex = 11,
                    category = "3",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list08.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "木柵公寓",
                    seo_keywords = "木柵公寓",
                  },
                new urban_column {
                    guid = "11",
                    title = "木柵公寓",
                    sortIndex = 11,
                    category = "3",
                    content = MapPath("/Content/Templates/Urban/ColumnDetail.html"),
                    list_pic = "Styles/images/demo/urban/urban-column/list08.jpg",
                    pic = "Styles/images/demo/urban/urban-column/slider01.jpg,Styles/images/demo/cases/slider01.jpg,Styles/images/demo/cases/slider03.jpg,Styles/images/demo/cases/slider04.jpg,Styles/images/demo/cases/slider05.jpg",
                    
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "木柵公寓",
                    seo_keywords = "木柵公寓",
                  },

            };
            if (context.urban_column.ToList().Count == 0)
            {
                urban_column.ForEach(s => context.urban_column.Add(s));
                context.SaveChanges();
            }
        }


        public void column_file()
        {
            var urban_column_files = new List<urban_column_files>
            {
                  new urban_column_files {
                    guid = "1",
                    title = "大事紀",
                    files = "Content/Upload/files/test.pdf",
                    sortIndex = 1,
                    category = "1",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                  new urban_column_files {
                    guid = "1",
                    title = "大事紀",
                    files = "Content/Upload/files/test.pdf",
                    sortIndex = 1,
                    category = "1",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new urban_column_files {
                    guid = "2",
                    title = "事業計畫內容",
                    files = "Content/Upload/files/test.pdf",
                    sortIndex = 2,
                    category = "1",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                   new urban_column_files {
                    guid = "2",
                    title = "事業計畫內容",
                    files = "Content/Upload/files/test.pdf",
                    sortIndex = 2,
                    category = "1",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new urban_column_files {
                    guid = "3",
                    title = "相關會議記錄",
                    files = "Content/Upload/files/test.pdf",
                    sortIndex = 3,
                    category = "1",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                   new urban_column_files {
                    guid = "3",
                    title = "相關會議記錄",
                    files = "Content/Upload/files/test.pdf",
                    sortIndex = 3,
                    category = "1",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                    new urban_column_files {
                    guid = "4",
                    title = "大事紀",
                    files = "Content/Upload/files/test.pdf",
                    sortIndex = 1,
                    category = "2",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                  new urban_column_files {
                    guid = "4",
                    title = "大事紀",
                    files = "Content/Upload/files/test.pdf",
                    sortIndex = 1,
                    category = "2",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new urban_column_files {
                    guid = "5",
                    title = "事業計畫內容",
                    files = "Content/Upload/files/test.pdf",
                    sortIndex = 2,
                    category = "2",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                   new urban_column_files {
                    guid = "5",
                    title = "事業計畫內容",
                    files = "Content/Upload/files/test.pdf",
                    sortIndex = 2,
                    category = "2",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
                   new urban_column_files {
                    guid = "6",
                    title = "相關會議記錄",
                    files = "Content/Upload/files/test.pdf",
                    sortIndex = 3,
                    category = "2",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                   new urban_column_files {
                    guid = "6",
                    title = "相關會議記錄",
                    files = "Content/Upload/files/test.pdf",
                    sortIndex = 3,
                    category = "2",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },

            };
            if (context.urban_column_files.ToList().Count == 0)
            {
                urban_column_files.ForEach(s => context.urban_column_files.Add(s));
                context.SaveChanges();
            }
        }

        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}