﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.Migrations;

namespace Web.Migrations.Seed
{
    public class CharitysSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
            category();
            data();
        }


        public void category()
        {
            //公益活動分類
            var charity_category = new List<charity_category>
            {
                 new charity_category {
                    guid = "1",
                    title = "霖園圖書館",
                    content =  MapPath("/Content/Templates/Charitys/charitys.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 1,
                    seo_description = "霖園圖書館",
                    seo_keywords = "霖園圖書館",
                    pic = "Styles/images/static/about/hero.jpg",
                  },
                 new charity_category {
                    guid = "1",
                    title = "霖園圖書館",
                     content =  MapPath("/Content/Templates/Charitys/charitys.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 1,
                    seo_description = "霖園圖書館",
                    seo_keywords = "霖園圖書館",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                 new charity_category {
                    guid = "2",
                    title = "台語金曲演唱會",
                     content =  MapPath("/Content/Templates/Charitys/charitys.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 2,
                    seo_description = "台語金曲演唱會",
                    seo_keywords = "台語金曲演唱會",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                 new charity_category {
                    guid = "2",
                    title = "台語金曲演唱會",
                     content =  MapPath("/Content/Templates/Charitys/charitys.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 2,
                    seo_description = "台語金曲演唱會",
                    seo_keywords = "台語金曲演唱會",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                  new charity_category {
                    guid = "3",
                    title = "國泰公益集團合辦活動",
                     content =  MapPath("/Content/Templates/Charitys/charitys.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 3,
                    seo_description = "國泰公益集團合辦活動",
                    seo_keywords = "國泰公益集團合辦活動",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                    new charity_category {
                    guid = "3",
                    title = "國泰公益集團合辦活動",
                     content =  MapPath("/Content/Templates/Charitys/charitys.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 3,
                    seo_description = "國泰公益集團合辦活動",
                    seo_keywords = "國泰公益集團合辦活動",
                     pic = "Styles/images/static/about/hero.jpg",
                  },

                  new charity_category {
                    guid = "4",
                    title = "贊助活動",
                     content = "",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 5,
                    seo_description = "贊助活動",
                    seo_keywords = "贊助活動",
                     pic = "Styles/images/static/about/hero.jpg",
                  },
                  new charity_category {
                    guid = "4",
                    title = "贊助活動",
                     content = "",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 5,
                    seo_description = "贊助活動",
                    seo_keywords = "贊助活動",
                     pic = "Styles/images/static/about/hero.jpg",
                  },

            };
            if (context.charity_category.ToList().Count == 0)
            {
                charity_category.ForEach(s => context.charity_category.Add(s));
                context.SaveChanges();
            }


        }

        public void data()
        {
            //公益活動
            var charitys = new List<charitys>
            {
                 new charitys {
                    guid = "1",
                    title = "內惟舘內教學-點心製作上課",
                    category = "1",
                    notes = "本次 CSR 社區關懷結合國泰健康管理於台南「文海硯」社區舉辦健康講座。",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "台南「文海硯」社區健康講座",
                    seo_keywords = "台南「文海硯」社區健康講座",
                    startdate = DateTime.Parse("2019-08-28 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list01.jpg",
                    tops = "Y",
                  },
                  new charitys {
                    guid = "1",
                    title = "內惟舘內教學-點心製作上課",
                    category = "1",
                    notes = "本次 CSR 社區關懷結合國泰健康管理於台南「文海硯」社區舉辦健康講座。",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "台南「文海硯」社區健康講座",
                    seo_keywords = "台南「文海硯」社區健康講座",
                    startdate = DateTime.Parse("2019-08-28 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list01.jpg",
                    tops = "Y",
                  },
                  new charitys {
                    guid = "2",
                    title = "台南「裕農花園新城」社區防災講座",
                    category = "1",
                    notes = "企業社區關懷 108.8.28 來到古都台南舉辦防災講座，本次活動於裕農路上的霖園...",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "台南「裕農花園新城」社區健康講座",
                    seo_keywords = "台南「裕農花園新城」社區健康講座",
                    startdate = DateTime.Parse("2019-08-28 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list02.jpg",
                    tops = "Y",
                  },
                  new charitys {
                    guid = "2",
                    title = "台南「裕農花園新城」社區防災講座",
                    category = "1",
                    notes = "企業社區關懷 108.8.28 來到古都台南舉辦防災講座，本次活動於裕農路上的霖園...",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "台南「裕農花園新城」社區健康講座",
                    seo_keywords = "台南「裕農花園新城」社區健康講座",
                    startdate = DateTime.Parse("2019-08-28 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list02.jpg",
                    tops = "Y",
                  },

                  new charitys {
                    guid = "3",
                    title = "「板橋第一新城」社區防災講座",
                    category = "2",
                    notes = "企業社區關懷活動 108.8.06 結合國泰文教基金會於霖園圖書館-板橋館舉辦防災推...",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "「板橋第一新城」社區防災講座",
                    seo_keywords = "「板橋第一新城」社區防災講座",
                    startdate = DateTime.Parse("2019-08-06 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list03.jpg",
                    tops = "Y",
                  },
                  new charitys {
                    guid = "3",
                    title = "「板橋第一新城」社區防災講座",
                    category = "2",
                    notes = "企業社區關懷活動 108.8.06 結合國泰文教基金會於霖園圖書館-板橋館舉辦防災推...",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "「板橋第一新城」社區防災講座",
                    seo_keywords = "「板橋第一新城」社區防災講座",
                    startdate = DateTime.Parse("2019-08-06 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list03.jpg",
                    tops = "Y",
                  },
                  new charitys {
                    guid = "4",
                    title = "三重「中正家園」社區防災講座",
                    category = "2",
                    notes = "企業社區關懷活動 108.8.01 來到新北市三重區「中正家園」舉辦社區防災推廣教...",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "三重「中正家園」社區防災講座",
                    seo_keywords = "三重「中正家園」社區防災講座",
                    startdate = DateTime.Parse("2019-08-01 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list04.jpg",
                    tops = "Y",
                  },
                   new charitys {
                    guid = "4",
                    title = "三重「中正家園」社區防災講座",
                    category = "2",
                    notes = "企業社區關懷活動 108.8.01 來到新北市三重區「中正家園」舉辦社區防災推廣教...",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "三重「中正家園」社區防災講座",
                    seo_keywords = "三重「中正家園」社區防災講座",
                    startdate = DateTime.Parse("2019-08-01 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list04.jpg",
                    tops = "Y",
                  },
                   new charitys {
                    guid = "5",
                    title = "國泰台北「名人廣場大樓」社區健康講座",
                    category = "3",
                    notes = "企業社區關懷活動 108.7.26 來到北市信義區永吉路的「名人廣場大樓」舉辦社區...",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "國泰台北「名人廣場大樓」社區健康講座",
                    seo_keywords = "國泰台北「名人廣場大樓」社區健康講座",
                    startdate = DateTime.Parse("2019-07-26 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list05.jpg",
                    tops = "Y",
                  },
                   new charitys {
                    guid = "5",
                    title = "國泰台北「名人廣場大樓」社區健康講座",
                    category = "3",
                    notes = "企業社區關懷活動 108.7.26 來到北市信義區永吉路的「名人廣場大樓」舉辦社區...",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "國泰台北「名人廣場大樓」社區健康講座",
                    seo_keywords = "國泰台北「名人廣場大樓」社區健康講座",
                    startdate = DateTime.Parse("2019-07-26 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list05.jpg",
                    tops = "Y",
                  },

                    new charitys {
                    guid = "6",
                    title = "國泰房地產指數季報 2019年第2季",
                    category = "3",
                    notes = "國泰房地產指數季報 2019年第2季",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "國泰房地產指數季報 2019年第2季",
                    seo_keywords = "國泰房地產指數季報 2019年第2季",
                    startdate = DateTime.Parse("2019-07-15 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list06.jpg",
                    tops = "N",
                  },
                     new charitys {
                    guid = "6",
                    title = "國泰房地產指數季報 2019年第2季",
                    category = "3",
                    notes = "國泰房地產指數季報 2019年第2季",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "國泰房地產指數季報 2019年第2季",
                    seo_keywords = "國泰房地產指數季報 2019年第2季",
                    startdate = DateTime.Parse("2019-07-15 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list06.jpg",
                    tops = "N",
                  },
                    new charitys {
                    guid = "7",
                    title = "台南「文海硯」社區健康講座",
                    category = "4",
                    notes = "本次 CSR 社區關懷結合國泰健康管理於台南「文海硯」社區舉辦健康講座。",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "台南「文海硯」社區健康講座",
                    seo_keywords = "台南「文海硯」社區健康講座",
                    startdate = DateTime.Parse("2019-07-01 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list01.jpg",
                    tops = "N",
                  },
                  new charitys {
                    guid = "7",
                    title = "台南「文海硯」社區健康講座",
                    category = "4",
                    notes = "本次 CSR 社區關懷結合國泰健康管理於台南「文海硯」社區舉辦健康講座。",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "台南「文海硯」社區健康講座",
                    seo_keywords = "台南「文海硯」社區健康講座",
                    startdate = DateTime.Parse("2019-07-01 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list01.jpg",
                    tops = "N",
                  },
                  new charitys {
                    guid = "8",
                    title = "台南「裕農花園新城」社區防災講座",
                    category = "4",
                    notes = "企業社區關懷 108.8.28 來到古都台南舉辦防災講座，本次活動於裕農路上的霖園...",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "台南「裕農花園新城」社區健康講座",
                    seo_keywords = "台南「裕農花園新城」社區健康講座",
                    startdate = DateTime.Parse("2019-06-18 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list07.jpg",
                    tops = "N",
                  },
                  new charitys {
                    guid = "8",
                    title = "台南「裕農花園新城」社區防災講座",
                    category = "4",
                    notes = "企業社區關懷 108.8.28 來到古都台南舉辦防災講座，本次活動於裕農路上的霖園...",
                    content =  MapPath("/Content/Templates/Charitys/charitysDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "台南「裕農花園新城」社區健康講座",
                    seo_keywords = "台南「裕農花園新城」社區健康講座",
                    startdate = DateTime.Parse("2019-06-18 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list07.jpg",
                    tops = "N",
                  },


            };
            if (context.charitys.ToList().Count == 0)
            {
                charitys.ForEach(s => context.charitys.Add(s));
                context.SaveChanges();
            }
        }

        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}