﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.Migrations;

namespace Web.Migrations.Seed
{
    public class MailContentsSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
            data();          
        }


        public void data()
        {
            //信件樣板
            var mail_contents = new List<mail_contents>
            {
                new mail_contents {
                    guid = "1",
                    title = "審核通知信件",
                    content = MapPath("/Content/Templates/Mail/VerifyAdd.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw"
                  },
                 new mail_contents {
                    guid = "2",
                    title = "客戶專區-聯絡諮詢表單",
                    content = MapPath("/Content/Templates/Mail/ContactForm.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw"
                  },
                  new mail_contents {
                    guid = "3",
                    title = "都市更新-聯絡諮詢表單",
                    content = MapPath("/Content/Templates/Mail/UrbanContact.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw"
                  },

                    new mail_contents {
                    guid = "4",
                    title = "(回覆)客戶專區-聯絡諮詢表單",
                    content = MapPath("/Content/Templates/Mail/ContactFormRe.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw"
                  },
                    new mail_contents {
                    guid = "5",
                    title = "(回覆)都市更新-聯絡諮詢表單",
                    content = MapPath("/Content/Templates/Mail/UrbanContactRe.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw"
                  },
                   new mail_contents {
                    guid = "6",
                    title = "預約賞屋",
                    content = MapPath("/Content/Templates/Mail/CasesReservation.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw"
                  },
                    new mail_contents {
                    guid = "7",
                    title = "(回覆)預約賞屋",
                    content = MapPath("/Content/Templates/Mail/CasesReservationRe.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw"
                  },
            };
            if (context.mail_contents.ToList().Count == 0)
            {
                mail_contents.ForEach(s => context.mail_contents.Add(s));
                context.SaveChanges();
            }
        }

        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}