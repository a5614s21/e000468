﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.Migrations;

namespace Web.Migrations.Seed
{
    public class NotesSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
            data();
           
        }

    
        public void data()
        {
            var notes_data = new List<notes_data>
            {
                 new notes_data {
                    guid = "1",
                    title = "人才招募",
                    content = MapPath("/Content/Templates/Notes/Career.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "人才招募",
                    seo_keywords = "人才招募",
                  },
                 new notes_data {
                    guid = "1",
                    title = "人才招募",
                    content = MapPath("/Content/Templates/Notes/Career.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "人才招募",
                    seo_keywords = "人才招募",
                  },
                 new notes_data {
                    guid = "2",
                    title = "隱私權政策",
                     content = MapPath("/Content/Templates/Notes/Privacy.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "隱私權政策",
                    seo_keywords = "隱私權政策",
                  },
                 new notes_data {
                    guid = "2",
                    title = "Privacy Policy",
                    content = MapPath("/Content/Templates/Notes/Privacy.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "Privacy Policy",
                    seo_keywords = "Privacy Policy",
                  },
                 new notes_data {
                    guid = "3",
                    title = "使用者條款",
                    content = MapPath("/Content/Templates/Notes/Terms.html"),                
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "使用者條款",
                    seo_keywords = "使用者條款",
                  },
                 new notes_data {
                    guid = "3",
                    title = "Terms of Service",
                    content = MapPath("/Content/Templates/Notes/Terms.html"),                  
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "Terms of Service",
                    seo_keywords = "Terms of Service",
                  },

                  new notes_data {
                    guid = "4",
                    title = "購屋試算說明",
                    content = MapPath("/Content/Templates/Notes/Calculation.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "購屋試算說明",
                    seo_keywords = "購屋試算說明",
                  },
                 new notes_data {
                    guid = "4",
                    title = "購屋試算說明",
                    content = MapPath("/Content/Templates/Notes/Calculation.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "購屋試算說明",
                    seo_keywords = "購屋試算說明",
                  },

                     new notes_data {
                    guid = "5",
                    title = "尊榮優惠",
                    content = MapPath("/Content/Templates/Notes/Vip.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "尊榮優惠",
                    seo_keywords = "尊榮優惠",
                  },
                 new notes_data {
                    guid = "5",
                    title = "尊榮優惠",
                    content = MapPath("/Content/Templates/Notes/Vip.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "尊榮優惠",
                    seo_keywords = "尊榮優惠",
                  },

                   new notes_data {
                    guid = "6",
                    title = "投資人專區-其他資訊",
                    content = MapPath("/Content/Templates/Notes/InvestorOthers.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "投資人專區-其他資訊",
                    seo_keywords = "投資人專區-其他資訊",
                  },
                 new notes_data {
                    guid = "6",
                    title = "投資人專區-其他資訊",
                    content = MapPath("/Content/Templates/Notes/InvestorOthers.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "投資人專區-其他資訊",
                    seo_keywords = "投資人專區-其他資訊",
                  },

            };
            if (context.notes_data.ToList().Count == 0)
            {
                notes_data.ForEach(s => context.notes_data.Add(s));
                context.SaveChanges();
            }
        }

        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}