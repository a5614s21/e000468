﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.Migrations;

namespace Web.Migrations.Seed
{
    public class CustomerSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {
            category();
            data();
            knowledge();
            contact_info();
            construction();
        }



        public void category()
        {
            //分類
            var community_category = new List<community_category>
            {
                 new community_category {
                    guid = "1",
                    title = "主題一",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 1,
                    seo_description = "主題一",
                    seo_keywords = "主題一",                 
                  },
                 new community_category {
                    guid = "1",
                    title = "主題一",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 1,
                    seo_description = "主題一",
                    seo_keywords = "主題一",                  
                  },
                 new community_category {
                    guid = "2",
                    title = "主題二",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 2,
                    seo_description = "主題二",
                    seo_keywords = "主題二",
                  },
                 new community_category {
                    guid = "2",
                    title = "主題二",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 2,
                    seo_description = "主題二",
                    seo_keywords = "主題二",
                  },
                  new community_category {
                    guid = "3",
                    title = "主題三",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 3,
                    seo_description = "主題三",
                    seo_keywords = "主題三",
                  },
                    new community_category {
                    guid = "3",
                    title = "主題三",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 3,
                    seo_description = "主題三",
                    seo_keywords = "主題三",
                  },

                  new community_category {
                    guid = "4",
                    title = "主題四",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 5,
                    seo_description = "主題四",
                    seo_keywords = "主題四",
                  },
                  new community_category {
                    guid = "4",
                    title = "主題四",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 5,
                    seo_description = "主題四",
                    seo_keywords = "主題四",
                  },
             
            };
            if (context.community_category.ToList().Count == 0)
            {
                community_category.ForEach(s => context.community_category.Add(s));
                context.SaveChanges();
            }
        }

        public void data()
        {
            //
            var communitys = new List<communitys>
            {
                  new communitys {
                    guid = "1",
                    title = "內惟舘內教學-點心製作上課",
                    category = "1",
                    notes = "本次 CSR 社區關懷結合國泰健康管理於台南「文海硯」社區舉辦健康講座。",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "曉明館館內教學-插花班",
                    seo_keywords = "曉明館館內教學-插花班",
                    startdate = DateTime.Parse("2019-08-28 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list01.jpg",
                    tops = "Y",
                  },
                  new communitys {
                    guid = "1",
                    title = "內惟舘內教學-點心製作上課",
                    category = "1",
                    notes = "本次 CSR 社區關懷結合國泰健康管理於台南「文海硯」社區舉辦健康講座。",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "曉明館館內教學-插花班",
                    seo_keywords = "曉明館館內教學-插花班",
                    startdate = DateTime.Parse("2019-08-28 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list01.jpg",
                    tops = "Y",
                  },
                  new communitys {
                    guid = "2",
                    title = "苓雅館內教學-香包設計",
                    category = "4",
                    notes = "企業社區關懷 108.8.28 來到古都台南舉辦防災講座，本次活動於裕農路上的霖園...",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "苓雅館內教學-香包設計",
                    seo_keywords = "苓雅館內教學-香包設計",
                    startdate = DateTime.Parse("2019-08-28 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list02.jpg",
                    tops = "Y",
                  },
                  new communitys {
                    guid = "2",
                    title = "苓雅館內教學-香包設計",
                    category = "4",
                    notes = "企業社區關懷 108.8.28 來到古都台南舉辦防災講座，本次活動於裕農路上的霖園...",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "苓雅館內教學-香包設計",
                    seo_keywords = "苓雅館內教學-香包設計",
                    startdate = DateTime.Parse("2019-08-28 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list02.jpg",
                    tops = "Y",
                  },

                  new communitys {
                    guid = "3",
                    title = "苓雅館內教學-紙影戲偶",
                    category = "2",
                    notes = "企業社區關懷活動 108.8.06 結合國泰文教基金會於霖園圖書館-板橋館舉辦防災推...",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "苓雅館內教學-紙影戲偶",
                    seo_keywords = "苓雅館內教學-紙影戲偶",
                    startdate = DateTime.Parse("2019-08-06 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list03.jpg",
                    tops = "Y",
                  },
                  new communitys {
                    guid = "3",
                    title = "苓雅館內教學-紙影戲偶",
                    category = "2",
                    notes = "企業社區關懷活動 108.8.06 結合國泰文教基金會於霖園圖書館-板橋館舉辦防災推...",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "苓雅館內教學-紙影戲偶",
                    seo_keywords = "苓雅館內教學-紙影戲偶",
                    startdate = DateTime.Parse("2019-08-06 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list03.jpg",
                    tops = "Y",
                  },
                  new communitys {
                    guid = "4",
                    title = "苓雅館內教學-設計",
                    category = "2",
                    notes = "企業社區關懷活動 108.8.01 來到新北市三重區「中正家園」舉辦社區防災推廣教...",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "苓雅館內教學-設計",
                    seo_keywords = "苓雅館內教學-設計",
                    startdate = DateTime.Parse("2019-08-01 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list04.jpg",
                    tops = "Y",
                  },
                   new communitys {
                    guid = "4",
                    title = "苓雅館內教學-設計",
                    category = "2",
                    notes = "企業社區關懷活動 108.8.01 來到新北市三重區「中正家園」舉辦社區防災推廣教...",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "苓雅館內教學-設計",
                    seo_keywords = "苓雅館內教學-設計",
                    startdate = DateTime.Parse("2019-08-01 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list04.jpg",
                    tops = "Y",
                  },
                   new communitys {
                    guid = "5",
                    title = "苓雅館內教學-喜歡想像畫",
                    category = "1",
                    notes = "企業社區關懷活動 108.7.26 來到北市信義區永吉路的「名人廣場大樓」舉辦社區...",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "苓雅館內教學-喜歡想像畫",
                    seo_keywords = "苓雅館內教學-喜歡想像畫",
                    startdate = DateTime.Parse("2019-07-26 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list05.jpg",
                    tops = "Y",
                  },
                   new communitys {
                    guid = "5",
                    title = "苓雅館內教學-喜歡想像畫",
                    category = "1",
                    notes = "企業社區關懷活動 108.7.26 來到北市信義區永吉路的「名人廣場大樓」舉辦社區...",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "苓雅館內教學-喜歡想像畫",
                    seo_keywords = "苓雅館內教學-喜歡想像畫",
                    startdate = DateTime.Parse("2019-07-26 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list05.jpg",
                    tops = "Y",
                  },

                    new communitys {
                    guid = "6",
                    title = "裕農館館內教學-有氧瑜珈班",
                    category = "2",
                    notes = "裕農館館內教學-有氧瑜珈班",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "裕農館館內教學-有氧瑜珈班",
                    seo_keywords = "裕農館館內教學-有氧瑜珈班",
                    startdate = DateTime.Parse("2019-07-15 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list06.jpg",
                    tops = "N",
                  },
                     new communitys {
                    guid = "6",
                    title = "裕農館館內教學-有氧瑜珈班",
                    category = "2",
                    notes = "裕農館館內教學-有氧瑜珈班",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "裕農館館內教學-有氧瑜珈班",
                    seo_keywords = "裕農館館內教學-有氧瑜珈班",
                    startdate = DateTime.Parse("2019-07-15 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list06.jpg",
                    tops = "N",
                  },
                    new communitys {
                    guid = "7",
                    title = "曉明館館內教學-插花班",
                    category = "1",
                    notes = "本次 CSR 社區關懷結合國泰健康管理於台南「文海硯」社區舉辦健康講座。",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "曉明館館內教學-插花班",
                    seo_keywords = "曉明館館內教學-插花班",
                    startdate = DateTime.Parse("2019-07-01 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list01.jpg",
                    tops = "N",
                  },
                  new communitys {
                    guid = "7",
                    title = "曉明館館內教學-插花班",
                    category = "1",
                    notes = "本次 CSR 社區關懷結合國泰健康管理於台南「文海硯」社區舉辦健康講座。",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "曉明館館內教學-插花班",
                    seo_keywords = "曉明館館內教學-插花班",
                    startdate = DateTime.Parse("2019-07-01 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list01.jpg",
                    tops = "N",
                  },
                  new communitys {
                    guid = "8",
                    title = "台南「裕農花園新城」社區防災講座",
                    category = "4",
                    notes = "企業社區關懷 108.8.28 來到古都台南舉辦防災講座，本次活動於裕農路上的霖園...",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    seo_description = "台南「裕農花園新城」社區健康講座",
                    seo_keywords = "台南「裕農花園新城」社區健康講座",
                    startdate = DateTime.Parse("2019-06-18 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list07.jpg",
                    tops = "N",
                  },
                  new communitys {
                    guid = "8",
                    title = "台南「裕農花園新城」社區防災講座",
                    category = "4",
                    notes = "企業社區關懷 108.8.28 來到古都台南舉辦防災講座，本次活動於裕農路上的霖園...",
                    content =  MapPath("/Content/Templates/Customer/CommunityDetail.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    seo_description = "台南「裕農花園新城」社區健康講座",
                    seo_keywords = "台南「裕農花園新城」社區健康講座",
                    startdate = DateTime.Parse("2019-06-18 00:00:00"),
                    pic = "Styles/images/demo/about/about-news/list07.jpg",
                    tops = "N",
                  },


            };
            if (context.communitys.ToList().Count == 0)
            {
                communitys.ForEach(s => context.communitys.Add(s));
                context.SaveChanges();
            }
        }

        public void knowledge() {

            var knowledges = new List<knowledges>
            {
                 new knowledges {
                    guid = "1",
                    title = "防災手冊",
                    content = "秉持著「誠信正派、專業負責、穩健經營」的理念，雖歷經多次房地產景氣循環波動，但仍能在競爭激烈的...",
                    pic = "Styles/images/static/customer/customer-knowledge/list01.jpg",
                    files = "Content/Upload/files/test.pdf",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 1,
                  },
                new knowledges {
                    guid = "1",
                    title = "防災手冊",
                    content = "秉持著「誠信正派、專業負責、穩健經營」的理念，雖歷經多次房地產景氣循環波動，但仍能在競爭激烈的...",
                    pic = "Styles/images/static/customer/customer-knowledge/list01.jpg",
                    files = "Content/Upload/files/test.pdf",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 1,
                  },

                 new knowledges {
                    guid = "2",
                    title = "房屋維護保養手冊",
                    content = "秉持著「誠信正派、專業負責、穩健經營」的理念，雖歷經多次房地產景氣循環波動，但仍能在競爭激烈的...",
                    pic = "Styles/images/static/customer/customer-knowledge/list02.jpg",
                    files = "Content/Upload/files/test.pdf",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 2,
                  },
                 new knowledges {
                    guid = "2",
                    title = "房屋維護保養手冊",
                    content = "秉持著「誠信正派、專業負責、穩健經營」的理念，雖歷經多次房地產景氣循環波動，但仍能在競爭激烈的...",
                    pic = "Styles/images/static/customer/customer-knowledge/list02.jpg",
                    files = "Content/Upload/files/test.pdf",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tenw",
                    sortIndex = 2,
                  },
                 new knowledges {
                    guid = "3",
                    title = "簡易房屋設備之保養維護",
                    content = "秉持著「誠信正派、專業負責、穩健經營」的理念，雖歷經多次房地產景氣循環波動，但仍能在競爭激烈的...",
                    pic = "Styles/images/static/customer/customer-knowledge/list03.jpg",
                    files = "Content/Upload/files/test.pdf",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 3,
                  },

                 new knowledges {
                    guid = "3",
                    title = "簡易房屋設備之保養維護",
                    content = "秉持著「誠信正派、專業負責、穩健經營」的理念，雖歷經多次房地產景氣循環波動，但仍能在競爭激烈的...",
                    pic = "Styles/images/static/customer/customer-knowledge/list03.jpg",
                    files = "Content/Upload/files/test.pdf",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 3,
                  },

                 new knowledges {
                    guid = "4",
                    title = "簡易房屋設備之保養維護",
                    content = "秉持著「誠信正派、專業負責、穩健經營」的理念，雖歷經多次房地產景氣循環波動，但仍能在競爭激烈的...",
                    pic = "Styles/images/static/customer/customer-knowledge/list04.jpg",
                    files = "Content/Upload/files/test.pdf",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 4,
                  },

                 new knowledges {
                    guid = "4",
                    title = "簡易房屋設備之保養維護",
                    content = "秉持著「誠信正派、專業負責、穩健經營」的理念，雖歷經多次房地產景氣循環波動，但仍能在競爭激烈的...",
                    pic = "Styles/images/static/customer/customer-knowledge/list04.jpg",
                    files = "Content/Upload/files/test.pdf",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 4,
                  },

            };
            if (context.knowledges.ToList().Count == 0)
            {
                knowledges.ForEach(s => context.knowledges.Add(s));
                context.SaveChanges();
            }
        }

        public void contact_info()
        {
            //分類
            var contact_info = new List<contact_info>
            {
                 new contact_info {
                    guid = "1",
                    title = "土地買賣",
                    content = "[{\"key\":\"title\",\"val\":[\"台北總公司-開發部\",\"專案三部-台中\",\"專案三部-台南\",\"專案三部-高雄\"]},{\"key\":\"tel\",\"val\":[\"02-2377-9968 分機 5420\",\"04-2301-2456\",\"06-220-7869\",\"07-237-6699\"]}]",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 1,
                  },
               new contact_info {
                    guid = "1",
                    title = "土地買賣",
                    content = "[{\"key\":\"title\",\"val\":[\"台北總公司-開發部\",\"專案三部-台中\",\"專案三部-台南\",\"專案三部-高雄\"]},{\"key\":\"tel\",\"val\":[\"02-2377-9968 分機 5420\",\"04-2301-2456\",\"06-220-7869\",\"07-237-6699\"]}]",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 1,
                  },
                new contact_info {
                    guid = "2",
                    title = "房屋銷售",
                    content = "[{\"key\":\"title\",\"val\":[\"台北總公司-專案一、二部\",\"專案三部-台中\",\"專案三部-台南\",\"專案三部-高雄\"]},{\"key\":\"tel\",\"val\":[\"02-2377-9968 分機 5522、5621 / 0800-068-036\",\"04-2301-2456 / 0800-017-736\",\"06-220-7869 / 0800-007-869\",\"07-237-6699 / 0800-050-051\"]}]",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 2,
                  },
                 new contact_info {
                    guid = "2",
                    title = "房屋銷售",
                    content = "[{\"key\":\"title\",\"val\":[\"台北總公司-專案一、二部\",\"專案三部-台中\",\"專案三部-台南\",\"專案三部-高雄\"]},{\"key\":\"tel\",\"val\":[\"02-2377-9968 分機 5522、5621 / 0800-068-036\",\"04-2301-2456 / 0800-017-736\",\"06-220-7869 / 0800-007-869\",\"07-237-6699 / 0800-050-051\"]}]",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 2,
                  },

            };
            if (context.contact_info.ToList().Count == 0)
            {
                contact_info.ForEach(s => context.contact_info.Add(s));
                context.SaveChanges();
            }
        }


        /// <summary>
        /// 建築工法
        /// </summary>
        public void construction()
        {
            var constructions = new List<constructions>
            {
                 new constructions {
                    guid = "1",
                    title = "建築-土建工程",
                    content = MapPath("/Content/Templates/Customer/Construction1.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 1,
                    seo_description = "建築-土建工程",
                    seo_keywords = "建築-土建工程",
                  },
               new constructions {
                    guid = "1",
                    title = "建築-土建工程",
                    content = MapPath("/Content/Templates/Customer/Construction1.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 1,
                    seo_description = "建築-土建工程",
                    seo_keywords = "建築-土建工程",
                  },
                new constructions {
                    guid = "2",
                    title = "建築-防水工程",
                    content = MapPath("/Content/Templates/Customer/Construction2.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 2,
                    seo_description = "建築-防水工程",
                    seo_keywords = "建築-防水工程",
                  },
                 new constructions {
                    guid = "2",
                    title = "建築 - 防水工程",
                    content = MapPath("/Content/Templates/Customer/Construction2.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 2,
                    seo_description = "建築-防水工程",
                    seo_keywords = "建築-防水工程",
                  },
                   new constructions {
                    guid = "3",
                    title = "建築-裝修工程",
                    content = MapPath("/Content/Templates/Customer/Construction3.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 3,
                    seo_description = "建築-裝修工程",
                    seo_keywords = "建築-裝修工程",
                  },
                   new constructions {
                    guid = "3",
                    title = "建築-裝修工程",
                    content = MapPath("/Content/Templates/Customer/Construction3.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 3,
                    seo_description = "建築-裝修工程",
                    seo_keywords = "建築-裝修工程",
                  },
                   new constructions {
                    guid = "4",
                    title = "機電-強弱電",
                    content = MapPath("/Content/Templates/Customer/Construction4.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 4,
                    seo_description = "機電-強弱電",
                    seo_keywords = "機電-強弱電",
                  },
                    new constructions {
                    guid = "4",
                    title = "機電-強弱電",
                    content = MapPath("/Content/Templates/Customer/Construction4.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 4,
                    seo_description = "機電-強弱電",
                    seo_keywords = "機電-強弱電",
                  },
                   new constructions {
                    guid = "5",
                    title = "機電-給排水",
                    content = MapPath("/Content/Templates/Customer/Construction5.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                    sortIndex = 5,
                    seo_description = "機電-給排水",
                    seo_keywords = "機電-給排水",
                  },
                   new constructions {
                    guid = "5",
                    title = "機電-給排水",
                    content = MapPath("/Content/Templates/Customer/Construction5.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                    sortIndex = 5,
                    seo_description = "機電-給排水",
                    seo_keywords = "機電-給排水",
                  },

            };
            if (context.constructions.ToList().Count == 0)
            {
                constructions.ForEach(s => context.constructions.Add(s));
                context.SaveChanges();
            }
        }


        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }

    }
}