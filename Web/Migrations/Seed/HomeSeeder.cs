﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using Web.Models;
using Web.Migrations;
using Newtonsoft.Json;

namespace Web.Migrations.Seed
{
    public class HomeSeeder
    {
        public Web.Models.Model context = new Web.Models.Model();

        public void run()
        {           
            data();
            index_info();
        }
               

        public void data()
        {
            //最新消息
            var banners = new List<banners>
            {
                 new banners {
                    guid = "1",
                    sortIndex = 1,
                    title = "Banner1",                  
                    link = "#",
                    pic = "Styles/images/static/home/banner01.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",                     
                  },
               new banners {
                    guid = "1",
                    sortIndex = 1,
                    title = "Banner1",
                    link = "#",
                    pic = "Styles/images/static/home/banner01.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
               new banners {
                    guid = "2",
                    sortIndex = 2,
                    title = "Banner2",
                    link = "#",
                    pic = "Styles/images/static/home/banner02.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
               new banners {
                    guid = "2",
                    sortIndex = 2,
                    title = "Banner2",
                    link = "#",
                    pic = "Styles/images/static/home/banner02.jpg",
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },
            };
            if (context.banners.ToList().Count == 0)
            {
                banners.ForEach(s => context.banners.Add(s));
                context.SaveChanges();
            }
        }

        public void index_info()
        {
            //x
            var index_info = new List<index_info>
            {
                 new index_info {
                    guid = "1",                   
                    title = "不變的承諾，\r\n在台灣最美好的土地上紮根",
                    sub_title = "以追求永恆、前瞻的建築為理念，\r\n為台灣這塊土地營造優質的生活及高質感建築。",
                    household = "53000",
                    area = "8100000",
                    content = MapPath("/Content/Templates/Home/Info.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "tw",
                  },
                 new index_info {
                    guid = "1",
                    title = "不變的承諾，\r\n在台灣最美好的土地上紮根",
                    sub_title = "以追求永恆、前瞻的建築為理念，\r\n為台灣這塊土地營造優質的生活及高質感建築。",
                    household = "53000",
                    area = "8100000",
                    content = MapPath("/Content/Templates/Home/Info.html"),
                    modifydate = DateTime.Now,
                    status = "Y",
                    create_date = DateTime.Now,
                    lang = "en",
                  },

            };
            if (context.index_info.ToList().Count == 0)
            {
                index_info.ForEach(s => context.index_info.Add(s));
                context.SaveChanges();
            }
        }

        private string MapPath(string seedFile)
        {
            if (HttpContext.Current != null)
                return HostingEnvironment.MapPath(seedFile);

            var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath; //was AbsolutePath but didn't work with spaces according to comments
            var directoryName = Path.GetDirectoryName(absolutePath);
            var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));


            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    return line;
                }
            }
            catch (IOException e)
            {
                return path;
            }
        }
    }
}