namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_form_subject_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.form_subject",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        category = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'名稱' ,'SCHEMA', N'dbo','TABLE', N'form_subject', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'form_subject', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'form_subject', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'form_subject', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'form_subject', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'form_subject', 'COLUMN', N'lang'");


        }
        
        public override void Down()
        {
            DropTable("dbo.form_subject");
        }
    }
}
