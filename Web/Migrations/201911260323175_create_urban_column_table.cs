namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_urban_column_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.urban_column",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        category = c.String(maxLength: 64),
                        content = c.String(),
                        files = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        en_title = c.String(maxLength: 255),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'都更案名稱' ,'SCHEMA', N'dbo','TABLE', N'urban_column', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'分類' ,'SCHEMA', N'dbo','TABLE', N'urban_column', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'urban_column', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'檔案下載' ,'SCHEMA', N'dbo','TABLE', N'urban_column', 'COLUMN', N'files'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'相關圖片' ,'SCHEMA', N'dbo','TABLE', N'urban_column', 'COLUMN', N'pic'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'urban_column', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'urban_column', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'urban_column', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'urban_column', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'urban_column', 'COLUMN', N'lang'");

            CreateTable(
                "dbo.urban_column_category",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        en_title = c.String(maxLength: 255),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'分類名稱' ,'SCHEMA', N'dbo','TABLE', N'urban_column_category', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'urban_column_category', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'urban_column_category', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'urban_column_category', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'urban_column_category', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'urban_column_category', 'COLUMN', N'lang'");


        }

        public override void Down()
        {
            DropTable("dbo.urban_column_category");
            DropTable("dbo.urban_column");
        }
    }
}
