namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_share_download_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.share_download",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        files = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        types = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'項目' ,'SCHEMA', N'dbo','TABLE', N'share_download', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'PDF檔案' ,'SCHEMA', N'dbo','TABLE', N'share_download', 'COLUMN', N'files'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'share_download', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'share_download', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'share_download', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'share_download', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'share_download', 'COLUMN', N'lang'");

        }
        
        public override void Down()
        {
            DropTable("dbo.share_download");
        }
    }
}
