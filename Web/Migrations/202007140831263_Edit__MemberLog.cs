namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__MemberLog : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.member_log", "types");
        }
        
        public override void Down()
        {
            AddColumn("dbo.member_log", "types", c => c.String(maxLength: 255));
        }
    }
}
