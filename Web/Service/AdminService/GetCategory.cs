﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using Web.AdminService;
using System.Runtime.Remoting;

namespace Web.AdminService
{
    public class GetCategory : Controller
    {
        /// <summary>
        /// 取得分類(全部)
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getCategory(string tables, dynamic data = null)
        {
            Model DB = new Model();
            dynamic re = null;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系
            string dataType = "";

            system_menu system_menu = DB.system_menu.Where(m => m.tables == tables).FirstOrDefault();

            if (data != null)
            {
                dataType = data.GetType().ToString();
            }

            switch (tables)
            {
                //社區關懷
                case "urban_column_category":

                    if (dataType == "")
                    {
                        re = Repository.Urban.UrbanColumnCategory.All(defLang);
                    }
                    else
                    {
                        if (dataType == "System.String")
                        {
                            string guid = Convert.ToString(data);
                            urban_column_category temp = Repository.Urban.UrbanColumnCategory.Single(defLang, guid);
                            re = temp.title;
                        }
                    }

                    break;
                //社區關懷
                case "community_category":

                    if (dataType == "")
                    {
                        re = Repository.Customer.CommunityCategory.All(defLang);
                    }
                    else
                    {
                        if (dataType == "System.String")
                        {
                            string guid = Convert.ToString(data);
                            community_category temp = Repository.Customer.CommunityCategory.Single(defLang, guid);
                            re = temp.title;
                        }
                    }

                    break;

                //公益活動分類
                case "charity_category":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.charity_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.charity_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).Where(m => m.guid == guid).FirstOrDefault().title;
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;
                //經典建案分類
                case "cases_classic":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.cases_classic.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.cases_classic.Where(m => m.status == "Y").Where(m => m.lang == defLang).Where(m => m.guid == guid).FirstOrDefault();
                                if (re != null)
                                {
                                    re = re.title;
                                }
                                else
                                {
                                    re = "";
                                }
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;
                //建案地區
                case "cases_category":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.cases_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.cases_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).Where(m => m.guid == guid).FirstOrDefault();
                                if (re != null)
                                {
                                    re = re.title;
                                }
                                else
                                {
                                    re = "";
                                }
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;
                //最新消息分類
                case "news_category":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.news_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.news_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).Where(m => m.guid == guid).FirstOrDefault();
                                if (re != null)
                                {
                                    re = re.title;
                                }
                                else
                                {
                                    re = "";
                                }
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;
                //群組
                case "roles":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.roles.Where(m => m.status == "Y").ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.roles.Where(m => m.status == "Y").Where(m => m.guid == guid).FirstOrDefault().title;
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;

                //使用者
                case "user":
                    if (tables != null && tables != "")
                    {
                        if (data == null)
                        {
                            re = DB.user.Where(m => m.status == "Y").ToList();
                        }
                        else
                        {
                            try
                            {
                                string guid = data.ToString();
                                re = DB.user.Where(m => m.status == "Y").Where(m => m.guid == guid).FirstOrDefault().name;
                            }
                            catch
                            {
                                re = "";
                            }
                        }
                    }
                    break;
            }

            return re;
        }
    }
}