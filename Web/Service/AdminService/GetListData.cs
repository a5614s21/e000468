﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using Web.AdminService;

namespace Web.AdminService
{
    public class GetListData
    {
        /// <summary>
        /// 取得列表資料提供給DataTable
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> getListData(string tables, NameValueCollection requests, string urlRoot)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            Dictionary<String, Object> list = new Dictionary<String, Object>();
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            int skip = int.Parse(requests["start"].ToString());
            int take = int.Parse(requests["length"].ToString());
            if (take == -1)
            {
                take = 9999;
            }
            int iTotalDisplayRecords = 0;
            dynamic listData = null;
            dynamic sSearch = null;

            if (!string.IsNullOrEmpty(requests["search[value]"]))
            {
                sSearch = Newtonsoft.Json.Linq.JArray.Parse(requests["search[value]"].ToString());
                sSearch = sSearch[0];
            }

            //取得預設傳值
            Dictionary<string, string> defaultSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(requests["sSearch"]))
            {
                try
                {
                    var defaultSearchStr = requests["sSearch"].ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Split(new[] { '=' }));
                    foreach (var item in defaultSearchStr)
                    {
                        defaultSearch.Add(item[0], item[1]);
                    }
                }
                catch { }
            }

            string columns = requests["order[0][column]"];

            string orderByKey = requests["columns[" + columns + "][data]"].ToString();
            string orderByType = requests["order[0][dir]"].ToString();
            string orderBy = formatOrderByKey(tables, orderByKey) + " " + orderByType.ToUpper();

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //工程進度
                case "project_progress":
                    if (model != null)
                    {
                        var data = model.Repository<project_progress>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                ////繳款紀錄
                //case "pay_record":
                //    if (model != null)
                //    {
                //        var data = model.Repository<pay_record>();

                //        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                //        if (!string.IsNullOrEmpty(requests["search[value]"]))
                //        {
                //            if (sSearch["keyword"].ToString() != "")
                //            {
                //                string keywords = sSearch["keyword"].ToString();
                //                models = models.Where(m => m.title.Contains(keywords));
                //            }
                //            if (sSearch["status"].ToString() != "")
                //            {
                //                string status = sSearch["status"].ToString();
                //                models = models.Where(m => m.status == status);
                //            }
                //        }

                //        iTotalDisplayRecords = models.ToList().Count;
                //        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                //    }
                //    break;

                ////常見問題
                //case "cus_problem":
                //    if (model != null)
                //    {
                //        var data = model.Repository<cus_problem>();

                //        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                //        if (!string.IsNullOrEmpty(requests["search[value]"]))
                //        {
                //            if (sSearch["keyword"].ToString() != "")
                //            {
                //                string keywords = sSearch["keyword"].ToString();
                //                models = models.Where(m => m.title.Contains(keywords));
                //            }
                //            if (sSearch["status"].ToString() != "")
                //            {
                //                string status = sSearch["status"].ToString();
                //                models = models.Where(m => m.status == status);
                //            }
                //        }

                //        iTotalDisplayRecords = models.ToList().Count;
                //        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                //    }
                //    break;

                ////線上建材
                //case "online_building":
                //    if (model != null)
                //    {
                //        var data = model.Repository<online_building>();

                //        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                //        if (!string.IsNullOrEmpty(requests["search[value]"]))
                //        {
                //            if (sSearch["keyword"].ToString() != "")
                //            {
                //                string keywords = sSearch["keyword"].ToString();
                //                models = models.Where(m => m.title.Contains(keywords));
                //            }
                //            if (sSearch["status"].ToString() != "")
                //            {
                //                string status = sSearch["status"].ToString();
                //                models = models.Where(m => m.status == status);
                //            }
                //        }

                //        iTotalDisplayRecords = models.ToList().Count;
                //        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                //    }
                //    break;

                //建築工法
                case "constructions":
                    if (model != null)
                    {
                        var data = model.Repository<constructions>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //投資事業
                case "about_investment":
                    if (model != null)
                    {
                        var data = model.Repository<about_investment>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //預約賞屋
                case "cases_reservation":
                    if (model != null)
                    {
                        var data = model.Repository<cases_reservation>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.name.Contains(keywords) || m.cases.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }
                        if (defaultSearch.Count() > 0)
                        {
                            string category = defaultSearch["category"].ToString();
                            models = models.Where(m => m.category == category);
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //Banner
                case "banners":

                    if (model != null)
                    {
                        var data = model.Repository<banners>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //都更表單
                case "urban_contacts":
                    if (model != null)
                    {
                        var data = model.Repository<urban_contacts>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.subject.Contains(keywords) || m.name.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //表單項目
                case "form_subject":

                    if (model != null)
                    {
                        var data = model.Repository<form_subject>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }
                        if (defaultSearch.Count() > 0)
                        {
                            string category = defaultSearch["category"].ToString();
                            models = models.Where(m => m.category == category);
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //工程進度
                case "cases_progress":
                    if (model != null)
                    {
                        var data = model.Repository<cases_progress>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }
                        if (defaultSearch.Count() > 0)
                        {
                            string category = defaultSearch["category"].ToString();
                            models = models.Where(m => m.category == category);
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //客戶會員專區 - 常見問題
                case "member_faq":
                    if (model != null)
                    {
                        var data = model.Repository<member_faq>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //都市更新專區檔案
                case "urban_column_files":
                    if (model != null)
                    {
                        var data = model.Repository<urban_column_files>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }
                        if (defaultSearch.Count() > 0)
                        {
                            string category = defaultSearch["category"].ToString();
                            models = models.Where(m => m.category == category);
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //聯絡資訊
                case "contact_info":
                    if (model != null)
                    {
                        var data = model.Repository<contact_info>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //投資人問答集
                case "share_faq":
                    if (model != null)
                    {
                        var data = model.Repository<share_faq>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //股東常會 & 股東辦理事項表單下載
                case "share_download":
                    if (model != null)
                    {
                        var data = model.Repository<share_download>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }
                        if (defaultSearch.Count() > 0)
                        {
                            string types = defaultSearch["types"].ToString();
                            models = models.Where(m => m.types == types);
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //投資人專區 - 靜態資料
                case "investor_info":
                    if (model != null)
                    {
                        var data = model.Repository<investor_info>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //財務資訊
                case "finance_report":
                    if (model != null)
                    {
                        var data = model.Repository<finance_report>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }
                        if (defaultSearch.Count() > 0)
                        {
                            string types = defaultSearch["types"].ToString();
                            models = models.Where(m => m.types == types);
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy + " , month desc").ToList().Skip(skip).Take(take);
                    }
                    break;

                //都市更新知識庫
                case "urban_knowledge":
                    if (model != null)
                    {
                        var data = model.Repository<urban_knowledge>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //都市更新專區
                case "urban_column":
                    if (model != null)
                    {
                        var data = model.Repository<urban_column>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.category == category);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //都市更新專區分類
                case "urban_column_category":
                    if (model != null)
                    {
                        var data = model.Repository<urban_column_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //都市更新Q&A
                case "urban_faq":
                    if (model != null)
                    {
                        var data = model.Repository<urban_faq>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //都市更新消息
                case "urban_news":
                    if (model != null)
                    {
                        var data = model.Repository<urban_news>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //居家常識
                case "knowledges":
                    if (model != null)
                    {
                        var data = model.Repository<knowledges>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //社區關懷分類
                case "community_category":
                    if (model != null)
                    {
                        var data = model.Repository<community_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //社區關懷
                case "communitys":
                    if (model != null)
                    {
                        var data = model.Repository<communitys>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.category.Contains(category));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //房地產指數
                case "about_houses":
                    if (model != null)
                    {
                        var data = model.Repository<about_houses>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
                //公司治理
                case "about_manages":
                    if (model != null)
                    {
                        var data = model.Repository<about_manages>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //重要公司內規
                case "regulations":
                    if (model != null)
                    {
                        var data = model.Repository<regulations>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //董事會決議事項
                case "resolutions":
                    if (model != null)
                    {
                        var data = model.Repository<resolutions>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //審計委員會
                case "auditcommittee":
                    if (model != null)
                    {
                        var data = model.Repository<auditcommittee>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //薪酬委員會
                case "remuneration":
                    if (model != null)
                    {
                        var data = model.Repository<remuneration>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //關於國建
                case "abouts":
                    if (model != null)
                    {
                        var data = model.Repository<abouts>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //得獎記事
                case "awards":
                    if (model != null)
                    {
                        var data = model.Repository<awards>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //公益活動分類
                case "charity_category":
                    if (model != null)
                    {
                        var data = model.Repository<charity_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //公益活動
                case "charitys":
                    if (model != null)
                    {
                        var data = model.Repository<charitys>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.category.Contains(category));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //經典建案分類
                case "cases_classic":
                    if (model != null)
                    {
                        var data = model.Repository<cases_classic>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //年度總覽
                case "annuals":
                    if (model != null)
                    {
                        var data = model.Repository<annuals>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //聯絡我們
                case "contacts":
                    if (model != null)
                    {
                        var data = model.Repository<contacts>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.subject.Contains(keywords) || m.name.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //熱銷建案地區
                case "cases_category":
                    if (model != null)
                    {
                        var data = model.Repository<cases_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //熱銷建案
                case "cases":
                    if (model != null)
                    {
                        var data = model.Repository<cases>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.category == category || m.classic == category);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //最新消息分類
                case "news_category":
                    if (model != null)
                    {
                        var data = model.Repository<news_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //最新消息
                case "news":
                    if (model != null)
                    {
                        var data = model.Repository<news>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.category.Contains(category));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //群組管理
                case "roles":
                    if (model != null)
                    {
                        var data = model.Repository<roles>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //使用者
                case "user":
                    if (model != null)
                    {
                        var data = model.Repository<user>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.username != "sysadmin");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.username.Contains(keywords) || m.name.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.role_guid.Contains(category));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //防火牆
                case "firewalls":
                    if (model != null)
                    {
                        var data = model.Repository<firewalls>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //系統日誌
                case "system_log":
                    if (model != null)
                    {
                        var data = model.Repository<system_log>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords) || m.ip.Contains(keywords) || m.notes.Contains(keywords));
                            }
                            /*if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }*/
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //資源回收桶
                case "ashcan":
                    if (model != null)
                    {
                        var data = model.Repository<ashcan>();

                        var models = data.ReadsWhere(m => m.from_guid != "");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //會員登入紀錄
                case "member_log":
                    if (model != null)
                    {
                        var data = model.Repository<member_log>();
                        var models = data.ReadsWhere(m => m.status != "D");
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords) || x.ip.Contains(keywords));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //繳款紀錄API呼叫紀錄
                case "payment_apilog":
                    if (model != null)
                    {
                        var data = model.Repository<payment_apilog>();
                        var models = data.ReadsWhere(x => x.status != "D");
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.ToList().Where(x => x.title.Contains(keywords) || x.ip.Contains(keywords) || x.create_date.Value.ToString("yyyy-MM-dd HH:mm:ss").Contains(keywords)).AsQueryable();
                            }
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
            }

            list.Add("data", DataTableListData.dataTableListData(listData, tables, urlRoot, requests));
            list.Add("iTotalDisplayRecords", iTotalDisplayRecords);

            return list;
        }

        /// <summary>
        /// 格式化排序欄位
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="orderByKey"></param>
        /// <returns></returns>
        public static string formatOrderByKey(string tables, string orderByKey)
        {
            string re = "";
            switch (orderByKey)
            {
                case "info":
                    re = "title";
                    break;

                case "view_info":
                    re = "nums";
                    break;

                case "forum_status":
                    re = "status";
                    break;

                case "action":
                    re = "guid";
                    break;

                case "user":
                    re = "user_guid";
                    break;

                case "category":
                    re = "category";
                    if (tables == "package_content")
                    {
                        re = "package_guid";
                    }
                    break;

                default:
                    re = orderByKey;
                    break;
            }
            return re;
        }
    }
}