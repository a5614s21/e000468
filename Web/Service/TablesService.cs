﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using Microsoft.Ajax.Utilities;

namespace Web.Service
{
    public class TablesService
    {
        /// <summary>
        /// 取得DataTable欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic dataTableTitle(string tables)
        {
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //建築工法
                case "constructions":
                    re = Repository.Customer.Constructions.dataTableTitle();
                    break;

                //投資事業
                //投資事業
                case "about_investment":
                    re = Repository.About.Investment.dataTableTitle();
                    break;
                //預約賞屋
                case "cases_reservation":
                    re = Repository.Cases.CasesReservation.dataTableTitle();
                    break;
                //首頁資訊
                case "index_info":
                    re = Repository.Home.IndexInfo.dataTableTitle();
                    break;

                //Banner
                case "banners":
                    re = Repository.Home.Banners.dataTableTitle();
                    break;

                //聯絡表單
                case "contacts":
                    re = Repository.Customer.Contact.dataTableTitle();
                    break;
                //都更表單
                case "urban_contacts":
                    re = Repository.Urban.UrbanContact.dataTableTitle();
                    break;
                //表單項目
                case "form_subject":
                    re = Repository.Forms.FormSubject.dataTableTitle();
                    break;
                //工程進度
                case "cases_progress":
                    re = Repository.Cases.CasesProgress.dataTableTitle();
                    break;
                //客戶工程進度
                case "project_progress":
                    re = Repository.Customer.ProjectProgress.dataTableTitle();
                    break;
                ////客戶繳款紀錄
                //case "pay_record":
                //    re = Repository.Customer.PayRecord.dataTableTitle();
                //    break;
                ////客戶常見問題
                //case "cus_problem":
                //    re = Repository.Customer.CusProblem.dataTableTitle();
                //    break;
                ////客戶線上建材
                //case "online_building":
                //    re = Repository.Customer.OnlineBuild.dataTableTitle();
                //    break;
                //客戶會員專區 - 常見問題
                case "member_faq":
                    re = Repository.Member.MemberFaq.dataTableTitle();
                    break;
                //都市更新專區檔案
                case "urban_column_files":
                    re = Repository.Urban.UrbanColumnFiles.dataTableTitle();
                    break;
                //聯絡資訊
                case "contact_info":
                    re = Repository.Customer.ContactInfo.dataTableTitle();
                    break;
                //其他資訊
                case "notes_data":
                    re = Repository.Notes.NotesData.dataTableTitle();
                    break;
                //投資人問答集
                case "share_faq":
                    re = Repository.Investor.ShaerFaq.dataTableTitle();
                    break;

                //股東常會 & 股東辦理事項表單下載
                case "share_download":
                    re = Repository.Investor.ShareDownload.dataTableTitle();
                    break;

                //股權結構
                case "shareholders":
                    re = Repository.Investor.Shareholders.dataTableTitle();
                    break;
                //投資人專區 - 靜態資料
                case "investor_info":
                    re = Repository.Investor.InvestorInfo.dataTableTitle();
                    break;

                //財務資訊
                case "finance_report":
                    re = Repository.Investor.FinanceReport.dataTableTitle();
                    break;
                //都市更新知識庫
                case "urban_knowledge":
                    re = Repository.Urban.UrbanKnowledge.dataTableTitle();
                    break;
                //都市更新專區
                case "urban_column":
                    re = Repository.Urban.UrbanColumn.dataTableTitle();
                    break;
                //都市更新專區分類
                case "urban_column_category":
                    re = Repository.Urban.UrbanColumnCategory.dataTableTitle();
                    break;
                //都市更新Q&A
                case "urban_faq":
                    re = Repository.Urban.UrbanFaq.dataTableTitle();
                    break;
                //都市更新消息
                case "urban_news":
                    re = Repository.Urban.UrbanNews.dataTableTitle();
                    break;
                //社區關懷分類
                case "knowledges":
                    re = Repository.Customer.Knowledges.dataTableTitle();
                    break;
                //社區關懷分類
                case "community_category":
                    re = Repository.Customer.CommunityCategory.dataTableTitle();
                    break;

                //社區關懷
                case "communitys":
                    re = Repository.Customer.CommunityDetail.dataTableTitle();
                    break;

                //房地產指數
                case "about_houses":
                    re = Repository.About.Houses.dataTableTitle();
                    break;

                //公司治理
                case "about_manages":
                    re = Repository.About.Manages.dataTableTitle();
                    break;

                //資訊安全
                case "manage_safety":
                    re = Repository.About.Safety.dataTableTitle();
                    break;

                //重要公司內規
                case "regulations":
                    re = Repository.About.Regulation.dataTableTitle();
                    break;

                //董事會決議事項
                case "resolutions":
                    re = Repository.About.Resolution.dataTableTitle();
                    break;

                //審計委員會
                case "auditcommittee":
                    re = Repository.About.Auditcommittee.dataTableTitle();
                    break;

                //薪酬委員會
                case "remuneration":
                    re = Repository.About.Remuneration.dataTableTitle();
                    break;

                //關於國建
                case "abouts":
                    re = Repository.About.About.dataTableTitle();
                    break;
                //得獎記事
                case "awards":
                    re = Repository.About.Awards.dataTableTitle();
                    break;
                //公益活動分類
                case "charity_category":
                    re = Repository.Charity.Category.dataTableTitle();
                    break;

                //公益活動
                case "charitys":
                    re = Repository.Charity.Data.dataTableTitle();
                    break;
                //經典建案分類
                case "cases_classic":
                    re = Repository.Cases.CasesClassic.dataTableTitle();
                    break;
                //年度總覽
                case "annuals":
                    re = Repository.Cases.Annuals.dataTableTitle();
                    break;
                //熱銷建案地區
                case "cases_category":
                    re = Repository.Cases.CasesCategory.dataTableTitle();
                    break;
                //熱銷建案
                case "cases":
                    re = Repository.Cases.CasesData.dataTableTitle();
                    break;
                //系統日誌
                case "system_log":
                    re = Repository.System.Log.dataTableTitle();
                    break;
                //防火牆
                case "firewalls":
                    re = Repository.System.Firewalls.dataTableTitle();
                    break;

                //最新消息
                case "news":
                    re = Repository.News.Data.dataTableTitle();
                    break;

                //最新消息分類
                case "news_category":
                    re = Repository.News.Category.dataTableTitle();
                    break;

                //群組管理
                case "roles":
                    re = Repository.Admins.Roles.dataTableTitle();
                    break;

                //使用者
                case "user":
                    re = Repository.Admins.User.dataTableTitle();
                    break;

                //資源回收桶
                case "ashcan":
                    re = Repository.System.Ashcan.dataTableTitle();
                    break;

                //會員登入紀錄
                case "member_log":
                    re = Repository.System.MemberLog.dataTableTitle();
                    break;

                //API呼叫紀錄
                case "payment_apilog":
                    re = Repository.System.PaymentApiLog.dataTableTitle();
                    break;
            }

            return re;
        }

        /// <summary>
        /// 取得預設排序
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy(string tables)
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //建築工法
                case "constructions":
                    re = Repository.Customer.Constructions.defaultOrderBy();
                    break;
                //投資事業
                case "about_investment":
                    re = Repository.About.Investment.defaultOrderBy();
                    break;
                //預約賞屋
                case "cases_reservation":
                    re = Repository.Cases.CasesReservation.defaultOrderBy();
                    break;
                //首頁資訊
                case "index_info":
                    re = Repository.Home.IndexInfo.defaultOrderBy();
                    break;
                //Banner
                case "banners":
                    re = Repository.Home.Banners.defaultOrderBy();
                    break;
                //聯絡表單
                case "contacts":
                    re = Repository.Customer.Contact.defaultOrderBy();
                    break;
                //都更表單
                case "urban_contacts":
                    re = Repository.Urban.UrbanContact.defaultOrderBy();
                    break;
                //表單項目
                case "form_subject":
                    re = Repository.Forms.FormSubject.defaultOrderBy();
                    break;
                //工程進度
                case "cases_progress":
                    re = Repository.Cases.CasesProgress.defaultOrderBy();
                    break;
                //客戶工程進度
                case "project_progress":
                    re = Repository.Customer.ProjectProgress.defaultOrderBy();
                    break;
                ////客戶繳款紀錄
                //case "pay_record":
                //    re = Repository.Customer.PayRecord.defaultOrderBy();
                //    break;
                ////客戶常見問題
                //case "cus_problem":
                //    re = Repository.Customer.CusProblem.defaultOrderBy();
                //    break;
                ////客戶線上建材
                //case "online_building":
                //    re = Repository.Customer.OnlineBuild.defaultOrderBy();
                //    break;
                //客戶會員專區 - 常見問題
                case "member_faq":
                    re = Repository.Member.MemberFaq.defaultOrderBy();
                    break;
                //都市更新專區檔案
                case "urban_column_files":
                    re = Repository.Urban.UrbanColumnFiles.defaultOrderBy();
                    break;
                //聯絡資訊
                case "contact_info":
                    re = Repository.Customer.ContactInfo.defaultOrderBy();
                    break;
                //其他資訊
                case "notes_data":
                    re = Repository.Notes.NotesData.defaultOrderBy();
                    break;
                //投資人問答集
                case "share_faq":
                    re = Repository.Investor.ShaerFaq.defaultOrderBy();
                    break;

                //股東常會 & 股東辦理事項表單下載
                case "share_download":
                    re = Repository.Investor.ShareDownload.defaultOrderBy();
                    break;
                //股權結構
                case "shareholders":
                    re = Repository.Investor.Shareholders.defaultOrderBy();
                    break;
                //投資人專區 - 靜態資料
                case "investor_info":
                    re = Repository.Investor.InvestorInfo.defaultOrderBy();
                    break;

                //財務資訊
                case "finance_report":
                    re = Repository.Investor.FinanceReport.defaultOrderBy();
                    break;
                //都市更新知識庫
                case "urban_knowledge":
                    re = Repository.Urban.UrbanKnowledge.defaultOrderBy();
                    break;
                //都市更新專區
                case "urban_column":
                    re = Repository.Urban.UrbanColumn.defaultOrderBy();
                    break;
                //都市更新專區分類
                case "urban_column_category":
                    re = Repository.Urban.UrbanColumnCategory.defaultOrderBy();
                    break;
                //都市更新Q&A
                case "urban_faq":
                    re = Repository.Urban.UrbanFaq.defaultOrderBy();
                    break;
                //都市更新消息
                case "urban_news":
                    re = Repository.Urban.UrbanNews.defaultOrderBy();
                    break;
                //社區關懷分類
                case "knowledges":
                    re = Repository.Customer.Knowledges.defaultOrderBy();
                    break;

                //社區關懷分類
                case "community_category":
                    re = Repository.Customer.CommunityCategory.defaultOrderBy();
                    break;

                //社區關懷
                case "communitys":
                    re = Repository.Customer.CommunityDetail.defaultOrderBy();
                    break;
                //房地產指數
                case "about_houses":
                    re = Repository.About.Houses.defaultOrderBy();
                    break;
                //公司治理
                case "about_manages":
                    re = Repository.About.Manages.defaultOrderBy();
                    break;

                //資訊安全
                case "manage_safety":
                    re = Repository.About.Safety.defaultOrderBy();
                    break;

                //重要公司內規
                case "regulations":
                    re = Repository.About.Regulation.defaultOrderBy();
                    break;

                //董事會決議事項
                case "resolutions":
                    re = Repository.About.Resolution.defaultOrderBy();
                    break;

                //審計委員會
                case "auditcommittee":
                    re = Repository.About.Auditcommittee.defaultOrderBy();
                    break;

                //薪酬委員會
                case "remuneration":
                    re = Repository.About.Remuneration.defaultOrderBy();
                    break;

                //關於國建
                case "abouts":
                    re = Repository.About.About.defaultOrderBy();
                    break;

                //得獎記事
                case "awards":
                    re = Repository.About.Awards.defaultOrderBy();
                    break;
                //公益活動分類
                case "charity_category":
                    re = Repository.Charity.Category.defaultOrderBy();
                    break;

                //公益活動
                case "charitys":
                    re = Repository.Charity.Data.defaultOrderBy();
                    break;
                //經典建案分類
                case "cases_classic":
                    re = Repository.Cases.CasesClassic.defaultOrderBy();
                    break;
                //年度總覽
                case "annuals":
                    re = Repository.Cases.Annuals.defaultOrderBy();
                    break;
                //最新消息
                case "news":
                    re = Repository.News.Data.defaultOrderBy();
                    break;
                //熱銷建案地區
                case "cases_category":
                    re = Repository.Cases.CasesCategory.defaultOrderBy();
                    break;
                //熱銷建案
                case "cases":
                    re = Repository.Cases.CasesData.defaultOrderBy();
                    break;
                //系統日誌
                case "system_log":
                    re.Clear();
                    re = Repository.System.Log.defaultOrderBy();
                    break;
                //防火牆
                case "firewalls":
                    re.Clear();
                    re = Repository.System.Firewalls.defaultOrderBy();
                    break;

                //會員登入紀錄
                case "member_log":
                    re.Clear();
                    re = Repository.System.MemberLog.defaultOrderBy();
                    break;

                //API呼叫紀錄
                case "payment_apilog":
                    re.Clear();
                    re = Repository.System.PaymentApiLog.defaultOrderBy();
                    break;
            }
            return re;
        }

        /// <summary>
        /// 是否使用語系
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static string useLang(string tables)
        {
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //建築工法
                case "constructions":
                    re = Repository.Customer.Constructions.useLang();
                    break;
                //投資事業
                case "about_investment":
                    re = Repository.About.Investment.useLang();
                    break;
                //預約賞屋
                case "cases_reservation":
                    re = Repository.Cases.CasesReservation.useLang();
                    break;
                //首頁資訊
                case "index_info":
                    re = Repository.Home.IndexInfo.useLang();
                    break;
                //Banner
                case "banners":
                    re = Repository.Home.Banners.useLang();
                    break;
                //聯絡表單
                case "contacts":
                    re = Repository.Customer.Contact.useLang();
                    break;
                //都更表單
                case "urban_contacts":
                    re = Repository.Urban.UrbanContact.useLang();
                    break;
                //表單項目
                case "form_subject":
                    re = Repository.Forms.FormSubject.useLang();
                    break;
                //工程進度
                case "cases_progress":
                    re = Repository.Cases.CasesProgress.useLang();
                    break;
                //客戶工程進度
                case "project_progress":
                    re = Repository.Customer.ProjectProgress.useLang();
                    break;
                ////客戶繳款紀錄
                //case "pay_record":
                //    re = Repository.Customer.PayRecord.useLang();
                //    break;
                ////客戶常見問題
                //case "cus_problem":
                //    re = Repository.Customer.CusProblem.useLang();
                //    break;
                ////客戶線上建材
                //case "online_building":
                //    re = Repository.Customer.OnlineBuild.useLang();
                //    break;
                //客戶會員專區 - 常見問題
                case "member_faq":
                    re = Repository.Member.MemberFaq.useLang();
                    break;
                //都市更新專區檔案
                case "urban_column_files":
                    re = Repository.Urban.UrbanColumnFiles.useLang();
                    break;
                //聯絡資訊
                case "contact_info":
                    re = Repository.Customer.ContactInfo.useLang();
                    break;
                //其他資訊
                case "notes_data":
                    re = Repository.Notes.NotesData.useLang();
                    break;
                //投資人問答集
                case "share_faq":
                    re = Repository.Investor.ShaerFaq.useLang();
                    break;

                //股東常會 & 股東辦理事項表單下載
                case "share_download":
                    re = Repository.Investor.ShareDownload.useLang();
                    break;
                //股權結構
                case "shareholders":
                    re = Repository.Investor.Shareholders.useLang();
                    break;
                //投資人專區 - 靜態資料
                case "investor_info":
                    re = Repository.Investor.InvestorInfo.useLang();
                    break;

                //財務資訊
                case "finance_report":
                    re = Repository.Investor.FinanceReport.useLang();
                    break;
                //都市更新知識庫
                case "urban_knowledge":
                    re = Repository.Urban.UrbanKnowledge.useLang();
                    break;
                //都市更新專區
                case "urban_column":
                    re = Repository.Urban.UrbanColumn.useLang();
                    break;
                //都市更新專區分類
                case "urban_column_category":
                    re = Repository.Urban.UrbanColumnCategory.useLang();
                    break;
                //都市更新Q&A
                case "urban_faq":
                    re = Repository.Urban.UrbanFaq.useLang();
                    break;
                //都市更新消息
                case "urban_news":
                    re = Repository.Urban.UrbanNews.useLang();
                    break;

                //居家常識
                case "knowledges":
                    re = Repository.Customer.Knowledges.useLang();
                    break;

                //社區關懷分類
                case "community_category":
                    re = Repository.Customer.CommunityCategory.useLang();
                    break;

                //社區關懷
                case "communitys":
                    re = Repository.Customer.CommunityDetail.useLang();
                    break;
                //房地產指數
                case "about_houses":
                    re = Repository.About.Houses.useLang();
                    break;
                //公司治理
                case "about_manages":
                    re = Repository.About.Manages.useLang();
                    break;

                //資訊安全
                case "manage_safety":
                    re = Repository.About.Safety.useLang();
                    break;

                //重要公司內規
                case "regulations":
                    re = Repository.About.Regulation.useLang();
                    break;

                //董事會決議事項
                case "resolutions":
                    re = Repository.About.Resolution.useLang();
                    break;

                //審計委員會
                case "auditcommittee":
                    re = Repository.About.Auditcommittee.useLang();
                    break;

                //薪酬委員會
                case "remuneration":
                    re = Repository.About.Remuneration.useLang();
                    break;

                //關於國建
                case "abouts":
                    re = Repository.About.About.useLang();
                    break;

                //得獎記事
                case "awards":
                    re = Repository.About.Awards.useLang();
                    break;
                //公益活動分類
                case "charity_category":
                    re = Repository.Charity.Category.useLang();
                    break;

                //公益活動
                case "charitys":
                    re = Repository.Charity.Data.useLang();
                    break;
                //經典建案分類
                case "cases_classic":
                    re = Repository.Cases.CasesClassic.useLang();
                    break;
                //年度總覽
                case "annuals":
                    re = Repository.Cases.Annuals.useLang();
                    break;
                //熱銷建案地區
                case "cases_category":
                    re = Repository.Cases.CasesCategory.useLang();
                    break;
                //熱銷建案
                case "cases":
                    re = Repository.Cases.CasesData.useLang();
                    break;

                //系統日誌
                case "system_log":
                    re = Repository.System.Log.useLang();
                    break;

                //防火牆
                case "firewalls":
                    re = Repository.System.Firewalls.useLang();
                    break;

                //最新消息
                case "news":
                    re = Repository.News.Data.useLang();
                    break;

                //最新消息分類
                case "news_category":
                    re = Repository.News.Category.useLang();
                    break;

                //群組管理
                case "roles":
                    re = Repository.Admins.Roles.useLang();
                    break;
                //使用者
                case "user":
                    re = Repository.Admins.User.useLang();
                    break;

                //網站基本資料
                case "web_data":
                    re = Repository.System.WebData.useLang();
                    break;
                //SMTP資料
                case "smtp_data":
                    re = Repository.System.Smtp.useLang();
                    break;

                //資源回收桶
                case "ashcan":
                    re = Repository.System.Ashcan.useLang();
                    break;
                //會員登入紀錄
                case "member_log":
                    re = Repository.System.MemberLog.useLang();
                    break;
                //API呼叫紀錄
                case "payment_apilog":
                    re = Repository.System.PaymentApiLog.useLang();
                    break;
            }

            return re;
        }

        /// <summary>
        /// 取得列表說明
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic listMessage(string tables)
        {
            dynamic re = null;
            switch (tables)
            {
                //防火牆
                case "firewalls":
                    re = Repository.System.Firewalls.listMessage();
                    break;

                //API呼叫紀錄
                case "payment_apilog":
                    re = Repository.System.PaymentApiLog.listMessage();
                    break;

                default:
                    re = "";
                    break;
            }

            return re;
        }

        /// <summary>
        /// 欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getColData(string tables, dynamic data)
        {
            dynamic re = null;
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //建築工法
                case "constructions":
                    re = Repository.Customer.Constructions.colFrom();
                    break;
                //投資事業
                case "about_investment":
                    re = Repository.About.Investment.colFrom();
                    break;
                //預約賞屋
                case "cases_reservation":
                    re = Repository.Cases.CasesReservation.colFrom();
                    break;
                //首頁資訊
                case "index_info":
                    re = Repository.Home.IndexInfo.colFrom();
                    break;
                //Banner
                case "banners":
                    re = Repository.Home.Banners.colFrom();
                    break;
                //聯絡表單
                case "contacts":
                    re = Repository.Customer.Contact.colFrom();
                    break;
                //都更表單
                case "urban_contacts":
                    re = Repository.Urban.UrbanContact.colFrom();
                    break;
                //表單項目
                case "form_subject":
                    re = Repository.Forms.FormSubject.colFrom();
                    break;
                //工程進度
                case "cases_progress":
                    re = Repository.Cases.CasesProgress.colFrom();
                    break;
                //客戶工程進度
                case "project_progress":
                    re = Repository.Customer.ProjectProgress.colFrom();
                    break;
                ////客戶繳款紀錄
                //case "pay_record":
                //    re = Repository.Customer.PayRecord.colFrom();
                //    break;
                ////客戶常見問題
                //case "cus_problem":
                //    re = Repository.Customer.CusProblem.colFrom();
                //    break;
                ////客戶線上建材
                //case "online_building":
                //    re = Repository.Customer.OnlineBuild.colFrom();
                //    break;
                //客戶會員專區 - 常見問題
                case "member_faq":
                    re = Repository.Member.MemberFaq.colFrom();
                    break;
                //都市更新專區檔案
                case "urban_column_files":
                    re = Repository.Urban.UrbanColumnFiles.colFrom();
                    break;
                //聯絡資訊
                case "contact_info":
                    re = Repository.Customer.ContactInfo.colFrom();
                    break;
                //其他資訊
                case "notes_data":
                    re = Repository.Notes.NotesData.colFrom();
                    break;
                //投資人問答集
                case "share_faq":
                    re = Repository.Investor.ShaerFaq.colFrom();
                    break;

                //股東常會 & 股東辦理事項表單下載
                case "share_download":
                    re = Repository.Investor.ShareDownload.colFrom();
                    break;
                //股權結構
                case "shareholders":
                    re = Repository.Investor.Shareholders.colFrom();
                    break;
                //投資人專區 - 靜態資料
                case "investor_info":
                    re = Repository.Investor.InvestorInfo.colFrom();
                    break;

                //財務資訊
                case "finance_report":
                    re = Repository.Investor.FinanceReport.colFrom(data);
                    break;
                //都市更新知識庫
                case "urban_knowledge":
                    re = Repository.Urban.UrbanKnowledge.colFrom();
                    break;
                //都市更新專區
                case "urban_column":
                    re = Repository.Urban.UrbanColumn.colFrom();
                    break;
                //都市更新專區分類
                case "urban_column_category":
                    re = Repository.Urban.UrbanColumnCategory.colFrom();
                    break;
                //都市更新Q&A
                case "urban_faq":
                    re = Repository.Urban.UrbanFaq.colFrom();
                    break;
                //都市更新消息
                case "urban_news":
                    re = Repository.Urban.UrbanNews.colFrom();
                    break;

                //居家常識
                case "knowledges":
                    re = Repository.Customer.Knowledges.colFrom();
                    break;
                //社區關懷分類
                case "community_category":
                    re = Repository.Customer.CommunityCategory.colFrom();
                    break;

                //社區關懷
                case "communitys":
                    re = Repository.Customer.CommunityDetail.colFrom();
                    break;

                //房地產指數
                case "about_houses":
                    re = Repository.About.Houses.colFrom();
                    break;
                //公司治理
                case "about_manages":
                    re = Repository.About.Manages.colFrom();
                    break;

                //資訊安全
                case "manage_safety":
                    re = Repository.About.Safety.colFrom();
                    break;

                //重要公司內規
                case "regulations":
                    re = Repository.About.Regulation.colFrom();
                    break;

                //董事會決議事項
                case "resolutions":
                    re = Repository.About.Resolution.colFrom();
                    break;

                //審計委員會
                case "auditcommittee":
                    re = Repository.About.Auditcommittee.colFrom();
                    break;

                //薪酬委員會
                case "remuneration":
                    re = Repository.About.Remuneration.colFrom();
                    break;

                //關於國建
                case "abouts":
                    re = Repository.About.About.colFrom();
                    break;
                //得獎記事
                case "awards":
                    re = Repository.About.Awards.colFrom();
                    break;
                //公益活動分類
                case "charity_category":
                    re = Repository.Charity.Category.colFrom();
                    break;

                //公益活動
                case "charitys":
                    re = Repository.Charity.Data.colFrom();
                    break;
                //經典建案分類
                case "cases_classic":
                    re = Repository.Cases.CasesClassic.colFrom();
                    break;
                //年度總覽
                case "annuals":
                    re = Repository.Cases.Annuals.colFrom();
                    break;
                //熱銷建案地區
                case "cases_category":
                    re = Repository.Cases.CasesCategory.colFrom();
                    break;
                //熱銷建案
                case "cases":
                    re = Repository.Cases.CasesData.colFrom();
                    break;
                //系統日誌
                case "system_log":
                    re = Repository.System.Log.colFrom();
                    break;
                //防火牆
                case "firewalls":
                    re = Repository.System.Firewalls.colFrom();
                    break;

                //最新消息
                case "news":
                    re = Repository.News.Data.colFrom();
                    break;

                //最新消息分類
                case "news_category":
                    re = Repository.News.Category.colFrom();
                    break;

                //群組管理
                case "roles":
                    re = Repository.Admins.Roles.colFrom();
                    break;
                //使用者
                case "user":
                    re = Repository.Admins.User.colFrom();
                    break;

                //網站基本資料
                case "web_data":
                    re = Repository.System.WebData.colFrom();
                    break;
                //SMTP資料
                case "smtp_data":
                    re = Repository.System.Smtp.colFrom();
                    break;

                //系統參數
                case "system_data":
                    re = Repository.System.Data.colFrom();
                    break;

                //資源回收桶
                case "ashcan":
                    re = Repository.System.Ashcan.colFrom();
                    break;

                //會員登入紀錄
                case "member_log":
                    re = Repository.System.MemberLog.colFrom();
                    break;

                //API呼叫紀錄
                case "payment_apilog":
                    re = Repository.System.PaymentApiLog.colFrom();
                    break;
            }

            return re;
        }

        /// <summary>
        /// 取得資料庫資料
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getPresetData(string tables, string guid)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            string re = "";

            Dictionary<String, Object> reValData = new Dictionary<String, Object>();
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //建築工法
                case "constructions":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<constructions>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //投資事業
                case "about_investment":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<about_investment>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //預約賞屋
                case "cases_reservation":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<cases_reservation>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //首頁資訊
                case "index_info":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<index_info>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //Banner
                case "banners":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<banners>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //聯絡表單
                case "contacts":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<contacts>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //都更表單
                case "urban_contacts":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<urban_contacts>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //表單項目
                case "form_subject":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<form_subject>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //工程進度
                case "cases_progress":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<cases_progress>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //客戶工程進度
                case "project_progress":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<project_progress>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                ////客戶繳款紀錄
                //case "pay_record":
                //    if (guid != null && guid != "")
                //    {
                //        var data = model.Repository<pay_record>();
                //        var tempData = data.ReadsWhere(m => m.guid == guid);
                //        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                //    }
                //    break;
                ////客戶常見問題
                //case "cus_problem":
                //    if (guid != null && guid != "")
                //    {
                //        var data = model.Repository<cus_problem>();
                //        var tempData = data.ReadsWhere(m => m.guid == guid);
                //        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                //    }
                //    break;
                ////客戶線上建材
                //case "online_building":
                //    if (guid != null && guid != "")
                //    {
                //        var data = model.Repository<online_building>();
                //        var tempData = data.ReadsWhere(m => m.guid == guid);
                //        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                //    }
                //    break;
                //客戶會員專區 - 常見問題
                case "member_faq":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<member_faq>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //都市更新專區檔案
                case "urban_column_files":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<urban_column_files>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //聯絡資訊
                case "contact_info":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<contact_info>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //其他資訊
                case "notes_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<notes_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //投資人問答集
                case "share_faq":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<share_faq>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //股東常會 & 股東辦理事項表單下載
                case "share_download":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<share_download>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //股權結構
                case "shareholders":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<shareholders>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //投資人專區 - 靜態資料
                case "investor_info":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<investor_info>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //財務資訊
                case "finance_report":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<finance_report>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //都市更新知識庫
                case "urban_knowledge":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<urban_knowledge>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //都市更新專區
                case "urban_column":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<urban_column>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //都市更新專區分類
                case "urban_column_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<urban_column_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //都市更新Q&A
                case "urban_faq":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<urban_faq>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //都市更新消息
                case "urban_news":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<urban_news>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //居家常識
                case "knowledges":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<knowledges>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //社區關懷分類
                case "community_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<community_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //社區關懷
                case "communitys":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<communitys>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //房地產指數
                case "about_houses":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<about_houses>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //公司治理
                case "about_manages":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<about_manages>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //資訊安全
                case "manage_safety":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<manage_safety>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //重要公司內規
                case "regulations":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<regulations>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //董事會決議事項
                case "resolutions":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<resolutions>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //審計委員會
                case "auditcommittee":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<auditcommittee>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //薪酬委員會
                case "remuneration":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<remuneration>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //關於國建
                case "abouts":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<abouts>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //得獎記事
                case "awards":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<awards>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //公益活動分類
                case "charity_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<charity_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //公益活動
                case "charitys":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<charitys>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //經典建案分類
                case "cases_classic":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<cases_classic>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //年度總覽
                case "annuals":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<annuals>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //熱銷建案地區
                case "cases_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<cases_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //熱銷建案
                case "cases":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<cases>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //系統日誌
                case "system_log":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_log>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //防火牆
                case "firewalls":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<firewalls>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //最新消息
                case "news":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<news>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);

                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //最新消息分類
                case "news_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<news_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //群組管理
                case "roles":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<roles>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }

                    break;

                //使用者
                case "user":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<user>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }

                    break;

                //網站基本資料
                case "web_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<web_data>();

                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //系統參數
                case "system_data":

                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_data>();
                        Guid newGuid = Guid.Parse(guid);
                        var tempData = data.ReadsWhere(m => m.guid == newGuid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //SMTP資料
                case "smtp_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<smtp_data>();

                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //會員登入紀錄
                case "member_log":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<member_log>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //API呼叫紀錄
                case "payment_apilog":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<payment_apilog>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
            }

            return re;
        }

        /// <summary>
        /// 回傳標題
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getDataTitle(string tables, string guid)
        {
            Model DB = new Model();
            string re = "";
            switch (tables)
            {
                case "investor_info":
                    try
                    {
                        investor_info data = Web.Repository.Investor.InvestorInfo.Single("tw", guid);
                        re = data.title;
                    }
                    catch { }

                    break;

                case "abouts":
                    try
                    {
                        abouts data = Repository.About.About.Single("tw", guid);
                        re = data.title;
                    }
                    catch { }
                    break;

                case "urban_column_files":
                    try
                    {
                        urban_column data = Repository.Urban.UrbanColumn.Single("tw", guid);
                        re = "[" + data.title + "]";
                    }
                    catch { }
                    break;

                case "cases_progress":
                    try
                    {
                        cases data = Repository.Cases.CasesData.Single("tw", guid);
                        if (data != null)
                        {
                            re = "[" + data.title + "]";
                        }
                    }
                    catch { }
                    break;

                case "cases_reservation":
                    try
                    {
                        cases data = Repository.Cases.CasesData.Single("tw", guid);
                        re = "[" + data.title + "]";
                    }
                    catch { }
                    break;
            }

            return re;
        }

        /// <summary>
        /// 複選選單選取判斷用
        /// </summary>
        /// <param name="tabels"></param>
        /// <param name="user_guid"></param>
        /// <param name="forum_guid"></param>
        /// <returns></returns>
        public static string selectMultipleSelected(string tabels, string user_guid, string forum_guid)
        {
            Model DB = new Model();

            string selected = "";
            /*
            switch(tabels)
            {
                case "forum_message":

                    int data = DB.forum_message.Where(m=>m.user_guid == user_guid).Where(m=>m.forum_guid == forum_guid).Select(a => new { guid = a.guid }).Count();
                    if(data > 0)
                    {
                        selected = " selected";
                    }
                    break;
            }
            */

            return selected;
        }

        /// <summary>
        /// 新增 修改
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="form"></param>
        /// <param name="Field"></param>
        /// <returns></returns>
        public static string saveData(string tables, string form, string Field, string guid, string actType, Dictionary<String, Object> FormObj = null)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            dynamic Model = null;//目前Model
            dynamic FromData = null;//表單資訊
            string lang = "";
            system_log system_Log = new system_log();//系統紀錄用

            var formArray = JsonConvert.DeserializeObject<Dictionary<string, object>>(form);

            string def_re_mail = "";

            try
            {
                def_re_mail = formArray["def_re_mail"].ToString();
            }
            catch { }

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //建築工法
                case "constructions":
                    FromData = JsonConvert.DeserializeObject<constructions>(form);
                    Model = model.Repository<constructions>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.constructions.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //投資事業
                case "about_investment":
                    FromData = JsonConvert.DeserializeObject<about_investment>(form);
                    Model = model.Repository<about_investment>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.about_investment.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //預約賞屋
                case "cases_reservation":
                    FromData = JsonConvert.DeserializeObject<cases_reservation>(form);
                    Model = model.Repository<cases_reservation>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.cases_reservation.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //首頁資訊
                case "index_info":
                    FromData = JsonConvert.DeserializeObject<index_info>(form);
                    Model = model.Repository<index_info>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.index_info.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //Banner
                case "banners":
                    FromData = JsonConvert.DeserializeObject<banners>(form);
                    Model = model.Repository<banners>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.banners.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //聯絡表單
                case "contacts":
                    FromData = JsonConvert.DeserializeObject<contacts>(form);
                    Model = model.Repository<contacts>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.contacts.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //都更表單
                case "urban_contacts":
                    FromData = JsonConvert.DeserializeObject<urban_contacts>(form);
                    Model = model.Repository<urban_contacts>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.urban_contacts.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //表單項目
                case "form_subject":
                    FromData = JsonConvert.DeserializeObject<form_subject>(form);
                    Model = model.Repository<form_subject>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.form_subject.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //工程進度
                case "cases_progress":
                    FromData = JsonConvert.DeserializeObject<cases_progress>(form);
                    Model = model.Repository<cases_progress>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.cases_progress.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //客戶工程進度
                case "project_progress":
                    FromData = JsonConvert.DeserializeObject<project_progress>(form);
                    Model = model.Repository<project_progress>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.project_progress.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                ////客戶繳款紀錄
                //case "pay_record":
                //    FromData = JsonConvert.DeserializeObject<pay_record>(form);
                //    Model = model.Repository<pay_record>();
                //    if (Field != "")
                //    {
                //        guid = FromData.guid;
                //        lang = FromData.lang;
                //        Model = DB.pay_record.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                //    }
                //    break;
                ////客戶常見問題
                //case "cus_problem":
                //    FromData = JsonConvert.DeserializeObject<cus_problem>(form);
                //    Model = model.Repository<cus_problem>();
                //    if (Field != "")
                //    {
                //        guid = FromData.guid;
                //        lang = FromData.lang;
                //        Model = DB.cus_problem.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                //    }
                //    break;
                ////客戶線上建材
                //case "online_building":
                //    FromData = JsonConvert.DeserializeObject<online_building>(form);
                //    Model = model.Repository<online_building>();
                //    if (Field != "")
                //    {
                //        guid = FromData.guid;
                //        lang = FromData.lang;
                //        Model = DB.online_building.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                //    }
                //    break;
                //客戶會員專區 - 常見問題
                case "member_faq":
                    FromData = JsonConvert.DeserializeObject<member_faq>(form);
                    Model = model.Repository<member_faq>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.member_faq.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //都市更新專區檔案
                case "urban_column_files":
                    FromData = JsonConvert.DeserializeObject<urban_column_files>(form);
                    Model = model.Repository<urban_column_files>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.urban_column_files.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //聯絡資訊
                case "contact_info":
                    FromData = JsonConvert.DeserializeObject<contact_info>(form);
                    Model = model.Repository<contact_info>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.contact_info.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //其他資訊
                case "notes_data":
                    FromData = JsonConvert.DeserializeObject<notes_data>(form);
                    Model = model.Repository<notes_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.notes_data.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //投資人問答集
                case "share_faq":
                    FromData = JsonConvert.DeserializeObject<share_faq>(form);
                    Model = model.Repository<share_faq>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.share_faq.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //股東常會 & 股東辦理事項表單下載
                case "share_download":
                    FromData = JsonConvert.DeserializeObject<share_download>(form);
                    Model = model.Repository<share_download>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.share_download.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //股權結構
                case "shareholders":
                    FromData = JsonConvert.DeserializeObject<shareholders>(form);
                    Model = model.Repository<shareholders>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.shareholders.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //投資人專區 - 靜態資料
                case "investor_info":
                    FromData = JsonConvert.DeserializeObject<investor_info>(form);
                    Model = model.Repository<investor_info>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.investor_info.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //財務資訊
                case "finance_report":
                    FromData = JsonConvert.DeserializeObject<finance_report>(form);
                    Model = model.Repository<finance_report>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.finance_report.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //都市更新知識庫
                case "urban_knowledge":
                    FromData = JsonConvert.DeserializeObject<urban_knowledge>(form);
                    Model = model.Repository<urban_knowledge>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.urban_knowledge.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //都市更新專區
                case "urban_column":
                    FromData = JsonConvert.DeserializeObject<urban_column>(form);
                    Model = model.Repository<urban_column>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.urban_column.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //都市更新專區分類
                case "urban_column_category":
                    FromData = JsonConvert.DeserializeObject<urban_column_category>(form);
                    Model = model.Repository<urban_column_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.urban_column_category.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //都市更新Q&A
                case "urban_faq":
                    FromData = JsonConvert.DeserializeObject<urban_faq>(form);
                    Model = model.Repository<urban_faq>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.urban_faq.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //都市更新消息
                case "urban_news":
                    FromData = JsonConvert.DeserializeObject<urban_news>(form);
                    Model = model.Repository<urban_news>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.urban_news.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //居家常識
                case "knowledges":
                    FromData = JsonConvert.DeserializeObject<knowledges>(form);
                    Model = model.Repository<knowledges>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.knowledges.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //社區關懷分類
                case "community_category":
                    FromData = JsonConvert.DeserializeObject<community_category>(form);
                    Model = model.Repository<community_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.community_category.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //社區關懷
                case "communitys":
                    FromData = JsonConvert.DeserializeObject<communitys>(form);
                    Model = model.Repository<communitys>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.communitys.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //房地產指數
                case "about_houses":
                    FromData = JsonConvert.DeserializeObject<about_houses>(form);
                    Model = model.Repository<about_houses>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.about_houses.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //公司治理
                case "about_manages":
                    FromData = JsonConvert.DeserializeObject<about_manages>(form);
                    Model = model.Repository<about_manages>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.about_manages.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //資訊安全
                case "manage_safety":
                    FromData = JsonConvert.DeserializeObject<manage_safety>(form);
                    Model = model.Repository<manage_safety>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.manage_safety.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //重要公司內規
                case "regulations":
                    FromData = JsonConvert.DeserializeObject<regulations>(form);
                    Model = model.Repository<regulations>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.regulations.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //董事會決議事項
                case "resolutions":
                    FromData = JsonConvert.DeserializeObject<resolutions>(form);
                    Model = model.Repository<resolutions>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.resolutions.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //審計委員會
                case "auditcommittee":
                    FromData = JsonConvert.DeserializeObject<auditcommittee>(form);
                    Model = model.Repository<auditcommittee>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.auditcommittee.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //薪酬委員會
                case "remuneration":
                    FromData = JsonConvert.DeserializeObject<remuneration>(form);
                    Model = model.Repository<remuneration>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.remuneration.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //關於國建
                case "abouts":
                    FromData = JsonConvert.DeserializeObject<abouts>(form);
                    Model = model.Repository<abouts>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.abouts.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //得獎記事
                case "awards":
                    FromData = JsonConvert.DeserializeObject<awards>(form);
                    Model = model.Repository<awards>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.awards.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //公益活動分類
                case "charity_category":
                    FromData = JsonConvert.DeserializeObject<charity_category>(form);
                    Model = model.Repository<charity_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.charity_category.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;

                //公益活動
                case "charitys":
                    FromData = JsonConvert.DeserializeObject<charitys>(form);
                    Model = model.Repository<charitys>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.charitys.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //經典建案分類
                case "cases_classic":
                    FromData = JsonConvert.DeserializeObject<cases_classic>(form);
                    Model = model.Repository<cases_classic>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.cases_classic.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //年度總覽
                case "annuals":
                    FromData = JsonConvert.DeserializeObject<annuals>(form);
                    Model = model.Repository<annuals>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.annuals.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //熱銷建案地區
                case "cases_category":
                    FromData = JsonConvert.DeserializeObject<cases_category>(form);
                    Model = model.Repository<cases_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.cases_category.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //熱銷建案
                case "cases":
                    FromData = JsonConvert.DeserializeObject<cases>(form);
                    Model = model.Repository<cases>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.cases.Where(m => m.guid == guid && m.lang == lang).FirstOrDefault();
                    }
                    break;
                //系統日誌
                case "system_log":

                    FromData = JsonConvert.DeserializeObject<system_log>(form);
                    Model = model.Repository<system_log>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.system_log.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //防火牆
                case "firewalls":

                    FromData = JsonConvert.DeserializeObject<firewalls>(form);
                    Model = model.Repository<firewalls>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.firewalls.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //最新消息
                case "news":
                    FromData = JsonConvert.DeserializeObject<news>(form);
                    Model = model.Repository<news>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.news.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;
                //最新消息分類
                case "news_category":

                    FromData = JsonConvert.DeserializeObject<news_category>(form);
                    Model = model.Repository<news_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.news_category.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }

                    break;
                //群組管理
                case "roles":

                    FromData = JsonConvert.DeserializeObject<roles>(form);
                    Model = model.Repository<roles>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.roles.Where(m => m.guid == guid).FirstOrDefault();
                    }

                    break;

                //網站基本資料
                case "web_data":
                    FromData = JsonConvert.DeserializeObject<web_data>(form);
                    Model = model.Repository<web_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.web_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //SMTP資料
                case "smtp_data":
                    FromData = JsonConvert.DeserializeObject<smtp_data>(form);
                    Model = model.Repository<smtp_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.smtp_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //資源回收桶
                case "ashcan":

                    FromData = JsonConvert.DeserializeObject<ashcan>(form);
                    Model = model.Repository<ashcan>();
                    if (Field != "")
                    {
                        Guid NewGuid = Guid.Parse(FromData.guid.ToString());
                        Model = DB.ashcan.Where(m => m.guid == NewGuid).FirstOrDefault();
                    }

                    break;
                //使用者
                case "user":

                    FromData = JsonConvert.DeserializeObject<user>(form);
                    Model = model.Repository<user>();

                    if (FromData.password != null && FromData.password != "")
                    {
                        FromData.password = FunctionService.md5(FromData.password);
                    }
                    //無輸入密碼進入修改情況下
                    if (FromData.password == "" && FromData.guid != "")
                    {
                        FromData.password = FormObj["defPassword"].ToString();
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.user.Where(m => m.guid == guid).FirstOrDefault();
                    }

                    break;

                //系統參數
                case "system_data":

                    FromData = JsonConvert.DeserializeObject<system_data>(form);
                    Model = model.Repository<system_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Guid newGuid = Guid.Parse(guid);
                        Model = DB.system_data.Where(m => m.guid == newGuid).FirstOrDefault();
                    }

                    break;
            }

            FromData.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            if (actType == "add")
            {
                FromData.guid = guid;
                FromData.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                Model.Create(FromData);
                Model.SaveChanges();

                system_Log.title = FunctionService.reSysLogTitle(FromData, "新增");//回傳log標題
            }
            else
            {
                if (Field == "")
                {
                    DateTime re_data = DateTime.Now;
                    if (tables.IndexOf("contacts") != -1 || tables == "cases_reservation")
                    {
                        FromData.re_date = re_data;
                    }

                    //假設非審核者修改
                    System.Web.HttpContext context = System.Web.HttpContext.Current;
                    try
                    {
                        if (context.Session["verify"].ToString() == "N")
                        {
                            List<string> verifyUseTab = Web.Controllers.SiteadminController.verifyTables();//取得有審核的資料表
                            if (verifyUseTab.IndexOf(tables) != -1)
                            {
                                FromData.verify = "E";//一旦修改就是改為未審核
                            }
                        }
                    }
                    catch { }

                    Model.Update(FromData);
                    Model.SaveChanges();

                    system_Log.title = FunctionService.reSysLogTitle(FromData, "修改");    //回傳log標題
                                                                                         //Model = FromData;
                                                                                         //DB.SaveChanges();

                    //回覆信件
                    if ((tables.IndexOf("contacts") != -1 || tables == "cases_reservation") && FromData.status == "Y")
                    {
                        web_data webData = DB.web_data.Where(m => m.lang == "tw").FirstOrDefault();//取得網站基本資訊
                        List<string> MailList = new List<string>();

                        if (!string.IsNullOrEmpty(formArray["email"].ToString()))
                        {
                            MailList = formArray["email"].ToString().Split(',').ToList();
                        }

                        /* var jss = new JavaScriptSerializer();
                         var dict = jss.Deserialize<Dictionary<string, string>>(form);*/

                        string mailContentID = "";

                        if (tables == "contacts")
                        {
                            mailContentID = "4";
                        }
                        if (tables == "urban_contacts")
                        {
                            mailContentID = "5";
                        }
                        if (tables == "cases_reservation")
                        {
                            mailContentID = "7";
                        }

                        NameValueCollection nvc = null;
                        if (formArray != null && FromData.re_mail == "Y" && def_re_mail == "N" && MailList.Count > 0)
                        {
                            nvc = new NameValueCollection(formArray.Count);
                            foreach (var k in formArray)
                            {
                                nvc.Add(k.Key, k.Value.ToString());
                            }
                            nvc.Add("re_date", re_data.ToString("yyyy-MM-dd"));
                            nvc.Add("sysName", nvc["name"].ToString());

                            FunctionService.sendMail(MailList, mailContentID, "tw", FunctionService.getWebUrl(), nvc);

                            /*  string sendGuid = FromData.guid;
                              Model = DB.contacts.Where(m => m.guid == sendGuid).FirstOrDefault();
                              Model.is_send = "Y";
                              Model.re_date = DateTime.Now;
                              DB.SaveChanges();*/
                        }
                    }
                }
                else
                {
                    //單一欄位修改
                    switch (Field)
                    {
                        case "status":
                            Model.status = FromData.status;
                            break;

                        case "sortIndex":
                            Model.sortIndex = FromData.sortIndex;
                            break;

                        case "logindate":
                            Model.logindate = FromData.logindate;
                            break;
                    }

                    DB.SaveChanges();
                }
            }

            if (FormObj != null)
            {
                #region 權限

                if (FormObj.ContainsKey("permissions") && FormObj["permissions"] != null && FormObj["permissions"].ToString() != "")
                {
                    NameValueCollection permissions = FunctionService.reSubmitFormDataJson(FormObj["permissions"].ToString());

                    role_permissions role_permissions_add = new role_permissions();

                    var delData = DB.role_permissions.Where(m => m.role_guid == guid).ToList();

                    foreach (var item in delData)
                    {
                        DB.role_permissions.Remove((role_permissions)item);
                    }

                    foreach (string key in permissions)
                    {
                        //Guid newGuid = Guid.NewGuid();
                        role_permissions_add = new role_permissions();
                        role_permissions_add.guid = Guid.NewGuid();
                        role_permissions_add.role_guid = guid;
                        role_permissions_add.permissions_guid = key.ToString();
                        role_permissions_add.permissions_status = permissions[key].ToString();
                        DB.role_permissions.Add(role_permissions_add);

                        if (key.ToString() == "3d7ffc73-f7f4-4b24-b4c6-7c35f6f129cb")
                        {
                            //預約鑑賞
                            role_permissions_add = new role_permissions();
                            role_permissions_add.guid = Guid.NewGuid();
                            role_permissions_add.role_guid = guid;
                            role_permissions_add.permissions_guid = "715db7df-0f81-4811-a420-f7113162ac87";
                            role_permissions_add.permissions_status = permissions[key].ToString();
                            DB.role_permissions.Add(role_permissions_add);

                            //工程進度
                            role_permissions_add = new role_permissions();
                            role_permissions_add.guid = Guid.NewGuid();
                            role_permissions_add.role_guid = guid;
                            role_permissions_add.permissions_guid = "e5bc75bb-2e96-48b8-a759-6889cee11ae9";
                            role_permissions_add.permissions_status = permissions[key].ToString();
                            DB.role_permissions.Add(role_permissions_add);
                        }

                        if (key.ToString() == "069c6fe0-eacf-44aa-b873-647975028848")
                        {
                            //都更相關檔案
                            role_permissions_add = new role_permissions();
                            role_permissions_add.guid = Guid.NewGuid();
                            role_permissions_add.role_guid = guid;
                            role_permissions_add.permissions_guid = "898f874e-9ec6-4946-aa45-5b8b24b828ea";
                            role_permissions_add.permissions_status = permissions[key].ToString();
                            DB.role_permissions.Add(role_permissions_add);
                        }

                        DB.SaveChanges();
                    }
                }

                #endregion
            }

            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            //系統紀錄
            try
            {
                if (FromData.lang == defLang || FromData.lang == null)
                {
                    system_Log.ip = FunctionService.GetIP();
                    system_Log.types = "";
                    system_Log.tables = tables;
                    system_Log.tables_guid = guid;
                    system_Log.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    system_Log.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    system_Log.guid = Guid.NewGuid().ToString();
                    Dictionary<String, String> ReUserData = FunctionService.ReUserData();
                    system_Log.username = ReUserData["username"].ToString();
                    DB.system_log.Add(system_Log);
                    DB.SaveChanges();
                    /*  if (getData["username"].ToString() != "sysadmin")
                      {
                      }*/
                }
            }
            catch { }

            //寄發審核通知
            try
            {
                if (FromData.lang == defLang)
                {
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    data.Add("tables", tables);
                    data.Add("actType", actType);
                    data.Add("guid", guid);
                    data.Add("subject", "<a href=\"" + FunctionService.getWebUrl() + "/siteadmin/" + tables + "/edit/" + guid + "\">" + system_Log.title + "</a>");
                    FunctionService.SendVerifyEmail(data);
                }
            }
            catch { }

            return guid;
        }
    }
}