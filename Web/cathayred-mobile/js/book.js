﻿// JavaScript Document
var http = "";
	//test mode
	//http = "http://localhost:88/";
	//http = "http://118.69.191.193";
	//release_mode
	http = "http://www.cathay-red.com.tw/cathayred-mobile";
	//http = "http://118.69.191.193/cathay";
function transfer(A)
{window.location.href=A;}
var tab3 = {"title":["公司簡介","經營理念","得獎紀事",],
   "detailTitle":["Company Introduction","Operation Philosophy","Prizewinner",],
   "infoDetail":["國泰建設成立於民國五十三年九月十四日，為國內知名集團企業─霖園集團成員之一，係國內資本雄厚業績優良之房地產開發公司，亦是營建業第一家股票上市公司。<br/><br/> 歷年來公司秉持「誠信正派、專業負責、穩健經營」的理念，其間雖歷經多次房地產景氣波動，但仍能在競爭激烈的市場中成長、茁壯，除持續深化品牌、專業外，實端賴廣大客戶的愛護支持與全體員工的辛勤耕耘，進而發展成為國內大型房地產開發公司，而且營業範圍遍及台灣各主要都會區，並先後在國內興建各類型房屋逾五萬戶。<br/><br/>除了核心的房地產開發本業外，多年來更積極佈建與本業相關聯的其他房地產事業，如：三井工程公司、霖園管理維護公司、國泰建經公司等房地產關聯性產業佈局，積極形塑成為全方位的國泰地產集團。而為擴大公司營業規模，近年來，更積極開發新事業，如：成立國泰健康管理公司、國泰商旅公司等，以開拓多角經營業務。<br/><br/> 此外，為強化房地產市場資訊，健全市場發展，並善盡企業之社會責任，自民國91年底起，與政治大學台灣房地產研究中心合作編製「國泰房地產指數季報」，於每季召開記者會發佈，目前已成為社會上最具參考性的房地產資訊之一。<br/><br/>","為確保企業在競爭激烈的商業環境中能永續經營，以及能塑造企業獨特超群的風格與文化。本公司揭櫫五大經營理念，做為企業永續經營及全體員工努力之指導方針；並建立「榮譽感」、「使命感」的工作態度。<br/><br/>另外，鑑於國內開發商良莠不齊，購屋糾紛時有所聞，消費者權益缺乏保障，為樹立優良之經營典範，乃揭示獨步全國的四大保證來確保客戶權益，不僅樹立了良好的品牌信譽，亦博得社會大眾的信賴。客戶是企業的命脈，本公司一向以客為尊，並積極追求卓越，提供全方位的優質服務。<br/><br/>",]}
	linkInfoDetail0 = http +"/api/AboutUs/getCompanyIntro.aspx";
	console.log("linkInfoDetail0="+linkInfoDetail0);
	linkInfoDetail1 = http +"/api/AboutUs/getCompanyOperation.aspx";
	linkInfoDetail2 = http +"/api/AboutUs/getPrizeWinner.aspx";   
	
	
function loadTab3()
{

	 var url = document.URL;
	 var data=url.split("#");
	 var number = data[1];
	 var treeImage = '<img src="image/About Us/Icon_tree@2x.png" style="width:100px;padding-left:5%;padding-top:10px;"/>'; 
	 var title     = '<div style="width:70%; padding-left:2.5%; line-height:60px; position:absolute; top:22%; margin-left:14%; 	font-size:60px;" >title detail</div>';
	  var buttonBack2 ='<div   onclick="defaultTab3();clickBackButton()"; style="position:absolute; left:1px;top:;width:auto; height:auto;float:left;"> <img id="backButton" src="image/Appointment/Btn_back@2x.png" style="width:150px;"/></div>';	
	 var imageNumber1 = '<img id="imageNumber1" style="padding-top:20px;padding-left:14%;;width:60%;padding-bottom:40px; " src="image/About Us/Img_FourPromises@2x.jpg"/>';
	 if(number != "")
	 {	 
		 buttonBack ='<div  onclick="defaultTab3();clickBackButton()"; style="position:absolute; left:-2.5%;top:;width:auto; height:auto;float:left;     "> <img id="backButton" src="image/Appointment/Btn_back@2x.png" style="width:150px;"/></div>';
		 arrow ='<div  style="position:absolute;left:17%;padding-top:25px;width:auto;height:auto;"> <img src="image/About Us/Arrow@2x.png" style="height:35px;"/> </div>';
		 document.getElementById("titleAboutUs").innerHTML='<strong>'+tab3.title[number]+'</strong>';
		 document.getElementById("topTab3").setAttribute("style","margin-bottom:10px;");
		 document.getElementById("contentTab3").setAttribute("style","display:none;");
		 document.getElementById("imageBuiding").setAttribute("style","display:none;");
		 document.getElementById("detailAboutUs").setAttribute("style","display:block;");  
		 document.getElementById("titleDetail").innerHTML=tab3.detailTitle[number];
		  if(number == 0)
		  {
			 document.getElementById("infoDetail").style.display='block'; 
			$.getJSON(linkInfoDetail0,
			{	  },
			 function(data) {
					 infoDetail0 = 	data[0].content.replace(/\r\n/gi,"<br />"); 
					 document.getElementById("infoDetail").innerHTML=buttonBack+infoDetail0;
			  });  
		  }
		  if(number == 1)
		  {
			  document.getElementById("infoDetail").style.display='block';
			  $.getJSON(linkInfoDetail1,
			  {	  },
			  function(data) {
				 infoDetail1 = 	data[0].content.replace(/\r\n/gi,"<br />");
				 document.getElementById("infoDetail").innerHTML=buttonBack+infoDetail1+imageNumber1; 	 
			  });
			  
		  }
		  if(number == 2)
		  {
			   document.getElementById("titleNumber2").style.display='block';
			   document.getElementById("detailNumber2").style.display='block';
			   
			   $.getJSON(linkInfoDetail2,
			  {	  },
			  function(data) {
				 console.log(data);
				 contentArray = data[0].content_array;
				 var infoTitleNumber2 = '<div style="font-size:45px; position:absolute; left:29%; bottom:72%; line-height:60px;">'+data[0].title+'</div>';
				 //document.getElementById("titleNumber2").innerHTML = buttonBack2 + treeImage +'<strong>' +infoTitleNumber2+'</strong>';
				 document.getElementById("titleNumber2").innerHTML = buttonBack2 + '<strong>' + infoTitleNumber2 + '</strong>';
				console.log(contentArray.length);
				/*for(i = 1 ; i <(data[0].content_array.length + 1); i ++)
				{	
					 id = 'infoDetail' + i;	
					 document.getElementById(id).style.display='block';		   
					 document.getElementById(id).innerHTML  = arrow + contentArray[0].content.replace(/\r\n/gi,"<br />");  
				}*/
				var info='';
				for(i = 0 ; i <contentArray.length; i ++)
				{
					info += arrow + '<div id="infoDetail1" style="border-bottom:#000 dashed;margin-left:20%; padding-bottom:2%;padding-top:2%;line-height:45px;width:77%;display:block;font-size:41px">'+contentArray[i].content.replace(/\r\n/gi,"<br />")+'</div>';
				}
				console.log(info);
				document.getElementById("detailNumber2").innerHTML ='<div id="detailNumber2" style="width:100%;height:auto;     background:url(image/About%20Us/Background_image@2x.png) repeat-y;  background-position:0 -50px;display:block;">'+ info+ '<br /> <br /></div>';
			  });
		  }
	  }
}
function defaultTab3()
{
	document.getElementById("infoDetail").style.display='none';
	document.getElementById("titleNumber2").style.display='none';
	document.getElementById("detailNumber2").style.display='none';
	
	document.getElementById("titleAboutUs").innerHTML='';
    document.getElementById("contentTab3").setAttribute("style","display:block;");
	document.getElementById("imageBuiding").setAttribute("style","display:block;padding-top:10px;padding-bottom:10px;margin:auto;width:95%;height:auto;border-top:#000 solid;border-bottom:#000 solid;text-align:center;margin-top:10px;");
    document.getElementById("detailAboutUs").setAttribute("style","display:none;");  
    document.getElementById("titleDetail").innerHTML='';
	
	var url=document.URL;
	var data = url.split("#");
	if(data.length>3)
	{
		var link = data[0] + "##" + data[2] + "#"+data[3];
		window.location.href=link;
	}
	else
	{
		var link=data[0]+"##"+data[2];	
		window.location.href=link;
	}
}

function trim(str) {
        return str.replace(/^\s+|\s+$/g,"");
}

function parseURLParams(url) {
  var queryStart = url.indexOf("?") + 1;
  var queryEnd   = url.indexOf("#") + 1 || url.length + 1;
  var query      = url.slice(queryStart, queryEnd - 1);

  if (query === url || query === "") return;

  var params  = {};
  var nvPairs = query.replace(/\+/g, " ").split("&");

  for (var i=0; i<nvPairs.length; i++) {
    var nv = nvPairs[i].split("=");
    var n  = decodeURIComponent(nv[0]);
    var v  = decodeURIComponent(nv[1]);
    if ( !(n in params) ) {
      params[n] = [];
    }
    params[n].push(nv.length === 2 ? v : null);
  }
  return params;
}

function book()
{
    var nameHotel = document.getElementById("nameHotel").textContent;
	var name = document.getElementById("name").value;
	var phone = document.getElementById("phone").value;
	var email = document.getElementById("email").value;
	console.log(phone.length);
	var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
	if(trim(name) == "")
		alert("姓名不得為空!");
	else
	{
		if(trim(phone) == "")
			alert("電話不得為空!");
		else
		{
			if(phone.length > 12 || phone.length < 10 )
			    alert("電話號碼長度有誤,請輸入最少10碼(包含市話區碼)之正確電話號碼!");
		//	else
		//	{
			//	if(email.search(emailRegEx)==-1)
			//		alert("Email格式有誤!");
				else
				{

					{
						var url = document.URL;
						var urlsplit = url.split("=");
						var conid= urlsplit[1].split("#");
						urlsplit[1] = conid[0];
						
						console.log(status);
						$.ajax({
						  async: true,
						  type: "POST",
						  //url: "http://118.69.191.193:81/api/Customer/newCustomer.aspx",
						  url: http+"/api/Customer/newCustomer.aspx",
						
						  crossDomain:true, 
						  error: function(xhr, status, error) {
						  alert('送出失敗,請稍候再試!');
						  },
						  data: {  
							  "con_object_id":urlsplit[1],
							  "name":name,
							  "phone":phone,
							  "email":email,
							  "req_type":status,
							  "processed":false
							}
						}).done(function( msg ) {
						  
						  if(msg == 'True')
						  	transfer('form_result.html?name='+name+'&phone='+phone+'&email='+email+'&nameHotel='+nameHotel+'&status='+status+'&ConOjectId='+urlsplit[1]);
							else
							alert('送出失敗,請稍候再試!');
						});
						
					}
				}
			
		}
	}
	
}

var truncate = function (fullStr, strLen, separator) {
    if (fullStr.length <= strLen) return fullStr;
    separator = separator || '...';
    
    var sepLen = separator.length,
        charsToShow = strLen - sepLen,
        frontChars = Math.ceil(charsToShow/2),
        backChars = Math.floor(charsToShow/2);
    
    return fullStr.substr(0, frontChars) + 
           separator + 
           fullStr.substr(fullStr.length - backChars);
};

function getDataResult()
{
	var url=document.URL;
	var params=parseURLParams(url);
	var nameHotel=params.nameHotel;
	var name=params.name;
	var phone=params.phone;
	var email=params.email;
	var status=params.status;
	
	console.log("status"+status);
	var text1 = "謝謝！您已完成預約看屋！<br />我們將會有專人儘快與您聯絡！"
	var text2 = "謝謝！您已完成索取資料！<br />我們將會有專人儘快與您聯絡！"
 	if(status==1)
		document.getElementById("status").innerHTML=text1;
	else
		document.getElementById("status").innerHTML=text2;
	document.getElementById("nameHotelResult").innerHTML=nameHotel;
	document.getElementById("nameResult").innerHTML=truncate(name[0],15);
	document.getElementById("phoneResult").innerHTML=phone;
	document.getElementById("emailResult").innerHTML=truncate(email[0],32);	
	//document.getElementById("emailResult").innerHTML=email;
}
var status=1;
//var flagCheck=0;
function CheckBox(check)
{
	status = check;
	//flagCheck = 1;
	if(check==1)
	{
		document.getElementById("checkBox1").src = "image/Appointment/ClickBox02@2x.png";
		document.getElementById("checkBox2").src = "image/Appointment/ClickBox01@2x.png";
	}
	else
	{
		document.getElementById("checkBox2").src = "image/Appointment/ClickBox02@2x.png";
		document.getElementById("checkBox1").src = "image/Appointment/ClickBox01@2x.png";
	}
}
    function image_slider()
  {
	  console.log(slidename);
  document.getElementById("slider").innerHTML="";
  $("#slider-wrapper").html(
  '<div id="slider" class="nivoSlider" style="height:500px"></div>'+
  ' <div ><img style="width:95%;height:3px" src="image/Product Info/SeparateBar.png" /></div>'+
  '  <div class="bottomq" style="width:40%;padding-left:30%" id="name" ></div>'+
  '<div id="tag" style="position:absolute;top:37.4%;text-align:center;'+
  'color:#ffffff;background:#808080;right:0%;height:70px;line-height:70px; '+
  'font-size:30px;width:150px;z-index:99 !important">示意圖</div>'
  );

   var img="";


   for(var i=0;i < slideimage.length;i++){
 
img+='<img height=500px  src="'+http+''+slideimage[i].replace("~","")+'" data-thumb="images/toystory.jpg" alt="" />'
  }
 
   document.getElementById("slider").innerHTML=img;  
  
   
 // $('#slider').nivoSlider();
 
  }

  	

  