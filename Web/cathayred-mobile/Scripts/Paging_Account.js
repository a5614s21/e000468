﻿//paging Account
// $(document).ready(function() {
//    initPage();
//});

function initPage_Account() {
    var curField = $("#" + hideFieldCurrentPage_Account);
    if (curField) {
        curField.val(1);
        // $get(hideButtonActionCtrl).click();

        generateRows_Account(1);
    }
}
function generateRows_Account(selected) {

    var pages = $("#pagination_Account").attr("page_count_Account");

    $("#pagination_Account").children().remove();

    if (pages <= 1)
        return;

    if (pages <= 6) {
        var pagers = "<div id='paginator_Account'>";
        for (i = 1; i <= pages; i++) {
            _class = (i == selected) ? "pagor selected" : "pagor";
            pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
        }
        pagers += "</div>";

        $("#pagination_Account").append(pagers);
        $(".pagor").click(function () {
            var index = $(".pagor").index(this);
            $("#" + hideFieldCurrentPage_Account).val(index + 1);
            $get(hideButtonActionCtrl_Account).click();
            $(".pagor").removeClass("selected");
            $(this).addClass("selected");
        });
    } else {
        if (selected < 6) {
            // Draw the first 2 then have ... link to last
            var pagers = "<div id='paginator_Account'>";
            for (i = 1; i <= 6; i++) {
                _class = (i == selected) ? "pagor selected" : "pagor";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagor'>" + Number(pages) + "</a><div style='clear:both;'></div></div>";

            $("#paginator_Account").remove();
            $("#pagination_Account").append(pagers);
            $(".pagor").click(function () {
                updatePage_Account(this);
            });
        } else if (selected > (Number(pages) - 4)) {
            // Draw ... link to first then have the last 2
            var pagers = "<div id='paginator_Account'><a href='#' class='pagor'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
            for (i = (Number(pages) - 4); i <= Number(pages); i++) {
                _class = (i == selected) ? "pagor selected" : "pagor";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='clear:both;'></div></div>";

            $("#paginator_Account").remove();
            $("#pagination_Account").append(pagers);
            $(".pagor").click(function () {
                updatePage_Account(this);
            });
        } else {
            // Draw the number 1 element, then draw ... 2 before and two after and ... link to last
            var pagers = "<div id='paginator_Account'><a href='#' class='pagor'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
            for (i = (Number(selected) - 2); i <= (Number(selected) + 2); i++) {
                _class = (i == selected) ? "pagor selected" : "pagor";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagor'>" + pages + "</a><div style='clear:both;'></div></div>";

            $("#paginator_Account").remove();
            $("#pagination_Account").append(pagers);
            $(".pagor").click(function () {
                updatePage_Account(this);
            });
        }
    }
}
function updatePageCount_Account(pagecount_Account) {
    $("#pagination_Account").attr("page_count_Account", pagecount_Account);
}
function updatePage_Account(elem) {
    // Retrieve the number stored and position elements based on that number
    var selected = $(elem).text();

    // First update pagination
    $("#" + hideFieldCurrentPage_Account).val(selected);
    $get(hideButtonActionCtrl_Account).click();

    // Then update links
    generateRows_Account(selected);
}