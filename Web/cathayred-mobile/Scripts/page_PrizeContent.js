﻿//paging PrizeContent
// $(document).ready(function() {
//    initPage();
//});

function initPage_PrizeContent() {
    var curField = $("#" + hideFieldCurrentPage_PrizeContent);
    if (curField) {
        curField.val(1);
        // $get(hideButtonActionCtrl).click();

        generateRows_PrizeContent(1);
    }
}
function generateRows_PrizeContent(selected) {

    var pages = $("#pagination_PrizeContent").attr("page_count_PrizeContent");

    $("#pagination_PrizeContent").children().remove();
    if (pages <= 1)
        return;

    if (pages <= 6) {
        var pagers = "<div id='paginator_PrizeContent'>";
        for (i = 1; i <= pages; i++) {
            _class = (i == selected) ? "pagorPrizeContent selected" : "pagorPrizeContent";
            pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
        }
        pagers += "</div>";

        $("#pagination_PrizeContent").append(pagers);
        $(".pagorPrizeContent").click(function () {
            var index = $(".pagorPrizeContent").index(this);
            $("#" + hideFieldCurrentPage_PrizeContent).val(index + 1);
            $get(hideButtonActionCtrl_PrizeContent).click();
            $(".pagorPrizeContent").removeClass("selected");
            $(this).addClass("selected");
        });
    } else {
        if (selected < 6) {
            // Draw the first 2 then have ... link to last
            var pagers = "<div id='paginator_PrizeContent'>";
            for (i = 1; i <= 6; i++) {
                _class = (i == selected) ? "pagorPrizeContent selected" : "pagorPrizeContent";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagorPrizeContent'>" + Number(pages) + "</a><div style='clear:both;'></div></div>";

            $("#paginator_PrizeContent").remove();
            $("#pagination_PrizeContent").append(pagers);
            $(".pagorPrizeContent").click(function () {
                updatePage_PrizeContent(this);
            });
        } else if (selected > (Number(pages) - 4)) {
            // Draw ... link to first then have the last 2
            var pagers = "<div id='paginator_PrizeContent'><a href='#' class='pagorPrizeContent'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
            for (i = (Number(pages) - 4); i <= Number(pages); i++) {
                _class = (i == selected) ? "pagorPrizeContent selected" : "pagorPrizeContent";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='clear:both;'></div></div>";

            $("#paginator_PrizeContent").remove();
            $("#pagination_PrizeContent").append(pagers);
            $(".pagorPrizeContent").click(function () {
                updatePage_PrizeContent(this);
            });
        } else {
            // Draw the number 1 element, then draw ... 2 before and two after and ... link to last
            var pagers = "<div id='paginator_PrizeContent'><a href='#' class='pagorPrizeContent'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
            for (i = (Number(selected) - 2); i <= (Number(selected) + 2); i++) {
                _class = (i == selected) ? "pagorPrizeContent selected" : "pagorPrizeContent";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagorPrizeContent'>" + pages + "</a><div style='clear:both;'></div></div>";

            $("#paginator_PrizeContent").remove();
            $("#pagination_PrizeContent").append(pagers);
            $(".pagorPrizeContent").click(function () {
                updatePage_PrizeContent(this);
            });
        }
    }
}
function updatePageCount_PrizeContent(pagecount_PrizeContent) {
    $("#pagination_PrizeContent").attr("page_count_PrizeContent", pagecount_PrizeContent);
}
function updatePage_PrizeContent(elem) {
    // Retrieve the number stored and position elements based on that number
    var selected = $(elem).text();

    // First update pagination
    $("#" + hideFieldCurrentPage_PrizeContent).val(selected);
    $get(hideButtonActionCtrl_PrizeContent).click();

    // Then update links
    generateRows_PrizeContent(selected);
}