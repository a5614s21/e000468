﻿//paging clientaccount
// $(document).ready(function() {
//    initPage();
//});

function initPage_clientaccount() {
    var curField = $("#" + hideFieldCurrentPage_clientaccount);
    if (curField) {
        curField.val(1);
        // $get(hideButtonActionCtrl).click();

        generateRows_clientaccount(1);
    }
}
function generateRows_clientaccount(selected) {

    var pages = $("#pagination_clientaccount").attr("page_count_clientaccount");

    $("#pagination_clientaccount").children().remove();
    if (pages <= 1)
        return;

    if (pages <= 6) {
        var pagers = "<div id='paginator_clientaccount'>";
        for (i = 1; i <= pages; i++) {
            _class = (i == selected) ? "pagorclientaccount selected" : "pagorclientaccount";
            pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
        }
        pagers += "</div>";

        $("#pagination_clientaccount").append(pagers);
        $(".pagorclientaccount").click(function () {
            var index = $(".pagorclientaccount").index(this);
            $("#" + hideFieldCurrentPage_clientaccount).val(index + 1);
            $get(hideButtonActionCtrl_clientaccount).click();
            $(".pagorclientaccount").removeClass("selected");
            $(this).addClass("selected");
        });
    } else {
        if (selected < 6) {
            // Draw the first 5 then have ... link to last
            var pagers = "<div id='paginator_clientaccount'>";
            for (i = 1; i <= 6; i++) {
                _class = (i == selected) ? "pagorclientaccount selected" : "pagorclientaccount";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagorclientaccount'>" + Number(pages) + "</a><div style='clear:both;'></div></div>";

            $("#paginator_clientaccount").remove();
            $("#pagination_clientaccount").append(pagers);
            $(".pagorclientaccount").click(function () {
                updatePage_clientaccount(this);
            });
        } else if (selected > (Number(pages) - 4)) {
            // Draw ... link to first then have the last 2
            var pagers = "<div id='paginator_clientaccount'><a href='#' class='pagorclientaccount'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
            for (i = (Number(pages) - 4); i <= Number(pages); i++) {
                _class = (i == selected) ? "pagorclientaccount selected" : "pagorclientaccount";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='clear:both;'></div></div>";

            $("#paginator_clientaccount").remove();
            $("#pagination_clientaccount").append(pagers);
            $(".pagorclientaccount").click(function () {
                updatePage_clientaccount(this);
            });
        } else {
            // Draw the number 1 element, then draw ... 2 before and two after and ... link to last
            var pagers = "<div id='paginator_clientaccount'><a href='#' class='pagorclientaccount'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
            for (i = (Number(selected) - 2); i <= (Number(selected) + 2); i++) {
                _class = (i == selected) ? "pagorclientaccount selected" : "pagorclientaccount";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagorclientaccount'>" + pages + "</a><div style='clear:both;'></div></div>";

            $("#paginator_clientaccount").remove();
            $("#pagination_clientaccount").append(pagers);
            $(".pagorclientaccount").click(function () {
                updatePage_clientaccount(this);
            });
        }
    }
}
function updatePageCount_clientaccount(pagecount_clientaccount) {
    $("#pagination_clientaccount").attr("page_count_clientaccount", pagecount_clientaccount);
}
function updatePage_clientaccount(elem) {
    // Retrieve the number stored and position elements based on that number
    var selected = $(elem).text();

    // First update pagination
    $("#" + hideFieldCurrentPage_clientaccount).val(selected);
    $get(hideButtonActionCtrl_clientaccount).click();

    // Then update links
    generateRows_clientaccount(selected);
}