﻿//paging PaymentHistory
// $(document).ready(function() {
//    initPage();
//});

function initPage_PaymentHistory() {
    var curField = $("#" + hideFieldCurrentPage_PaymentHistory);
    if (curField) {
        curField.val(1);
        // $get(hideButtonActionCtrl).click();

        generateRows_PaymentHistory(1);
    }
}
function generateRows_PaymentHistory(selected) {

    var pages = $("#pagination_PaymentHistory").attr("page_count_PaymentHistory");

    $("#pagination_PaymentHistory").children().remove();
    if (pages <= 1)
        return;

    if (pages <= 6) {
        var pagers = "<div id='paginator_PaymentHistory'>";
        for (i = 1; i <= pages; i++) {
            _class = (i == selected) ? "pagorPaymentHistory selected" : "pagorPaymentHistory";
            pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
        }
        pagers += "</div>";

        $("#pagination_PaymentHistory").append(pagers);
        $(".pagorPaymentHistory").click(function () {
            var index = $(".pagorPaymentHistory").index(this);
            $("#" + hideFieldCurrentPage_PaymentHistory).val(index + 1);
            $get(hideButtonActionCtrl_PaymentHistory).click();
            $(".pagorPaymentHistory").removeClass("selected");
            $(this).addClass("selected");
        });
    } else {
        if (selected < 6) {
            // Draw the first 2 then have ... link to last
            var pagers = "<div id='paginator_PaymentHistory'>";
            for (i = 1; i <= 6; i++) {
                _class = (i == selected) ? "pagorPaymentHistory selected" : "pagorPaymentHistory";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagorPaymentHistory'>" + Number(pages) + "</a><div style='clear:both;'></div></div>";

            $("#paginator_PaymentHistory").remove();
            $("#pagination_PaymentHistory").append(pagers);
            $(".pagorPaymentHistory").click(function () {
                updatePage_PaymentHistory(this);
            });
        } else if (selected > (Number(pages) - 4)) {
            // Draw ... link to first then have the last 2
            var pagers = "<div id='paginator_PaymentHistory'><a href='#' class='pagorPaymentHistory'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
            for (i = (Number(pages) - 4); i <= Number(pages); i++) {
                _class = (i == selected) ? "pagorPaymentHistory selected" : "pagorPaymentHistory";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='clear:both;'></div></div>";

            $("#paginator_PaymentHistory").remove();
            $("#pagination_PaymentHistory").append(pagers);
            $(".pagorPaymentHistory").click(function () {
                updatePage_PaymentHistory(this);
            });
        } else {
            // Draw the number 1 element, then draw ... 2 before and two after and ... link to last
            var pagers = "<div id='paginator_PaymentHistory'><a href='#' class='pagorPaymentHistory'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
            for (i = (Number(selected) - 2); i <= (Number(selected) + 2); i++) {
                _class = (i == selected) ? "pagorPaymentHistory selected" : "pagorPaymentHistory";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagorPaymentHistory'>" + pages + "</a><div style='clear:both;'></div></div>";

            $("#paginator_PaymentHistory").remove();
            $("#pagination_PaymentHistory").append(pagers);
            $(".pagorPaymentHistory").click(function () {
                updatePage_PaymentHistory(this);
            });
        }
    }
}
function updatePageCount_PaymentHistory(pagecount_PaymentHistory) {
    $("#pagination_PaymentHistory").attr("page_count_PaymentHistory", pagecount_PaymentHistory);
}
function updatePage_PaymentHistory(elem) {
    // Retrieve the number stored and position elements based on that number
    var selected = $(elem).text();

    // First update pagination
    $("#" + hideFieldCurrentPage_PaymentHistory).val(selected);
    $get(hideButtonActionCtrl_PaymentHistory).click();

    // Then update links
    generateRows_PaymentHistory(selected);
}