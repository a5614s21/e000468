﻿//paging customer
// $(document).ready(function() {
//    initPage();
//});

function initPage_customer() {
    var curField = $("#" + hideFieldCurrentPage_customer);
    if (curField) {
        curField.val(1);
        // $get(hideButtonActionCtrl).click();

        generateRows_customer(1);
    }
}
function generateRows_customer(selected) {

    var pages = $("#pagination_customer").attr("page_count_customer");

    $("#pagination_customer").children().remove();
    if (pages <= 1)
        return;

    if (pages <= 6) {
        var pagers = "<div id='paginator_customer'>";
        for (i = 1; i <= pages; i++) {
            _class = (i == selected) ? "pagorcustomer selected" : "pagorcustomer";
            pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
        }
        pagers += "</div>";

        $("#pagination_customer").append(pagers);
        $(".pagorcustomer").click(function () {
            var index = $(".pagorcustomer").index(this);
            $("#" + hideFieldCurrentPage_customer).val(index + 1);
            $get(hideButtonActionCtrl_customer).click();
            $(".pagorcustomer").removeClass("selected");
            $(this).addClass("selected");
        });
    } else {
        if (selected < 6) {
            // Draw the first 5 then have ... link to last
            var pagers = "<div id='paginator_customer'>";
            for (i = 1; i <= 6; i++) {
                _class = (i == selected) ? "pagorcustomer selected" : "pagorcustomer";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagorcustomer'>" + Number(pages) + "</a><div style='clear:both;'></div></div>";

            $("#paginator_customer").remove();
            $("#pagination_customer").append(pagers);
            $(".pagorcustomer").click(function () {
                updatePage_customer(this);
            });
        } else if (selected > (Number(pages) - 4)) {
            // Draw ... link to first then have the last 2
            var pagers = "<div id='paginator_customer'><a href='#' class='pagorcustomer'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
            for (i = (Number(pages) - 4); i <= Number(pages); i++) {
                _class = (i == selected) ? "pagorcustomer selected" : "pagorcustomer";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='clear:both;'></div></div>";

            $("#paginator_customer").remove();
            $("#pagination_customer").append(pagers);
            $(".pagorcustomer").click(function () {
                updatePage_customer(this);
            });
        } else {
            // Draw the number 1 element, then draw ... 2 before and two after and ... link to last
            var pagers = "<div id='paginator_customer'><a href='#' class='pagorcustomer'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
            for (i = (Number(selected) - 2); i <= (Number(selected) + 2); i++) {
                _class = (i == selected) ? "pagorcustomer selected" : "pagorcustomer";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagorcustomer'>" + pages + "</a><div style='clear:both;'></div></div>";

            $("#paginator_customer").remove();
            $("#pagination_customer").append(pagers);
            $(".pagorcustomer").click(function () {
                updatePage_customer(this);
            });
        }
    }
}
function updatePageCount_customer(pagecount_customer) {
    $("#pagination_customer").attr("page_count_customer", pagecount_customer);
}
function updatePage_customer(elem) {
    // Retrieve the number stored and position elements based on that number
    var selected = $(elem).text();

    // First update pagination
    $("#" + hideFieldCurrentPage_customer).val(selected);
    $get(hideButtonActionCtrl_customer).click();

    // Then update links
    generateRows_customer(selected);
}