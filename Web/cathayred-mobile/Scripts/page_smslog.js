﻿//paging smslog
// $(document).ready(function() {
//    initPage();
//});

function initPage_smslog() {
    var curField = $("#" + hideFieldCurrentPage_smslog);
    if (curField) {
        curField.val(1);
        // $get(hideButtonActionCtrl).click();

        generateRows_smslog(1);
    }
}
function generateRows_smslog(selected) {

    var pages = $("#pagination_smslog").attr("page_count_smslog");

    $("#pagination_smslog").children().remove();
    if (pages <= 1)
        return;

    if (pages <= 6) {
        var pagers = "<div id='paginator_smslog'>";
        for (i = 1; i <= pages; i++) {
            _class = (i == selected) ? "pagorsmslog selected" : "pagorsmslog";
            pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
        }
        pagers += "</div>";

        $("#pagination_smslog").append(pagers);
        $(".pagorsmslog").click(function () {
            var index = $(".pagorsmslog").index(this);
            $("#" + hideFieldCurrentPage_smslog).val(index + 1);
            $get(hideButtonActionCtrl_smslog).click();
            $(".pagorsmslog").removeClass("selected");
            $(this).addClass("selected");
        });
    } else {
        if (selected < 6) {
            // Draw the first 5 then have ... link to last
            var pagers = "<div id='paginator_smslog'>";
            for (i = 1; i <= 6; i++) {
                _class = (i == selected) ? "pagorsmslog selected" : "pagorsmslog";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagorsmslog'>" + Number(pages) + "</a><div style='clear:both;'></div></div>";

            $("#paginator_smslog").remove();
            $("#pagination_smslog").append(pagers);
            $(".pagorsmslog").click(function () {
                updatePage_smslog(this);
            });
        } else if (selected > (Number(pages) - 4)) {
            // Draw ... link to first then have the last 2
            var pagers = "<div id='paginator_smslog'><a href='#' class='pagorsmslog'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
            for (i = (Number(pages) - 4); i <= Number(pages); i++) {
                _class = (i == selected) ? "pagorsmslog selected" : "pagorsmslog";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='clear:both;'></div></div>";

            $("#paginator_smslog").remove();
            $("#pagination_smslog").append(pagers);
            $(".pagorsmslog").click(function () {
                updatePage_smslog(this);
            });
        } else {
            // Draw the number 1 element, then draw ... 2 before and two after and ... link to last
            var pagers = "<div id='paginator_smslog'><a href='#' class='pagorsmslog'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
            for (i = (Number(selected) - 2); i <= (Number(selected) + 2); i++) {
                _class = (i == selected) ? "pagorsmslog selected" : "pagorsmslog";
                pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
            }
            pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagorsmslog'>" + pages + "</a><div style='clear:both;'></div></div>";

            $("#paginator_smslog").remove();
            $("#pagination_smslog").append(pagers);
            $(".pagorsmslog").click(function () {
                updatePage_smslog(this);
            });
        }
    }
}
function updatePageCount_smslog(pagecount_smslog) {
    $("#pagination_smslog").attr("page_count_smslog", pagecount_smslog);
}
function updatePage_smslog(elem) {
    // Retrieve the number stored and position elements based on that number
    var selected = $(elem).text();

    // First update pagination
    $("#" + hideFieldCurrentPage_smslog).val(selected);
    $get(hideButtonActionCtrl_smslog).click();

    // Then update links
    generateRows_smslog(selected);
}