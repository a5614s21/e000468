
// $(document).ready(function() {
//    initPage();
//});

function initPage() {
    var curField = $("#" + hideFieldCurrentPage);
    if (curField) {
        curField.val(1);
       // $get(hideButtonActionCtrl).click();

        generateRows(1);
    }
}
function generateRows(selected) {
      
    var pages = $("#pagination").attr("page_count");

    $("#pagination").children().remove();
    if (pages <= 1)
        return;
       
	if (pages <= 6){	    
	    var pagers = "<div id='paginator'>";
	    for (i = 1; i <= pages; i++) {
	        _class = (i == selected) ? "pagorconstructor selected" : "pagorconstructor";
	        pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
	    }
        pagers+="</div>";
        
	    $("#pagination").append(pagers);
		$(".pagorconstructor").click(function() 
		{
		    var index = $(".pagorconstructor").index(this);		    
		    $("#"+hideFieldCurrentPage).val(index+1);
			$get(hideButtonActionCtrl).click();
			$(".pagorconstructor").removeClass("selected");
			$(this).addClass("selected");
});
      	
	} else {
		if (selected < 6) {
			// Draw the first 6 then have ... link to last
			var pagers = "<div id='paginator'>";
			for (i = 1; i <= 6; i++) {
			    _class = (i == selected)?"pagorconstructor selected":"pagorconstructor";				
                pagers += "<a href='#' class='"+_class+"'>" + i + "</a>";
			}
			pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagorconstructor'>" + Number(pages) + "</a><div style='clear:both;'></div></div>";
			
			$("#paginator").remove();
			$("#pagination").append(pagers);
			$(".pagorconstructor").click(function() {
				updatePage(this);
			});
		} else if (selected > (Number(pages) - 4)) {
			// Draw ... link to first then have the last 4
		    var pagers = "<div id='paginator'><a href='#' class='pagorconstructor'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
		    for (i = (Number(pages) - 4); i <= Number(pages); i++) {
		        _class = (i == selected) ? "pagorconstructor selected" : "pagorconstructor";
		        pagers += "<a href='#' class='" + _class + "'>" + i + "</a>";
		    }
		    pagers += "<div style='clear:both;'></div></div>";
    		
		    $("#paginator").remove();
		    $("#pagination").append(pagers);
		    $(".pagorconstructor").click(function() {
			    updatePage(this);
		    });		
		} else {
			// Draw the number 1 element, then draw ... 2 before and two after and ... link to last
		var pagers = "<div id='paginator'><a href='#' class='pagorconstructor'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
			for (i = (Number(selected) - 2); i <= (Number(selected) + 2); i++) {		
			    _class = (i == selected)?"pagorconstructor selected":"pagorconstructor";				
                pagers += "<a href='#' class='"+_class+"'>" + i + "</a>";
			}
			pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagorconstructor'>" + pages + "</a><div style='clear:both;'></div></div>";
			
			$("#paginator").remove();
			$("#pagination").append(pagers);
			$(".pagorconstructor").click(function() {
				updatePage(this);
			});			
		}
	}
}
function updatePageCount(pagecount) {
    $("#pagination").attr("page_count", pagecount);
}
function updatePage(elem) {
	// Retrieve the number stored and position elements based on that number
	var selected = $(elem).text();

	// First update pagination
	$("#"+hideFieldCurrentPage).val(selected);
	$get(hideButtonActionCtrl).click();
	
	// Then update links
	generateRows(selected);
}