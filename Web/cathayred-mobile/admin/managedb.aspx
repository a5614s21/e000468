﻿<%@ page language="C#" autoeventwireup="true" inherits="DBManager, App_Web_m2l3g225" enableeventvalidation="false" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<%@ Register src="../usControls/Paging.ascx" tagname="Paging" tagprefix="uc1" %>

<%@ Register src="../usControls/Paging_customer.ascx" tagname="Paging_customer" tagprefix="uc2" %>

<%@ Register src="../usControls/Paging_smslog.ascx" tagname="Paging_smslog" tagprefix="uc4" %>

<%@ Register src="../usControls/Paging_PrizeTitle.ascx" tagname="Paging_PrizeTitle" tagprefix="uc3" %>

<%@ Register src="../usControls/Paging_PrizeContent.ascx" tagname="Paging_PrizeContent" tagprefix="uc5" %>

<%@ Register src="../usControls/Paging_Account.ascx" tagname="Paging_Account" tagprefix="uc6" %>

<%@ Register src="../usControls/Adminusername.ascx" tagname="Adminusername" tagprefix="uc7" %>

<%@ Register src="../usControls/Paging_clientaccount.ascx" tagname="Paging_clientaccount" tagprefix="uc8" %>

<%@ Register src="../usControls/Paging_Paymenthistory.ascx" tagname="Paging_Paymenthistory" tagprefix="uc9" %>

<%@ Register src="../usControls/Paging_conprogress.ascx" tagname="Paging_conprogress" tagprefix="uc10" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=8;FF=3;OtherUA=4" />
    <title></title>  
      <script src="../Scripts/System_Tooltip.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.8.3.js" type="text/javascript"></script>
    <link href="../Styles/tooltip.css" rel="stylesheet" type="text/css" /> 
     <script src="../Scripts/Paging_Account.js" type="text/javascript"></script>
       <script src="../Scripts/page.js" type="text/javascript"></script>
         <script src="../Scripts/page_customer.js" type="text/javascript"></script>
    <script src="../Scripts/page_smslog.js" type="text/javascript"></script>
     <script src="../Scripts/page_PrizeTitle.js" type="text/javascript"></script>
    <script src="../Scripts/page_PrizeContent.js" type="text/javascript"></script>
    <script src="../Scripts/page_clientaccount.js" type="text/javascript"></script>
    <script src="../Scripts/Paging_paymenthistory.js" type="text/javascript"></script>

    <script src="../Scripts/Paging_conprogress.js" type="text/javascript"></script>

    <style type="text/css">
        .div
        {
            width:80%;
            vertical-align:middle;
            margin:0 auto; 
           
        }
    
        .style14
        {
            width: 100%;
        }
        .style17
        {
            width: 154px;
        }
        .style18
        {
            color: #FFFFFF;
            background-color: #3366FF;
        }
     
        </style>
          
</head>
<body>
    <form id="form1" runat="server"> 
     <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <div class="div">
    <div style="text-align:right;">        
      
        <uc7:Adminusername ID="Adminusername1" runat="server" />
      
    </div>
        <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="5" 
            Width="100%" Height="100%">
       
       
            <asp:TabPanel runat="server" HeaderText="User" ID="TabPanel1">
                <HeaderTemplate>
                    使用者
                
</HeaderTemplate>
<ContentTemplate>
                <div>
                    <asp:HiddenField ID="hdf_current_page_Account" runat="server" Value="1" />
                    <asp:Button style="display:none" ID="btn_current_page_Account" runat="server" 
                        Text="Account" onclick="btn_current_page_Account_Click" />
                </div>
                <div>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                    
                                <asp:GridView ID="gv_user" runat="server" AutoGenerateColumns="False" 
                        Width="100%" BackColor="White" BorderColor="#3366CC" BorderStyle="None" 
                        BorderWidth="1px" CellPadding="10" onrowcancelingedit="gv_user_RowCancelingEdit" 
                        onrowcommand="gv_user_RowCommand" onrowdeleting="gv_user_RowDeleting" 
                        onrowediting="gv_user_RowEditing" onrowupdating="gv_user_RowUpdating" 
                        ShowFooter="True" DataKeyNames="id" EmptyDataText="No result found!" PageSize="5">
                        <Columns>
                            <asp:TemplateField HeaderText="使用者">
                                <EditItemTemplate>
                                    <asp:Label ID="lbl_username" runat="server" Text='<%# Bind("username") %>'></asp:Label>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txt_username" runat="server"></asp:TextBox>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("username") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="密碼">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txt_password" runat="server" Text='<%# Bind("userpassword") %>' 
                                        TextMode="Password" ToolTip="Type password here"></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txt_password" runat="server"></asp:TextBox>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='****************'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="姓名">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txt_name_user" runat="server" Text='<%# Bind("name_user") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txt_name_user" runat="server" Width="100%"></asp:TextBox>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("name_user") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="30%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="電話">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txt_phone_user" runat="server" Text='<%# Bind("phone") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txt_phone_user" runat="server" Width="100%"></asp:TextBox>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("phone") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <EditItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" 
                                        CommandName="Update" Text="儲存變更"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                                        CommandName="Cancel" Text="取消"></asp:LinkButton>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:Button ID="btn_add_new_user" runat="server" CommandName="insert" Text="新增" />
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                        CommandName="Edit" Text="快速編輯"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" 
                                        CommandName="Delete" Text="刪除" 
                                        onclientclick="return confirm('Do you want to delete ?')"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="20%" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <table class="style14">
                                <tr>
                                    <td class="style18">
                                        Username</td>
                                    <td class="style18">
                                        Password</td>
                                    <td class="style18">
                                        Name</td>
                                    <td class="style18">
                                        Phone</td>
                                    <td class="style18">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txt_username_empty" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txt_password_empty" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txt_nameuser_empty" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txt_phoneuser_empty" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="btn_addnew_empty" runat="server" CommandName="emptyinsert" 
                                            Text="新增" />
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#003399" />
                        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <SortedAscendingCellStyle BackColor="#EDF6F6" />
                        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                        <SortedDescendingCellStyle BackColor="#D6DFDF" />
                        <SortedDescendingHeaderStyle BackColor="#002876" />
                    </asp:GridView>
                   </ContentTemplate>
                   <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btn_current_page_Account"/>
                   </Triggers>
                    </asp:UpdatePanel>
                    </div>
                    <table>
                    <tr>
                        <td><uc6:Paging_Account ID="Paging_Account1" runat="server" />
                            </td>
                    </tr>
                    </table>

                
</ContentTemplate>
            







</asp:TabPanel>
            <asp:TabPanel runat="server" HeaderText="Constructor" ID="ConsTab">
                <HeaderTemplate>
                    建案
                
</HeaderTemplate>
                







<ContentTemplate>
                        <asp:HiddenField ID="hdfCurrentPage" runat="server" Value="1" />
                    <asp:Button style="display:none;" ID="btncurrentpage_constructor" runat="server"   
                    Text="Button" onclick="btncurrentpage_constructor_Click"  />
                  <div>
                     
                      <table class="style14">
                          <tr>
                              <td>
                                  <asp:Button ID="btn_add_cons" runat="server" OnClick="btn_add_cons_Click" 
                                      Text="新增建案" Width="80px" />
                              </td>
                                  <td style="text-align: right">&nbsp;</td>
                                  <td style="text-align: right" width="1%">
                                      <asp:DropDownList ID="dl_search_zone" runat="server" AutoPostBack="True" 
                                          OnSelectedIndexChanged="dl_search_zone_SelectedIndexChanged">
                                      </asp:DropDownList>
                              </td>
                          </tr>
                      </table>
                
                  </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                          <asp:GridView ID="gv_constructor" runat="server" 
                         AutoGenerateColumns="False" DataKeyNames="ConObjID" 
                        Height="100%" Width="100%" BackColor="White" 
                        BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="5" 
                        onrowcancelingedit="gb_constructor_RowCancelingEdit" 
                        onrowediting="gb_constructor_RowEditing" 
                        onrowupdating="gb_constructor_RowUpdating" 
                        onrowdatabound="gv_constructor_RowDataBound" 
                        EmptyDataText="No result found!" PageSize="5"><Columns>
                            <asp:TemplateField HeaderText="ID" InsertVisible="False">
                                <EditItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("ConObjID") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <a class="tooltip_a" href="#" onmouseover="showtip('名稱:<d><%#Eval("Name_ZH") %></d>\<br>樓層: <span><%#Eval("Floors") %></span><br>位置:<span><%#Eval("Location") %></span><br>連絡電話:<span><%#Eval("Phone") %></span><br>規劃坪數:<span><%#Eval("Spaces")%></span><br>通知電話號碼1:<span><%#Eval("Notifi_Phone1")%></span><br>通知電話號碼2:<span><%#Eval("Notifi_Phone2")%></span><br>顯示順序:<span><%#Eval("SortedOrder") %></span><br>區域:<span><%#(Eval("Area").ToString() == "1") ? "北" : (Eval("Area").ToString() == "2") ? "中" : "南" %></span><br>基本說明文字:<span><%#Eval("BasicInfo_Text")%></span><br>基地位置文字:<span><%#Eval("LocationMap_Text")%></span><br>是否顯示於前端網站:<span><%#(Eval("Visible").ToString() == "True")? "是" : "否" %></span><br>')"
                                        onmouseout="hidetip()">
                                        <div>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("ConObjID") %>'></asp:Label>
                                        </div>
                                    </a>
                                </ItemTemplate>
                                <ItemStyle Width="10%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="名稱">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txt_name" runat="server" Text='<%# Bind("Name_ZH") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                  <a class="tooltip_a" href="#" onmouseover="showtip('名稱:<d><%#Eval("Name_ZH") %></d>\<br>樓層: <span><%#Eval("Floors") %></span><br>位置:<span><%#Eval("Location") %></span><br>連絡電話:<span><%#Eval("Phone") %></span><br>規劃坪數:<span><%#Eval("Spaces")%></span><br>通知電話號碼1:<span><%#Eval("Notifi_Phone1")%></span><br>通知電話號碼2:<span><%#Eval("Notifi_Phone2")%></span><br>顯示順序:<span><%#Eval("SortedOrder") %></span><br>區域:<span><%#(Eval("Area").ToString() == "1") ? "北" : (Eval("Area").ToString() == "2") ? "中" : "南" %></span><br>基本說明文字:<span><%#Eval("BasicInfo_Text")%></span><br>基地位置文字:<span><%#Eval("LocationMap_Text")%></span><br>是否顯示於前端網站:<span><%#(Eval("Visible").ToString() == "True")? "是" : "否" %></span><br>')"
                                        onmouseout="hidetip()">
                              <div>
                              <asp:Label ID="Label2" runat="server" Text='<%# Bind("Name_ZH") %>'></asp:Label>
                              </div>
                                   </a>
                                </ItemTemplate>
                                <ItemStyle Width="20%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="區域">
                                <EditItemTemplate>
                                    <asp:DropDownList ID="dl_area" runat="server" Width="100%">
                                    </asp:DropDownList>
                                    <asp:Label ID="lbl_area" runat="server" Text='<%# Bind("Area") %>' 
                                        Visible="False"></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                  <a class="tooltip_a" href="#" onmouseover="showtip('名稱:<d><%#Eval("Name_ZH") %></d>\<br>樓層: <span><%#Eval("Floors") %></span><br>位置:<span><%#Eval("Location") %></span><br>連絡電話:<span><%#Eval("Phone") %></span><br>規劃坪數:<span><%#Eval("Spaces")%></span><br>通知電話號碼1:<span><%#Eval("Notifi_Phone1")%></span><br>通知電話號碼2:<span><%#Eval("Notifi_Phone2")%></span><br>顯示順序:<span><%#Eval("SortedOrder") %></span><br>區域:<span><%#(Eval("Area").ToString() == "1") ? "北" : (Eval("Area").ToString() == "2") ? "中" : "南" %></span><br>基本說明文字:<span><%#Eval("BasicInfo_Text")%></span><br>基地位置文字:<span><%#Eval("LocationMap_Text")%></span><br>是否顯示於前端網站:<span><%#(Eval("Visible").ToString() == "True")? "是" : "否" %></span><br>')"
                                        onmouseout="hidetip()">
                               <div>
                                 <asp:Label ID="Label3" runat="server" 
                                    Text='<%# (Eval("Area").ToString() == "1") ? "北" : (Eval("Area").ToString() == "2") ? "中" : "南" %>'>'>
                                    </asp:Label> <%--Text='<%# Bind("Area") %>'--%>
                               </div>
                                 </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="顯示順序">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txt_sorted_order" runat="server" Text='<%# Bind("SortedOrder") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                  <a class="tooltip_a" href="#" onmouseover="showtip('名稱:<d><%#Eval("Name_ZH") %></d>\<br>樓層: <span><%#Eval("Floors") %></span><br>位置:<span><%#Eval("Location") %></span><br>連絡電話:<span><%#Eval("Phone") %></span><br>規劃坪數:<span><%#Eval("Spaces")%></span><br>通知電話號碼1:<span><%#Eval("Notifi_Phone1")%></span><br>通知電話號碼2:<span><%#Eval("Notifi_Phone2")%></span><br>顯示順序:<span><%#Eval("SortedOrder") %></span><br>區域:<span><%#(Eval("Area").ToString() == "1") ? "北" : (Eval("Area").ToString() == "2") ? "中" : "南" %></span><br>基本說明文字:<span><%#Eval("BasicInfo_Text")%></span><br>基地位置文字:<span><%#Eval("LocationMap_Text")%></span><br>是否顯示於前端網站:<span><%#(Eval("Visible").ToString() == "True")? "是" : "否" %></span><br>')"
                                        onmouseout="hidetip()">
                                <div>
                                  <asp:Label ID="Label4" runat="server" Text='<%# Bind("SortedOrder") %>'></asp:Label>
                                </div>
                               </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="建立日期">
                                <EditItemTemplate>
                                    <asp:Label ID="lbl_created_date" runat="server" Text='<%# Bind("CreatedDate") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                  <a class="tooltip_a" href="#" onmouseover="showtip('名稱:<d><%#Eval("Name_ZH") %></d>\<br>樓層: <span><%#Eval("Floors") %></span><br>位置:<span><%#Eval("Location") %></span><br>連絡電話:<span><%#Eval("Phone") %></span><br>規劃坪數:<span><%#Eval("Spaces")%></span><br>通知電話號碼1:<span><%#Eval("Notifi_Phone1")%></span><br>通知電話號碼2:<span><%#Eval("Notifi_Phone2")%></span><br>顯示順序:<span><%#Eval("SortedOrder") %></span><br>區域:<span><%#(Eval("Area").ToString() == "1") ? "北" : (Eval("Area").ToString() == "2") ? "中" : "南" %></span><br>基本說明文字:<span><%#Eval("BasicInfo_Text")%></span><br>基地位置文字:<span><%#Eval("LocationMap_Text")%></span><br>是否顯示於前端網站:<span><%#(Eval("Visible").ToString() == "True")? "是" : "否" %></span><br>')"
                                        onmouseout="hidetip()">
                               <div>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("CreatedDate") %>'></asp:Label>
                               </div>
                                 </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="是否顯示於前端網站">
                                <EditItemTemplate>
                                    <div style="text-align:center;">
                                        <asp:CheckBox ID="chk_visible" runat="server" Checked='<%# Bind("Visible") %>' />
                                    </div>
                                </EditItemTemplate>
                                <ItemTemplate>
                                  <a class="tooltip_a" href="#" onmouseover="showtip('名稱:<d><%#Eval("Name_ZH") %></d>\<br>樓層: <span><%#Eval("Floors") %></span><br>位置:<span><%#Eval("Location") %></span><br>連絡電話:<span><%#Eval("Phone") %></span><br>規劃坪數:<span><%#Eval("Spaces")%></span><br>通知電話號碼1:<span><%#Eval("Notifi_Phone1")%></span><br>通知電話號碼2:<span><%#Eval("Notifi_Phone2")%></span><br>顯示順序:<span><%#Eval("SortedOrder") %></span><br>區域:<span><%#(Eval("Area").ToString() == "1") ? "北" : (Eval("Area").ToString() == "2") ? "中" : "南" %></span><br>基本說明文字:<span><%#Eval("BasicInfo_Text")%></span><br>基地位置文字:<span><%#Eval("LocationMap_Text")%></span><br>是否顯示於前端網站:<span><%#(Eval("Visible").ToString() == "True")? "是" : "否" %></span><br>')"
                                        onmouseout="hidetip()">
                                    <div style="text-align:center;">
                                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Visible") %>' 
                                            Enabled="false" />
                                    </div>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" 
                                        CommandName="Update" Text="儲存變更"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                                        CommandName="Cancel" Text="取消"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>

                                    <asp:HyperLink ID="HyperLink1" runat="server" ForeColor="Blue"
                                        NavigateUrl='<%# Eval("ConObjID","~/admin/modifycons.aspx?id={0}") %>'>修改細項資料</asp:HyperLink>
                                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False"
                                        CommandName="Edit" Text="快速編輯"></asp:LinkButton>
                                    
                                </ItemTemplate>
                            </asp:TemplateField></Columns>
                        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#003399" />
                        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <SortedAscendingCellStyle BackColor="#EDF6F6" />
                        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                        <SortedDescendingCellStyle BackColor="#D6DFDF" />
                        <SortedDescendingHeaderStyle BackColor="#002876" />
                    </asp:GridView>
                         </ContentTemplate>
                         <Triggers> 
                            <asp:AsyncPostBackTrigger ControlID="btncurrentpage_constructor" />
                            <asp:AsyncPostBackTrigger ControlID="dl_search_zone" EventName="SelectedIndexChanged" />
                         </Triggers>
                    </asp:UpdatePanel>
                   <table>
                        <tr>
                               <td><uc1:Paging ID="Paging1" runat="server" /></td>
                        </tr>
                   </table>
                
</ContentTemplate> 
            







</asp:TabPanel>
            <asp:TabPanel ID="TabPanel3" runat="server" HeaderText="Customer">
                <HeaderTemplate>
                    我有興趣
                
</HeaderTemplate>
                







<ContentTemplate>
                           <div>
                    <asp:HiddenField ID="hdfcurrent_page_customer" runat="server" Value="1" />
                        <asp:Button style="display:none" ID="btncurrnet_page_customer" runat="server" 
                            Text="Customer" onclick="btncurrnet_page_customer_Click" />
                        </div>
                        <div>
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                            
                        <table class="style14">
                            <tr>
                                <td width="32">
                                    起日</td>
                                <td width="10%">
                                    <asp:TextBox ID="txt_search_cus_from" runat="server"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                                        TargetControlID="txt_search_cus_from" FilterType="Custom, Numbers" 
                                        ValidChars="/\-" runat="server" Enabled="True"></asp:FilteredTextBoxExtender>
                                    <asp:CalendarExtender ID="CalendarExtender1" 
                                        TargetControlID="txt_search_cus_from" runat="server" Format="MM/dd/yyyy" 
                                        Enabled="True"></asp:CalendarExtender>
                                </td>
                                <td width="32">
                                    迄日</td>
                                <td width="10%">
                                    <asp:TextBox ID="txt_search_cus_to" runat="server"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" 
                                        TargetControlID="txt_search_cus_to" FilterType="Custom, Numbers" 
                                        ValidChars="/\-" runat="server" Enabled="True"></asp:FilteredTextBoxExtender>
                                    <asp:CalendarExtender ID="CalendarExtender2" 
                                        TargetControlID="txt_search_cus_to" runat="server" Format="MM/dd/yyyy" 
                                        Enabled="True"></asp:CalendarExtender>
                                </td>
                                <td width="32">
                                    建案</td>
                                <td width="10%">
                                    <asp:DropDownList ID="dl_search_cus_cons_object" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td width="48">
                                    已處理</td>
                                <td width="10%">
                                    <asp:DropDownList ID="dl_saerch_cus_pro" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td width="48">
                                    <asp:Button ID="btn_search_cus" runat="server" Text="搜尋" 
                                        onclick="btn_search_cus_Click" Height="26px" ValidationGroup="tk" />
                                </td>
                                <td>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                        ControlToCompare="txt_search_cus_from" ControlToValidate="txt_search_cus_to" 
                                        ErrorMessage="迄日 &amp;gt; 起日" Font-Bold="True" Operator="GreaterThan" 
                                        ValidationGroup="tk"></asp:CompareValidator>
                                </td>
                            </tr>
                          
                        </table>     
                        </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>                                           
                        <div>
                    <asp:Button ID="btn_export_to_excel" runat="server" Text="匯出成Excel檔" 
                        onclick="btn_export_to_excel_Click" /> </div>            
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                    <asp:GridView ID="gv_customer" runat="server" AutoGenerateColumns="False" 
                        BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" 
                        CellPadding="5" EmptyDataText="No result found!" 
                        Width="100%" 
                        onrowcancelingedit="gv_customer_RowCancelingEdit" 
                        onrowediting="gv_customer_RowEditing" onrowupdating="gv_customer_RowUpdating" 
                        DataKeyNames="id" PageSize="2">
                        <Columns>
                            <asp:TemplateField HeaderText=" 姓名">
                                <EditItemTemplate>
                                    <asp:Label ID="TextBox1" runat="server" Text='<%# Bind("name") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="建案">
                                <EditItemTemplate>
                                    <asp:Label ID="TextBox2" runat="server" Text='<%# Bind("name_zh") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("name_zh") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="電話">
                                <EditItemTemplate>
                                    <asp:Label ID="TextBox3" runat="server" Text='<%# Bind("phone") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("phone") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email">
                                <EditItemTemplate>
                                    <asp:Label ID="TextBox4" runat="server" Text='<%# Bind("email") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("email") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="需求類型">
                                <EditItemTemplate>
                                    <asp:Label ID="TextBox5" runat="server" Text='<%# (Eval("req_type").ToString() == "1") ? "預約看屋" : "索取資料" %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# (Eval("req_type").ToString() == "1") ? "預約看屋" : "索取資料" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="建立日期">
                                <EditItemTemplate>
                                    <asp:Label ID="TextBox6" runat="server" Text='<%# Bind("created_date") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("created_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="已處理">
                                <EditItemTemplate>
                                    <div style="text-align:center;">
                                        <asp:CheckBox ID="chk_processed" runat="server" 
                                            Checked='<%# Bind("processed") %>' style="text-align: center" ></asp:CheckBox>
                                    </div>
                                </EditItemTemplate>
                                <ItemTemplate>
                                <div style="text-align:center;">
                                    <asp:CheckBox ID="chk_processed" runat="server" Checked='<%# Bind("processed") %>' Enabled="false"></asp:CheckBox>
                                </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <EditItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" 
                                        CommandName="Update" Text="儲存變更"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                                        CommandName="Cancel" Text="取消"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                        CommandName="Edit" Text="快速編輯"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#003399" />
                        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <SortedAscendingCellStyle BackColor="#EDF6F6" />
                        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                        <SortedDescendingCellStyle BackColor="#D6DFDF" />
                        <SortedDescendingHeaderStyle BackColor="#002876" />
                    </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btncurrnet_page_customer" />
                       
                    </Triggers>
                    </asp:UpdatePanel>
                     <table>
                        <tr>
                            <td>                           
                                <uc2:Paging_customer ID="Paging_customer1" runat="server" />                   
                            </td>
                        </tr>
                     </table>
                
</ContentTemplate>
            







</asp:TabPanel>
            <asp:TabPanel ID="TabPanel4" runat="server" HeaderText="SMS Log">
                <HeaderTemplate>
                    簡訊通知Log
                
</HeaderTemplate>
                







<ContentTemplate>
                 <div>
                           <asp:HiddenField ID="hdfcurrent_page_smslog" runat="server" Value="1" />
                    <asp:Button style="display:none" ID="btn_current_page_smslog" runat="server" Text="smslog" 
                        onclick="btn_current_page_smslog_Click" />
                        </div>
                <div>
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                         <table class="style14">
                                    <tr>
                                        <td width="32">
                                            起日</td>
                                        <td width="10%">
                                            <asp:TextBox ID="txt_search_sms_from" runat="server"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="txt_search_sms_from_FilteredTextBoxExtender" 
                                                runat="server" Enabled="True" FilterType="Custom, Numbers" 
                                                TargetControlID="txt_search_sms_from" ValidChars="/\-">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:CalendarExtender ID="txt_search_sms_from_CalendarExtender" runat="server" 
                                                Enabled="True" Format="MM/dd/yyyy" TargetControlID="txt_search_sms_from">
                                            </asp:CalendarExtender>
                                        </td>
                                        <td width="32">
                                            迄日</td>
                                        <td width="10%">
                                            <asp:TextBox ID="txt_search_sms_to" runat="server" Height="22px"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="txt_search_sms_to_FilteredTextBoxExtender" 
                                                runat="server" Enabled="True" FilterType="Custom, Numbers" 
                                                TargetControlID="txt_search_sms_to" ValidChars="/\-">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:CalendarExtender ID="txt_search_sms_to_CalendarExtender" runat="server" 
                                                Enabled="True" Format="MM/dd/yyyy" TargetControlID="txt_search_sms_to">
                                            </asp:CalendarExtender>
                                        </td>
                                        <td width="32">
                                            建案</td>
                                        <td width="10%">
                                            <asp:DropDownList ID="dl_search_sms_cons_object" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td width="48">
                                            <asp:Button ID="btn_search_sms" runat="server" Height="26px" 
                                                OnClick="btn_search_sms_Click" Text="搜尋" ValidationGroup="tk_sms" />
                                        </td>
                                        <td>
                                            <asp:CompareValidator ID="CompareValidator2" runat="server" 
                                                ControlToCompare="txt_search_sms_from" ControlToValidate="txt_search_sms_to" 
                                                ErrorMessage="迄日 &amp;gt; 起日" Font-Bold="True" Operator="GreaterThan" 
                                                ValidationGroup="tk_sms"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                </table>

                      </ContentTemplate>
                    </asp:UpdatePanel>
                       </div>
                   
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                                <asp:GridView ID="gv_smslog" runat="server" BackColor="White" 
                                    BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="5" 
                                    Width="100%" AutoGenerateColumns="False" EmptyDataText="No Result found!" 
                                    style="margin-right: 2px">
                                    <Columns>
                                        <asp:BoundField DataField="name_zh" HeaderText="建案" />
                                        <asp:BoundField DataField="SmsContent" HeaderText="簡訊內容" />
                                        <asp:BoundField DataField="CreatedDate" HeaderText="建立日期" />
                                    </Columns>
                                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                                    <RowStyle BackColor="White" ForeColor="#003399" />
                                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                    <SortedAscendingCellStyle BackColor="#EDF6F6" />
                                    <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                                    <SortedDescendingCellStyle BackColor="#D6DFDF" />
                                    <SortedDescendingHeaderStyle BackColor="#002876" />
                                </asp:GridView>
                                 </ContentTemplate>
                                 <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btn_current_page_smslog" />
                                 </Triggers>
                        </asp:UpdatePanel>
                         <table>
                                    <tr>
                                            <td><uc4:Paging_smslog ID="Paging_smslog1" runat="server" />
                                    </td>
                                    </tr>
                                </table>
                
</ContentTemplate>
            







</asp:TabPanel>
            <asp:TabPanel ID="TabPanel5" runat="server" HeaderText="TabPanel5">
                <HeaderTemplate>
                    關於國泰
                
</HeaderTemplate>
                







<ContentTemplate>
                    <asp:TabContainer ID="TabContainer2" runat="server" ActiveTabIndex="0" 
                        CssClass="" Width="100%" Height="100%">
                        <asp:TabPanel ID="TabPanel6" runat="server" HeaderText="TabPanel1">
                            <HeaderTemplate>
                                基本資料
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table class="style14" cellpadding="10">
                                    <tr>
                                        <td class="style17">
                                            公司簡介</td>
                                        <td>
                                            <asp:TextBox ID="txt_aboutus_com" runat="server" Height="220px" 
                                                TextMode="MultiLine" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style17">
                                            經營理念</td>
                                        <td>
                                            <asp:TextBox ID="txt_aboutus_ope" runat="server" Height="220px" 
                                                TextMode="MultiLine" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style17">
                                            &nbsp;</td>
                                        <td>
                                            <asp:Button ID="btn_update_about_us" runat="server" 
                                                onclick="btn_update_about_us_Click" Text="儲存變更" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:TabPanel>
                        <asp:TabPanel ID="TabPanel7" runat="server" HeaderText="TabPanel2">
                            <HeaderTemplate>
                                得獎紀事標題
                            </HeaderTemplate>
                            <ContentTemplate>
                            <div>
                            <asp:HiddenField ID="hdfcurrent_page_prizetitle" runat="server" Value="1" />
                                <asp:Button style="display:none" ID="btn_current_page_Prizetitle" runat="server" 
                                    Text="PrizeTitle" onclick="btn_current_page_Prizetitle_Click" />
                            </div>
                                <div>
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                            <asp:GridView ID="gv_prize_title" runat="server" 
                                                AutoGenerateColumns="False" CellPadding="10" ShowFooter="True" Width="100%" 
                                                onrowcancelingedit="gv_aboutus_prize_title_RowCancelingEdit" 
                                                onrowdeleting="gv_aboutus_prize_title_RowDeleting" 
                                                onrowediting="gv_aboutus_prize_title_RowEditing" 
                                                onrowupdating="gv_aboutus_prize_title_RowUpdating" BackColor="White" 
                                                BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" DataKeyNames="id" 
                                                EmptyDataText="No result found!" 
                                                onrowcommand="gv_aboutus_prize_title_RowCommand" PageSize="5">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Prize Title">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txt_prize_title" runat="server" Text='<%# Bind("title") %>' 
                                                                Width="100%"></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txt_prize_title" runat="server" Width="100%"></asp:TextBox>
                                                        </FooterTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("title") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="70%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action">
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" 
                                                                CommandName="Update" Text="儲存變更"></asp:LinkButton>
                                                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                                                                CommandName="Cancel" Text="取消"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            
                                                                        <asp:Button ID="btn_add_title" runat="server" Text="新增" 
                                                                            CommandName="insert" />
                                                                    
                                                        </FooterTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                                                CommandName="Edit" Text="快速編輯"></asp:LinkButton>
                                                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                                                                CommandName="Delete" Text="刪除" 
                                                                onclientclick="return confirm('Do you want to delete ?')"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30%" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <table class="style14">
                                                        <tr>
                                                            <td class="style18" width="70%">
                                                                Prize Title</td>
                                                            <td class="style18" width="30%">
                                                                Action</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="70%">
                                                                <asp:TextBox ID="txt_prize_title" runat="server" Width="100%"></asp:TextBox>
                                                            </td>
                                                            <td width="30%">
                                                                <table class="style14">
                                                                    <tr>
                                                                        <td width="30%">
                                                                            &nbsp;</td>
                                                                        <td>
                                                                            <asp:Button ID="btn_add_prize_title" runat="server" style="text-align: right" 
                                                                                Text="新增" CommandName="emptyinsert" />
                                                                        </td>
                                                                        <td width="30%">
                                                                            &nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EmptyDataTemplate>
                                                <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                                <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                                                <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                                                <RowStyle BackColor="White" ForeColor="#003399" />
                                                <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                                <SortedAscendingCellStyle BackColor="#EDF6F6" />
                                                <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                                                <SortedDescendingCellStyle BackColor="#D6DFDF" />
                                                <SortedDescendingHeaderStyle BackColor="#002876" />
                                            </asp:GridView>
                                             </ContentTemplate>
                                             <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btn_current_page_Prizetitle" />
                                             </Triggers>
                                    </asp:UpdatePanel>
                                      </div>
                                      <table>
                                        <tr>
                                            <td>
                                                
                                                <uc3:Paging_PrizeTitle ID="Paging_PrizeTitle1" runat="server" />
                                                
                                            </td>
                                        </tr>
                                      </table>
                            </ContentTemplate>
                        </asp:TabPanel>
                        <asp:TabPanel ID="TabPanel" runat="server" HeaderText="TabPanel3">
                            <HeaderTemplate>
                                得獎紀事項目
                            </HeaderTemplate>
                            <ContentTemplate>
                            <div>
                                <asp:HiddenField ID="hdf_current_page_PrizeContent" runat="server" Value="1" />
                                <asp:Button style="display:none" ID="btn_urrent_page_PrizeContent" runat="server" 
                                    onclick="btn_urrent_page_PrizeContent_Click" Text="PrizeContent" />
                            </div>
                            <div>
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                          
                                <asp:GridView ID="gv_prize_content" runat="server" 
                                    AutoGenerateColumns="False" CellPadding="4" ShowFooter="True" Width="100%" 
                                    BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" 
                                    EmptyDataText="No result found!" 
                                    onrowcancelingedit="gv_aboutus_prize_content_RowCancelingEdit" 
                                    onrowcommand="gv_aboutus_prize_content_RowCommand" 
                                    onrowcreated="gv_aboutus_prize_content_RowCreated" 
                                    onrowdatabound="gv_prize_content_RowDataBound" 
                                    onrowdeleting="gv_aboutus_prize_content_RowDeleting" 
                                    onrowediting="gv_aboutus_prize_content_RowEditing" 
                                    onrowupdating="gv_aboutus_prize_content_RowUpdating" PageSize="15" 
                                    DataKeyNames="id">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Prize Title">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="dl_prize_title" runat="server" DataTextField="title" 
                                                    DataValueField="ID" Width="100%">
                                                </asp:DropDownList>
                                                <asp:Label ID="lbl_prize_title" runat="server" 
                                                    Text='<%# Bind("prize_title_id") %>' Visible="False"></asp:Label>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="dl_prize_title" runat="server" DataTextField="title" 
                                                    DataValueField="ID" Width="100%">
                                                </asp:DropDownList>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("title") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="40%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Prize content">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txt_prize_content" runat="server" Height="100px" 
                                                    Text='<%# Bind("content") %>' TextMode="MultiLine" Width="100%"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txt_prize_content" runat="server" Height="100px" 
                                                    TextMode="MultiLine" Width="100%"></asp:TextBox>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox ID="TextBox3" runat="server" Height="100px" ReadOnly="True" 
                                                    Text='<%# Bind("content") %>' TextMode="MultiLine" Width="100%"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle Width="40%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <EditItemTemplate>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="True" 
                                                    CommandName="Update" Text="儲存變更"></asp:LinkButton>
                                                &nbsp;<asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="False" 
                                                    CommandName="Cancel" Text="取消"></asp:LinkButton>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:Button ID="btn_add_content" runat="server" Text="新增" 
                                                    CommandName="insert" />
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton5" runat="server" CausesValidation="False" 
                                                    CommandName="Edit" Text="快速編輯"></asp:LinkButton>
                                                &nbsp;<asp:LinkButton ID="LinkButton6" runat="server" CausesValidation="False" 
                                                    CommandName="Delete" Text="刪除" 
                                                    onclientclick="return confirm('Do you want to delete ?')"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <table class="style14">
                                            <tr>
                                                <td width="40%" class="style18">
                                                    Prize Title</td>
                                                <td width="50%" class="style18">
                                                    Prize Content</td>
                                                <td width="10%" class="style18">
                                                    Action</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="dl_prize_title" runat="server" Width="100%" 
                                                        DataTextField="title" DataValueField="ID">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txt_prize_content" runat="server" TextMode="MultiLine" 
                                                        Width="100%" Height="100px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btn_add_content" runat="server" Text="新增" 
                                                        CommandName="emptyinsert" />
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                                    <RowStyle BackColor="White" ForeColor="#003399" />
                                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                    <SortedAscendingCellStyle BackColor="#EDF6F6" />
                                    <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                                    <SortedDescendingCellStyle BackColor="#D6DFDF" />
                                    <SortedDescendingHeaderStyle BackColor="#002876" />
                                </asp:GridView>
                                      
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btn_urrent_page_PrizeContent" />
                                </Triggers>
                                </asp:UpdatePanel>
                               
                                    </div>
                                     <table>
                                     <tr>
                                        <td><uc5:Paging_PrizeContent ID="Paging_PrizeContent1" runat="server" />
                                         </td>
                                     </tr>
                                     </table>
                            </ContentTemplate>
                        </asp:TabPanel>
                    </asp:TabContainer>
                
</ContentTemplate>
            







</asp:TabPanel>
            <asp:TabPanel ID="AccountTab" runat="server" HeaderText="TabPanel6">
                <HeaderTemplate>
                    客戶資料
                
</HeaderTemplate>
                







<ContentTemplate>
    <div>
        <asp:HiddenField ID="hdf_current_page_clientaccount" runat="server" Value="1" />
        <asp:Button Style="display: none" ID="btn_current_page_clientaccount" runat="server"
            Text="clientaccount" OnClick="btn_current_page_clientaccount_Click" />
        <div style="position: absolute;">
            <asp:UpdatePanel ID="UpdatePanel11" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    身份證字號:
                    <asp:TextBox ID="txt_search_client" runat="server" Width="100px"></asp:TextBox><asp:Button
                        ID="btn_search_client" runat="server" OnClick="btn_search_client_Click" Text="搜尋" /></ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div style="text-align: right;">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="fu_excelfile"
                Style="text-align: right" ErrorMessage="請選擇Excel檔" ValidationGroup="account"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                    ID="RegularExpressionValidator1" runat="server" ControlToValidate="fu_excelfile"
                    ErrorMessage="請選擇Excel檔" SetFocusOnError="True" ValidationExpression="[a-zA-Z\\].*(.xls|.XLS|.xlsx|.XLSX)$"
                    ValidationGroup="account"></asp:RegularExpressionValidator><asp:Label ID="lbl_filepath"
                        runat="server"></asp:Label><asp:FileUpload ID="fu_excelfile" runat="server" />
            <asp:Button ID="btn_import_client" runat="server" Text="匯入主檔" ValidationGroup="account"
                OnClick="btn_import_client_Click" />
        </div>
    </div>
                    <div><asp:UpdatePanel ID="UpdatePanel9" runat="server"><ContentTemplate><asp:GridView ID="gv_account" runat="server" AutoGenerateColumns="False" 
                            BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" 
                            CellPadding="10" DataKeyNames="id" EmptyDataText="No result found!" 
                            onrowcancelingedit="gv_account_RowCancelingEdit" onrowediting="gv_account_RowEditing" 
                            onrowupdating="gv_account_RowUpdating" PageSize="5" 
                            Width="100%" onrowcommand="gv_account_RowCommand"><Columns><asp:TemplateField HeaderText="身份證字號"><EditItemTemplate><asp:Label ID="lbl_username" runat="server" Text='<%# Bind("userid") %>'></asp:Label></EditItemTemplate><ItemTemplate><asp:Label ID="Label7" runat="server" Text='<%# Bind("userid") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="密碼"><EditItemTemplate><asp:TextBox ID="txt_password" runat="server" 
                                            Text='<%# Bind("client_password") %>'
                                            ToolTip="Type password here"></asp:TextBox></EditItemTemplate><ItemTemplate><asp:Label ID="Label8" runat="server" Text='<%# Bind("client_password") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="生日"><EditItemTemplate><asp:TextBox ID="txt_birthday" runat="server" Text='<%# Bind("birthday") %>'></asp:TextBox></EditItemTemplate><ItemTemplate><asp:Label ID="lbl_birthday" runat="server" Text='<%# Bind("birthday") %>'></asp:Label></ItemTemplate><ItemStyle Width="30%" /></asp:TemplateField>
                                <asp:TemplateField HeaderText="電話"><EditItemTemplate><asp:TextBox ID="txt_phone_user" runat="server" Text='<%# Bind("phone") %>'></asp:TextBox></EditItemTemplate><ItemTemplate><asp:Label ID="Label10" runat="server" Text='<%# Bind("phone") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="停用"><EditItemTemplate><div style="text-align:center;"><asp:CheckBox ID="chk_disable" Checked='<%# Bind("disable") %>' runat="server" /></div>
                                    </EditItemTemplate>
                                    <ItemTemplate><div style="text-align:center;"><asp:CheckBox ID="chk_disable" Checked='<%# Bind("disable") %>' runat="server" Enabled="false"/></div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False"><EditItemTemplate><asp:LinkButton ID="LinkButton7" runat="server" CausesValidation="True" 
                                            CommandName="Update" Text="儲存變更"></asp:LinkButton>&nbsp;<asp:LinkButton ID="LinkButton8" runat="server" CausesValidation="False" 
                                            CommandName="Cancel" Text="取消"></asp:LinkButton></EditItemTemplate><ItemTemplate><asp:HyperLink class="ttt" ID="hypDown" runat="server" 
                                            NavigateUrl='<%# Eval("ID","~/admin/managedb_Info.aspx?id={0}") %>'>快速編輯</asp:HyperLink><%--<asp:LinkButton ID="LinkButton9" runat="server" CausesValidation="False" 
                                            CommandName="Edit" Text="快速編輯"></asp:LinkButton>--%>&nbsp;<asp:LinkButton ID="LinkButton10" runat="server" CausesValidation="false" 
                                            CommandName="Reset Password" onclientclick="return confirm('Do you want to reset password ?')" 
                                            Text="重設密碼"></asp:LinkButton></ItemTemplate><ItemStyle Width="20%" /></asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" /><HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" /><PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" /><RowStyle BackColor="White" ForeColor="#003399" /><SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" /><SortedAscendingCellStyle BackColor="#EDF6F6" /><SortedAscendingHeaderStyle BackColor="#0D4AC4" /><SortedDescendingCellStyle BackColor="#D6DFDF" /><SortedDescendingHeaderStyle BackColor="#002876" /></asp:GridView>
                        
</ContentTemplate>
<Triggers><asp:AsyncPostBackTrigger ControlID="btn_current_page_clientaccount"   />
</Triggers>
</asp:UpdatePanel>








                          <table><tr><td><uc8:Paging_clientaccount ID="Paging_clientaccount1" runat="server" />








                               
                            </td>
                        </tr>
                    </table>
                    </div>
                  
                
</ContentTemplate>
            







</asp:TabPanel>
            <asp:TabPanel ID="PaymentHistoryTab" runat="server" HeaderText="PaymentHistory">
                <HeaderTemplate>
                    繳款紀錄
                
</HeaderTemplate>
                







<ContentTemplate>
                    <div>
                        <div style="position:absolute;">
                            <asp:UpdatePanel ID="UpdatePanel13" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                            身份證字號: <asp:TextBox ID="txt_search_payment" runat="server" Width="100px"></asp:TextBox>
                            <asp:Button ID="btn_search_payment" runat="server" Text="搜尋" 
                                    onclick="btn_search_payment_Click"/>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div style="text-align: right;">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ControlToValidate="fu_excelfile_payment" style="text-align:right"
                                ErrorMessage="請選擇Excel檔" ValidationGroup="import"></asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" 
                            ControlToValidate="fu_excelfile_payment" 
                            ErrorMessage="請選擇Excel檔" SetFocusOnError="True" 
                            ValidationExpression="[a-zA-Z\\].*(.xls|.XLS|.xlsx|.XLSX)$" 
                            ValidationGroup="import"></asp:RegularExpressionValidator>
                            <asp:Label ID="lbl_filepath_payment" runat="server"></asp:Label>
                            <asp:FileUpload ID="fu_excelfile_payment" runat="server" />
                            <asp:Button ID="btn_import_payment" runat="server" Text="匯入主檔" 
                                OnClick="btn_import_payment_Click" ValidationGroup="import" />
                        </div>
                    </div>
                    <div>
                    <asp:HiddenField ID="hdf_current_page_paymenthistory" runat="server" Value="1" />
                    <asp:Button style="display:none" ID="btn_current_page_paymenthistory" runat="server" 
                        Text="Paymenthistory" onclick="btn_current_page_paymenthistory_Click" />
                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>                   
                        <asp:GridView ID="gv_payment_history" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="10" DataKeyNames="id" EmptyDataText="No result found!" PageSize="5" Width="100%" OnRowCancelingEdit="gv_payment_history_RowCancelingEdit" OnRowEditing="gv_payment_history_RowEditing" OnRowUpdating="gv_payment_history_RowUpdating">
                            <Columns>
                                <asp:TemplateField HeaderText="身份證字號">
                                    <EditItemTemplate>
                                        <asp:Label ID="lbl_username" runat="server" Text='<%# Bind("userid") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_username" runat="server" Text='<%# Bind("userid") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="工地名稱">
                                    <EditItemTemplate>
                                        <asp:Label ID="lbl_conobj" runat="server" Text='<%# Bind("con_obj_name") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_conobj" runat="server" Text='<%# Bind("con_obj_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="棟戶">
                                    <EditItemTemplate>
                                        <asp:Label ID="lbl_house" runat="server" Text='<%# Bind("house") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_house" runat="server" Text='<%# Bind("house") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="期別">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_type" runat="server" Text='<%# Bind("type") %>' Width="60px"></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_type" runat="server" Text='<%# Bind("type") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="應繳日期">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_should_pay_date" runat="server" Text='<%# Bind("should_pay_date") %>' Width="90px"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender5" TargetControlID="txt_should_pay_date" Format="yyyy/MM/dd" runat="server">
                                        </asp:CalendarExtender>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" TargetControlID="txt_should_pay_date" FilterType="Custom,Numbers" ValidChars="/\_-" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_should_pay_date" runat="server" Text='<%# Bind("should_pay_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="應繳金額">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_should_pay_amount" runat="server" Text='<%# Bind("should_pay_amount") %>' Width="60px"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txt_should_pay_amount" FilterType="Numbers" runat="server">
                                    </asp:FilteredTextBoxExtender>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_should_pay_amount" runat="server" Text='<%# Bind("should_pay_amount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="實繳日期">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_actual_pay_date" runat="server" Text='<%# Bind("actual_pay_date") %>' Width="90px"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender6" TargetControlID="txt_actual_pay_date" Format="yyyy/MM/dd" runat="server">
                                        </asp:CalendarExtender>
                                         <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" TargetControlID="txt_actual_pay_date" FilterType="Custom,Numbers" ValidChars="/\_-" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_actual_pay_date" runat="server" Text='<%# Bind("actual_pay_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="實繳金額">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_actual_pay_amount" runat="server" Text='<%# Bind("actual_pay_amount") %>' Width="60px"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txt_actual_pay_amount" FilterType="Numbers" runat="server">
                                    </asp:FilteredTextBoxExtender>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_actual_pay_amount" runat="server" Text='<%# Bind("actual_pay_amount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="LinkButton11" runat="server" CausesValidation="True" CommandName="Update" Text="儲存變更"></asp:LinkButton>
                                        &nbsp;<asp:LinkButton ID="LinkButton12" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton13" runat="server" CausesValidation="False" CommandName="Edit" Text="快速編輯"></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="20%" />
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                            <RowStyle BackColor="White" ForeColor="#003399" />
                            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                            <SortedAscendingCellStyle BackColor="#EDF6F6" />
                            <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                            <SortedDescendingCellStyle BackColor="#D6DFDF" />
                            <SortedDescendingHeaderStyle BackColor="#002876" />
                        </asp:GridView>
                             </ContentTemplate>
                             <Triggers>
                             <asp:AsyncPostBackTrigger ControlID="btn_current_page_paymenthistory" />
                             </Triggers>
                        </asp:UpdatePanel>
                        <table>
                        <tr>
                            <td>
                                <uc9:Paging_Paymenthistory ID="Paging_Paymenthistory1" runat="server" />
                            </td>
                        </tr>
                        </table>
                    </div>
                
</ContentTemplate>
            







</asp:TabPanel>
            <asp:TabPanel ID="ConProgressTab" runat="server" HeaderText="Con Progress">
                <HeaderTemplate>
                    工程進度
                
</HeaderTemplate>
                







<ContentTemplate>
                    <div>
                        <div style="text-align: right;">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                ControlToValidate="fu_con_progress" style="text-align:right"
                                ErrorMessage="請選擇Excel檔" ValidationGroup="con_progress"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                ControlToValidate="fu_con_progress" 
                                ErrorMessage="請選擇Excel檔" SetFocusOnError="True" 
                                ValidationExpression="[a-zA-Z\\].*(.xls|.XLS|.xlsx|.XLSX)$" 
                                ValidationGroup="con_progress"></asp:RegularExpressionValidator>
                            <asp:FileUpload ID="fu_con_progress" runat="server" Height="22px" />
                            <asp:Button ID="btn_import_con_progress" runat="server" Text="匯入主檔" 
                                ValidationGroup="con_progress" OnClick="btn_import_con_progress_Click"/>
                        </div>
                    </div>
                    <div>
                    <asp:HiddenField ID="hdf_con_progress" runat="server" Value="1" />
                        <asp:Button style="display:none" ID="btn_conprogress" runat="server" Text="Progress" 
                            onclick="btn_conprogress_Click" />
                        <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>
                      
                    <asp:GridView ID="gv_con_progress" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="10" DataKeyNames="id" EmptyDataText="No result found!" PageSize="5" Width="100%" OnRowCancelingEdit="gv_con_progress_RowCancelingEdit" OnRowEditing="gv_con_progress_RowEditing" OnRowUpdating="gv_con_progress_RowUpdating">
                        <Columns>
                            <asp:TemplateField HeaderText="工地名稱">
                                <EditItemTemplate>
                                    <asp:Label ID="lbl_conobj" runat="server" Text='<%# Bind("con_obj_name") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_conobj" runat="server" Text='<%# Bind("con_obj_name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="期別">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txt_type" runat="server" Text='<%# Bind("type") %>'></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="txt_type" FilterType="Numbers" runat="server">
                                    </asp:FilteredTextBoxExtender>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_type" runat="server" Text='<%# Bind("type") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="工程進度">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txt_progress" runat="server" Text='<%# Bind("progress") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_progress" runat="server" Text='<%# Bind("progress") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="開始日期">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txt_start_date" runat="server" Text='<%# Bind("start_date") %>'></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender3" TargetControlID="txt_start_date" Format="yyyy/MM/dd" runat="server">
                                    </asp:CalendarExtender>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" TargetControlID="txt_start_date" FilterType="Custom,Numbers" ValidChars="/\-_" runat="server">
                                    </asp:FilteredTextBoxExtender>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_start_date" runat="server" Text='<%# Bind("start_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="完工日期">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txt_end_date" runat="server" Text='<%# Bind("end_date") %>'></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender4" TargetControlID="txt_end_date" Format="yyyy/MM/dd" runat="server">
                                    </asp:CalendarExtender>
                                     <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" TargetControlID="txt_end_date" FilterType="Custom,Numbers" ValidChars="/\-_" runat="server">
                                    </asp:FilteredTextBoxExtender>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_end_date" runat="server" Text='<%# Bind("end_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <EditItemTemplate>
                                    <asp:LinkButton ID="LinkButton14" runat="server" CausesValidation="True" CommandName="Update" Text="儲存變更"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="LinkButton15" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton16" runat="server" CausesValidation="False" CommandName="Edit" Text="快速編輯"></asp:LinkButton>
                                    <asp:HyperLink ID="HyperLink1" runat="server" ForeColor="Blue"
                                        NavigateUrl='<%# Eval("id","~/admin/updateimages.aspx?con_progress_id={0}") %>'>上傳照片</asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle Width="20%" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#003399" />
                        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <SortedAscendingCellStyle BackColor="#EDF6F6" />
                        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                        <SortedDescendingCellStyle BackColor="#D6DFDF" />
                        <SortedDescendingHeaderStyle BackColor="#002876" />
                    </asp:GridView>

                      </ContentTemplate>
                      <Triggers>
                      <asp:AsyncPostBackTrigger ControlID="btn_conprogress"/>
                      </Triggers>
                        </asp:UpdatePanel>
                        <table>
                            <tr>
                                <td>
                                    <uc10:Paging_conprogress ID="Paging_conprogress1" runat="server" />
                                </td>
                            </tr>
                        </table>
                
</ContentTemplate>
            







</asp:TabPanel>
        </asp:TabContainer>
     
    </div>
    </form>
</body>
</html>
