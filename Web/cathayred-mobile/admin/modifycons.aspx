﻿<%@ page language="C#" autoeventwireup="true" inherits="DBModify, App_Web_m2l3g225" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register src="../usControls/Adminusername.ascx" tagname="Adminusername" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .div
        {
            width:80%;
            vertical-align:middle;
            margin:0 auto;
        }
        .style14
        {
            width: 100%;
        }
        .style15
        {
            background-color: #3366FF;
        }
        .style16
        {
            color: #FFFFFF;
        }
        </style>
        
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <div class="div">
        <uc1:Adminusername ID="Adminusername1" runat="server" />
        
        <asp:TabContainer runat="server" ActiveTabIndex="0" Width="100%" 
            ID="tc_cons_detail">
            <asp:TabPanel runat="server" HeaderText="Basic Info" ID="tp_cons_basic_info" 
                meta:resourceKey="tp_cons_basic_info_resource">
                <HeaderTemplate>
                    基本資料
                </HeaderTemplate>
                <ContentTemplate>
                    <table cellpadding="7" cellspacing="0" class="style14" frame="border">
                        <tr>
                            <td class="style6" width="30%">
                                名稱</td>
                            <td width="70%">
                                <asp:TextBox ID="txt_name_zh" runat="server" 
                                                meta:resourceKey="txt_name_zhResource1" Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                ControlToValidate="txt_name_zh" ErrorMessage="*此為必填欄位"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style6" width="30%">
                                樓層</td>
                            <td width="70%">
                                <asp:TextBox ID="txt_floors" runat="server" 
                                                meta:resourceKey="txt_floorsResource1" Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                ControlToValidate="txt_floors" ErrorMessage="*此為必填欄位"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style11" width="30%">
                                位置</td>
                            <td width="70%">
                                <asp:TextBox ID="txt_location" runat="server" 
                                                meta:resourceKey="txt_locationResource1" Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                                ControlToValidate="txt_location" ErrorMessage="*此為必填欄位"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style11" width="30%">
                                連絡電話</td>
                            <td width="70%">
                                <asp:TextBox ID="txt_phone" runat="server" 
                                                meta:resourceKey="txt_phoneResource1" Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                                ControlToValidate="txt_phone" ErrorMessage="*此為必填欄位"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style7" width="30%">
                                規劃坪數</td>
                            <td class="style3" width="70%">
                                <asp:TextBox ID="txt_space" runat="server" 
                                                meta:resourceKey="txt_spaceResource1" Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                                ControlToValidate="txt_space" ErrorMessage="*此為必填欄位"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style11" width="30%">
                                通知電話號碼1</td>
                            <td width="70%">
                                <asp:TextBox ID="txt_not_phone1" runat="server" 
                                                meta:resourceKey="txt_not_phone1Resource1" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style11" width="30%">
                                通知電話號碼2</td>
                            <td width="70%">
                                <asp:TextBox ID="txt_not_phone2" runat="server" 
                                                meta:resourceKey="txt_not_phone2Resource1" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style11" width="30%">
                                顯示順序</td>
                            <td width="70%">
                                <asp:TextBox ID="txt_sorted_order" runat="server" 
                                                meta:resourceKey="txt_sorted_orderResource1" Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                                                ControlToValidate="txt_sorted_order" ErrorMessage="*此為必填欄位"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style6" width="30%">
                                區域</td>
                            <td width="70%">
                                <asp:RadioButton ID="rbtn_1" runat="server" Checked="True" GroupName="zone" 
                                                meta:resourceKey="rbtn_1Resource1" Text="北" />
                                <asp:RadioButton ID="rbtn_2" runat="server" GroupName="zone" 
                                                meta:resourceKey="rbtn_2Resource1" Text="中" />
                                <asp:RadioButton ID="rbtn_3" runat="server" GroupName="zone" 
                                                meta:resourceKey="rbtn_3Resource1" Text="南" />
                            </td>
                        </tr>
                        <tr>
                            <td class="style6" width="30%">
                                是否顯示於前端網站</td>
                            <td width="70%">
                                <asp:CheckBox ID="chk_visible" runat="server" 
                                                meta:resourceKey="chk_visibleResource1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="style6" width="30%">
                                基本說明文字</td>
                            <td width="70%">
                                <asp:TextBox ID="txt_bas_info_text" runat="server" 
                                                meta:resourceKey="txt_bas_info_textResource1" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style11" width="30%">
                                基地位置文字</td>
                            <td width="70%">
                                <asp:TextBox ID="txt_loc_info_text" runat="server" 
                                                meta:resourceKey="txt_loc_info_textResource1" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style8" width="30%">
                                <table class="style13" width="100%">
                                    <tr>
                                        <td class="style10" width="40%">
                                            主頁圖</td>
                                        <td>
                                            <asp:Image ID="img_title" runat="server" Height="100px" 
                                                            meta:resourceKey="img_titleResource1" style="text-align: left" 
                                                            Width="100%" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="style9" width="70%">
                                <asp:FileUpload ID="fuTitleImage" runat="server" 
                                                meta:resourceKey="fuTitleImageResource1" />
                                <asp:RegularExpressionValidator ID="FileUpLoadValidator1" runat="server" 
                                                ControlToValidate="fuTitleImage" 
                                                ErrorMessage="僅能選擇JPG,PNG及GIF圖檔" 
                                                meta:resourceKey="FileUpLoadValidator1Resource1" 
                                                
                                                
                                    
                                    ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpeg|.JPEG|.jpg|.JPG|.gif|.GIF|.png|.PNG)$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style6" width="30%">
                                <table class="style13" width="100%">
                                    <tr>
                                        <td class="style10" width="40%">
                                            外觀全景圖</td>
                                        <td>
                                            <asp:Image ID="img_bas_info" runat="server" Height="100px" 
                                                            meta:resourceKey="img_bas_infoResource1" Width="100%" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="70%">
                                <asp:FileUpload ID="fuBasicInfoImage" runat="server" 
                                                meta:resourceKey="fuBasicInfoImageResource1" />
                                <asp:RegularExpressionValidator ID="FileUpLoadValidator2" runat="server" 
                                                ControlToValidate="fuBasicInfoImage" 
                                                ErrorMessage="僅能選擇JPG,PNG及GIF圖檔" 
                                                meta:resourceKey="FileUpLoadValidator2Resource1" 
                                                
                                                
                                    
                                    ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpeg|.JPEG|.jpg|.JPG|.gif|.GIF|.png|.PNG)$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style6" width="30%">
                                <table class="style13" width="100%">
                                    <tr>
                                        <td class="style10" width="40%">
                                            基地位置圖</td>
                                        <td>
                                            <asp:Image ID="img_loc_map" runat="server" Height="100px" 
                                                            meta:resourceKey="img_loc_mapResource1" Width="100%" 
                                                CssClass="style17" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="style6" width="70%">
                                <asp:FileUpload ID="fuLocInfoImage" runat="server" 
                                                meta:resourceKey="fuLocInfoImageResource1" />
                                <asp:RegularExpressionValidator ID="FileUpLoadValidator3" runat="server" 
                                                ControlToValidate="fuLocInfoImage" 
                                                ErrorMessage="僅能選擇JPG,PNG及GIF圖檔" 
                                                meta:resourceKey="FileUpLoadValidator3Resource1" 
                                                
                                                
                                    
                                    ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpeg|.JPEG|.jpg|.JPG|.gif|.GIF|.png|.PNG)$" 
                                    CssClass="style17"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style11" width="30%">
                                &nbsp;</td>
                            <td width="70%">
                                <asp:Button ID="btn_update" runat="server" 
                                    meta:resourceKey="btn_updateResource1" OnClick="btn_update_Click" 
                                    Text="儲存變更" />
                                <asp:Button ID="btn_submit" runat="server" 
                                    meta:resourceKey="btn_submitResource1" OnClick="btn_submit_Click" 
                                    Text="送出" />
                                <asp:Button ID="btn_back_to_list" runat="server" CausesValidation="False"
                                    OnClick="btn_back_to_list_Click" Text="回上一頁" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:TabPanel>
            <asp:TabPanel runat="server" HeaderText="Environment Introduce" 
                ID="tp_env_info" meta:resourceKey="tp_env_info_resource">
                <HeaderTemplate>
                    環境設施
                </HeaderTemplate>
                <ContentTemplate>
                <div>
                 
                    <asp:GridView ID="gv_env_info" runat="server" AutoGenerateColumns="False" 
                                    meta:resourcekey="GridView1Resource1" 
                                    onrowcancelingedit="gv_env_info_RowCancelingEdit" 
                                    onrowediting="gv_env_info_RowEditing" 
                        onrowupdating="gv_env_info_RowUpdating" Width="100%" DataKeyNames="EnvIntroID" 
                                    onrowcommand="gv_env_info_RowCommand" ShowFooter="True" 
                        CellPadding="4" onrowdeleting="gv_env_info_RowDeleting" 
                                    style="margin-top: 0px" BackColor="White" 
                        BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" 
                        EmptyDataText="No result found!">
                        <Columns>
                            <asp:TemplateField HeaderText="Name" SortExpression="EnvName">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txt_new_env_name" runat="server" Text='<%# Bind("EnvName") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txt_new_env_name" runat="server" Width="100%"></asp:TextBox>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("EnvName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Image">
                                <EditItemTemplate>
                                    <asp:Image ID="image" runat="server" Height="130px" 
                                                    ImageUrl='<%# Eval("EnvImage") %>' Width="280px" />
                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <table class="style14">
                                        <tr>
                                            <td width="30%">
                                                <asp:FileUpload ID="fu_new_env_img" runat="server" style="margin-left: 0px" 
                                                    Width="100%" />
                                            </td>
                                        </tr>
                                    </table>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:Image ID="image" runat="server" Height="130px" 
                                                    ImageUrl='<%# Eval("EnvImage") %>' Width="280px" />
                                </ItemTemplate>
                                <ItemStyle Width="40%" />
                            </asp:TemplateField>
                            <asp:TemplateField AccessibleHeaderText="Action">
                                <EditItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" 
                                                    CommandName="Update" Text="儲存變更"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                                                    CommandName="Cancel" Text="取消"></asp:LinkButton>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:Button ID="btn_add_env_info" runat="server" CommandName="insert" 
                                                    Text="Add New" />
                                </FooterTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" 
                                                    CommandName="Edit" Text="快速編輯"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="False" 
                                                    CommandName="Delete" Text="刪除" 
                                        onclientclick="return confirm('Do you want to delete?')"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="10%" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <table class="style14">
                                <tr class="style16">
                                    <td class="style15" width="50%">
                                        Name</td>
                                    <td class="style15" width="40%">
                                        Image</td>
                                    <td class="style15">
                                        Action</td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <asp:TextBox ID="txt_new_env_name" runat="server" Width="100%"></asp:TextBox>
                                    </td>
                                    <td width="40%">
                                        <asp:FileUpload ID="fu_new_env_img" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btn_add_env_info" runat="server" Text="Add New" 
                                            CommandName="insert from empty" />
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" ForeColor="#003399" />
                        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <SortedAscendingCellStyle BackColor="#EDF6F6" />
                        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                        <SortedDescendingCellStyle BackColor="#D6DFDF" />
                        <SortedDescendingHeaderStyle BackColor="#002876" />
                    </asp:GridView>
                  
                </div> 
                </ContentTemplate>
            </asp:TabPanel>
        </asp:TabContainer>
     
    </div>
    </form>
</body>
</html>
