﻿<%@ page language="C#" autoeventwireup="true" inherits="admin_Update_Cons_Image, App_Web_m2l3g225" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register src="../usControls/Adminusername.ascx" tagname="Adminusername" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .div
        {
            width:80%;
            vertical-align:middle;
            margin:0 auto;
        }
        .style14
        {
            width: 100%;
        }
        .auto-style1
        {
            color: #FFFFFF;
            background-color: #3333FF;
        }
        .auto-style2
        {
            background-color: #66CCFF;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="div">
        <uc1:Adminusername ID="Adminusername1" runat="server" />
        <div>
            <asp:Label ID="lbl_info" runat="server"></asp:Label>
            <asp:Button runat="server" CausesValidation="False" Text="確認" 
                ID="btn_back_to_list" OnClick="btn_back_to_list_Click"></asp:Button>
            <asp:GridView runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id" EmptyDataText="No result found!" ShowFooter="True" BackColor="White" 
                BorderColor="#3366CC" BorderWidth="1px" BorderStyle="None" Width="100%" ID="gv_cons_progress_img" style="margin-top: 0px" meta:resourceKey="GridView1Resource1" OnRowCancelingEdit="gv_cons_progress_img_RowCancelingEdit" OnRowCommand="gv_cons_progress_img_RowCommand" OnRowEditing="gv_cons_progress_img_RowEditing" OnRowUpdating="gv_cons_progress_img_RowUpdating" OnRowDeleting="gv_cons_progress_img_RowDeleting" >
                <Columns>
                    <asp:TemplateField HeaderText="Image">
                        <EditItemTemplate>
                            <div style="text-align:center";">
                                <asp:Image ID="image" runat="server" Height="215" 
                                                        ImageUrl='<%# Eval("img") %>' Width="430" />
                                <asp:FileUpload ID="FileUpload1" runat="server" />
                            </div>
                        </EditItemTemplate>
                        <FooterTemplate>
                                <asp:FileUpload ID="fu_img" runat="server" style="margin-left: 0px" 
                                            Width="100%" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <div style="text-align:center";">
                            <asp:Image ID="image0" runat="server" Height="215" 
                                                    ImageUrl='<%# Eval("img") %>' Width="430" />
                            </div>
                        </ItemTemplate>
                        <ItemStyle Width="40%"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField AccessibleHeaderText="Action">
                        <EditItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" 
                                                    CommandName="Update" Text="儲存變更"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                                                    CommandName="Cancel" Text="取消"></asp:LinkButton>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="btn_add_env_info" runat="server" CommandName="insert" 
                                                    Text="新增" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" 
                                                    CommandName="Edit" Text="快速編輯"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="False" 
                                                    CommandName="Delete" Text="刪除" 
                                        onclientclick="return confirm('Do you want to delete?')"></asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle Width="10%"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    <table class="style14">
                        <tr class="style16">
                            <td class="auto-style1" width="70%">Image</td>
                            <td class="auto-style1">Action</td>
                        </tr>
                        <tr>
                            <td width="70%" class="auto-style2">
                                <asp:FileUpload ID="fu_img" runat="server" />
                            </td>
                            <td class="auto-style2">
                                <asp:Button ID="btn_add_img" runat="server" Text="新增" 
                                            CommandName="insert from empty" />
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <FooterStyle BackColor="#99CCCC" ForeColor="#003399"></FooterStyle>
                <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF"></HeaderStyle>
                <PagerStyle HorizontalAlign="Left" BackColor="#99CCCC" ForeColor="#003399"></PagerStyle>
                <RowStyle BackColor="White" ForeColor="#003399"></RowStyle>
                <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99"></SelectedRowStyle>
                <SortedAscendingCellStyle BackColor="#EDF6F6">
                </SortedAscendingCellStyle>
                <SortedAscendingHeaderStyle BackColor="#0D4AC4">
                </SortedAscendingHeaderStyle>
                <SortedDescendingCellStyle BackColor="#D6DFDF">
                </SortedDescendingCellStyle>
                <SortedDescendingHeaderStyle BackColor="#002876">
                </SortedDescendingHeaderStyle>
            </asp:GridView>

        </div>
    </div>
    </form>
</body>
</html>
