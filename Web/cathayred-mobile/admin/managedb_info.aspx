﻿<%@ page language="C#" autoeventwireup="true" inherits="admin_managedb_info, App_Web_m2l3g225" clientidmode="Static" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   
    <link href="../css/managedb_info.css" rel="stylesheet" type="text/css" />

</head>
<body>
  
    <form id="form1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div style=" margin:0px auto; width:830px;">
    <table>
      <tr>
                    <td class="th1">
                        ID
                    </td>
                    <td class="td1">
                        <asp:Label ID="lblid" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="th1">
                        身份證字號
                    </td>
                    <td class="td1">
                        <asp:TextBox ID="txtuserid" runat="server" CssClass="full"
                            Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="th1">
                        密碼
                    </td>
                    <td class="td1">
                        <asp:TextBox ID="txtpassword" runat="server"
                            CssClass="full" />
                    </td>
                </tr>
                <tr>
                    <td class="th1">
                      生日
                    </td>
                    <td class="td1">
                        <asp:TextBox ID="txtbirthday" runat="server" CssClass="full" />
                    </td>
                </tr>
                   <tr>
                    <td class="th1">
                      電話
                    </td>
                    <td class="td1">
                        <asp:TextBox ID="txtphone" runat="server" CssClass="full" />
                    </td>
                </tr>
                <tr>
                    <td class="th1">
                        停用
                    </td>
                    <td class="td1">
                        <asp:CheckBox ID="chkdisable" runat="server" CssClass="full" />
                    </td>
                </tr>
                   <tr>
                                <td colspan="3">
                                    
                                    <div style="margin-top: 20px; padding-left:200px; height:50px; font-family:微軟正黑體 !important; font-size:14px;">
                                        <asp:Button style=" float:left; " ID="btnUpdate" runat="server" Text="資料更新" 
                                            CssClass="btn btn-red" onclick="btnUpdate_Click" 
                                            />
                                    
                                    </div>
                                </td>
                            </tr>
    </table>
    </div>
    </form>
</body>
</html>
