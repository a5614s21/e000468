﻿<%@ page language="C#" autoeventwireup="true" inherits="Login, App_Web_m2l3g225" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        *{margin:0;padding:0;}
        body
        {
          background-image:url("../images/dark_bg.png");
        }
        .wrapper
        {
          
            text-align:center;
            position:relative;
            min-height:678px;
            width:100%;
            top:200px;
        }
        .login-index
        {
            width:300px;
            vertical-align:middle;
            margin:0 auto;
            text-align:center;
        }
        .title-index
        {
            color:White;
            width:auto;
            vertical-align:middle;
        }
        .btn
        {
        }
        .input
        {
            border: medium none;
            border-radius: 4px 4px 4px 4px;
            background-image: none;
            display: block;
            font-size: 16px;
            line-height: 1.4;
            padding: 0.4em;
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.9) inset;
        }
        .style1
        {
            width: auto;
            text-align:center;
            vertical-align: middle;
            height: 18px;
            color:White;
            font-weight: bold;
            font-size: large;
        }
        .style2
        {
            width: 69px;
            vertical-align: middle;
            height: 18px;
            color: White;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
<div class="wrapper">
    <div class="login-index">
        <table>
            <tbody>
                <tr>
                <td class="style1">
                    <p>
                        國泰建設手機版官網上稿管理系統</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <table>
			<tbody>
                <tr>
                    <td class="style2">帳號</td>
                    <td><asp:TextBox ID="txt_id" CssClass="input" runat="server" Width="210px"></asp:TextBox></td>
                </tr>

				<tr>
                    <td class="style2">密碼</td>
                    <td><asp:TextBox ID="txt_pass" CssClass="input" runat="server" TextMode="Password" 
                            Width="210px"></asp:TextBox></td>
                </tr>

				<tr>
                <td colspan="2">
                <asp:Button ID="btn_login" CssClass="btn" runat="server" Text="登入" 
                        onclick="btn_login_Click" Width="300px" Height="40px" /></td>
                </tr>
			</tbody>
        </table>
    </div>
</div>
    </form>
</body>
</html>
