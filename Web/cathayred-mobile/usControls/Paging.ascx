﻿<%@ control language="C#" autoeventwireup="true" inherits="usercontrol_Paging, App_Web_ugysufyk" %>

<%--<div class="page_div_top">
    <font class="page"><span>Trang:</span> </font>&nbsp;
    <font class="page_current" title="Trang 1">[1]</font>&nbsp;&nbsp;
    <a class="page" href="" title="Trang 2">2</a>&nbsp;&nbsp;
    <a class="page"href="" title="Trang 7">..</a>&nbsp;&nbsp;
    <a class="page" href="" title="Trang sau"><img src="listudv.php_files/page_next.gif"></a>&nbsp;&nbsp;
    <a class="page" href="" title="Trang cuối"><img src="listudv.php_files/page_last.gif"></a>        
</div>
--%>

<style type="text/css">
a.pagorconstructor {
	height:20px;
	padding-left:4px;
	padding-right:4px;
	display:block;
	float:left;
	border:1px solid #CCC;
	color: #444;
	margin-right:4px;
	text-decoration:none;
	text-align:center;
}

a.pagorconstructor:hover {
	background:#ADDADC;
	border:1px solid #33A8AD;
	color:white;
}

a.selected {
	background:#A3E9EC;
	border:1px solid #6DCED2;
}
</style>
<%--<script src="../Scripts/page.js" type="text/javascript"></script>--%>
<div id='pagination' page_count='<%=m_nTotalPage_constructor%>'>    
</div>

<script type="text/javascript" language="javascript">
    var hideButtonActionCtrl = '<%=hideButtonActionCtrl%>';
    var hideFieldCurrentPage = '<%=hideFieldCurrentPage%>';

     $(document).ready(function() {
        initPage();
    }); 
</script>

