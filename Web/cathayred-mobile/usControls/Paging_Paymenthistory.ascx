﻿<%@ control language="C#" autoeventwireup="true" inherits="usControls_Paging_Paymenthistory, App_Web_ugysufyk" %>
<style type="text/css">
a.pagorPaymentHistory {
	height:20px;
	padding-left:4px;
	padding-right:4px;
	display:block;
	float:left;
	border:1px solid #CCC;
	color: #444;
	margin-right:4px;
	text-decoration:none;
	text-align:center;
}

a.pagorPaymentHistory:hover {
	background:#ADDADC;
	border:1px solid #33A8AD;
	color:white;
}

a.selected {
	background:#A3E9EC;
	border:1px solid #6DCED2;
}
</style>
<div id='pagination_PaymentHistory' page_count_PaymentHistory='<%=m_nTotalPage_PaymentHistory%>'>    
</div>
<%--<script src="../Scripts/Paging_paymenthistory.js" type="text/javascript"></script>
--%><script type="text/javascript" language="javascript">
    var hideButtonActionCtrl_PaymentHistory = '<%=hideButtonActionCtrl_PaymentHistory%>';
    var hideFieldCurrentPage_PaymentHistory = '<%=hideFieldCurrentPage_PaymentHistory%>';

    $(document).ready(function () {
        initPage_PaymentHistory();
    }); 
</script>