﻿<%@ control language="C#" autoeventwireup="true" inherits="usControls_Paging_customer, App_Web_ugysufyk" %>
<style type="text/css">
a.pagorcustomer {
	height:20px;
	padding-left:4px;
	padding-right:4px;
	display:block;
	float:left;
	border:1px solid #CCC;
	color: #444;
	margin-right:4px;
	text-decoration:none;
	text-align:center;
}

a.pagorcustomer:hover {
	background:#ADDADC;
	border:1px solid #33A8AD;
	color:white;
}

a.selected {
	background:#A3E9EC;
	border:1px solid #6DCED2;
}
</style>
<div id='pagination_customer' page_count_customer='<%=m_nTotalPage%>'>    
</div>
<%--<script src="../Scripts/page_customer.js" type="text/javascript"></script>--%>
<script type="text/javascript" language="javascript">
    var hideButtonActionCtrl_customer = '<%=hideButtonActionCtrl_customer%>';
    var hideFieldCurrentPage_customer = '<%=hideFieldCurrentPage_customer%>';

    $(document).ready(function () {
        initPage_customer();
    }); 
</script>