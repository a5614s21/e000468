﻿<%@ control language="C#" autoeventwireup="true" inherits="usControls_Paging_clientaccount, App_Web_ugysufyk" %>
<style type="text/css">
a.pagorclientaccount {
	height:20px;
	padding-left:4px;
	padding-right:4px;
	display:block;
	float:left;
	border:1px solid #CCC;
	color: #444;
	margin-right:4px;
	text-decoration:none;
	text-align:center;
}

a.pagorclientaccount:hover {
	background:#ADDADC;
	border:1px solid #33A8AD;
	color:white;
}

a.selected {
	background:#A3E9EC;
	border:1px solid #6DCED2;
}
</style>
<div id='pagination_clientaccount' page_count_clientaccount='<%=m_nTotalPage_clientaccount%>'>    
</div>
<script type="text/javascript" language="javascript">
    var hideButtonActionCtrl_clientaccount = '<%=hideButtonActionCtrl_clientaccount%>';
    var hideFieldCurrentPage_clientaccount = '<%=hideFieldCurrentPage_clientaccount%>';

    $(document).ready(function () {
        initPage_clientaccount();
    });
</script>