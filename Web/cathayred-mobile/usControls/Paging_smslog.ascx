﻿<%@ control language="C#" autoeventwireup="true" inherits="usControls_Paging_smslog, App_Web_ugysufyk" %>
<style type="text/css">
a.pagorsmslog {
	height:20px;
	padding-left:4px;
	padding-right:4px;
	display:block;
	float:left;
	border:1px solid #CCC;
	color: #444;
	margin-right:4px;
	text-decoration:none;
	text-align:center;
}

a.pagorsmslog:hover {
	background:#ADDADC;
	border:1px solid #33A8AD;
	color:white;
}

a.selected {
	background:#A3E9EC;
	border:1px solid #6DCED2;
}
</style>
<%--<script src="../Scripts/page_smslog.js" type="text/javascript"></script>--%>
<div id='pagination_smslog' page_count_smslog='<%=m_nTotalPage_smslog%>'>    
</div>

<script type="text/javascript" language="javascript">
    var hideButtonActionCtrl_smslog = '<%=hideButtonActionCtrl_smslog%>';
    var hideFieldCurrentPage_smslog = '<%=hideFieldCurrentPage_smslog%>';

    $(document).ready(function () {
        initPage_smslog();
    }); 
</script>