﻿<%@ control language="C#" autoeventwireup="true" inherits="usControls_Paging_conprogress, App_Web_ugysufyk" %>
<style type="text/css">
a.pagorconprogress {
	height:20px;
	padding-left:4px;
	padding-right:4px;
	display:block;
	float:left;
	border:1px solid #CCC;
	color: #444;
	margin-right:4px;
	text-decoration:none;
	text-align:center;
}

a.pagorconprogress:hover {
	background:#ADDADC;
	border:1px solid #33A8AD;
	color:white;
}

a.selected {
	background:#A3E9EC;
	border:1px solid #6DCED2;
}
</style>
<div id='pagination_conprogress' page_count_conprogress='<%=m_nTotalPage_conprogress%>'>    
</div>
<script type="text/javascript" language="javascript">
    var hideButtonActionCtrl_conprogress = '<%=hideButtonActionCtrl_conprogress%>';
    var hideFieldCurrentPage_conprogress = '<%=hideFieldCurrentPage_conprogress%>';

    $(document).ready(function () {
        initPage_conprogress();
    });
</script>