﻿<%@ control language="C#" autoeventwireup="true" inherits="usControls_Paging_Account, App_Web_ugysufyk" %>
<style type="text/css">
a.pagor {
	height:20px;
	padding-left:4px;
	padding-right:4px;
	display:block;
	float:left;
	border:1px solid #CCC;
	color: #444;
	margin-right:4px;
	text-decoration:none;
	text-align:center;
}

a.pagor:hover {
	background:#ADDADC;
	border:1px solid #33A8AD;
	color:white;
}

a.selected {
	background:#A3E9EC;
	border:1px solid #6DCED2;
}
</style>
<div id='pagination_Account' page_count_Account='<%=m_nTotalPage%>'>    
</div>
<%--<script src="../Scripts/Paging_Account.js" type="text/javascript"></script>
--%><script type="text/javascript" language="javascript">
    var hideButtonActionCtrl_Account = '<%=hideButtonActionCtrl_Account%>';
    var hideFieldCurrentPage_Account = '<%=hideFieldCurrentPage_Account%>';

    $(document).ready(function () {
        initPage_Account();
    }); 
</script>