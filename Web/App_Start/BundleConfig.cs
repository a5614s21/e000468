﻿using System.Web;
using System.Web.Optimization;

namespace Web
{
    public class BundleConfig
    {
        // 如需統合的詳細資訊，請瀏覽 https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // 使用開發版本的 Modernizr 進行開發並學習。然後，當您
            // 準備好可進行生產時，請使用 https://modernizr.com 的建置工具，只挑選您需要的測試。
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));


            bundles.Add(new StyleBundle("~/Content/styless").Include(
                     "~/Styles/style.css",
                     "~/Styles/sweetalert.css"));


            bundles.Add(new ScriptBundle("~/Scripts/js").Include(
                 "~/Scripts/default/jquery.min.js",
                 "~/Scripts/default/env.js",
                 "~/Scripts/plugins/bootstrap.js",
                 "~/Scripts/plugins/actual.js",
                 "~/Scripts/plugins/imgLiquid.js",
                 "~/Scripts/plugins/slick.js",
                 "~/Scripts/plugins/textillate.js",
                 "~/Scripts/plugins/animateNumber.js",
                 "~/Scripts/plugins/parallax.js",
                 "~/Scripts/plugins/mCustomScrollbar.js",
                 "~/Scripts/plugins/lightcase/js/lightcase.js",
                 "~/Scripts/plugins/twzipcode.js",
                 "~/Scripts/default/ui.js",
                 "~/Scripts/sweetalert.min.js",
                 "~/Scripts/jquery.blockUI.js",
                 "~/Scripts/sweetalert.min.js",
                 "~/Content/Design/home.js"));

            /*   bundles.Add(new StyleBundle("~/Content/css").Include(
                         "~/Styles/style.css",
                         "~/Styles/sweetalert.css"));


               bundles.Add(new ScriptBundle("~/Scripts/plugins").Include(
                    "~/Scripts/default/jquery.min.js",
                    "~/Scripts/default/env.js",
                    "~/Scripts/plugins/bootstrap.js",
                    "~/Scripts/plugins/actual.js",
                    "~/Scripts/plugins/imgLiquid.js",
                    "~/Scripts/plugins/slick.js",
                    "~/Scripts/plugins/textillate.js",
                    "~/Scripts/plugins/animateNumber.js",
                    "~/Scripts/plugins/parallax.js",
                    "~/Scripts/plugins/mCustomScrollbar.js",
                    "~/Scripts/plugins/twzipcode.js",
                    "~/Scripts/default/ui.js",
                    "~/Scripts/sweetalert.min.js",
                    "~/Scripts/jquery.blockUI.js",
                    "~/Scripts/sweetalert.min.js",
                    "~/Content/Design/home.js"));*/
        }
    }
}
