namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class finance_report
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }

        public string year { get; set; }
        public int month { get; set; }

        public string files { get; set; }

        public int? sortIndex { get; set; }

        [StringLength(1)]
        public string status { get; set; }
        public string types { get; set; }


        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }    

        [StringLength(30)]
        public string lang { get; set; }
        [StringLength(30)]
        public string verify { get; set; }

        public string verify_info { get; set; }

    }
}
