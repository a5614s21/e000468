﻿namespace Web.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class Model : DbContext
    {
        // 您的內容已設定為使用應用程式組態檔 (App.config 或 Web.config)
        // 中的 'Model' 連接字串。根據預設，這個連接字串的目標是
        // 您的 LocalDb 執行個體上的 'Web.Models.Model' 資料庫。
        //
        // 如果您的目標是其他資料庫和 (或) 提供者，請修改
        // 應用程式組態檔中的 'Model' 連接字串。
        public Model()
            : base("name=Model")
        {
        }

        public virtual DbSet<ashcan> ashcan { get; set; }
        public virtual DbSet<contacts> contacts { get; set; }
        public virtual DbSet<google_analytics> google_analytics { get; set; }
        public virtual DbSet<language> language { get; set; }
        public virtual DbSet<news> news { get; set; }
        public virtual DbSet<news_category> news_category { get; set; }
        public virtual DbSet<role_permissions> role_permissions { get; set; }
        public virtual DbSet<roles> roles { get; set; }
        public virtual DbSet<smtp_data> smtp_data { get; set; }
        public virtual DbSet<system_data> system_data { get; set; }
        public virtual DbSet<system_menu> system_menu { get; set; }
        public virtual DbSet<user> user { get; set; }
        public virtual DbSet<user_role> user_role { get; set; }
        public virtual DbSet<web_data> web_data { get; set; }

        public virtual DbSet<taiwan_city> taiwan_city { get; set; }

        public virtual DbSet<mail_contents> mail_contents { get; set; }
        public virtual DbSet<firewalls> firewalls { get; set; }

        public virtual DbSet<cases> cases { get; set; }

        public virtual DbSet<cases_category> cases_category { get; set; }

        public virtual DbSet<cases_classic> cases_classic { get; set; }

        public virtual DbSet<annuals> annuals { get; set; }
        public virtual DbSet<system_log> system_log { get; set; }
        public virtual DbSet<charity_category> charity_category { get; set; }
        public virtual DbSet<charitys> charitys { get; set; }

        public virtual DbSet<awards> awards { get; set; }

        public virtual DbSet<abouts> abouts { get; set; }

        public virtual DbSet<about_manages> about_manages { get; set; }
        public virtual DbSet<regulations> regulations { get; set; }
        public virtual DbSet<resolutions> resolutions { get; set; }

        public virtual DbSet<about_houses> about_houses { get; set; }

        public virtual DbSet<community_category> community_category { get; set; }

        public virtual DbSet<communitys> communitys { get; set; }

        public virtual DbSet<knowledges> knowledges { get; set; }

        public virtual DbSet<urban_news> urban_news { get; set; }

        public virtual DbSet<urban_knowledge> urban_knowledge { get; set; }

        public virtual DbSet<urban_faq> urban_faq { get; set; }
        public virtual DbSet<urban_column> urban_column { get; set; }
        public virtual DbSet<urban_column_category> urban_column_category { get; set; }

        public virtual DbSet<investor_info> investor_info { get; set; }

        public virtual DbSet<finance_report> finance_report { get; set; }

        public virtual DbSet<shareholders> shareholders { get; set; }

        public virtual DbSet<share_download> share_download { get; set; }
        public virtual DbSet<share_faq> share_faq { get; set; }

        public virtual DbSet<notes_data> notes_data { get; set; }

        public virtual DbSet<contact_info> contact_info { get; set; }
        public virtual DbSet<urban_column_files> urban_column_files { get; set; }
        public virtual DbSet<member_faq> member_faq { get; set; }

        public virtual DbSet<cases_progress> cases_progress { get; set; }
        public virtual DbSet<form_subject> form_subject { get; set; }

        public virtual DbSet<urban_contacts> urban_contacts { get; set; }

        public virtual DbSet<banners> banners { get; set; }
        public virtual DbSet<index_info> index_info { get; set; }
        public virtual DbSet<cases_reservation> cases_reservation { get; set; }

        public virtual DbSet<about_investment> about_investment { get; set; }

        public virtual DbSet<constructions> constructions { get; set; }

        public virtual DbSet<member_log> member_log { get; set; }

        public virtual DbSet<payment_apilog> payment_apilog { get; set; }

        public virtual DbSet<project_progress> project_progress { get; set; }
        public virtual DbSet<manage_safety> manage_safety { get; set; }
        public virtual DbSet<auditcommittee> auditcommittee { get; set; }
        public virtual DbSet<remuneration> remuneration { get; set; }

        //public virtual DbSet<pay_record> pay_record { get; set; }

        //public virtual DbSet<cus_problem> cus_problem { get; set; }

        //public virtual DbSet<online_building> online_building { get; set; }



        // 針對您要包含在模型中的每種實體類型新增 DbSet。如需有關設定和使用
        // Code First 模型的詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=390109。
    }
}