namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class about_houses
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }

        public string year { get; set; }

        public string news_file1 { get; set; }
        public string news_file2 { get; set; }
        public string news_file3 { get; set; }
        public string news_file4 { get; set; }

        public string season_file1 { get; set; }
        public string season_file2 { get; set; }
        public string season_file3 { get; set; }
        public string season_file4 { get; set; }

        public string detect_file1 { get; set; }
        public string detect_file2 { get; set; }
        public string detect_file3 { get; set; }
        public string detect_file4 { get; set; }

        public string files { get; set; }

        public int? sortIndex { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }    

        [StringLength(30)]
        public string lang { get; set; }
        [StringLength(30)]
        public string verify { get; set; }

        public string verify_info { get; set; }

        public string pic { get; set; }

        public string pic_alt { get; set; }
    }
}
