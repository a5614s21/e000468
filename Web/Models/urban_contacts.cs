namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class urban_contacts
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string name { get; set; }
        public string email { get; set; }

        public string tel { get; set; }
        public string fax { get; set; }

        public string house_status { get; set; }
        public string address { get; set; }
        public string residential { get; set; }
        public string business { get; set; }

        public string land_number { get; set; }
        public string area { get; set; }

        public string subject { get; set; }
     
        [StringLength(1)]
        public string status { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }


        [Column(TypeName = "ntext")]
        public string content { get; set; }

        public DateTime? re_date { get; set; }
        [Column(TypeName = "ntext")]
        public string re_content { get; set; }

        [StringLength(1)]
        public string re_mail { get; set; }
    }
}
