namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class web_data
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        [StringLength(200)]
        public string title { get; set; }

        [Column(TypeName = "text")]
        public string url { get; set; }

        [StringLength(255)]
        public string phone { get; set; }

        [StringLength(255)]
        public string fax { get; set; }

        [StringLength(255)]
        public string servicemail { get; set; }

        public string ContactEmail { get; set; }
        public string UrbanContactEmail { get; set; }
        public string CaseEmail { get; set; }

        [StringLength(255)]
        public string ext_num { get; set; }

        [StringLength(255)]
        public string address { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }

        [StringLength(30)]
        public string lang { get; set; }

        public string seo_keywords { get; set; }

        public string seo_description { get; set; }

        public string contact_info { get; set; }

        public string global_url { get; set; }

        public string mail_app_url { get; set; }

        public string facebook_url { get; set; }

        public int? token_timeout { get; set; }

        public string token_url { get; set; }
    }
}