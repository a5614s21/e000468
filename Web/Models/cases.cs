namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cases
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }

        public string sub_title { get; set; }

        public string year { get; set; }


        [StringLength(64)]
        public string category { get; set; }

        public string notes { get; set; }

        public string content { get; set; }

        public string phone { get; set; }

        public string address { get; set; }
        public string url { get; set; }
        public DateTime? modifydate { get; set; }

   

        [StringLength(1)]
        public string status { get; set; }

        public string list_pic { get; set; }

        public string list_pic_alt { get; set; }

        public string pic { get; set; }

        public string pic_alt { get; set; }

        public DateTime? create_date { get; set; }

        [StringLength(30)]
        public string lang { get; set; }

        public string seo_keywords { get; set; }

        public string seo_description { get; set; }

        public int sortIndex { get; set; }

        [StringLength(1)]
        public string tops { get; set; }

        [StringLength(1)]
        public string header { get; set; }
        public string header_pic { get; set; }
        public string header_pic_alt { get; set; }

        public string map_link { get; set; }

        public string floor { get; set; }
        public string pings { get; set; }

        public string index_pic { get; set; }
        public string index_pic_alt { get; set; }
       
        public string classic { get; set; }

        public DateTime? start_date { get; set; }
        public DateTime? end_date { get; set; }

        public string related { get; set; }//預留關聯
        [StringLength(1)]
        public string reservation  { get; set; }//預約賞屋

        public string hmmcu { get; set; }//建案(工地)代號

        public string files { get; set; } //型錄

        public string get_mail { get; set; } //接受賞屋信件

        [StringLength(30)]
        public string verify { get; set; }

        public string verify_info { get; set; }

    }
}
