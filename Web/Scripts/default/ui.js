$(function () {

  'use strict';

  /*=============================================
    variables
   =============================================*/

  var $window = $(window);

  var $header = $('#header'),
      $footer = $('#footer');

  var svgChevronLeft = '<i class="icon icon-chevron-left"></i>';
  var svgChevronRight = '<i class="icon icon-chevron-right"></i>';

  /*=============================================
    init
   =============================================*/

  /*-- basic -----------------*/

  // draggable false
  $('img').on('dragstart', function (e) {
    e.preventDefault();
  });

  // select input text
  $(document).on('click', 'input[type="text"]', function () { this.select(); });

  /*-- plugins ---------------*/

  // imgLiquid
  $('.imgFill').imgLiquid();

  // selectpicker
  $('.selectpicker').selectpicker({
    noneSelectedText: '',
    dropupAuto: false,
  });

  // scrollbar
	$('.scrollbar').mCustomScrollbar({
		axis: 'y',
    theme: 'dark-thin',
	});

  // twzipcode
  $('#twzipcode').twzipcode({
    onCountySelect: function() {
      $('#twzipcode .selectpicker').selectpicker('refresh');
      $('#twzipcode .bootstrap-select').addClass('changed');
    }
  });

  // wow no-plugin
  $('.wow').each(function () {
    var $this = $(this),
      animateClass = $this.attr('class').split('wow')[1],
      duration = $this.data('wow-duration'),
      delay = $this.data('wow-delay');
    $this.removeClass(animateClass).css({
      'animationDuration': duration,
      'animationDelay': delay,
    });
    function customWow() {
      var windowTop = $window.scrollTop() + $window.height(),
        thisTop = $this.offset().top;
      if (windowTop >= thisTop) {
        $this.css('visibility', 'visible').addClass('animated').addClass(animateClass);
      }
    }
    $window.on('load', function () {
      if ($('.wp').find('.preloader').length > 0) {
        setTimeout(function () {
          customWow();
        }, 5000);
      } else {
        customWow();
      }
    });
    $window.on('scroll', function () {
      customWow();
    });
  });

  /*=============================================
    header
   =============================================*/

  // header on load
  $window.on('load', function() {
    setTimeout(function() {
      $header.removeClass('loading');
    }, 500);
  }).trigger('load');

  // header on scroll
  $window.on('load scroll', function () {
    var windowTop = $window.scrollTop();
    $header.removeClass('opened');
    if (windowTop > 0) {
      $header.addClass('scroll');
    } else {
      $header.removeClass('scroll');
    }
  }).trigger('scroll');

  $header.each(function () {

    /*-- desktop menu ----------*/

    // header toggle
    $header.find('.header-toggle').click(function () {
      $header.addClass('opened');
    });

    // submenu
    $header.find('.ml-tt').each(function() {
      var $this = $(this);
      if ( $this.siblings('.ml-sm').length ) {
        $this.children('.ml-tt-txt').on('mouseenter', function() {
          if ( window.innerWidth >= 1200 ) {
            $this.parent('li').addClass('active');
          }
        });
        $this.parent('li').on('mouseleave', function() {
          if ( window.innerWidth >= 1200 ) {
            $this.parent('li').removeClass('active');
            $this.parent('li').find('.sm-slider').children('ul').slick('slickCurrentSlide');
          }
        });
      }
    });

    // submenu slider
    $header.find('.ml-sm').each(function () {
      if ( $(this).find('.sm-slider').length ) {
        var $slider = $(this).find('.sm-slider').children('ul');
        var options =  {
          slidesToShow: 4,
          slidesToScroll: 1,
          arrows: true,
          dots: false,
          prevArrow: '<div class="slick-arrow slick-prev"><button type="button" class="btn btn-circle">' + svgChevronLeft + '</button></div>',
          nextArrow: '<div class="slick-arrow slick-next"><button type="button" class="btn btn-circle">' + svgChevronRight + '</button></div>',
          responsive: [
            {
              breakpoint: 1200,
              settings: 'unslick',
            }
          ]
        };
        $window.on('load resize', function() {
          if ( window.innerWidth >= 1200 && !$slider.hasClass('slick-initialized') ) {
            $slider.slick(options);
          }
        }).trigger('resize');
        $window.on('scroll', function() {
          sliderRefresh()
        });
        $header.find('.header-toggle').click(function() {
          sliderRefresh()
        });
        function sliderRefresh() {
          if ( window.innerWidth >= 1200 ) {
            $slider.slick('setPosition');
          }
        }
      }
    });

    /*-- mobile menu -----------*/

    // menu toggle
    $header.find('.menu-toggle').click(function() {
      $('html').toggleClass('body-fixed');
      $header.toggleClass('menu-opened');
      if ( !$header.hasClass('menu-opened') ) {
        $header.find('.ml-tt').removeClass('active');
        $header.find('.ml-sm').slideUp();
      }
    });

    // submenu
    $header.find('.ml-tt').each(function() {
      var $this = $(this);
      if ( $this.siblings('.ml-sm').length ) {
        $this.children('.ml-tt-txt').click(function(e) {
          if ( window.innerWidth < 1200 ) {
            $this.toggleClass('active');
            $this.siblings('.ml-sm').slideToggle();
            $this.parent('li').siblings('li').children('.ml-tt').removeClass('active');
            $this.parent('li').siblings('li').children('.ml-sm').slideUp();
            e.preventDefault();
          }
        });
      }
    });

  });

  /*=============================================
    footer
   =============================================*/

  $footer.each(function () {

    // scroll top
    $(this).find('.footer-scroll').children('button').click(function () {
      $('html, body').animate({ scrollTop: 0 }, 1200);
    });

  });

  /*=============================================
    pages
   =============================================*/

  /*== pages ========================== */

  $('.wp').each(function() {

    var $wp = $(this),
        pageId = $wp.attr('id'),
        pageData = $wp.data('page');

    // menu current
    $header.find('.mainlinks li[data-id="' + pageId + '"]').addClass('current');

    // hero nav
    $wp.find('.hero-nav').each(function() {
      var $this = $(this);
      $this.find('li[data-page="' + pageData + '"]').addClass('current');
      $this.find('.hero-nav-toggle').children('.btn').click(function() {
        $this.toggleClass('menu-opened');
        $this.find('.hero-nav-menu').slideToggle(500);
      });
    });

    // primary head
    var $phead = $wp.find('.primary-head');
    $window.on('load resize', function() {
      if ( window.innerWidth >= 1200 && !$phead.find('.line').length ) {
        $phead.each(function() {
          var breadWidth = $(this).find('.breadcrumb').outerWidth() + 20;
          $(this).find('.title').prepend('<span class="line left"></span>').append('<span class="line right"></span>');
          if ( !$phead.find('.dropmenu').length ) {
            $(this).find('.title').children('.left').css({
              'flex-basis': breadWidth,
            });
          } else {
            $(this).find('.title').children('.left').css({
              'flex-basis': breadWidth,
              'padding-left': 220,
            });
          }
          $(this).find('.title').children('.right').css({
            'padding-right': breadWidth,
          });
        });
      } else if ( window.innerWidth < 1200 && $phead.find('.line').length ) {
        $phead.find('.title').find('.line').remove();
      }
    }).trigger('resize');

    // primary head subnav
    $phead.find('.subnav').each(function() {
      var $this = $(this);
      $this.find('.subnav-toggle').children('.btn').click(function() {
        $this.toggleClass('menu-opened');
        $this.find('.subnav-menu').slideToggle(500);
      });
    });

    // responsive table
		$wp.find('.table-responsive').each(function() {
			var $this = $(this),
				  columns = $(this).find('th').length;
      $window.on('load resize', function() {
        if ( window.innerWidth < 768 ) {
          if ( !$this.find('.rowspan').length ) {
            $this.find('td[rowspan]').each(function() {
              var rowspan = parseFloat($(this).attr('rowspan')) - 1,
                  thisTxt = $(this).html(),
                  $nextAll = $(this).parent('tr').nextAll().slice(0, rowspan);
              $nextAll.prepend('<td class="rowspan">' + thisTxt + '</td>');
            });
          }
          if ( !$this.find('[data-title]').length ) {
            $this.find('tr').each(function() {
              for ( var i = 0; i < columns; i++ ) {
                var title = $this.find('thead').find('th').eq(i).text();
                $(this).children('td').eq(i).attr('data-title', title);
              }
            });
          }
        } else {
          if ( $this.find('.rowspan').length ) {
            $this.find('.rowspan').remove();
          }
          if ( $this.find('[data-title]').length ) {
            $this.find('[data-title]').each(function() {
              $(this).removeAttr('data-title');
            });
          }
        }
      }).trigger('resize');
    });

    // case slider
    $wp.find('.case-sliders').each(function() {
      var $slider = $(this).find('.slider-main').children('.slider'),
          $nav = $(this).find('.slider-thumb').children('.slider');
      $slider.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        prevArrow: '<div class="slick-arrow slick-prev"><button type="button" class="btn btn-circle">' + svgChevronLeft + '</button></div>',
        nextArrow: '<div class="slick-arrow slick-next"><button type="button" class="btn btn-circle">' + svgChevronRight + '</button></div>',
        asNavFor: $nav,
      });
      $nav.slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        asNavFor: $slider,
        focusOnSelect: true,
      });
    });

    // case popup
    $wp.find('#case-popup-toggle').click(function() {
      $wp.find('.case-popup').fadeIn();
      $('html').addClass('body-fixed');
    });
    $wp.find('.case-popup-close').children('.btn').click(function() {
      casePopUpClose();
    });
    $wp.find('.case-popup-fade').click(function() {
      casePopUpClose();
    });
    function casePopUpClose() {
      $wp.find('.case-popup').fadeOut();
      $('html').removeClass('body-fixed');
    }

    // selectpicker for form
    $('.form-style').find('.selectpicker').each(function() {
      var $this = $(this);
      $this.parents('.form-group').addClass('has-select').css('z-index', '1');
      $this.on('hidden.bs.select', function() {
        setTimeout(function() {
          if ( !$this.parents('.form-control').siblings('.form-control').find('.dropdown.show').length ) {
            $this.parents('.form-group').css('z-index', '1');
          }
        }, 500);
      });
      $this.on('show.bs.select', function() {
        $this.parents('.form').find('.form-group.has-select').css('z-index', '1');
        $this.parents('.form-group').css('z-index', '5');
      });
    });

    // faq list
    $wp.find('.faq-box').each(function() {
      var $this = $(this);
      $this.find('.faq-box-q').click(function() {
        $this.toggleClass('active').siblings('.faq-box').removeClass('active');
        $this.find('.faq-box-a').slideToggle();
        $this.siblings('.faq-box').find('.faq-box-a').slideUp();
      });
    });

  });

  /*== home =========================== */

  $('#home').each(function () {

    var $home = $(this);

    $window.on('load', function() {
      $('html, body').animate({ scrollTop: 0 }, 500);
    });

    /*-- banner ----------------*/

    var $hb = $home.find('.home-banner'),
        $hbSlider = $hb.find('.banner-bg').children('ul');

    // banner slider
    changeSlide(0);
    runSlider();
    function runSlider() {
      var total = $hbSlider.children('li').length;
      var i = 1;
      setInterval(function () {
        changeSlide(i);
        i++;
        if (i === total) i = 0;
      }, 7000);
    }
    function changeSlide(slideNum) {
      var slideNum = slideNum;
      activeSlide($hbSlider, slideNum);
    }
    function activeSlide(target, slideNum) {
      target.children('li').eq(slideNum).addClass('active').siblings('li').removeClass('active');
    }

    // mobile banner
    $window.on('load resize', function() {
      if ( window.innerWidth >= 992 && !$hb.find('.banner-inner.container').length ) {
        $($hb.find('.banner-bg').siblings()).wrapAll('<div class="banner-inner container"></div>');
        $hb.find('.hg').unwrap('.for-hg');
        $hb.find('.content').unwrap('.for-content');
      } else if ( window.innerWidth < 992 && $hb.find('.banner-inner.container').length ) {
        $hb.find('.banner-inner .content').unwrap();
        $hb.find('.hg').wrap('<div class="banner-inner for-hg"></div>');
        $hb.find('.content').wrap('<div class="banner-inner for-content"></div>');
      }
      if ( window.innerWidth < 480 ) {
        var h = window.innerHeight - 125;
        $hb.find('.banner-bg').css('height', h);
        $hb.find('.for-hg').css('height', h);
      } else {
        $hb.find('.banner-bg').removeAttr('style');
        $hb.find('.for-hg').removeAttr('style');
      }
    }).trigger('resize');

    var $hbInner = $hb.find('.banner-inner');

    // banner text
    $window.on('load', function() {
      if ( window.innerWidth >= 992 && !$hbInner.find('.hg').find('.title').find('.tlt').length ) {
        $hbInner.find('.hg').find('.title').each(function() {
          var $this = $(this);
          var text = $this.find('span.hidden').eq(0).text() + $this.find('span.hidden').eq(1).text();
          $this.append('<span class="tlt">' + text + '</span>');
        });
        bannerTextillate();
      } else if ( window.innerWidth < 992 ) {
        $hbInner.find('.hg').find('.title').each(function() {
          $(this).find('span.hidden').removeClass('hidden').addClass('tlt');
        });
        bannerTextillate();
      }
    }).trigger('load');

    function bannerTextillate() {
      $hbInner.find('.hg').find('.tlt').textillate({
        loop: false,
        in: {
          effect: 'flipInY',
          delay: 50,
        },
      });
      setTimeout(function() {
        $hbInner.find('.hg').find('.title .tlt').each(function() {
          $(this).css('opacity', '1');
          $(this).textillate('start');
        });
        setTimeout(function() {
          $hbInner.find('.hg').find('.subtitle .tlt').each(function() {
            $(this).css('opacity', '1');
            $(this).textillate('start');
          });
        }, 900);
      }, 250);
    }

    // banner run numbers
    $hbInner.find('.content').find('.num').each(function() {
      var $this = $(this),
          thisNum = $this.data('number');
      var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
      $window.on('load', function() {
        $('html').addClass('body-fixed');
        setTimeout(function() {
          $this.animateNumber({
            number: thisNum,
            numberStep: comma_separator_number_step
          }, 1500, function() {
            $('html').removeClass('body-fixed');
          });
        }, 1000);
      }).trigger('load');
    });

    /*-- news ------------------*/

    var $hn = $home.find('.home-news'),
      $hnSlider = $hn.find('.list').children('ul');

    // news slider
    $hnSlider.slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      vertical: true,
      //autoplay: true,
      //autoplaySpeed: 4000,
    });

    // news on resize
    $window.on('load resize', function() {
      if ( window.innerWidth >= 992 && $hn.find('.foot').length ) {
        var arrows = $hn.find('.foot').find('.arrows').html(),
            button  = $hn.find('.foot').find('.button').html();
        $hn.find('.head').append('<div class="arrows">' + arrows + '</div><div class="button">' + button + '</div>');
        $hn.find('.foot').remove();
      } else if ( window.innerWidth < 992 && !$hn.find('.foot').length ) {
        var arrows = $hn.find('.head').find('.arrows').html(),
            button  = $hn.find('.head').find('.button').html();
        $hn.children('.container').append('<div class="foot"><div class="arrows">' + arrows + '</div><div class="button">' + button + '</div></div>');
        $hn.find('.head').find('.arrows').remove();
        $hn.find('.head').find('.button').remove();
      }
      // news arrows
      $hn.find('.arrows').find('.arrow').each(function () {
        if ($(this).hasClass('arrow-prev')) {
          $(this).children('.btn').click(function () {
            $hnSlider.slick('slickPrev');
          });
        } else if ($(this).hasClass('arrow-next')) {
          $(this).children('.btn').click(function () {
            $hnSlider.slick('slickNext');
          });
        }
      });
    }).trigger('resize');

    /*-- cases -----------------*/

    var $hc = $home.find('.home-cases'),
        $hcSlider = $hc.find('.cases-slider').children('.slider'),
        $hcNav = $hc.find('.cases-nav');

    $hcSlider.each(function () {
      if ($(this).find('.case').length <= 2) {
        // slider padding
        $(this).parent('.cases-slider').css('padding', 0);
      } else {
        // slider navigate
        $(this).parent('.cases-slider').prepend('<ul class="ns navigate">' +
          '<li class="item prev" data-dir="prev">' +
          '<div class="pic pic-fill"></div>' +
          '<div class="inner"><div class="name"></div></div>' +
          '</li>' +
          '<li class="item next" data-dir="next">' +
          '<div class="pic pic-fill"></div>' +
          '<div class="inner"><div class="name"></div></div>' +
          '</li>' +
          '</ul>');
      }
    });

    // cases slider
    $hcSlider.on('init afterChange', function (slick) {
      var $current = $hcSlider.find('.slick-current'),
        $prev = $current.prev('.slick-slide'),
        $next = $current.next('.slick-slide');
      var $navigate = $hcSlider.siblings('.navigate');
      $navigate.find('.item').each(function () {
        var thisDir = $(this).data('dir');
        $(this).addClass('changed');
        itemContent(this, thisDir);
      });
      function itemContent(target, dir) {
        var bgImg, name;
        if (dir === 'prev') {
          bgImg = $prev.find('.case-pic').children('img').attr('src');
          name = $prev.find('.case-content').find('.title').text();
        } else if (dir === 'next') {
          bgImg = $next.find('.case-pic').children('img').attr('src');
          name = $next.find('.case-content').find('.title').text();
        }
        $(target).find('.pic').css('backgroundImage', 'url(' + bgImg + ')');
        $(target).find('.name').text(name);
      }
    });
    $hcSlider.on('beforeChange', function () {
      var $navigate = $hcSlider.siblings('.navigate');
      $navigate.find('.item').each(function () {
        $(this).removeClass('changed');
      });
    });
    $hcSlider.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      appendDots: $hcNav.find('.dots'),
    });

    $hcSlider.siblings('.navigate').find('.item').click(function() {
      if ( $(this).hasClass('prev') ) {
        $hcSlider.slick('slickPrev');
      } else if ( $(this).hasClass('next') ) {
        $hcSlider.slick('slickNext');
      }
    });

    $hcNav.find('.arrow').each(function () {
      if ($(this).hasClass('arrow-prev')) {
        $(this).children('.btn').click(function () {
          $hcSlider.slick('slickPrev');
        });
      } else if ($(this).hasClass('arrow-next')) {
        $(this).children('.btn').click(function () {
          $hcSlider.slick('slickNext');
        });
      }
    });

  });

  /*== cases-calculation ========================== */
  $(".countBtn").click(function(event){
    event.preventDefault();
    $(".calculate_results").slideDown();
  })
  /*== about ========================== */

  $('#about').each(function() {

    var $about = $(this);
    var $acSlider = $about.find('.cases-slider').children('.slider');

    $acSlider.slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      prevArrow: '<div class="slick-arrow slick-prev"><button type="button" class="btn btn-circle">' + svgChevronLeft + '</button></div>',
      nextArrow: '<div class="slick-arrow slick-next"><button type="button" class="btn btn-circle">' + svgChevronRight + '</button></div>',
      responsive: [
        {
          breakpoint: 1400,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });

  });

  /*== customer ======================= */

  $('#customer').each(function() {

    // logout button
    $(this).find('.member-logout').each(function() {
      var $this = $(this),
          thisWidth = $this.children('.btn').outerWidth();
      $window.on('load resize', function() {
        if ( window.innerWidth < 768 ) {
          $this.css('left', window.innerWidth/2 - thisWidth/2);
        } else {
          $this.css('left', '25px');
        }
      }).trigger('resize');
    });
    $window.on('load scroll', function() {
      var spacing;
      if ( window.innerWidth >= 480 ) {
        spacing = 50;
      } else if ( window.innerWidth < 480 ) {
        spacing = 35;
      }
      if ( $window.scrollTop() >= $('#main').outerHeight() - window.innerHeight + spacing ) {
        $('.member-logout').addClass('sticky');
      } else {
        $('.member-logout').removeClass('sticky');
      }
    }).trigger('scroll');

  });
  $('.showcase').each(function() {
    $(this).lightcase();
  });

});