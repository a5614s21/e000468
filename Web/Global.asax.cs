﻿using Google.Apis.Auth.OAuth2;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Web.Models;
using Web.Service;

namespace Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BackgroundWork work = new BackgroundWork();
            work.StartWork();
        }
        public class BackgroundWork
        {

            public static object oLock = new object();
            private static Timer timer;


            // 開始背景作業
            public void StartWork()
            {
                TimeSpan delayTime = new TimeSpan(0, 0, 5); // 應用程式起動後多久開始執行
                TimeSpan intervalTime = new TimeSpan(0, 1, 0); // 應用程式起動後間隔多久重複執行
                TimerCallback timerDelegate = new TimerCallback(BatchMethod);  // 委派呼叫方法
                timer = new Timer(timerDelegate, null, delayTime, intervalTime);  // 產生 timer
            }

            // 背景批次方法
            private void BatchMethod(object pStatus)
            {

                lock (oLock)
                {
                    try
                    {

                        #region 排程

                        //每天更新
                        string exchangeIpdateTime = DateTime.Now.ToString("HH");
                        string exchangeIpdateTimeMin = DateTime.Now.ToString("mm");
                        string exchangeIpdateTimeDay = DateTime.Now.ToString("dd");

                        //DateTime tt = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));


                        var GaData = new GoogleAnalyticsService();
                        ServiceAccountCredential credential = GaData.GoogleAnalytics();
                        Google.Apis.Analytics.v3.Data.GaData ThisYear = GaData.ThisYear(credential);
                        string json = JsonConvert.SerializeObject(ThisYear.Rows);

                        Model DB = new Model();

                        int ThisYearNum = DB.google_analytics.Where(m => m.area == "ThisYear").ToList().Count;

                        if (ThisYearNum == 0)
                        {
                            google_analytics GAThisYear = new google_analytics();
                            GAThisYear.guid = Guid.NewGuid();
                            GAThisYear.area = "ThisYear";
                            GAThisYear.content = "";
                            GAThisYear.create_date = DateTime.Now;
                            GAThisYear.modifydate = DateTime.Now;
                            DB.google_analytics.Add(GAThisYear);
                            DB.SaveChanges();

                        }
                        else
                        {
                            google_analytics GAThisYear = DB.google_analytics.Where(m => m.area == "ThisYear").FirstOrDefault();
                            GAThisYear.content = json;
                            DB.SaveChanges();

                        }



                        System.Threading.Thread.Sleep(500);
                        #endregion
                    }
                    catch (Exception ex)
                    { }
                }
            }
        }
    }
}
